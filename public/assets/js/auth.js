function validateEmail(sEmail) {
    if(!sEmail){
        toastr.error("Please enter email address", 'Error !');
        return 0;
    }
    var reEmail = /^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/;
    if(!sEmail.match(reEmail)) {
        toastr.error("Invalid Email Address", 'Error !');
        $("input[type=email]").val("");
        return 0;
    }
    return 1;
}



$('#ursdvfcd').on('submit', function (e) {
    e.preventDefault(e);
    let formData = $(this).serializeArray();
    let url = $(this).attr('action');
    if(validateEmail(formData[1].value)){
        $('#ursdvfcd').block({ message: 'Processing' });
        $.ajax({
            url: url,
            data: formData,
            type: 'post',
            success: function (response) {
                $('#ursdvfcd').unblock();
                toastr.success(response.message, 'Success');
                $('#resetPassword').modal('hide');
                $('#verifyCode').modal('show');
            },
            error: function (error) {
                $('#ursdvfcd').unblock();
                toastr.error(error.responseJSON.error, 'Error');
            }
        })
    }
});
$('#urvrvfcd').on('submit', function (e) {
    e.preventDefault(e);
    let formData = $(this).serializeArray();
    let url = $(this).attr('action');
   // if(formData[1].value){
        $('#urvrvfcd').block({ message: 'Processing' });
        $.ajax({
            url: url,
            data: formData,
            type: 'post',
            success: function (response) {
                $('#urvrvfcd').unblock();
                localStorage.setItem('_token_', formData[1].value);
                toastr.success(response.message, 'Success');
                $('#verifyCode').modal('hide');
                $('#updatePassword').modal('show');
            },
            error: function (error) {
                $('#urvrvfcd').unblock();
                toastr.error(error.responseJSON.error, 'Error');
            }
        })
  //  }
});
$('#uruppw').on('submit', function (e) {
    e.preventDefault(e);
    let formData = $(this).serializeArray();
    formData.push({name: 'code', value: localStorage.getItem('_token_')});
    let url = $(this).attr('action');
    $('#uruppw').block({ message: 'Processing' });
    $.ajax({
        url: url,
        data: formData,
        type: 'post',
        success: function (response) {
            $('#uruppw').unblock();
            localStorage.removeItem('_token_');
            toastr.success(response.message, 'Success');
            setTimeout(function () {
                window.location.reload();
            }, 1000);
        },
        error: function (error) {
             $('#uruppw').unblock();
            let result = error.responseJSON;
            if(result.error){
                toastr.error(error.responseJSON.error, 'Error');
            }else{
                toastr.error(result.errors[Object.keys(result.errors)[0]], 'Error');
            }
            
        }
    })
});
$('.registerFrm').on('click', function () {
    $.ajax({
        url: base_url + '/show-assesment-form',
        success: function (response) {
            $('#assestmentData').html(response);
            $('#assestment_section0').addClass('active');
        },
    })
});

$('#regForm').on('submit', function (e) {
    e.preventDefault(e);
    let formData = new FormData(document.getElementById('regForm'));
    formData.append('file', $('#upload').get(0).files[0]);
    let url = $(this).attr('action');
    $('#regForm').block({ message: 'Processing' });
    $.ajax({
        url: url,
        data: formData,
        type: 'post',
        processData: false,
        contentType: false,
        success: function (response) {
            $('#regForm').unblock();
            localStorage.removeItem('_token_');
            toastr.success("Registration was successful, Please check your email for verification code!", 'Success');
            $('#registerModal').modal('hide');
            $('#sendCode').modal('show');
            // setTimeout(function () {
            //   //  window.location.reload();
            // }, 1000);
        },
        error: function (error) {
             $('#regForm').unblock();
            let result = error.responseJSON;
            if(result.error){
                toastr.error(error.responseJSON.error, 'Error');
            }else{
                toastr.error(result.errors[Object.keys(result.errors)[0]], 'Error');
            }
            
        }
    })
});

$('#verifyAccount').on('submit', function (e) {
    e.preventDefault(e);
    let url = $(this).attr('action');
    $('#verifyAccount').block({ message: 'Processing' });
    let form = new FormData(document.getElementById('verifyAccount'));
    $.ajax({
        url: url,
        data: form,
        type: 'post',
        processData: false,
        contentType: false,
        success: function (response) {
            $('#verifyAccount').unblock();
            if(response.error){
                toastr.error(response.error, 'Error');
            }else{
            toastr.success(response.message, 'Success');
            }
            
            setTimeout(function () {
               window.location.reload();
            }, 2000);
        },
        error: function (error) {
            console.log(error);

             $('#verifyAccount').unblock();
            let result = error.responseJSON;
            if(result.error){
                toastr.error(error.responseJSON.error, 'Error');
            }else{
                toastr.error(result.errors[Object.keys(result.errors)[0]], 'Error');
            }
            
        }
    })
});

function resendCode(){
    $('#verifyAccount').block({ message: 'Processing' });
    $.ajax({
        url: base_url + '/resend-code',
       // data: {email: email},
        type: 'get',
        success: function (response) {
            $('#verifyAccount').unblock();
            if(response.error){
                toastr.error(response.error, 'Error');
            }else{
            toastr.success(response.message, 'Success');
            }
          
        },
        error: function (error) {
            
        }
    })
}

function hasClass(elem, className){
    if ($(elem).hasClass(className))
        return true;
    return false;
}
function addClass(elem){
    if ($(elem).hasClass('active'))
        return true;
    $(elem).addClass('active');
}
function removeClass(elem){
    if ($(elem).hasClass('active'))
        $(elem).removeClass('active');
}
function back(index){
    console.log(index);
    if(index == 0)
        return false;
    else{
        var elem = '#assestment_section'+(index);
        removeClass(elem);
        let oldI = --index;
        elem = '#assestment_section'+(oldI);
        addClass(elem);
    }
}
function next(_this, index) {
    console.log(_this);
    let oldI = index;
    let newI = ++index;
    var elem = '#assestment_section'+(--index);
    removeClass(elem);
    elem = '#assestment_section'+(++index);
    console.log($(elem).length);
    if($(elem).length > 0){
        console.log('inner', elem);
        addClass(elem);
    }
    else{
        addClass('#assestment_section'+(--index));
    }
}
function submitAssesmentForm() {
    $.confirm({
        title: "Are you sure you want to submit this assesment form ?",
        buttons: {
            submit: {
                btnClass: 'btn-info',
                action: function () {
                    let formdata = $('#assesmentForm').serializeArray();
                    $.ajax({
                        url: $('#assesmentForm').attr('action'),
                        data: formdata,
                        type: 'post',
                        success: function (response) {
                            $('#assesmentModal').modal('hide');
                            $('#successModal').modal('show');
                        },
                        error: function (error) {
                            let errors = error.responseJSON.errors;
                            Object.keys(errors).forEach(key=>{
                                toastr.error(errors[key], "Error!");
                            });
                        }
                    });
                },
            },
            cancel: function () {
                //close
            },
        }
    });
}
function readURL(input, target) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#'+target).attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}




















