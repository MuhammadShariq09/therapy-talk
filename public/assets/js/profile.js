$('#updatePassword').on('submit', function (e) {
    e.preventDefault(e);
    $.ajax({
        url: base_url + '/update-password',
        data: $('#updatePassword').serializeArray(),
        type: 'post',
        success: function (response) {
            toastr.success(response.message, 'Success');
            $('#updatePasswordModal').modal('hide');
        },
        error: function (error) {
            let errors = error.responseJSON.errors;
            Object.keys(errors).forEach(key => {
                toastr.error(errors[key], "Error!");
            });
        }
    })
})


var $uploadCrop,
    tempFilename,
    rawImg,
    imageId;

$uploadCrop = $('#upload-demo').croppie({
    enableExif: true,
    viewport: {
        width: 200,
        height: 200,
        type: 'circle'
    },
    boundary: {
        width: 250,
        height: 250
    }
});
$('#upload').on('change', function(e){
    console.log('here');
    var f = e.target.files[0];
    if(f.size > 5242880){
        toastr.error('You cant not select file greater than 5 MB');
        $(this).val('');
    }else{
        $('#cropImagePop').modal('toggle');
        var reader = new FileReader();
        reader.onload = function(e) {
            $uploadCrop.croppie('bind', {
                url: e.target.result
            }).then(function() {
                console.log('jQuery bind complete');
            });
        }
        reader.readAsDataURL(this.files[0]);
    }
});

$('#cropImageBtn').on('click', function(ev) {
    $uploadCrop.croppie('result', {
        type: 'base64',
        format: 'jpeg',
        size: { width: 300, height: 300 }
    }).then(function(resp) {
        $('#personal_img').attr('src', resp);
        $('#image').val(resp);
        $.ajax({
            url: base_url + '/updateProfileImage',
            data: {data : resp},
            type: 'post',
            success: function(resp){

            }
        })
        $('#cropImagePop').modal('hide');
    });
});



