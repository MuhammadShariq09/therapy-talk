function reportTherapist(id){
	var url = base_url + '/report';
    $('.load').block({ message: 'Processing' });
	$.ajax({
		url: url,
        type: 'post',
        data:{
           therapist_id: id,
           session_id: $('#session_id').val(),
           message: $('#report-message').val()
        },
		success: function (response) {
			if(response.success == true){
                $('.load').unblock({ message: 'Processing' });
				toastr.success("Report added successfully", 'Success');
                $('#exampleModalCenter').hide();	
                setTimeout(function() {
                    window.location.href = base_url + '/my-session-logs';
                }, 1000);
			}
		},
		error: function (error) {
            $('.load').unblock({ message: 'Processing' });
			let errors = error.responseJSON.errors;
			Object.keys(errors).forEach(key=>{
				toastr.error(errors[key], "Error!");
			});
		}
	});
}

function giveFeedback(id){
    var url = base_url + '/feedback';
    var rate = $(".rating").rate("getValue");
   
	$.ajax({
		url: url,
        type: 'post',
        data:{
           therapist_id: id,
           session_id: $('#session_id').val(),
           message: $('#feedback').val(),
           rating: rate
        },
		success: function (response) {
			if(response.success == true){
                toastr.success("Feedback added successfully", 'Success');
                setTimeout(function() {
                window.location.href = base_url + '/my-session-logs';
            }, 700);
			}
		},
		error: function (error) {
			let errors = error.responseJSON.errors;
			Object.keys(errors).forEach(key=>{
				toastr.error(errors[key], "Error!");
			});
		}
	});
}

function paypalPay(id){
    var url = base_url + '/payment/'+ id;
    
	$.ajax({
		url: url,
        type: 'get',
		success: function (response) {
			if(response.success == true){
                toastr.success(response.message, 'Success');
                setTimeout(function() {
                window.location.href = base_url + '/my-session-logs';
            }, 5000);
			}
		},
		error: function (error) {
			let errors = error.responseJSON.errors;
			Object.keys(errors).forEach(key=>{
				toastr.error(errors[key], "Error!");
			});
		}
	});
}



$(function () {
    var today, datepicker;
    today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());

    $('#timepicker').timepicker({
        uiLibrary: 'bootstrap4',
        format: 'hh:MM TT'
    });

    $('#datepicker').datepicker({
        uiLibrary: 'bootstrap4',
        minDate: today
    });

    $('#bookMe').on('click', function(){
        let date = $(this).data('date');
        let time = $(this).data('time');
        let reason = $(this).data('reason');
        let type = $(this).data('type');
        let duration = $(this).data('duration');
        $('#datepicker').val(date);
        $('#timepicker').val(time);
        $('#reason').val(reason);
        $('#type').val(type);
        $('#duration').val(duration);
    })



    $('#bookAppointment').on('submit', function (e) {
        e.preventDefault(e);
        let url = $(this).attr('action');
        let formdata = $(this).serializeArray();
        $('#bookAppointment').block({ message: 'Processing' });
        $.ajax({
            url: url,
            data: formdata,
            type: 'post',
            success: function (response) {
                $('#bookAppointment').unblock();
                toastr.success(response.message, 'Success');
                if($('#redirectTo')){
                    window.location.href = base_url + '/my-session-logs'
                }
                $('#bookAppointmentModal').modal('hide');
                $('#bookAppointment input').val('');
                $('#bookAppointment textarea').val('');
                $('#bookAppointment select').val('');
            },
            error: function (error) {
                $('#bookAppointment').unblock();
                let errors = error.responseJSON.errors;
                Object.keys(errors).forEach(key => {
                    toastr.error(errors[key], "Error!");
                });
            }
        })
    })

})
