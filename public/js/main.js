function changeStatus(id, status){
	var url = document.getElementById('url').value + '/therapist/change-status/'+ id + '/' + status;
	console.log(url);

	$.ajax({
		url: url,
		type: 'get',
		success: function (response) {
			if(status == 3 && response.success == true){
				toastr.success("Session started successfully", 'Success');
				$('#start').hide();
				$('#complete').show();
			}else{
				toastr.success("Session completed successfully", 'Success');
				$('#complete').hide();
				window.location.href = document.getElementById('url').value + '/therapist/appointments';
			}
		},
		error: function (error) {
			let errors = error.responseJSON.errors;
			Object.keys(errors).forEach(key=>{
				toastr.error(errors[key], "Error!");
			});
		}
	});
}

$(document).ready(function(){



	$(".left1").boxLoader({

	    direction:"x",

	    position: "-50%",

	    effect: "fadeIn",

	    duration: "2s",

	    windowarea: "60%"

});



	$(".right1").boxLoader({

	    direction:"x",

	    position: "50%",

	    effect: "fadeIn",

	    duration: "2s",

	    windowarea: "60%"

});







	$(".inn").boxLoader({

	    direction:"none",

	    position: "100%",

	    effect: "fadeIn",

	    duration: "2s",

	    windowarea: "30%"

});









$(".botTop").boxLoader({

	    direction:"y",

	    position: "100%",

	    effect: "none",

	    duration: "2s",

	    windowarea: "10%"

});

 

	$(".up i").click(scroll);



});





function scroll(){



	$("html, body").animate({

        scrollTop: $(".boxes").offset().top

    }, {

    	queue: false,

    	duration: 1000});



}