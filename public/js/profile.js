$(function () {
    $('.vpcpb').on('click', function () {
        let data = {
            oldpassword : $('#vpop').val(),
            newpassword : $('#vpnp').val(),
            confirmnewpassword : $('#vpcnp').val(),
        };
        console.log(base_url + '/therapist/change-password');
        $.ajax({
            url: base_url + '/therapist/change-password',
            data: data,
            type: 'post',
            success: function (response) {
                toastr.success(response.message, "Success!");
                $('#vpcpm').modal('hide');
            },
            error: function (error) {
                let errors = error.responseJSON.errors;
                Object.keys(errors).forEach(key=>{
                    toastr.error(errors[key], "Error!");
                });
            }
        })
    });

});


