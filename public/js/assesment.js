$('.reject').on('click', function () {
    let id = $(this).data('id');
    $.confirm({
        class: 'ass_rej',
        title: 'Are you sure you want to reject this assessment?',
        content: '' +
            '<form action="" class="formName">' +
            '<div class="form-group">' +
            '<label>Enter Rejection Reason here</label>' +
            '<input type="text" placeholder="Your rejection reason" class="reason form-control" required />' +
            '</div>' +
            '</form>',
        buttons: {
            formSubmit: {
                text: 'Submit',
                btnClass: 'btn-info',
                action: function () {
                    var reason = this.$content.find('.reason').val();
                    if (!reason) {
                        $.alert('provide a valid rejection reason');
                        return false;
                    }
                    let post = new Object();
                    post.reason = reason;
                    post.id = id;
                    $('#assesment').block({ message: 'Processing' });
                    $.ajax({
                        url: base_url + '/admin/reject-assement-form',
                        data: post,
                        type: 'post',
                        success: function (response) {
                            toastr.success('Assessment Rejected successfully', 'Success');
                            $('#assesment').unblock();
                            setTimeout(function(){ window.location.reload(); }, 700);

                        },
                        error: function (response) {

                        }
                    })

                }
            },
            cancel: function () {
                //close
            },
        }
    });
});
$('.approve').on('click', function () {
    let id = $(this).data('id');
    $.confirm({
        class: 'ass_rej',
        title: 'Are you sure you want to approve this assessment?',
        buttons: {
            formSubmit: {
                text: 'Submit',
                btnClass: 'btn-info',
                action: function () {
                    let post = new Object();
                    post.id = id;
                    $('#assesment').block({ message: 'Processing' });
                    $.ajax({
                        url: base_url + '/admin/approve-assement-form/'+id,
                        data: post,
                        type: 'post',
                        success: function (response) {
                            toastr.success('Assessment approved successfully', 'Success');
                            $('#assesment').unblock();
                            setTimeout(function(){ window.location.reload(); }, 700);

                        },
                        error: function (response) {

                        }
                    })

                }
            },
            cancel: function () {
                //close
            },
        }
    });
});
