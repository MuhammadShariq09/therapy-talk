$(function () {
    $('#card').mask("0000-0000-0000-0000", {placeholder: "____-____-____-____"});
    $('#cvv').mask("000", {placeholder: "___"});
    $('#expiry').mask("00/0000", {placeholder: "__/____"});

    // $('#card-number').mask("0000-0000-0000-0000", {placeholder: "____-____-____-____"});
     $('#card-cvv').mask("000", {placeholder: "___"});
     $('#card-expiry').mask("00/0000", {placeholder: "__/____"});

    $('#expiry').on('change', function () {
        let value = $(this).val();
        if(value.length < 7){
            $(this).val('');
        }
        else{
            let exp = value.split('/');
            let month = exp[0]+1;
            let year = exp[1];

            if(month < new Date().getMonth() || year < new Date().getFullYear()){
                $(this).val('');
            }
        }
    });

    $('.subpkg').on('click', function () {
          let package = {
              packageid: $(this).data('packageid'),
              packagename: $(this).data('packagename'),
              packageamount: $(this).data('packageamount'),
              packagemonth: $(this).data('packagemonth'),
          }
          //$('.package_title').text(package.packagename+': '+package.packagemonth+' Months');
          $('.package_title').text(package.packagename+': 1 Month');
          $('.package_fee').text('Fee: $'+package.packageamount.toFixed(2));
          localStorage.setItem('package', JSON.stringify(package));
    })
});

function savePackage(){
    let package = localStorage.getItem('package');
    package = JSON.parse(package);
    package.card = $('#card').val();
    package.cvv = $('#cvv').val();
    package.expiry = $('#expiry').val();
    package.card_holder = $('#card_holder').val();
   
    $.ajax({
        url: base_url + '/therapist/subscribe-package',
        data: package,
        type: 'post',
        success: function (response) {
            toastr.success(response.message, 'Success');
            setTimeout(function () {
                window.location.href = base_url + '/therapist';
            }, 700);
        },
        error: function (error) {
            let errors = error.responseJSON.errors;
            Object.keys(errors).forEach(key=>{
                toastr.error(errors[key], "Error!");
            });
        }
    })

}

function saveUserPackage(){
    let package = localStorage.getItem('package');
    console.log(package);
    
    package = JSON.parse(package);
    package.card = $('#card').val();
    package.cvv = $('#cvv').val();
    package.expiry = $('#expiry').val();
    package.card_holder = $('#card_holder').val();
    $('#subscribeModal').block({ message: 'Processing' });
    $.ajax({
        url: $('#url').val() + '/user/subscribe-package',
        data: package,
        type: 'post',
        success: function (response) {
            $('#subscribeModal').unblock({ message: 'Processing' });
            toastr.success(response.message, 'Success');
            setTimeout(function () {
                window.location.href = $('#url').val() + '/subscription-logs';
            }, 1000);
        },
        error: function (error) {
            $('#subscribeModal').unblock({ message: 'Processing' });
            let errors = error.responseJSON.errors;
            Object.keys(errors).forEach(key=>{
                toastr.error(errors[key], "Error!");
            });
        }
    })

}

function renewUserPackage(){
    let package = localStorage.getItem('package');
    package = JSON.parse(package);
    package.card = $('#card').val();
    package.cvv = $('#cvv').val();
    package.expiry = $('#expiry').val();
    package.card_holder = $('#card_holder').val();
    $('#subscribeModal').block({ message: 'Processing' });
    $.ajax({
        url: $('#url').val() + '/renew-package-subscription',
        data: package,
        type: 'post',
        success: function (response) {
            $('#subscribeModal').unblock({ message: 'Processing' });
            toastr.success(response.message, 'Success');
            setTimeout(function () {
                window.location.href = $('#url').val() + '/subscription-logs';
            }, 1000);
        },
        error: function (error) {
            $('#subscribeModal').unblock({ message: 'Processing' });
            let errors = error.responseJSON.errors;
            Object.keys(errors).forEach(key=>{
                toastr.error(errors[key], "Error!");
            });
        }
    })

}

 function stripeResponseHandler(status, response) {
    var token = response['id'];
    document.getElementById('stripe_token').value = token;
    console.log('input token',  $('#stripe_token').val());
    }

    function getStripeToken(){
      console.log('here');
    var date = $('#session-card-expiry').val();
    var arr = date.split('/');
    Stripe.setPublishableKey($('#stripKey').val());

    Stripe.createToken({
          number: $('#session-card-number').val(),
          cvc: $('#session-card-cvc').val(),
          exp_month: arr[0],
          exp_year: arr[1]
        }, stripeResponseHandler);
    }

    function pay(){
    
    $('#direct-modal').block({ message: 'Processing' });
    $.ajax({
        url: "{{route('user.appointment.payment')}}",
        data: {
          id : $('#session-id').val(),
          stripe_token : $('#stripe_token').val(),
          card : $('#session-card-number').val(),
          cvv : $('#session-card-cvc').val(),
          expiry : $('#session-card-expiry').val(),
          card_holder : $('#session-card-holder').val()
        },
        type: 'post',
        success: function (response) {
            $('#direct-modal').unblock({ message: 'Processing' });
            toastr.success(response.message, 'Success');
            setTimeout(function () {
                window.location.href = $('#url').val() + '/my-session-logs';
            }, 1000);
        },
        error: function (error) {
            $('#direct-modal').unblock({ message: 'Processing' });
            let errors = error.responseJSON.errors;
            Object.keys(errors).forEach(key=>{
                toastr.error(errors[key], "Error!");
            });
        }
    })

}

function buyNew(){
    let package = localStorage.getItem('package');
    package = JSON.parse(package);
    package.card = $('#card-number').val();
    package.cvv = $('#card-cvc').val();
    package.expiry = $('#card-expiry').val();
    package.card_holder = $('#card-holder').val();
    $('#payment-modal').block({ message: 'Processing' });
    $.ajax({
        url:  $('#url').val() + '/subscribe-new-package',
        data: package,
        type: 'post',
        success: function (response) {
            $('#payment-modal').unblock({ message: 'Processing' });
            toastr.success(response.message, 'Success');
            setTimeout(function () {
                window.location.href = $('#url').val() + '/subscription-logs';
            }, 1000);
        },
        error: function (error) {
            $('#payment-modal').unblock({ message: 'Processing' });
            let errors = error.responseJSON.errors;
            Object.keys(errors).forEach(key=>{
                toastr.error(errors[key], "Error!");
            });
        }
    })

}




