var $uploadCrop,
    tempFilename,
    rawImg,
    imageId;

$uploadCrop = $('#upload-demo').croppie({
    enableExif: true,
    viewport: {
        width: 200,
        height: 200,
        type: 'circle'
    },
    boundary: {
        width: 250,
        height: 250
    }
});
var nogo;
$('#upload').on('change', function(e){
    var f = e.target.files[0];
    if(f.size > 5242880){
        toastr.error('You cant not select file greater than 5 MB');
        $(this).val('');
        nogo=1;
    }else{
        console.log('here');
        nogo=0;
        $('#cropImagePop').modal('toggle');
        var reader = new FileReader();
        reader.onload = function(e) {
            $uploadCrop.croppie('bind', {
                url: e.target.result
            }).then(function() {
                console.log('jQuery bind complete');
            });
        }
        reader.readAsDataURL(this.files[0]);
    }
});

$('#cropImageBtn').on('click', function(ev) {
    $uploadCrop.croppie('result', {
        type: 'base64',
        format: 'jpeg',
        size: { width: 300, height: 300 }
    }).then(function(resp) {
        if(!nogo){
            $('#user_image').attr('src', resp);
        }else{
            $('#user_image').attr('src', base_url + 'images/Profile_03.png');
        }
        $('#personal_img_base64').val(resp);
        $('#cropImagePop').modal('hide');
    });
});


$('.select-modal').on('select2:opening', function (e) {
    let this_ = $(this);
    let type = $(this).data('type');
    $('#dataModal').modal('toggle');
    console.log('i am here');
    fetchDataModal(type)
    setTimeout(function(){
        this_.select2("close"); }, 100);
});

function readURL(input, target) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#'+target).attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function fetchDataModal(type){
    let url = base_url + "/therapist-data-modal?type="+type;
    let values = $('#'+type).val();
    console.log(values)
    $.ajax({
        url: url,
        type: 'post',
        data: {values: values},
        success: function (response) {
            $('#dataModalRes').html(response);
        }
    });
}
$(document).on('click', '.submitDataModal', function(){
    let type = $(this).data('type');
    let values = [];
    $.each($("input[name='datamodal[]']:checked"), function(){
        values.push($(this).val());
    });
    $('#'+type).val(values);
    $('#'+type).trigger('change');

});
$(document).on("keyup", '#search', function() {
    var value = $(this).val().trim().toLowerCase();
    $("#filter li").filter(function() {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
});
$(document).on("click", '.cat-sorting li a', function() {
    var value = $(this).text().trim().toLowerCase();
    if(value == 'all'){
        $('#filter li').show();
    }else{
        $("#filter li").filter(function() {
            console.log('value', value);
            let tosearch =  $(this).text().trim().toLowerCase()[0];
            console.log('value', tosearch);
            $(this).toggle(tosearch.indexOf(value) > -1)
        });
    }

});

$('#ursdvfcd').on('submit', function (e) {
    e.preventDefault(e);
    let formData = $(this).serializeArray();
    let url = $(this).attr('action');
    if(validateEmail(formData[1].value)){
        $('#ursdvfcd').block({ message: 'Processing' });
        $.ajax({
            url: url,
            data: formData,
            type: 'post',
            success: function (response) {
                $('#ursdvfcd').unblock();
                toastr.success(response.message, 'Success');
                $('#resetPassword').modal('hide');
                $('#verifyCode').modal('show');
            },
            error: function (error) {
                $('#ursdvfcd').unblock();
                toastr.error(error.responseJSON.error, 'Error');
            }
        })
    }
});
$('#urvrvfcd').on('submit', function (e) {
    e.preventDefault(e);
    let formData = $(this).serializeArray();
    let url = $(this).attr('action');
    if(formData[1].value){
        $('#urvrvfcd').block({ message: 'Processing' });
        $.ajax({
            url: url,
            data: formData,
            type: 'post',
            success: function (response) {
                $('#urvrvfcd').unblock();
                localStorage.setItem('_token_', formData[1].value);
                toastr.success(response.message, 'Success');
                $('#verifyCode').modal('hide');
                $('#updatePassword').modal('show');
            },
            error: function (error) {
                $('#urvrvfcd').unblock();
                toastr.error(error.responseJSON.error, 'Error');
            }
        })
    }
});
$('#uruppw').on('submit', function (e) {
    e.preventDefault(e);
    let formData = $(this).serializeArray();
    formData.push({name: 'code', value: localStorage.getItem('_token_')});
    let url = $(this).attr('action');
    $('#uruppw').block({ message: 'Processing' });
    $.ajax({
        url: url,
        data: formData,
        type: 'post',
        success: function (response) {
            $('#uruppw').unblock();
            localStorage.removeItem('_token_');
            toastr.success(response.message, 'Success');
            setTimeout(function () {
                window.location.reload();
            }, 1000);
        },
        error: function (error) {
             $('#uruppw').unblock();
             toastr.error(error.responseJSON.error, 'Error');
        }
    })
});
