<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assesment extends Model
{
    protected $table = 'assesment_forms';

    protected $fillable = ['email', 'data', 'status', 'reason', 'user_id'];

    protected $casts = [
        'craeted_at' => 'date'
    ];
}
