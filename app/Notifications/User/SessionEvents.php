<?php

namespace App\Notifications\User;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class SessionEvents extends Notification
{
    use Queueable;

    public function __construct($user, $session)
    {
        $this->user = $user;
        $this->session = $session;
    }

    public function via($notifiable)
    {
        return ['mail', 'database'];
    }


    public function toMail($notifiable)
    {
        $date = date('m/d/Y', strtotime($this->session->requested_date));
        if($this->session->status == 0 && $this->session->is_paid == 0){
            $data = [
                'title' => 'Session Booking',
                'message' => 'Your appointment request at '.$date.' '.$this->session->requested_time.' has been sent to the therapist “'.$this->session->therapist['full_name'].'”.',
                'name' => $this->user->full_name
            ];
        }
        else if($this->session->status == 1 && $this->session->is_paid == 0){
            $data = [
                'title' => 'Session Request Approved',
                'message' => 'Therapist has accepted your appointment request. Please pay to book the appointment.',
                'name' => $this->user->full_name
            ];
        }
        else if($this->session->status == 2 && $this->session->is_paid == 0){
            $data = [
                'title' => 'Session Request Rejected',
                'message' => 'Your appointment request has been rejected by the therapist, View your session log and see Therapist`s suggested date and time.',
                'name' => $this->user->full_name
            ];
        }
        else {
            $data = [
                'title' => 'Appointment Booked',
                'message' => 'Your appointment at '.$date.' '.$this->session->requested_time.' with “'.$this->session->therapist['full_name'].'” has been booked.',
                'name' => $this->user->full_name
            ];
        }
        return (new MailMessage)
                    ->from('info@therapy-talk.com', 'My Therapy Talk')
                    ->subject($data['title'])
                    ->view('email.session', compact('data'));
    }

    
    public function toArray($notifiable)
    {
        $date = date('m/d/Y', strtotime($this->session->requested_date));
        if($this->session->status == 0 && $this->session->is_paid == 0){
            $data = [
                'title' => 'Session Booking',
                'message' => 'Your appointment request at '.$date.' '.$this->session->requested_time.' has been sent to the therapist “'.$this->session->therapist['full_name'].'”.',
                'name' => $this->user->full_name,
                'url' => url('/my-session-logs')
            ];
        }
        else if($this->session->status == 1 && $this->session->is_paid == 0){
            $data = [
                'title' => 'Session Request Approved',
                'message' => 'Therapist has accepted your appointment request. Please pay to book the appointment.',
                'name' => $this->user->full_name,
                'url' => url('/my-session-logs')
            ];
        }
        else if($this->session->status == 2 && $this->session->is_paid == 0){
            $data = [
                'title' => 'Session Request Rejected',
                'message' => 'Your appointment request has been rejected by the therapist, View your session log and see Therapist`s suggested date and time.',
                'name' => $this->user->full_name,
                'url' => url('/my-session-logs')
            ];
        }
        else {
            $data = [
                'title' => 'Appointment Booked',
                'message' => 'Your appointment at '.$date.' '.$this->session->requested_time.' with “'.$this->session->therapist['full_name'].'” has been booked.',
                'name' => $this->user->full_name,
                'url' => url('/my-appointments')
            ];
        }
     
        return [
            'title' => $data['title'],
            'message' => $data['message'],
            'session_id' => $this->session->id,
            'user_id' => $notifiable->id,
            'url' => $data['url']
        ];
    }
}
