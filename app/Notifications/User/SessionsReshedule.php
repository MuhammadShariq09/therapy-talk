<?php

namespace App\Notifications\User;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class SessionsReshedule extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $session)
    {
        $this->user = $user;
        $this->session = $session;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if($this->session->status == 0){
            $data = [
                'title' => 'Session Reshedule Request',
                'message' => 'Your appointment reschedule request has been sent to the therapist.',
                'name' => $this->user->full_name
            ];
        }
        else if($this->session->status == 1){
            $data = [
                'title' => 'Session Reshedule Request Approved',
                'message' => 'Your appointment reschedule request has been accepted by the therapist.',
                'name' => $this->user->full_name
            ];
        }
        else if($this->session->status == 2){
            $data = [
                'title' => 'Session Reshedule Request Rejected',
                'message' => 'Therapist has rejected your appointment request. Please see the suggested time and date and book again',
                'name' => $this->user->full_name
            ];
        }
        return (new MailMessage)
                    ->from('info@therapy-talk.com', 'My Therapy Talk')
                    ->subject($data['title'])
                    ->view('email.session', compact('data'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        if($this->session->status == 0){
            $data = [
                'title' => 'Session Reshedule Request',
                'message' => 'Your appointment reschedule request has been sent to the therapist.',
                'name' => $this->user->full_name,
                'url' => url('/my-session-logs')
            ];
        }
        else if($this->session->status == 1){
            $data = [
                'title' => 'Session Reshedule Request Approved',
                'message' => 'Your appointment reschedule request has been accepted by the therapist.',
                'name' => $this->user->full_name,
                'url' => url('/my-session-logs')
            ];
        }
        else if($this->session->status == 2){
            $data = [
                'title' => 'Session Reshedule Request Rejected',
                'message' => 'Therapist has rejected your appointment request. Please see the suggested time and date and book again',
                'name' => $this->user->full_name,
                'url' => url('/my-session-logs')
            ];
        }
        return [
            'title' => $data['title'],
            'message' => $data['message'],
            'session_id' => $this->session->id,
            'user_id' => $notifiable->id,
            'url' => $data['url']
        ];
    }
}
