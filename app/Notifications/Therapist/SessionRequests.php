<?php

namespace App\Notifications\Therapist;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class SessionRequests extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($session)
    {
        $this->session = $session;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $date = date('m/d/Y', strtotime($this->session->requested_date));
        if($this->session->status == 0 && $this->session->is_paid == 0){
            $data = [
                'title' => 'Session Appointment Request',
                'message' => 'You have received an appointment booking request. Please view the details and respond.',
                'action' => 'View Session Logs',
                'action_url' => route('therapist.session.logs'),
                'name' => $this->session->therapist->full_name
            ];
        }
        else if($this->session->status == 1 && $this->session->is_paid == 0){
            $data = [
                'title' => 'Session Appointment Request',
                'message' => 'You have accepted the appointment booking request. Waiting for the user to pay for the appointment.',
                'action' => 'View Session Logs',
                'action_url' => route('therapist.session.logs'),
                'name' => $this->session->therapist->full_name
            ];
        }
        else {
            $data = [
                'title' => 'Appointment Booked',
                'message' => 'User has booked appointment with you on '.$date.' '.$this->session->requested_time.' .',
                'action' => 'View Session Logs',
                'action_url' => route('therapist.session.logs'),
                'name' => $this->session->therapist->full_name
            ];
        }
        
        return (new MailMessage)
                    ->from('info@therapy-talk.com', 'My Therapy Talk')
                    ->subject($data['title'])
                    ->view('email.session', compact('data'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $date = date('m/d/Y', strtotime($this->session->requested_date));
        if($this->session->status == 0 && $this->session->is_paid == 0){
            $data = [
                'title' => 'Session Appointment Request',
                'message' => 'You have received an appointment booking request. Please view the details and respond.',
                'action' => 'View Session Logs',
                'url' => route('therapist.session.logs'),
                'name' => $this->session->therapist->full_name
            ];
        }
        else if($this->session->status == 1 && $this->session->is_paid == 0){
            $data = [
                'title' => 'Session Appointment Request',
                'message' => 'You have accepted the appointment booking request. Waiting for the user to pay for the appointment.',
                'action' => 'View Session Logs',
                'url' => route('therapist.session.logs'),
                'name' => $this->session->therapist->full_name
            ];
        }
        else {
            $data = [
                'title' => 'Appointment Booked',
                'message' => 'User has booked appointment with you on '.$date.' '.$this->session->requested_time.' .',
                'action' => 'View Session Logs',
                'url' => route('therapist.appointments'),
                'name' => $this->session->therapist->full_name
            ];
        }
        return [
            'title' => $data['title'],
            'message' => $data['message'],
            'session_id' => $this->session->id,
            'user_id' => $notifiable->id,
            'url' => $data['url']
        ];
    }
}
