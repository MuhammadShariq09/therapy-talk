<?php

namespace App\Notifications\Therapist;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ResheduleRequests extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
   public function __construct($user, $session)
    {
        $this->user = $user;
        $this->session = $session;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $date = date('m/d/Y', strtotime($this->session->suggested_date));
         if($this->session->status == 0){
            $data = [
                'title' => 'Session Reshedule Request',
                'message' => 'User has requested to reschedule the appointment. Please view details to respond.',
                'action' => 'View Session Logs',
                'action_url' => route('therapist.session.logs'),
                'name' => $this->user->first_name . " ".$this->user->last_name
            ];
        }
        else if($this->session->status == 1){
            $data = [
                'title' => 'Session Reshedule Request Approved',
                'message' => 'You have accepted the appointment reschedule request. Appointment has been booked on “'.$date.'” at “'.$this->session->suggested_time.'”',
                'action' => 'View Session Logs',
                'action_url' => route('therapist.session.logs'),
                'name' => $this->user->first_name . " ".$this->user->last_name
            ];
        }
        else if($this->session->status == 2){
            $data = [
                'title' => 'Session Reshedule Request Rejected',
                'message' => 'You have rejected the appointment reschedule request. Suggested date and time are sent to the user.',
                'action' => 'View Session Logs',
                'action_url' => route('therapist.session.logs'),
                'name' => $this->user->first_name . " ".$this->user->last_name
            ];
        }
        return (new MailMessage)
                    ->from('info@therapy-talk.com', 'My Therapy Talk')
                    ->subject($data['title'])
                    ->view('email.session', compact('data'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $date = date('m/d/Y', strtotime($this->session->suggested_date));
        if($this->session->status == 0){
            $data = [
                'title' => 'Session Reshedule Request',
                'message' => 'User has requested to reschedule the appointment. Please view details to respond.',
                'action' => 'View Session Logs',
                'url' => route('therapist.session.logs'),
                'name' => $this->user->first_name . " ".$this->user->last_name
            ];
        }
        else if($this->session->status == 1){
            $data = [
                'title' => 'Session Reshedule Request Approved',
                'message' => 'You have accepted the appointment reschedule request. Appointment has been booked on “'.$date.'” at “'.$this->session->suggested_time.'”',
                'action' => 'View Session Logs',
                'url' => route('therapist.session.logs'),
                'name' => $this->user->first_name . " ".$this->user->last_name
            ];
        }
        else if($this->session->status == 2){
            $data = [
                'title' => 'Session Reshedule Request Rejected',
                'message' => 'You have rejected the appointment reschedule request. Suggested date and time are sent to the user.',
                'action' => 'View Session Logs',
                'url' => route('therapist.session.logs'),
                'name' => $this->user->first_name . " ".$this->user->last_name
            ];
        }
        return [
            'title' => $data['title'],
            'message' => $data['message'],
            'session_id' => $this->session->id,
            'user_id' => $notifiable->id,
            'url' => $data['url']
        ];
    }
}
