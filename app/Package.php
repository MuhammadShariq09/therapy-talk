<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $fillable = [
        'title', 'amount', 'month','video_call', 'voice_call', 'live_chat', 'year', 'start_date', 'end_date', 'status'
    ];

    protected $table = 'packages';
    
}
