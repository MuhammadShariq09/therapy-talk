<?php

namespace App\Http\Requests;
use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;

class StoreUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'full_name' => ['required', 'max:50'],
           // 'last_name' => ['required', 'alpha', 'max:50'],
            'email' => ['required','string', 'email', 'max:255', 'unique:users'],
            'phone' => ['required', 'min:8'],
            'address' => ['required'],
            'date_of_birth' => ['required'],
            'password' => ['required', 'string', 'min:8'],
            'confirm_password' => ['required','same:password'],
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'first_name.required' => 'First name field is required.',
            'first_name.alpha' => 'First name should be alphabatic.',
            'first_name.max'  => 'First name is too long.',
            'last_name.required' => 'Last name field is required.',
            'last_name.alpha' => 'Last name should be alphabatic.',
            'last_name.max'  => 'Last name is too long.',
            'email.required' => 'Email field is required.',
            'email.email'  => 'In-valid email format.',
            'email.max' => 'Email is too long.',
            'email.unique' => 'Email already exists.',
            'phone.required' => 'Phone number is required.',
            'phone.min' => 'Phone number is too short.',
            'address.required' => 'Address field is required.',
            'date_of_birth.required' => 'Date of birth field is required.',
            //'date_of_birth.date_format' => 'Invalid date format. Date should be in m/d/Y format.',
            'password.required' => 'Password field is required.',
            'password.min' => 'Password atleast eight characters long.',
            'confirm_password.required' => 'Confirm password field is required.',
            'confirm_password.same' => 'Confirm password mis-matched.',
        ];
    }
}
