<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAdminProfile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'full_name' => ['required', 'max:50'],
            //'last_name' => ['required', 'alpha', 'max:50'],
            'phone' => ['required', 'min:14'],
            'address' => ['required'],
        ];
    }


    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
    */
    public function messages()
    {
        return [
            'full_name.required' => 'Full name field is required.',
            'full_name.alpha' => 'Full name should be alphabatic.',
            'full_name.max'  => 'Full name is too long.',
            'last_name.required' => 'Last name field is required.',
            'last_name.alpha' => 'Last name should be alphabatic.',
            'last_name.max'  => 'Last name is too long.',
            'phone.required' => 'Phone number is required.',
            'phone.min' => 'Phone number is too short.',
            'address.required' => 'Address field is required.',
        ];
    }
}
