<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class StoreTherapist extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'full_name' => ['required', 'alpha', 'max:50'],
           // 'last_name' => ['required', 'alpha', 'max:50'],
             'email' => ['required', 'string', 'email', 'max:255', 'unique:therapists'],
            // 'repeater-group.*.practice_website',
            // 'repeater-group.*.phone',
            // 'repeater-group.*.address',
           // 'repeater-group.*.city',
            //'repeater-group.*.state',
           // 'repeater-group.*.country',
           // 'repeater-group.*.zipcode',
//            'practice_website' => ['required'],
//            'phone' => ['required', 'min:8'],
//            'address' => ['required'],
            // 'therapist_type' => ['required'],
            // 'license_type' => ['required', 'string'],
            // 'specialities_id' => ['required', 'array'],
            // 'treatments_id' => ['required', 'array'],
            // 'modalities_id' => ['required', 'array'],
            // 'age_groups_id' => ['required', 'array'],
            // 'ethnicities_id' => ['required', 'array'],
            // 'languages_id' => ['required', 'array'],
            // 'statement' => ['required'],
            // 'time_availability' => ['required'],
            // 'reference_name' => ['required'],
            // 'reference_title' => ['required'],
            // 'reference_email' => ['required', 'string', 'email'],
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'full_name.required' => 'First name field is required.',
            'full_name.alpha' => 'First name should be alphabatic.',
            'full_name.max'  => 'First name is too long.',
            // 'last_name.required' => 'Last name field is required.',
            // 'last_name.alpha' => 'Last name should be alphabatic.',
            // 'last_name.max'  => 'Last name is too long.',
            'email.required' => 'Email field is required.',
            'email.email'  => 'In-valid email format.',
            'email.max' => 'Email is too long.',
            'email.unique' => 'Email already exists.',
            'practice_website.required' => 'Practice website field is required.',
            //'practice_website.active_url' => 'Practice wesite is invalid.',
            'phone.required' => 'Phone number is required.',
            'phone.min' => 'Phone number is too short.',
            'address.required' => 'Address field is required.',
            'therapist_type.required' => 'Therapist type field is required.',
            'license_type.required' => 'License type field is required.',
            'specialities_id.*.required' => "Select Therapist Speciality",
            'treatments_id.*.required' => "Select Therapist Treatment Orientation",
            'modalities_id.*.required' => "Select Therapist Modality",
            'age_groups_id.*.required' => "Select Therapist Focus Client Age Group",
            'ethnicities_id.*.required' => "Select Therapist Focus Client Ethinicity",
            'languages_id.*.required' => "Select Therapist spoken languages",
            'statement.required' => 'Statement field is required.',
            'time_availability.required' => 'Time availability field is required.',
            'reference_name.required' => 'Reference name field is required.',
            'reference_name.alpha' => 'Reference name should be alphabatic.',
            'reference_name.max' => 'Reference name is too long.',
            'reference_title.required' => 'Reference title field is required.',
            'reference_email.required' => 'Reference email field is required.',
            'reference_email.email' => 'In-valid format of email.',
            'reference_email.max' => 'Reference email is too long.',
        ];
    }
}
