<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\MatchOldPassword;
class UpdateAdminPassword extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'current_password' => ['required', 'string', 'min:8', new MatchOldPassword],
            'new_password' => ['required', 'string', 'min:8'],
            'confirm_password' => ['required','same:new_password'],
        ];
    }


    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
    */
    public function messages()
    {
        return [
            'current_password.required' => 'Current password field is required.',
            'current_password.min' => 'Current password is too short.',
            'new_password.required'  => 'New password field is required.',
            'new_password.min' => 'New password id too short.',
            'confirm_password.required' => 'Confirm password field is required.',
            'confirm_password.same'  => 'Confirm password mis-matched.',
        ];
    }
}
