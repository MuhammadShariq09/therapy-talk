<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'full_name' => ['required', 'max:50'],
          //  'last_name' => ['required', 'alpha', 'max:50'],
            'phone' => ['required', 'min:8'],
            'address' => ['required'],
           // 'date_of_birth' => ['required'],
        ];
    }


    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
    */
    public function messages()
    {
        return [
            'first_name.required' => 'First name field is required.',
            'first_name.alpha' => 'First name should be alphabatic.',
            'first_name.max'  => 'First name is too long.',
            'last_name.required' => 'Last name field is required.',
            'last_name.alpha' => 'Last name should be alphabatic.',
            'last_name.max'  => 'Last name is too long.',
            'phone.required' => 'Phone number is required.',
            'phone.min' => 'Phone number must be atleast 8 characters long.',
            'address.required' => 'Address field is required.',
            'date_of_birth.required' => 'Date of birth field is required.',
            //'date_of_birth.date_format' => 'Invalid date format. Date should be in m/d/Y format.',
        ];
    }
}
