<?php

namespace App\Http\Middleware;

use Closure;

class Subscription
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth('therapist')->user();
        $user->load('subscriptions');
        if($user->is_verified == "1" && $user->is_active == "1"){
            if(!$user->subscriptions){
                return redirect()->route('therapist.subscribe-package');
            }
        }
        return $next($request);
    }
}
