<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use PayPal\Api\Item;
use PayPal\Api\Payer;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Payment;
use PayPal\Api\ItemList;
use PayPal\Api\WebProfile;
use PayPal\Api\InputFields;
use PayPal\Api\Transaction;
use PayPal\Api\RedirectUrls;
use PayPal\Api\PaymentExecution;
use App\Payment as PaymentLog;
use App\Session as SessionLog;

use App\Notifications\User\SessionEvents;
use App\Notifications\Therapist\SessionRequests;

class PaypalController extends Controller
{
    public function createPayment(Request $request)
    {

       // dd($request->all());
        $apiContext = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential(
                'AVUP0K6WDXB2E_dFwj90drJocMW-qGWr8la_FD1R3wbGIoE2OoWlPyyN4rK-8o4UmcrlRdFbxW-MF8fc',     // ClientID
                'EJsBq_k73E03MfCJ1221wadXgPgafn2jgBdrliUfrjxxv_x_pGaulsYVQGTSeou5ar8rEJX7lXbq-aWc'      // ClientSecret
            )
        );
    
        $payer = new Payer();
        $payer->setPaymentMethod("paypal");
    
        $item1 = new Item();
        $item1->setName('Ground Coffee 40 oz')
            ->setCurrency('USD')
            ->setQuantity(1)
            ->setSku("123123") // Similar to `item_number` in Classic API
            ->setPrice(7.5);
        $item2 = new Item();
        $item2->setName('Granola bars')
            ->setCurrency('USD')
            ->setQuantity(5)
            ->setSku("321321") // Similar to `item_number` in Classic API
            ->setPrice(2);
    
        $itemList = new ItemList();
        $itemList->setItems(array($item1, $item2));
    
        $details = new Details();
        $details->setShipping(1.2)
            ->setTax(1.3)
            ->setSubtotal(17.50);
    
        $amount = new Amount();
        $amount->setCurrency("USD")
            ->setTotal(20)
            ->setDetails($details);
    
        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription("Payment description")
            ->setInvoiceNumber(uniqid());
    
        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl("http://laravel-paypal-example.test")
            ->setCancelUrl("http://laravel-paypal-example.test");
    
        // Add NO SHIPPING OPTION
        $inputFields = new InputFields();
        $inputFields->setNoShipping(1);
    
        $webProfile = new WebProfile();
        $webProfile->setName('test' . uniqid())->setInputFields($inputFields);
    
        $webProfileId = $webProfile->create($apiContext)->getId();
    
        $payment = new Payment();
        $payment->setExperienceProfileId($webProfileId); // yno shipping
        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions(array($transaction));
    
        try {
            $payment->create($apiContext);
        } catch (Exception $ex) {
            echo $ex;
            exit(1);
        }
    
       
    
        return $payment;
    }

    public function executePayment(Request $request) {
        $apiContext = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential(
                'AVUP0K6WDXB2E_dFwj90drJocMW-qGWr8la_FD1R3wbGIoE2OoWlPyyN4rK-8o4UmcrlRdFbxW-MF8fc',     // ClientID
                'EJsBq_k73E03MfCJ1221wadXgPgafn2jgBdrliUfrjxxv_x_pGaulsYVQGTSeou5ar8rEJX7lXbq-aWc'      // ClientSecret
            )
        );
    
        $paymentId = $request->paymentID;
        $payment = Payment::get($paymentId, $apiContext);
    
        $execution = new PaymentExecution();
        $execution->setPayerId($request->payerID);
    
        // $transaction = new Transaction();
        // $amount = new Amount();
        // $details = new Details();
    
        // $details->setShipping(2.2)
        //     ->setTax(1.3)
        //     ->setSubtotal(17.50);
    
        // $amount->setCurrency('USD');
        // $amount->setTotal(21);
        // $amount->setDetails($details);
        // $transaction->setAmount($amount);
    
        // $execution->addTransaction($transaction);

        
    
        try {
            $result = $payment->execute($execution, $apiContext);
            $response = $result->toArray();
          
            $payment = PaymentLog::create([
                'user_id' => 1,
                'transaction_id' => $response['transactions'][0]['invoice_number'],
                'status' => 0,
                'type' => 1,
                'payload' => $response
            ]);

            $sessionLog = SessionLog::with('therapist')->where('user_id', auth()->user()->id)->where('id', $response['transactions'][0]['invoice_number'])->first();
            $sessionLog->is_paid = 1;
            $sessionLog->save();
            
            $userData = auth()->user();
           
             auth()->user()->notify(new SessionEvents($userData,$sessionLog));
             $sessionLog->therapist->notify(new SessionRequests($sessionLog));

           
        } catch ( \PayPal\Exception\PayPalConnectionException $ex) {
            echo $ex;
            exit(1);
        }
    
        return $result;
    }
}
