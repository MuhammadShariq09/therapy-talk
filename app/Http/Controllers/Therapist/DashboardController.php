<?php

namespace App\Http\Controllers\Therapist;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Session;
use DB;

class DashboardController extends Controller
{
    public function index()
    {
        $new = $this->custom_number_format(Session::where('trainer_id', auth()->user()->id)->where('status', 0)->count());
        $completed = $this->custom_number_format(Session::where('trainer_id', auth()->user()->id)->where('status', 4)->count());
        $earning = $this->custom_number_format(Session::where('trainer_id', auth()->user()->id)->where('is_paid', 1)->select(DB::raw('sum(amount) as total'))->pluck('total')->first());
    
        $totalAppointments = $this->totalAppointments();

        return view('therapist.dashboard', compact('new', 'completed', 'earning'))->with(['totalAppointments'=>$totalAppointments]);

    }

    public function totalAppointments(){
        $records = Session::
            select(DB::raw("Count(id) as count, MONTH(created_at) month"))
            ->where('trainer_id', auth()->user()->id)
            ->groupBy(DB::raw("year(created_at), MONTH(created_at)"))
            ->get();

        $month = Session::select(DB::raw("Count(id) as count"))
                ->orderBy("created_at")
                ->groupBy(DB::raw("month(created_at)"))
                ->get()->toArray();
        $month = array_column($month, 'count');

        $months = collect(['', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']);

        $chartData = collect([]);

        for ($i = 1; $i <= 12; $i++){
            $std = new \stdClass();
            $std->key = $months[$i];
            $std->value = $records->where('month', $i)->sum('count');
            $chartData->push($std);
        }

        return $chartData;


    }

    public function custom_number_format($n) {
        if ($n >= 1000) {
            $n_format = floor($n).'K';
        }else if ($n >= 1000000) {
            $n_format = floor($n / 1000000) . 'M';
        } else if ($n >=1000000000 ) {
            $n_format = floor($n / 1000000000) . 'B';
        } else {
            $n_format = floor($n);
        }
        return $n_format;
    }
}
