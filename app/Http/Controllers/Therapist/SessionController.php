<?php

namespace App\Http\Controllers\Therapist;

use App\AgeGroup;
use App\Ethnicity;
use App\Http\Controllers\Controller;
use App\Language;
use App\Modality;
use App\SessionRejection;
use App\SessionReshedule;
use App\Subscription;
use App\Package;
use App\Speciality;
use App\Therapist;
use App\TherapistAddress;
use App\TreatmentOrientation;
use App\User;
use App\TherapistPayment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Session as SessionLog;
//use App\Mail\User\SessionEvents;
//use App\Mail\User\SessionsReshedule;
//use App\Mail\Therapist\SessionRequests;
//use App\Mail\Therapist\ResheduleRequests;
use App\Mail\RescheduleApprovedAppointments;
use App\Mail\AdminMail;

use App\Notifications\User\SessionEvents;
use App\Notifications\Therapist\SessionRequests;

use App\Notifications\User\SessionsReshedule;
use App\Notifications\Therapist\ResheduleRequests;

class SessionController extends Controller
{

    protected $user;


    public function index(Request $request){
        $user = auth('therapist')->user();
        $sessions = $user->sessions()->with('payment')->orderBy('id','desc');
        if($request->filled('from') && $request->filled('to')) {
          
            $sessions->where(function ($query) use ($request) {
                $query->whereBetween('requested_date', [date('Y-m-d', strtotime($request->from)), date('Y-m-d', strtotime($request->to))]);
            });
        }
        $sessions = $sessions->get();    

        return view('therapist.pages.sessions.index', compact('sessions'));
    }

    public function approveRescheduleRequest($id){
     
        $user = auth('therapist')->user();
        $session = SessionReshedule::where('id', $id)->first();
        $session->status = 1;
        $session->save();
       
        $sessionLog = SessionLog::with('therapist','user')->find($session->session_id);
     
        // \Mail::to($sessionLog->user->email)->send(new SessionsReshedule($sessionLog->user, $session));
        // \Mail::to($user->email)->send(new ResheduleRequests($user, $session));

        auth()->user()->notify(new SessionsReshedule($sessionLog->user, $session));
        $sessionLog->therapist->notify(new ResheduleRequests($user, $session));

        return redirect()->back()->with(['success' => 'Appointment reschedule request is approved']);
    }

    public function rejectRescheduleRequest(Request $request, $id){
        $user = auth('therapist')->user();
        $session = SessionReshedule::where('id', $id)->first();
        $session->status = 2;
        $session->t_suggested_date =  Carbon::parse($request->t_suggested_date)->format('Y-m-d');
        $session->t_suggested_time = $request->t_suggested_time;
        $session->save();

        $sessionLog = SessionLog::with('user')->find($session->session_id);
        // \Mail::to($sessionLog->user->email)->send(new SessionsReshedule($sessionLog->user, $session));
        // \Mail::to($user->email)->send(new ResheduleRequests($user, $session));
        auth()->user()->notify(new SessionsReshedule($sessionLog->user, $session));
        $session->therapist->notify(new ResheduleRequests($user, $session));


        return redirect()->back()->with(['success' => 'Appointment reschedule request is rejected']);
    }

    public function rescheduleApprovedAppointment(Request $request, $id){
        $user = auth('therapist')->user();
        $session = SessionLog::where('trainer_id', $user->id)->where('id', $id)->first();

        if($session->payment){
            $from = Carbon::parse($session->payment->created_at);
            $to = Carbon::now();
            $diff_in_hours = $to->diffInHours($from);

            if($diff_in_hours < 48){

                $session->reschedule()->save(new SessionReshedule([
                    'session_id' => $session->id,
                    'suggested_date' =>  Carbon::parse($request->suggested_date)->format('Y-m-d'),
                    'suggested_time' => $request->suggested_time,
                    'status' => 0
                ]));
                
                $r = SessionReshedule::where('session_id',$session->id)->first();
                $t_msg = "Your appointment reschedule request has been sent";
                $u_msg = "Therapist has requested for the appointment to be rescheduled. Please view the details and respond in time.";
                
                \Mail::to($user->email)->send(new RescheduleApprovedAppointments($user, $t_msg));
                \Mail::to($session->user->email)->send(new RescheduleApprovedAppointments($session->user, $u_msg));

                $message = "Session has been rescheduled between the User:".$session->user->name." and Therapist: ".$user->name." on Date: ".$r->suggested_date." at Time: ".$r->suggested_time.".";
                \Mail::to(\App\Admin::all())->send(new AdminMail($message, 'Reschedule Session'));

                return redirect('therapist/session/logs/'.$id)->with(['success' => 'Appointment is reschedules waiting for user to accept or reject']);
            }
        }
        return redirect('session/logs/');
    }

    public function view(Request $request, $id){
        $user = auth('therapist')->user();
        $session = SessionLog::where('trainer_id', $user->id)->where('id', $id)->first();
        $diff_in_hours = 0;
        if($session->payment){
            $from = Carbon::parse($session->payment->created_at);
            $to = Carbon::now();
            $diff_in_hours = $to->diffInHours($from);
        }

        $session->load('reschedule');

        return view('therapist.pages.sessions.view', compact('session', 'diff_in_hours'));
    }

    public function approveSessionLog(Request $request, $id){
        $user = auth('therapist')->user();
        $session = SessionLog::where('trainer_id', $user->id)->where('id', $id)->first();
        $session->status = 1;
        $session->save();

        // \Mail::to($session->user->email)->send(new SessionEvents($session->user, $session));
        // \Mail::to($user->email)->send(new SessionRequests($session));

        $session->user->notify(new SessionEvents($session->user,$session));
        $session->therapist->notify(new SessionRequests($session));

        return redirect()->route('therapist.session.logs');
    }

    public function rejectSessionLog(Request $request, $id){
        $user = auth('therapist')->user();
        $session = SessionLog::where('trainer_id', $user->id)->where('id', $id)->first();
        $session->status = 2;
        $session->save();

        $session->rejections()->save(new SessionRejection([
            'reason' => $request->reason,
            'suggested_date' => Carbon::parse($request->suggested_date)->format('Y-m-d'),
            'suggested_time' => $request->suggested_time,
            'status' => 0
        ]));
      
        //\Mail::to($session->user->email)->send(new SessionEvents($session->user, $session));

        $session->user->notify(new SessionEvents($session->user,$session));
       
        return redirect('/therapist/session/logs');

    }


    public function createRandomRecord(Request $request){
        $date = Carbon::now()->addDay(2);
        $me = auth('therapist')->user();

        $users = User::where('is_active',"1")->get();
        foreach ($users as $user){
            SessionLog::create([
                'user_id' => $user->id,
                'trainer_id' => $me->id,
                'reason' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labo re et dolore magna aliqua. Enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
                'type' => 0,
                'duration' => 1,
                'requested_date' => Carbon::parse($date)->format('Y-m-d'),
                'requested_time' => Carbon::parse($date)->format('H:i:s'),
                'status' => 0,
            ]);
        }
        return redirect('session/logs');
    }

    public function myAppointments(Request $request){
         $user = auth('therapist')->user();
         $sessions = $user->sessions()->where('is_paid', 1)->where('status', '!=', 0)->orderBy('id','desc');
         if($request->filled('from') && $request->filled('to')) {
            $sessions->where(function ($query) use ($request) {
                $query->whereBetween('requested_date',[date('Y-m-d', strtotime($request->from)), date('Y-m-d', strtotime($request->to))]);
            });
        }

        if($request->filled('duration')) {
            $sessions->where(function ($query) use ($request) {
                $query->where('duration', $request->duration);
            });
        }

        if($request->filled('status')) {
            $sessions->where(function ($query) use ($request) {
                $query->where('status', $request->status);
            });
        }

        $sessions = $sessions->get();
        
        return view('therapist.pages.appointments.index', compact('sessions'));
    }

    public function myAppointmentsDetail($id){
        $user = auth('therapist')->user();
        $session = SessionLog::where('trainer_id', $user->id)->where('id', $id)->where('is_paid', 1)->first();
        $diff_in_hours = 0;
       
        if($session->payment){
            $from = Carbon::parse($session->payment->created_at);
            $to = Carbon::now();
            $diff_in_hours = $to->diffInHours($from);
        }

        $session->load('reschedule');
        
        if(isset($session->reschedule[0])){
            $date  = $session->reschedule[0]->suggested_date;
            $time  = $session->reschedule[0]->suggested_time;
        }else{
            $date  = $session->requested_date;
            $time  = $session->requested_time;
        }
      
      
        $dt = Carbon::now();    //$time ==  $dt->toTimeString()
        if($date == $dt->toDateString())
        {
            $chat = 1;
        }else{
            $chat = 0;
        }

        if(isset($session->reschedule[0])){
            $from = Carbon::parse($session->reschedule[0]->suggested_date);
        }else{
            $from = Carbon::parse($session->requested_date);
        }
        
        $to = Carbon::now();     
        $diff_in_days = $to->diffInDays($from);
     
       return view('therapist.pages.appointments.view', compact('session', 'diff_in_hours', 'diff_in_days', 'date', 'time', 'chat'));
   }

   public function changeStatus($id, $status){
    $session = SessionLog::find($id);
    $session->status = $status;
    $session->save();

    return response()->json(['success'=>true]);
   }

   public function paymentLogs(Request $request)
   {
        $user = auth('therapist')->user();
        $sessions = SessionLog::with('user')->where('trainer_id', $user->id)->where('status', 4)->orderBy('id','desc');
       //data filter
        if($request->filled('from') && $request->filled('to')) {
            $sessions->where(function ($query) use ($request) {
                $query->whereBetween('requested_date', [date('Y-m-d', strtotime($request->from)), date('Y-m-d', strtotime($request->to))]);
            });
        }
        
        // month filter
        if($request->filled('month') ) {
            $sessions->where(function ($query) use ($request) {
                $query->where('month', $request->month);
            });
        }else{
            $sessions->where('month', date('m'));
        }
        $sessions = $sessions->get(); 

        $payments = TherapistPayment::where('therapist_id', $user->id)->orderBy('id', 'DESC');
        
        if($request->filled('month')) {
            $payments->where(function ($query) use ($request) {
                $query->where('month', $request->month);
            });
        }
        else{
            $payments->where('month', date('m')); 
        }
        $payments = $payments->first(); 

        $current_month = date('m');
        $current_year = date('Y');
        
        $total = TherapistPayment::select(\DB::raw('SUM(amount) as total'))->where('therapist_id', $user->id)->pluck('total')->first();

        // $paid_sessions = Session::where('trainer_id', $user->id)->where('month',$current_month)->where('year',$current_year)->whereStatus(4)->where('is_therapist_paid',1)->get();
        // $paid = count($paid_sessions)>0 ? true : false;
      
        return view('therapist.pages.payments.payment-log', compact('total', 'payments', 'sessions', 'paid'));
    }



}














































