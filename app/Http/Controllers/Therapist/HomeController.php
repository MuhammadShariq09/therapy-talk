<?php

namespace App\Http\Controllers\Therapist;

use App\AgeGroup;
use App\Ethnicity;
use App\Http\Controllers\Controller;
use App\Language;
use App\Modality;
use App\Subscription;
use App\Package;
use App\Speciality;
use App\Therapist;
use App\TherapistAddress;
use App\TreatmentOrientation;
use App\State;
use App\City;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{

    protected $user;

    public function __construct(){
    }

    public function index()
    {

        $user = auth('therapist')->user();
        $user->load('offices.states', 'offices.cities');
       
        $speciality = Speciality::orderBy('name', 'ASC')->get();
        $modality = Modality::orderBy('name', 'ASC')->get();
        $ethnicity = Ethnicity::orderBy('name', 'ASC')->get();
        $agegroup = AgeGroup::orderBy('name', 'ASC')->get();
        $treatment = TreatmentOrientation::orderBy('name', 'ASC')->get();
        $language = Language::orderBy('name', 'ASC')->get();

        return view('therapist.pages.profile.index', compact('user', 'speciality','modality', 'ethnicity', 'agegroup', 'treatment', 'language', 'url'));

    }

    public function subscribePackage(Request $request){
        $user = auth('therapist')->user();
        $user->load('subscriptions');
        if($user->subscriptions){
            return redirect('/therapist');
        }
        $packages = Package::all();
        return view('therapist.pages.subscription.index',compact('packages'));
    }

    public function submitSubscribePackage(Request $request){
        $user = auth('therapist')->user();
        $user->load('subscriptions');
        if($user->subscriptions){
            return redirect('/therapist');
        }
        $packages = Package::all();
        $request->validate([
            'packageid' => 'required',
            'packagename' => 'required',
            'packageamount' => 'required',
            'packagemonth' => 'required',
            'card' => 'required',
            'cvv' => 'required',
            'expiry' => 'required',
            'card_holder' => 'required',
        ]);

        Subscription::create([
            'user_id' => $user->id,
            'package_id' => $request->packageid,
            'status' => 1,
            'type' => 0
        ]);

        return response()->json(['message' => 'You have successfully subscribed the package']);
    }

    public function changePassword(Request $request){
        $request->validate([
            'oldpassword' => 'required',
            'newPassword' => 'required',
            'confirmNewPassword' => 'required',
         ],[
             'oldpassword.required' => "Current Password is required",
             'newPassword.required' => "New Password is required",
             'confirmNewPassword.required' => "Re-Type Password is required",
         ]
     );

        $user = auth('therapist')->user();
        if(Hash::check($request->oldpassword, $user->newpassword)){
            $user->password = bcrypt($request->newpassword);
            $user->save();
            return $this->responseSuccess('Password is Updated successfully');
        }
        else{
            return $this->responseWithError([ 'password' => ['Old password do not match to our records'] ]);
        }
    }

    public function editProfile(Request $request){

        $user = auth('therapist')->user();
        $user->load('offices');

        $speciality = Speciality::orderBy('name', 'ASC')->get();
        $modality = Modality::orderBy('name', 'ASC')->get();
        $ethnicity = Ethnicity::orderBy('name', 'ASC')->get();
        $agegroup = AgeGroup::orderBy('name', 'ASC')->get();
        $treatment = TreatmentOrientation::orderBy('name', 'ASC')->get();
        $language = Language::orderBy('name', 'ASC')->get();
        $states = State::orderBy('name', 'ASC')->get();
        $city = City::orderBy('city', 'ASC')->get();

        return view('therapist.pages.profile.edit-profile', compact('user', 'speciality','modality', 'ethnicity', 'agegroup', 'treatment', 'language', 'states', 'city'));
    }


    public function updateProfile(Request $request){
        $validate = $request->validate([
            'full_name' => ['required', 'string', 'max:255'],
           // 'email' => ['required', 'string', 'email', 'max:255', 'unique:therapists', 'unique:users'],
          //  'password' => ['required', 'string', 'min:8', 'confirmed'],
          //  'password_confirmation' => 'required|min:8',
            'repeater-group.*.practice_website' => 'required',
            'repeater-group.*.phone' => 'required',
            'repeater-group.*.address' => 'required',
            'repeater-group.*.city' => 'required',
            'repeater-group.*.state' => 'required',
            'repeater-group.*.country' => 'required',
            'repeater-group.*.zipcode' => 'required',
          //  'accept' => 'required|in:1'
      ],
      [
        'repeater-group.*.practice_website.required' => 'Practice Website field is required.',
        'repeater-group.*.phone.required' => 'Phone field is required.',
        'repeater-group.*.address.required' => 'Address field is required.',
        'repeater-group.*.city.required' => 'City field is required.',
        'repeater-group.*.state.required' => 'State field is required.',
        'repeater-group.*.country.required' => 'Country field is required.',
        'repeater-group.*.zipcode.required' => 'Zipcode field is required.',
        'accept.required' => 'Please accept terms and conditions before registration'
      ]
    );
      
       
        $therapist = auth('therapist')->user();
        $therapist->fill($request->except(['email', 'password']));
       // $therapist->is_verified = "0";
        $therapist->rejected_reason = "";
        $therapist->is_rejected = "0";
        $therapist->save();


        $therapist->offices()->delete();
        foreach ($request->{'repeater-group'} as $office) {
            $therapist->offices()->save(new TherapistAddress($office));
        }
        if($therapist->is_verified==0){
            Session::flash('message','Profile has been send for approval from admin');
        }else{
            Session::flash('message','Your profile is updated .');
        }
        return redirect()->route('therapist.profile');
    }

}














































