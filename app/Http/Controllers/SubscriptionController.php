<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{
    Subscription,
    SubscriptionLog,
    PackageUsage,
    Package
};
use Cartalyst\Stripe\Stripe;
use App\Session as SessionLog;
use App\Notifications\User\GeneralNotification;
use App\Notifications\AdminNotification;
use DateTime, Notification;



class SubscriptionController extends Controller
{
    public function subscriptionLogs(Request $request)
    {
        $subcriptionLogs = SubscriptionLog::where('user_id', auth()->user()->id)->orderBy('id', 'DESC');
        if($request->filled('from') && $request->filled('to')) {
            $subcriptionLogs->where(function ($query) use ($request) {
                $query->whereBetween('created_at', [date('Y-m-d', strtotime($request->from)), date('Y-m-d', strtotime($request->to))]);
            });
        }

        $subcriptionLogs = $subcriptionLogs->get();
        $user = auth()->user()->load('subscriptions');
        $package = Package::where('id', $user->subscriptions->package_id)->first();
        $usage = PackageUsage::where('user_id', auth()->user()->id)->first();
        $all_packages = Package::whereType(1)->get();
       
        return view('user.subscription-log', compact('subcriptionLogs', 'usage', 'package', 'user', 'all_packages'));
    }

    public function userSubscription(Request $request){
        $user = auth()->user();
        $request->validate([
            'packageid' => 'required',
            'packagename' => 'required',
            'packageamount' => 'required',
            'packagemonth' => 'required',
            'card' => 'required',
            'cvv' => 'required',
            'expiry' => 'required',
            'card_holder' => 'required',
        ]);

        $package = Package::find($request->packageid);

        $date = new DateTime('+1 month');

        $subscription = new Subscription();
        $subscription->user_id = $user->id;
        $subscription->package_id = $request->packageid;
        $subscription->amount = $package->amount;
        $subscription->expiry_date = $date->format('Y-m-d');
        $subscription->status =1 ;
        $subscription->type = 1; // type 1 for user and type 0 for therapist
        $subscription->save(); 
       
        $response = $this->pay($request, $subscription->amount, 1, $subscription->id);

        $exist = PackageUsage::where('user_id',$user->id)->first();
       
         PackageUsage::updateOrCreate(
        ['user_id' => $user->id],
        [
            'user_id' => $user->id,
            'live_chat' => isset($exist) ? $exist->live_chat+$package->live_chat : $package->live_chat,
            'voice_call' => isset($exist) ? $exist->voice_call+$package->voice_call : $package->voice_call,
            'video_call' => isset($exist) ? $exist->video_call+$package->video_call : $package->video_call,
        ]);

        $log = SubscriptionLog::create([
            'user_id' => $user->id,
            'type' => 1 ,// type 1 for user and type 0 for therapist
            'package_id' => $subscription->package_id,
            'title' => $package->title,
            'amount' => $package->amount,
            'subscribe_date'=> date('Y-m-d', strtotime($subscription->created_at)),
            'expiry_date' => date('Y-m-d', strtotime($subscription->expiry_date)),
        ]);
       
        auth()->user()->notify(new GeneralNotification("You are subscribed to a package".$package->title, 'Package subscribed'));
        Notification::send(\App\Admin::all(), new AdminNotification("User has subscribed to a new package", 'Package subscribed'));

        return response()->json(['message' => 'You have successfully subscribed the package']);
    }

    public function usePackage($id, $type=0)
    {
        $sessionLog = SessionLog::with('therapist')
        ->where('user_id', auth()->user()->id)
        ->where('id', $id)
        ->first();
       
        if($sessionLog)
        {
            $sessionLog->is_paid = 1;
            $sessionLog->payment_type = 1;
            $sessionLog->save();
        
            //update ussage
            $package_usage = PackageUsage::where('user_id', auth()->user()->id)->first();
            if($sessionLog->session_type==1){
                $package_usage->live_chat_availed = $package_usage->live_chat_availed+1;
                $package_usage->save();
            }
            else if($sessionLog->session_type==2){
                $package_usage->voice_call_availed = $package_usage->voice_call_availed+1;
                $package_usage->save();
            }
            else{
                $package_usage->video_call_availed = $package_usage->video_call_availed+1;
                $package_usage->save();
            }
            if($type==0){
            auth()->user()->notify(new GeneralNotification("Session payment is done using package", 'Session Payment'));
            $sessionLog->therapist->notify(new GeneralNotification("User has paid session fee.", 'Session Payment'));
            }
            
        }
        return response()->json(['message' => 'Session payment is done using package']);
    }

    public function renewPackage(Request $request){
        $request->validate([
            'card' => 'required',
            'cvv' => 'required',
            'expiry' => 'required',
            'card_holder' => 'required',
        ]);

        $date = new DateTime('+1 month');

        $subscription = Subscription::where('user_id', auth()->user()->id)->first();

        $response = $this->pay($request, $subscription->amount, 1, $subscription->id);

        $subscription->expiry_date = $date->format('Y-m-d');
        $subscription->save();

        $package = Package::find($subscription->package_id);
        
        $pkg_usage = PackageUsage::where('user_id',auth()->user()->id)->first();
        $pkg_usage->live_chat = $pkg_usage->live_chat+$package->live_chat;
        $pkg_usage->voice_call = $pkg_usage->voice_call+$package->voice_call;
        $pkg_usage->video_call = $pkg_usage->video_call+$package->video_call;
        $pkg_usage->save();

        $log = SubscriptionLog::create([
            'user_id' => auth()->user()->id,
            'type' => 1 ,// type 1 for user and type 0 for therapist
            'package_id' => $subscription->package_id,
            'title' => $package->title,
            'amount' => $package->amount,
            'subscribe_date'=> date('Y-m-d', strtotime($subscription->updated_at)),
            'expiry_date' => date('Y-m-d', strtotime($subscription->expiry_date)),
        ]);

        $update_usage = $this->usePackage($request->id, 1);
       
        auth()->user()->notify(new GeneralNotification("Your package renewal request has been completed and your new sesion payment has been made through your current package. ", 'Package subscribed'));
        Notification::send(\App\Admin::all(), new AdminNotification("User has renew thier current package", 'Package Renew'));

        return response()->json(['message' => 'Session payment is done using package']);
        
    }

    public function renewPackageSubscription(Request $request){
        $request->validate([
            'card' => 'required',
            'cvv' => 'required',
            'expiry' => 'required',
            'card_holder' => 'required',
        ]);

        $date = new DateTime('+1 month');

        $subscription = Subscription::where('user_id', auth()->user()->id)->first();

        $response = $this->pay($request, $subscription->amount, 1, $subscription->id);

        $subscription->expiry_date = $date->format('Y-m-d');
        $subscription->save();

        $package = Package::find($subscription->package_id);
        
        $pkg_usage = PackageUsage::where('user_id',auth()->user()->id)->first();
        $pkg_usage->live_chat = $pkg_usage->live_chat+$package->live_chat;
        $pkg_usage->voice_call = $pkg_usage->voice_call+$package->voice_call;
        $pkg_usage->video_call = $pkg_usage->video_call+$package->video_call;
        $pkg_usage->save();

        $log = SubscriptionLog::create([
            'user_id' => auth()->user()->id,
            'type' => 1 ,// type 1 for user and type 0 for therapist
            'package_id' => $subscription->package_id,
            'title' => $package->title,
            'amount' => $package->amount,
            'subscribe_date'=> date('Y-m-d', strtotime($subscription->updated_at)),
            'expiry_date' => date('Y-m-d', strtotime($subscription->expiry_date)),
        ]);
       
        auth()->user()->notify(new GeneralNotification("Your package renewal request has been completed.", 'Package Renewal'));
        Notification::send(\App\Admin::all(), new AdminNotification("User has renew thier current package", 'Package Renewal'));

        return response()->json(['message' => 'Package renewal has been done.']);
        
    }

    public function pay(Request $request, $amount, $type, $id){
        $stripe = Stripe::make(config('services.stripe.SECRET_KEY'));

        $order_id = rand(1, 10000);
        try{
            $charge = $stripe->charges()->create([
                'amount' => $amount,
                'currency' => 'usd',
                'source' => $request->stripe_token,
                'transfer_group' => $order_id,
            ]);
        }
        catch (\Exception $e){
            return $e->getMessage();
           // return redirect()->back()->withErrors(['stripe' => $e->getMessage()]);
        }

        $payment = Payment::create([
            'user_id' => auth()->user()->id,
            'transaction_id' => $id,
            'status' => 0,
            'type' => $type,
            'payload' => $charge
        ]);

        return ['id' => $payment->id, 'payload' => $charge];
    }

    public function subscribeNewPackage(Request $request){
        $user = auth()->user();
        $request->validate([
            'packageid' => 'required',
            'packagename' => 'required',
            'packageamount' => 'required',
            'card' => 'required|min:16|max:16',
            'cvv' => 'required|min:3|max:3',
            'expiry' => 'required',
            'card_holder' => 'required',
        ]);

        $package = Package::find($request->packageid);

        $date = new DateTime('+1 month');

        $subscription = Subscription::where('user_id', $user->id)->first();
        $subscription->user_id = $user->id;
        $subscription->package_id = $request->packageid;
        $subscription->amount = $package->amount;
        $subscription->expiry_date = $date->format('Y-m-d');
        $subscription->status =1 ;
        $subscription->type = 1; // type 1 for user and type 0 for therapist
        $subscription->save(); 
        
       
        $response = $this->pay($request, $subscription->amount, 1, $subscription->id);

        $exist = PackageUsage::where('user_id',$user->id)->first();
       
         PackageUsage::updateOrCreate(
        ['user_id' => $user->id],
        [
            'user_id' => $user->id,
            'live_chat' => isset($exist) ? $exist->live_chat+$package->live_chat : $package->live_chat,
            'voice_call' => isset($exist) ? $exist->voice_call+$package->voice_call : $package->voice_call,
            'video_call' => isset($exist) ? $exist->video_call+$package->video_call : $package->video_call,
        ]);

        $log = SubscriptionLog::create([
            'user_id' => $user->id,
            'type' => 1 ,// type 1 for user and type 0 for therapist
            'package_id' => $subscription->package_id,
            'title' => $package->title,
            'amount' => $package->amount,
            'subscribe_date'=> date('Y-m-d', strtotime($subscription->created_at)),
            'expiry_date' => date('Y-m-d', strtotime($subscription->expiry_date)),
        ]);
       
        auth()->user()->notify(new GeneralNotification("You are subscribed to a package".$package->title, 'Package subscribed'));
        Notification::send(\App\Admin::all(), new AdminNotification("User has subscribed to a new package", 'Package subscribed'));

        return response()->json(['message' => 'You have successfully subscribed the package']);
    }



    



}
