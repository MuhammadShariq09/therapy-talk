<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Assesment;
use App\Subscription;
use App\Package;
use App\Mail\User\AssesmentForm;
use App\User;
use App\Mail\AdminMail;
use DateTime;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('frontend.index');
    }

    public function profile()
    {
        $id = auth()->user()->id;
        $user = User::with('states', 'cities')->find($id);
        return view('user.profile', ['user' => $user]);
    }

    public function editProfile(){
        $id = auth()->user()->id;
        $user = User::with('states', 'cities')->find($id);
        $states = \DB::table('states')->get();
        return view('user.edit_profile', ['user' => $user, 'states'=>$states]);

    }

    public function updatePassword(Request $request){
        $request->validate([
           'oldpassword' => 'required',
           'newPassword' => 'required',
           'confirmNewPassword' => 'required',
        ],[
            'oldpassword.required' => "Current Password is required",
            'newPassword.required' => "New Password is required",
            'confirmNewPassword.required' => "Confirm New Password is required",
        ]
    );
        $user = auth()->user();
        if(Hash::check($request->oldpassword, $user->password)){
            if($request->newPassword == $request->confirmNewPassword){
                $user->password = Hash::make($request->newPassword);
                $user->save();

                return $this->responseSuccess('Password Updated successfully');
            }
            return $this->responseWithError(['newPassword' => 'The Password and confirm password should be same']);
        }
        return $this->responseWithError(['oldpassword' => 'Old Password do not match']);
    }

    public function updateProfile(Request $request){
       
        $request->validate([
            'full_name' => ['required', 'string', 'max:255'],
           // 'last_name' => ['required', 'string', 'max:255'],
            'date_of_birth' => ['required'],
            'phone' => ['required', 'string', 'min:8'],
            'address' => 'required'
        ],[
            'phone.min' => 'The phone must be at least 8 digits.'
        ]);

        $user = auth()->user();
        $user->fill($request->except(['email', 'password']));
        $user->save();
        return redirect()->route('user.profile')->with('success','Profile is updated!');
    }

    public function becomeAMember(){
        return view('auth.become_a_member');
    }

    public function submitAssesmentForm(Request $request){
        $request->validate([
            'email' => ['required', 'string', 'email', 'max:255', 'unique:therapists'],
            'item.*' => 'required'
        ],[
            'item.*' => 'Please fill up all the questions in assestment form !',
           // 'email.unique'=> "You have already submitted"
        ]);
        Assesment::create([
            'user_id' => auth()->user()->id,
            'email' => $request->email,
            'data' => implode('|', $request->item)
        ]);

        $user_id = auth()->user()->id;

        $user = User::find($user_id)->update(['is_assessment'=>1 ]);

        \Mail::to($request->email)->send(new AssesmentForm());
        $message = "A new User has filled out an assessment form.";
        \Mail::to(\App\Admin::all())->send(new AdminMail($message, 'Assesment Form'));
        return $this->responseSuccess('You assesment has been submitted successfully');
    }

    
    

    
}





















