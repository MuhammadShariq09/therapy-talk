<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\{
    Session,
    Package,
    Payment,
    Therapist,
    Subscription,
    TherapistPayment
};
use Carbon\Carbon;
use DB;
use App\Notifications\User\GeneralNotification;

class PaymentController extends Controller
{
    public function index(Request $request){
        $sessions = Session::with('payment', 'therapist')->where('is_paid', 1)->orderBy('id','desc');
        if($request->filled('from') && $request->filled('to')) {
          
            $sessions->where(function ($query) use ($request) {
                $query->whereBetween('requested_date', [date('Y-m-d', strtotime($request->from)), date('Y-m-d', strtotime($request->to))]);
            });
        }
  
        if($request->filled('status')) {
            $sessions->where(function ($query) use ($request) {
                $query->where('status', $request->status);
            });
        }

        $sessions = $sessions->get();
        $packages = Package::get();
        return view('admin.payments.index', compact('sessions', 'packages'));
    }

    public function view($id){
        $session = Session::with('payment', 'therapist', 'user')->where('id', $id)->orderBy('id','desc')->first();
        $diff_in_days = 0;
        if($session->payment){
            $created = new Carbon($session->payment->created_at);
            $now = Carbon::now();
            $diff_in_days = $created->diffForHumans($now);
        }
        return view('admin.sessions.view', compact('session','diff_in_days'));
    }

    public function subscriptions(){
        $subscriptions = Subscription::with('package', 'user')->get();
     //   dd($subscriptions);
        return view('admin.payments.subscriptions', compact('subscriptions'));
    }

    public function editPackage(Request $request, $id){
        
        $pkg = Package::find($id)->update(['amount' => $request->amount]);
        $sessions = Session::with('payment', 'therapist')->where('is_paid', 1)->orderBy('id','desc')->get();
        $packages = Package::get();
        return view('admin.payments.index', compact('sessions', 'packages'));
    }

    public function payTherapist(Request $request){
       // dd($request->all());
       $current_month = date('m');
       $current_year = date('Y');
       $current_month_sessions = Session::with('user')->where('trainer_id', $request->therapist_id)
                                ->where('month',$current_month)
                                ->where('year',$current_year)
                                ->whereStatus(4)
                                ->get();
        foreach($current_month_sessions as $row){
            $row->update(['is_therapist_paid'=>1]);
        }
       $payment = TherapistPayment::updateOrCreate(
        ['therapist_id'=>$request->therapist_id, 'month'=>$request->month,'year'=>$request->year],
        [
            'therapist_id'=>$request->therapist_id,
            'sessions'=>$request->sessions,
            'live_chat'=>$request->live_chat,
            'voice_call'=>$request->voice_call,
            'video_call'=>$request->video_call,
            'month'=>$request->month,
            'year'=>$request->year,
            'amount'=>$request->amount,
            'status'=>1
        ]);
       if($request->status==1){
        return response()->json(['message' =>'Payable mark as paid.']);
        }
        else{
            return response()->json(['message' =>'Payable mark as un-paid.']);
        }
    }

    public function payables(Request $request){
        //$therapists = Session::with('payment', 'therapist')->where('is_paid', 1)->orderBy('id','desc');
        $therapists = Therapist::join('sessions', 'sessions.trainer_id', '=', 'therapists.id')
                        ->select('sessions.*', 'therapists.*', DB::raw('YEAR(sessions.created_at) year, MONTH(sessions.created_at) month'))
                       // ->groupby('year','month')
                        ->where('sessions.status', 4)
                        ->orderBy('sessions.id','desc');
        if($request->filled('from') && $request->filled('to')) {
          
            $therapists->where(function ($query) use ($request) {
                $query->whereBetween('requested_date', [date('Y-m-d', strtotime($request->from)), date('Y-m-d', strtotime($request->to))]);
            });
        }
  
        if($request->filled('status')) {
            $therapists->where(function ($query) use ($request) {
                $query->where('status', $request->status);
            });
        }

        $therapists = $therapists->get();
     // dd($therapists);
        $users = Session::with('payment', 'user')->where('is_refund', 1)->orderBy('id','desc')->get();
     
        return view('admin.payments.payables', compact('therapists', 'users'));
    }

    public function payablesView($id){
        $therapist = Therapist::find($id);
        $therapist_sessions = Session::get('trainer_id', $id);
        $current_month = date('m');
        $current_year = date('Y');
        $current_month_sessions = Session::with('user')->where('trainer_id', $id)->where('month',$current_month)->where('year',$current_year)->whereStatus(4)->where('is_therapist_paid',0)->get();
        $total_sessions = count($current_month_sessions);
        $total_live_chat = $this->session_type_count($therapist->id, $current_month, $current_year, 1)*config('app.live_chat');
        $total_voice_call = $this->session_type_count($therapist->id, $current_month, $current_year, 2)*config('app.voice_call');
        $total_video_call = $this->session_type_count($therapist->id, $current_month, $current_year, 3)*config('app.video_call');
        $net_total = $total_live_chat + $total_voice_call + $total_video_call;
       
       // $payments = Session::with('user')->where('trainer_id', $id)->where('month',$current_month)->where('year',$current_year)->whereStatus(4)->where('is_therapist_paid',1)->get();
       // $paid = count($payments)>0 ? true : false;
        return view('admin.payments.payable-view', compact('therapist','therapist_sessions', 'total_sessions', 'current_month', 'current_year', 'current_month_sessions', 'total_live_chat','total_voice_call','total_video_call','net_total'));
    }

    public function refund($id){
        $Session = Session::find($id);
        $Session->refund_paid = "1";
        $Session->save();
        return back()->with('message', 'Refund marked paid');
    }

    function session_type_count($trainer_id, $month, $year, $type)
    {
        if($type===3){ //video call
            $count = \DB::table('sessions')->where('trainer_id', $trainer_id)->whereStatus(4)->where('month', $month)->where('year', $year)->where('session_type',3)->where('is_therapist_paid',0)->count();
        }
        else if($type===2){ //voice call
            $count = \DB::table('sessions')->where('trainer_id', $trainer_id)->whereStatus(4)->where('month', $month)->where('year', $year)->where('session_type',2)->where('is_therapist_paid',0)->count();
        }
        else{ //live chat
            $count = \DB::table('sessions')->where('trainer_id', $trainer_id)->whereStatus(4)->where('month', $month)->where('year', $year)->where('session_type',1)->where('is_therapist_paid',0)->count();
        }
        
        return $count;
    }

   



}
