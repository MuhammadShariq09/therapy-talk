<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\TherapistAddress;
use Illuminate\Http\Request;
use App\Therapist;
use App\Modality;
use App\AgeGroup;
use App\Language;
use App\Ethnicity;
use App\Speciality;
use App\TreatmentOrientation;
use Session;
use Hash;
use App\Http\Requests\StoreTherapist;
use App\Mail\AdminMail;
use App\Chat\Soachat;
use App\Notifications\User\GeneralNotification;

class TherapistController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /*
    *   Name : getActiveTherapists()
    *   Purpose : Get all active therapists listing
    */
    public function getActiveTherapists() {
        $therapists = Therapist::where('is_active', "1")->where('is_verified', "1")->orderBy('id', 'DESC')->get();
        return view('admin.therapists.active-therapist-listing',['therapists' => $therapists]);
    }


    /*
    *   Name : getBlockedTherapists()
    *   Purpose : Get all blocked therapists listing
    */
    public function getBlockedTherapists() {
        $therapists = Therapist::where('is_active', "0")->orderBy('id', 'DESC')->get();
        return view('admin.therapists.blocked-therapist-listing',['therapists' => $therapists]);
    }

    /*
    *   Name : getVerifiedTherapists()
    *   Purpose : Get all blocked therapists listing
    */
    public function getUnVerifiedTherapists() {
        $therapists = Therapist::where('is_verified', "0")->orderBy('id', 'DESC')->get();
        return view('admin.therapists.verified-therapist-listing',['therapists' => $therapists]);
    }

    /*
    *   Name : addTherapist()
    *   Purpose : add therapist view
    */
    public function addTherapist() {
        $states = \DB::table('states')->get();
        $modalities = Modality::orderBy('name', 'ASC')->get();
        $languages = Language::orderBy('name', 'ASC')->get();
        $ethnicities = Ethnicity::orderBy('name', 'ASC')->get();
        $specialities = Speciality::orderBy('name', 'ASC')->get();
        $age_groups = AgeGroup::orderBy('name', 'ASC')->get();
        $treatment_orientations = TreatmentOrientation::orderBy('name', 'ASC')->get();
        return view('admin.therapists.add-therapist',compact('states','modalities', 'languages', 'ethnicities', 'specialities', 'age_groups', 'treatment_orientations'));
    }



    /*
    *   Name : submitTherapist()
    *   Purpose : submit Therapist data
    */
    public function submitTherapist(Request $request) {
        //Therapist::truncate();
        $validate = $request->validate([
            'full_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:therapists', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'password_confirmation' => 'required|min:8',
            'repeater-group.*.practice_website' => 'required',
            'repeater-group.*.phone' => 'required',
            'repeater-group.*.address' => 'required',
            'repeater-group.*.city' => 'required',
            'repeater-group.*.state' => 'required',
            'repeater-group.*.country' => 'required',
            'repeater-group.*.zipcode' => 'required',
          //  'accept' => 'required|in:1'
      ],
      [
        'repeater-group.*.practice_website.required' => 'Practice Website field is required.',
        'repeater-group.*.phone.required' => 'Phone field is required.',
        'repeater-group.*.address.required' => 'Address field is required.',
        'repeater-group.*.city.required' => 'City field is required.',
        'repeater-group.*.state.required' => 'State field is required.',
        'repeater-group.*.country.required' => 'Country field is required.',
        'repeater-group.*.zipcode.required' => 'Zipcode field is required.',
        'accept.required' => 'Please accept terms and conditions before registration'
      ]
    );
        $random = str_shuffle('abcdefghjklmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ1234567890!$%^&!$%^&');
        $password = substr($random, 0, 8);
        $therapist = new Therapist();
        $therapist->fill($request->except(['password']));
        $therapist->password = Hash::make($password);
        $therapist->is_verified = "1";
        $therapist->is_active = "1";
        $therapist->save();
        
        foreach ($request->{'repeater-group'} as $office) {
            $therapist->offices()->save(new TherapistAddress($office));
        }
        Soachat::addUser($therapist->id, $therapist->full_name, NULL, 1);
        // \Mail::to(\App\Admin::all())->send(new AdminMail("A new therapist account has been registered.", 'Therapist Registered'));
        Session::flash('message','Therapist added successfully!');
        return redirect('/admin/active-therapists');
    }

    public function viewTherapist($id) {
        $modalities = Modality::orderBy('name', 'ASC')->get();
        $languages = Language::orderBy('name', 'ASC')->get();
        $ethnicities = Ethnicity::orderBy('name', 'ASC')->get();
        $specialities = Speciality::orderBy('name', 'ASC')->get();
        $age_groups = AgeGroup::orderBy('name', 'ASC')->get();
        $treatment_orientations = TreatmentOrientation::orderBy('name', 'ASC')->get();
        $therapist = Therapist::find($id);
        $therapist->load('offices.states', 'offices.cities');
        return view('admin.therapists.view-therapist',compact('modalities', 'languages', 'ethnicities', 'specialities', 'age_groups', 'treatment_orientations', 'therapist'));
    }

    /*
    * Name : updateTherapistStatus()
    * Purpose : Update Therapist status from admin
    */
    public function updateTherapistStatus(Request $request)
    {
        $user = Therapist::findOrFail($request->target);
        $user->toggleActivation();
        $user->save();
        //$user->notify(new ProfileUpdated());
        return response()->json(['status' => 200, 'msg' => 'Status updated successfully']);
    }


    /*
    * Name : updateTherapistVerified()
    * Purpose : Update Therapist to verified from admin
    */
    public function updateTherapistVerified(Request $request)
    {
        $user = Therapist::findOrFail($request->target);
        $user->toggleVerification();
        $user->save();
        //$user->notify(new ProfileUpdated());
        return response()->json(['status' => 200, 'msg' => 'Therapist verified successfully']);
    }


    public function deleteTherapist($id){
        (Therapist::find($id))->delete($id);
        return back()->with('message', 'Therapist deleted successfully');
    }
    public function approveTherapist($id){
        $Therapist = Therapist::find($id);
        $Therapist->is_verified = "1";
        $Therapist->is_active = "1";
        $Therapist->is_rejected = 0;
        $Therapist->rejected_reason = "";
        $Therapist->save();
        return back()->with('message', 'Therapist verified successfully');
    }

    public function blockTherapist($id){
        $Therapist = Therapist::find($id);
        $Therapist->is_active = "0";
        $Therapist->save();
        return back()->with('message', 'Therapist blocked successfully');
    }

    public function unblockTherapist($id){
        $Therapist = Therapist::find($id);
        $Therapist->is_active = "1";
        $Therapist->save();
        return back()->with('message', 'Therapist unblocked successfully');
    }

    public function rejectTherapist(Request $request){
        $Therapist = Therapist::find($request->id);
        $Therapist->is_rejected = 1;
        $Therapist->rejected_reason = $request->reason;
        $Therapist->save();
        
        \Mail::to($Therapist->email)->send(new AdminMail("Admin has rejected your account. Please edit your profile or contact admin.", 'Account Rejected'));
        return $this->responseSuccess('Therapist rejected successfully');
    }


}
