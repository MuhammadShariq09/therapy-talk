<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\User;
use Session;
use Hash;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUser;
use App\Http\Requests\UpdateUser;
use App\Mail\AdminMail;

class UserController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }


    /*
    *   Name : getActiveUsers()
    *   Purpose : Get all active users listing
    */
    public function getActiveUsers() {
        $users = User::where('is_active', "1")->orderBy('id', 'DESC')->get();
        return view('admin.users.active-user-listing',['users' => $users]);
    }


    /*
    *   Name : getBlockedUsers()
    *   Purpose : Get all blocked users listing
    */
    public function getBlockedUsers() {
        $users = User::where('is_active', "0")->orderBy('id', 'DESC')->get();
        return view('admin.users.blocked-user-listing',['users' => $users]);
    }

    /*
    *   Name : addUser()
    *   Purpose : add user view
    */
    public function addUser() {
        $states = \DB::table('states')->get();
        return view('admin.users.add-user', compact('states'));
    }


    /*
    *   Name : submitUser()
    *   Purpose : submit user data
    */
    public function submitUser(StoreUser $request) {

        $user = new User();
        $user->fill($request->except(['password']));
        $user->password = Hash::make($request->password);
        $user->is_active = "1";
        $user->save();

       // \Mail::to(\App\Admin::all())->send(new AdminMail("A new user account has been registered.", 'User Registered'));

        Session::flash('message','User added successfully!');
        return redirect('/admin/users');
    }

    public function viewUser($id) {
        $user = User::find($id);
        return view('admin.users.view-user',['user' => $user]);
    }

    public function editUser($id) {
        $user = User::find($id);
        return view('admin.users.edit-user',['user' => $user]);
    }



    public function updateUser(UpdateUser $request) {
        if($request->filled('password') && strlen($request->password) <9) {
            Session::flash('error','Password cannot be less than 8 characters.');
            return redirect('/admin/users');
        }
        $user = User::find($request->user_id);
        $user->fill($request->except(['password']));
        if($request->password) {
            $user->password = Hash::make($request->password);
        }
        $user->save();
        Session::flash('message','User Profile updated successfully');
        return redirect('/admin/users');
    }

    /*
    * Name : updateUserStatus()
    * Purpose : Update user status from admin
    */
    public function updateUserStatus(Request $request)
    {
        $user = User::findOrFail($request->target);
        $user->toggleActivation();
        $user->save();
        //$user->notify(new ProfileUpdated());
        return response()->json(['status' => 200, 'msg' => 'Status updated successfully']);
    }


    public function deleteUser($id){
        (User::find($id))->delete($id);
        return back()->with('message', 'User deleted successfully');
    }



}
