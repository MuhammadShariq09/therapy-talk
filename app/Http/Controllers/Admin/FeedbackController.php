<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Feedback;
use App\Review;

class FeedbackController extends Controller
{
    public function index(Request $request){
        $feedbacks = Review::with('user','therapist')->orderBy('id','desc');
        if($request->filled('from') && $request->filled('to')) {
          
            $feedbacks->where(function ($query) use ($request) {
                $query->whereBetween('created_at', [date('Y-m-d', strtotime($request->from)), date('Y-m-d', strtotime($request->to))]);
            });
        }
        $feedbacks = $feedbacks->get();
        return view('admin.feedback.index', compact('feedbacks'));
    }

    public function view($id){
        $feedback = Review::with('user','therapist')->where('id', $id)->orderBy('id','desc')->first();
        
        return view('admin.feedback.view', compact('feedback'));
    }

    public function reports(Request $request){
        $feedbacks = Feedback::with('user', 'therapist')->orderBy('id','desc');
        if($request->filled('from') && $request->filled('to')) {
          
            $feedbacks->where(function ($query) use ($request) {
                $query->whereBetween('created_at', [date('Y-m-d', strtotime($request->from)), date('Y-m-d', strtotime($request->to))]);
            });
        }
        $feedbacks = $feedbacks->get();
        return view('admin.reports.index', compact('feedbacks'));
    }

    public function reportView($id){
        $feedback = Feedback::with('user', 'therapist')->where('id', $id)->orderBy('id','desc')->first();
        
        return view('admin.reports.view', compact('feedback'));
    }
}
