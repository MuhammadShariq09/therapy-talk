<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use Auth;
use App\{
    Admin,
    User,
    Therapist,
    Session as SessionLog
};
use Session, DB, Hash;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateAdminProfile;
use App\Http\Requests\UpdateAdminPassword;


class AdminController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        $users = $this->custom_number_format(User::where('is_active', '=', "1")->count());
        $therapists = $this->custom_number_format(Therapist::where('is_active', '=', "1")->count());
        $sessions = $this->custom_number_format(SessionLog::count());

        $totalAppointments = $this->totalAppointments();
        $totalTherapists = $this->totalTherapists();
        $totalUsers = $this->totalUsers();
        
        return view('admin.dashboard', compact('users', 'therapists', 'sessions'))->with(['totalAppointments'=>$totalAppointments, 'totalTherapists'=>$totalTherapists, 'totalUsers'=>$totalUsers]);
    }

    public function totalAppointments(){
        $records = SessionLog::
            select(DB::raw("Count(id) as count, MONTH(created_at) month"))
           // ->where('status', [1,3,4])
            ->groupBy(DB::raw("year(created_at), MONTH(created_at)"))
            ->get();

        $month = SessionLog::select(DB::raw("Count(id) as count"))
                ->orderBy("created_at")
                ->groupBy(DB::raw("month(created_at)"))
                ->get()->toArray();
        $month = array_column($month, 'count');

        $months = collect(['', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']);

        $chartData = collect([]);

        for ($i = 1; $i <= 12; $i++){
            $std = new \stdClass();
            $std->key = $months[$i];
            $std->value = $records->where('month', $i)->sum('count');
            $chartData->push($std);
        }

        return $chartData;


    }

    public function totalTherapists(){
        $records = Therapist::
            select(DB::raw("Count(id) as count, MONTH(created_at) month"))
            ->groupBy(DB::raw("year(created_at), MONTH(created_at)"))
            ->get();

        $month = Therapist::select(DB::raw("Count(id) as count"))
                ->orderBy("created_at")
                ->groupBy(DB::raw("month(created_at)"))
                ->get()->toArray();
        $month = array_column($month, 'count');

        $months = collect(['', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']);

        $chartData = collect([]);

        for ($i = 1; $i <= 12; $i++){
            $std = new \stdClass();
            $std->key = $months[$i];
            $std->value = $records->where('month', $i)->sum('count');
            $chartData->push($std);
        }

        return $chartData;


    }

    public function totalUsers(){
        $records = User::
            select(DB::raw("Count(id) as count, MONTH(created_at) month"))
            ->groupBy(DB::raw("year(created_at), MONTH(created_at)"))
            ->get();

        $month = User::select(DB::raw("Count(id) as count"))
                ->orderBy("created_at")
                ->groupBy(DB::raw("month(created_at)"))
                ->get()->toArray();
        $month = array_column($month, 'count');

        $months = collect(['', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']);

        $chartData = collect([]);

        for ($i = 1; $i <= 12; $i++){
            $std = new \stdClass();
            $std->key = $months[$i];
            $std->value = $records->where('month', $i)->sum('count');
            $chartData->push($std);
        }

        return $chartData;


    }



    public function editProfile(Request $request) {
        $id = Auth::user()->id;
        $user = Admin::find($id);
        return view('admin.edit-profile',['user' => $user]);
    }


    /*
    *   Name : updateProfile()
    *   Purpose : Update Admin profile detail
    */
    public function updateProfile(UpdateAdminProfile $request) {
        $admin = Auth::user();
        $admin->fill($request->all());
        $admin->save();
        Session::flash('message','Your profile is updated');
        return redirect('/admin/edit-profile');
    }


    /*
    NAme = updatePassword()
    Purpose = Update Password
    */
    public function updatePassword(UpdateAdminPassword $request){
        Auth::user()->update(['password'=> Hash::make($request->new_password)]);
        return response()->json(['success'=>'Password updated successfully']);
    }

    public function custom_number_format($n) {
        if ($n >= 1000) {
            $n_format = floor($n).'K';
        }else if ($n >= 1000000) {
            $n_format = floor($n / 1000000) . 'M';
        } else if ($n >=1000000000 ) {
            $n_format = floor($n / 1000000000) . 'B';
        } else {
            $n_format = floor($n);
        }
        return $n_format;
    }



}
