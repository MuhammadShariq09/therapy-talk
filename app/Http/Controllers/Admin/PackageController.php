<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Package;
use Session;


class PackageController extends Controller
{
    public function index(Request $request)
    {
        $packages = Package::whereType(1)->orderBy('id','desc');
        if($request->filled('from') && $request->filled('to')) {
          
            $packages->where(function ($query) use ($request) {
                $query->whereBetween('created_at', [date('Y-m-d', strtotime($request->from)), date('Y-m-d', strtotime($request->to))]);
            });
        }
        $packages = $packages->get();
        
        return view('admin.packages.index', compact('packages'));
    }

    public function view($id){
        $package = Package::find($id);
        
        return view('admin.packages.view', compact('package'));
    }

    public function edit($id){
        $package = Package::find($id);
       
        return view('admin.packages.edit', compact('package'));
    }

    public function update(Request $request) {
        //dd($request->all());
        $package = Package::find($request->id);
        $package->fill($request->all());
        $package->status = isset($request->status) ? $request->status : 0;
        $package->save();
        Session::flash('message','Package has been updated successfully');
        return redirect('/admin/package-management');
    }


}
