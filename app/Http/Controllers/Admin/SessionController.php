<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Session;
use Carbon\Carbon;

class SessionController extends Controller
{
    public function index(Request $request){
        $sessions = Session::with('payment', 'therapist')->orderBy('id','desc');
        if($request->filled('from') && $request->filled('to')) {
          
            $sessions->where(function ($query) use ($request) {
                $query->whereBetween('requested_date', [date('Y-m-d', strtotime($request->from)), date('Y-m-d', strtotime($request->to))]);
            });
        }

        if($request->filled('status')) {
            $sessions->where(function ($query) use ($request) {
                $query->where('status', $request->status);
            });
        }

        $sessions = $sessions->get();
       
        return view('admin.sessions.index', compact('sessions'));
    }

    public function view($id){
        $session = Session::with('payment', 'therapist', 'user')->where('id', $id)->orderBy('id','desc')->first();
        $diff_in_days = 0;
        $rating = \DB::select("SELECT IFNULL(AVG(r.rating), 0) AS rating FROM reviews as r WHERE r.therapist_id=".$session->therapist->id."");
        $therapist_rating = $rating[0]->rating;
        if($session->payment){
            $created = new Carbon($session->payment->created_at);
            $now = Carbon::now();
            $diff_in_days = $created->diffForHumans($now);
        }
        return view('admin.sessions.view', compact('session','diff_in_days', 'therapist_rating'));
    }
}
