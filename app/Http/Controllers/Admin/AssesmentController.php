<?php
namespace App\Http\Controllers\Admin;
use App\Assesment;
use App\Mail\AssesmentResponse;
use Illuminate\Http\Request;
use Auth;
use App\Admin;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Session;
use Hash;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateAdminProfile;
use App\Http\Requests\UpdateAdminPassword;


class AssesmentController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        $assements = Assesment::orderBy('id','desc')->get();
        return view('admin.assesments.index', compact('assements'));
    }

    public function downloadAssesment(Request $request, $id){
        $assesment = Assesment::find($id);
        $data = [];
        $json = Storage::disk('local')->get('assesment.json');
        $data = json_decode($json, true);
        $data = collect($data);
        if($assesment){
            $answers = explode('|',$assesment->data);
            $email = $assesment->email;
            $pdf = \PDF::loadView('admin.assesments.form', compact('data', 'answers', 'email'));
            return $pdf->download('assesment_form.pdf');
        }
        abort(404);

    }

    public function rejectAssesment(Request $request){
        $resource = Assesment::find($request->id);
        if($resource){
            $resource->reason = $request->reason;
            $resource->status = 2;
            $resource->save();

            $payload = [
                'to' => $resource->email,
                'reason' => $request->reason,
                'type' => 'rejected',
            ];
            Mail::to($resource->email)->send(new AssesmentResponse($payload));
            return $this->responseSuccess('The Assessment has been rejected successfully');
        }
    }

    public function approveAssesment(Request $request, $id){
        $resource = Assesment::find($id);
        if($resource){
            $resource->status = 1;
            $resource->save();

            $payload = [
                'to' => $resource->email,
                'reason' => '',
                'action' => url('complete-your-registration/'.encrypt($request->id).'?email='.$resource->email),
                'type' => 'accepted',
            ];

            //dd($payload);
            Mail::to($resource->email)->send(new AssesmentResponse($payload));
            return $this->responseSuccess('The Assessment has been approved successfully');
        }
    }





}
