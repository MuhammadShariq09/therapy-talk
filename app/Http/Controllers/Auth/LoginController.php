<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Therapist;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';
    protected $redirectToTherapist = '/therapist';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->middleware('guest:admin')->except('logout');
        $this->middleware('guest:therapist')->except('logout');
    }

    public function showLoginForm(){
        $states = \DB::table('states')->get();
        return view('auth.login', ['url' => '', 'states'=> $states]);
    }

    public function showTherapistLoginForm()
    {
        return view('therapist.auth.login', ['url' => 'therapist']);
    }

    public function showAdminLoginForm()
    {
        return view('auth.adminlogin', ['url' => 'admin']);
    }

    protected function login(Request $request)
    {
        $this->validateLogin($request);
       
        $in_active = User::where('email', $request->email)->where('is_active','=','0')->first();
     
        if($in_active){
            return redirect()->back()->with(['error' => 'Account is blocked or may be in active. Please contact Admin to activate.']);
        }else{
            if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

                return $this->sendLockoutResponse($request);
            }

            if ($this->attemptLogin($request)) {
                return $this->sendLoginResponse($request);
            }

            // If the login attempt was unsuccessful we will increment the number of attempts
            // to login and redirect the user back to the login form. Of course, when this
            // user surpasses their maximum number of attempts they will get locked out.
            $this->incrementLoginAttempts($request);

            return $this->sendFailedLoginResponse($request);
        }
       
    }


    public function adminLogin(Request $request)
    {
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {

            return redirect()->intended('/admin');
        }
        return back()->withInput($request->only('email', 'remember'));
    }

    public function TherapistLogin(Request $request)
    {
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);
            //dd($request->all());
        $therapist = Therapist::where('email', $request->email)->first();
        // dd($therapist);
        if($therapist && $therapist->is_active == 0){
            return redirect()->back()->with(['error' => 'Account is blocked']);
        }

        if (Auth::guard('therapist')->attempt([ 'email' => $request->email, 'password' => $request->password ], $request->get('remember'))) {
            return redirect($this->redirectToTherapist);
        }

        return $this->sendFailedLoginResponse($request);
    }
}
