<?php

namespace App\Http\Controllers\Auth;

use App\AgeGroup;
use App\Assesment;
use App\Ethnicity;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreTherapist;
use App\Language;
use App\Modality;
use App\Speciality;
use App\Therapist;
use App\TherapistAddress;
use App\TreatmentOrientation;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Contracts\Encryption\EncryptException;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use App\Chat\Soachat;
use App\Mail\User\AssesmentForm;
use App\Mail\AdminMail;
use App\Mail\RegisterMail;
use Session;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/profile';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function showRegistrationForm(){
        return redirect('login');
    }

    public function showRegisterForm(){
        
        return view('auth.user_register');
    }

    public function completeRegistration(Request $request, $id){
        $states = \DB::table('states')->get();
        try{
            $assessment_token =$id;
            $assessment_id = decrypt($id);
            $res = Assesment::find($assessment_id);
            $email = $res->email;

            return view('auth.register', compact('assessment_token', 'email','states'));
        }catch (\Exception $e){
            return $e->getMessage();
            return redirect('/login');
        }

    }

    public function becomeAMember(){
        $data = [];
        $json = Storage::disk('local')->get('assesment.json');
        $data = json_decode($json, true);
        $data = collect($data);
       
        return view('auth.become_a_member',compact('data'));
    }

    public function showAssesmentForm(){
        $data = [];
        $json = Storage::disk('local')->get('assesment.json');
        $data = json_decode($json, true);
        $data = collect($data);
       
        return view('auth.assesment',compact('data'));
    }

    public function submitAssesmentForm(Request $request){
        $request->validate([
            'email' => ['required', 'string', 'email', 'max:255', 'unique:assesment_forms', 'unique:users', 'unique:therapists'],
            'item.*' => 'required'
        ],[
            'item.*' => 'Please fill up all the questions in assestment form !',
           // 'email.unique'=> "You have already submitted"
        ]);
        Assesment::create([
            'email' => $request->email,
            'data' => implode('|', $request->item)
        ]);

        \Mail::to($request->email)->send(new AssesmentForm());
        $message = "A new User has filled out an assessment form.";
        \Mail::to(\App\Admin::all())->send(new AdminMail($message, 'Assesment Form'));
        return $this->responseSuccess('You assesment has been submitted successfully');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'full_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:admins', 'unique:therapists', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'password_confirmation' => 'required|min:8',
            'phone' => ['required', 'string', 'min:10'],
            'address' => 'required',
            'state' => 'required',
            'city' => 'required',
            'file' => 'required|file|max:5000',
            //'date_of_birth' => 'required',
          //  'date_of_birth' => 'required|date_format:Y-m-d|before:today|nullable',
            'accept' => 'required|in:1'
        ],[
            'phone.min' => "The phone must be atleast 10 digits",
            //'date_of_birth.date_format' => "Future date cannot be accepted, Add appropriate DOB",
            'accept.required' => 'Please accept terms and conditions before registration',
            'file.required' => 'Profile image is required. ',
            'file.file' => 'Profile image must be a valid image file. ',
            'file.size' => 'Profile image size should be 5MB maximum'
        ]);
    }

    public function registerTherapist(Request $request) {
     
      $validate = $request->validate([
            'full_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:therapists', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'password_confirmation' => 'required|min:8',
            'repeater-group.*.practice_website' => 'required',
            'repeater-group.*.phone' => 'required',
            'repeater-group.*.address' => 'required',
            'repeater-group.*.city' => 'required',
            'repeater-group.*.state' => 'required',
            'repeater-group.*.country' => 'required',
            'repeater-group.*.zipcode' => 'required',
          //  'accept' => 'required|in:1'
      ],
      [
        'repeater-group.*.practice_website.required' => 'Practice Website field is required.',
        'repeater-group.*.phone.required' => 'Phone field is required.',
        'repeater-group.*.address.required' => 'Address field is required.',
        'repeater-group.*.city.required' => 'City field is required.',
        'repeater-group.*.state.required' => 'State field is required.',
        'repeater-group.*.country.required' => 'Country field is required.',
        'repeater-group.*.zipcode.required' => 'Zipcode field is required.',
        'accept.required' => 'Please accept terms and conditions before registration'
      ]
    );
     
        $therapist = new Therapist();
        $therapist->fill($request->except(['password']));
        $therapist->password = Hash::make($request->password);
        $therapist->is_verified = "0";
        $therapist->is_active = "0";
        $therapist->save();

        foreach ($request->{'repeater-group'} as $office) {
            $therapist->offices()->save(new TherapistAddress($office));
        }

        Soachat::addUser($therapist->id, $therapist->full_name, NULL, 1);

        $message = "Therapist ".$request->name." ".$request->last_name." has filled out the registration form.";
        \Mail::to($therapist->email)->send(new AdminMail($message, 'Thank you for your registration at My Therapy Talk. '));
        \Mail::to(\App\Admin::all())->send(new AdminMail($message, 'Therapist Registered'));

        auth()->guard('therapist')->login($therapist);

        return redirect('/register/therapist?success=true');
    }

    public function register(Request $request)
    {
       // $this->performValidate($request);

        event(new Registered($user = $this->create($request->all())));

       // $this->guard()->login($user);

        //return $this->registered($request, $user) ?: redirect($this->redirectPath());
        return "";
    }

    public function performValidate(Request $request)
    {
        $this->validator($request->all())->validate();

        try {
            $assessment_id = decrypt($request->assessment_token);
        } catch (\Exception $e) {
            request()->session()->flash('error', 'You can not perform registration');
            return redirect()->back();
        }

        $res = Assesment::find($assessment_id);

        if (!$res || $res->status != 1) {
            request()->session()->flash('error', 'You can not perform registration');
            return redirect()->intended('login');
        }
    }

    protected function create(array $request)
    {
        //dd($request);
        //$assessment_id = decrypt($request['assessment_token'] ?? '');
        //if($request['assessment_token']){ $res = Assesment::find($assessment_id); }
        $this->validator($request)->validate();

        $user = new User;
        $user->full_name = $request['full_name'];
        $user->email = $res->email ?? $request['email'];
        $user->password = Hash::make($request['password']);
        $user->phone = $request['phone'] ?? '';
        $user->address = $request['address'] ?? '';
        $user->date_of_birth = $request['date_of_birth'] ?? '';
        $user->country = $request['country'];
        $user->state = $request['state'] ?? '';
        $user->city = $request['city'] ?? '';
        $user->zipcode = $request['zipcode'] ?? '';
        $user->auth_code = $this->generateCode(4);
        $user->is_active=1;
        $user->save();
      
     
        if(request()->hasFile('file')) {
            $allowedfileExtension = [ 'jpg', 'png'];
            $file = request()->file('file');
            $imageName = time().'.'.$file->getClientOriginalExtension();
           // $filename = $file->store('media/user/'.md5($user->id));
            $filename = $file->move(public_path('media/user'), $imageName);
            $user->image = 'media/user/'.$imageName;
            $user->save();
        }

        Session::put('email', $user->email);

        Soachat::addUser($user->id, $user->name, NULL, 1);

        $message = "User ".$request['full_name']." has filled out the registration form.";
        
        \Mail::to(\App\Admin::all())->send(new AdminMail($message, 'User Registered'));
        \Mail::to($user->email)->send(new RegisterMail('Account Register Verification', $user->auth_code));


       
        return $user;
    }

    public function verifyAccount(Request $request){
        $request->validate([
            "code.*"  => "required",
        ],[
            'code.*.required' => 'Code field is required.'
        ]);
        $code = implode('', $request->code);
        $email = Session::get('email');
        $user = User::where('email', $email)->where('auth_code', $code)->first();
        if($user){
            $user->is_active = 1;
            $user->save();
            return response()->json(['message'=>"Account Verified successfully. Please login with your email."]);
        }else{
            return response()->json(['error'=>"Invalid Code."]);
        }
    }

    public function resendCode(Request $request){
        $email = Session::get('email');
        $code = $this->generateCode(4);
        $user = User::where('email', $email)->update(['auth_code'=> $code]);
       
        \Mail::to($email)->send(new RegisterMail('Account Register Verification', $code));
        return response()->json(['message'=>"Code sent. Please check your email."]);
    }


    protected function createTherapist(Request $request)
    {
        $this->validator($request->all())->validate();
        $therapist = Therapist::create([
            'full_name' => $request['full_name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'phone' => $request['phone'],
            'address' => $request['address'],
            'latitude' => $request['latitude'],
            'longitude' => $request['longitude'],
            'country' => $request['country'],
            'state' => $request['state'],
            'city' => $request['city'],
            'postal_code' => $request['postal_code'],
        ]);
        return redirect()->intended('login/therapist');
    }

    public function showTherapistRegisterForm(Request $request){
       
        if($request->success){
            return view('therapist.auth.register-success');
        }
        $speciality = Speciality::orderBy('name', 'ASC')->get();
        $modality = Modality::orderBy('name', 'ASC')->get();
        $ethnicity = Ethnicity::orderBy('name', 'ASC')->get();
        $agegroup = AgeGroup::orderBy('name', 'ASC')->get();
        $treatment = TreatmentOrientation::orderBy('name', 'ASC')->get();
        $language = Language::orderBy('name', 'ASC')->get();
        $states = \DB::table('states')->get();
        $url = 'therapist';

        return view('therapist.auth.register', compact('speciality','modality', 'ethnicity', 'agegroup', 'treatment', 'language', 'url', 'states'));
    }

    public function getPartials(Request $request){
        $data = [];
        if($request->type == 'specialities'){
            $data = Speciality::orderBy('name', 'ASC')->get();
        }
        if($request->type == 'modality'){
            $data = Modality::orderBy('name', 'ASC')->get();
        }
        if($request->type == 'ethnicity'){
            $data = Ethnicity::orderBy('name', 'ASC')->get();
        }
        if($request->type == 'agegroup'){
            $data = AgeGroup::orderBy('name', 'ASC')->get();
        }
        if($request->type == 'treatment'){
            $data = TreatmentOrientation::orderBy('name', 'ASC')->get();
        }
        if($request->type == 'language'){
            $data = Language::orderBy('name', 'ASC')->get();
        }

        return view('therapist.auth.partials', ['selected'=>($request->values) ?? [], 'data' => $data, 'type' => $request->type]);

    }

    function generateCode($length = 0) 
    {
        $numbers = '0123456789';
        $charactersLength = strlen($numbers);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $numbers[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    


}
