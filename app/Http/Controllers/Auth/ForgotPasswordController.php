<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Token;
use App\User;
use App\Therapist;
use App\Admin;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Password;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

  //  use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showLinkRequestForm()
    {
        return view('auth.passwords.email');
    }

    public function broker()
    {
        return Password::broker('admins');
    }

    public function sendResetLinkEmail(Request $request)
    {
       // $this->validateEmail($request);
        
        $userStatus=Admin::where('email','=',$request->email)->first();
       
        $response = $this->broker()->sendResetLink(
            $request->only('email')
        );
     
        return $response == Password::RESET_LINK_SENT
                    ? $this->sendResetLinkResponse($response)
                    : $this->sendResetLinkFailedResponse($request, $response);
    }

    public function sendResetLinkResponse(Request $request, $response)
    {
        return back()->with('status', trans($response));
    }

    /**
     * Get the response for a failed password reset link.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function sendResetLinkFailedResponse(Request $request, $response)
    {
        return back()
                ->withInput($request->only('email'))
                ->withErrors(['email' => trans($response)]);
    }



    public function sendVerificationCode(Request $request){
      //  $user = User::whereEmail($request->email)->first();
      $user = \DB::table('users')->whereEmail($request->email)->first();
       
        if($user){
            $token = Token::create([
                'user_id' => $user->id
            ]);
           
           
            if ($token->sendCode($user)) {
                $request->session()->put("token_id", $token->id);
                $request->session()->put("user_id", $user->id);
                $request->session()->put("remember", $request->get('remember'));

                return response()->json(['message' => 'Please check your email for password reset code']);
            }

            $token->delete();// delete token because it can't be sent
            return response()->json(['error' => 'Unable to send verification code'], 422);
        }

        return response()->json(['error' => 'Email not found'], 422);
    }

    public function verifyVerificationCode(Request $request){
        // dd($request->all());
        if (! $request->session()->has("token_id", "user_id")) {
            return response()->json(['error' => 'Invalid Operation'], 422);
        }
        $token = Token::find($request->session()->get("token_id"));
        if (! $token ||
            ! $token->isValid() ||
            $request->code !== $token->code 
           // || (int)session()->get("user_id") !== $token->user->id
        ) {
            return response()->json(['error' => 'Invalid token'], 422);
        }else{
            return response()->json(['message' => 'Verification code accepted']);
        }
    }

    public function updatePassword(Request $request){
        $request->validate([
            'password' => 'required',
            'confirm_password' => 'required'
            ]);
        if($request->password == $request->confirm_password && $request->password !== ''){
            if (! $request->session()->has("token_id", "user_id")) {
                return response()->json(['error' => 'Invalid Operation'], 422);
            }
            $token = Token::find($request->session()->get("token_id"));
            if (! $token ||
                ! $token->isValid() ||
                $request->code !== $token->code 
               // || (int)session()->get("user_id") !== $token->user->id
            ) {
                return response()->json(['error' => 'Invalid token'], 422);
            }else{
                $user = \DB::table('users')->where('id', $token->user_id)->update(['password'=>Hash::make($request->password)]);
            //   dd($user);
            //     $user->password = Hash::make($request->password);
            //     $user->save();

                $token->used = true;
                $token->save();

                return response()->json(['message' => 'Password updated successfully']);

            }
        }
        return response()->json(['error' => 'Password should be same and can not be empty'], 422);


    }


    public function sendVerificationCodeTherapist(Request $request){
       // $therapist = Therapist::whereEmail($request->email)->first();
        $therapist = \DB::table('therapists')->where('email', $request->email)->first();    
       
        if($therapist){
            $token = Token::create([
                'therapist_id' => $therapist->id
            ]);
            
            if ($token->sendCodeTherapist($therapist)) {
               
                $request->session()->put("token_id", $token->id);
                $request->session()->put("therapist_id", $therapist->id);
                $request->session()->put("remember", $request->get('remember'));

                return response()->json(['message' => 'Please check your email for password reset code']);
            }

            $token->delete();// delete token because it can't be sent
            return response()->json(['error' => 'Unable to send verification code'], 422);
        }

        return response()->json(['error' => 'Email not found'], 422);
    }

    public function verifyVerificationCodeTherapist(Request $request){
        $request->validate([
            'code' => 'required'
            ]);
                  
        if (! $request->session()->has("token_id", "therapist_id")) {
            return response()->json(['error' => 'Invalid Operation'], 422);
        }
        $token = Token::find($request->session()->get("token_id"));
        if (! $token ||
            ! $token->isValid() ||
            $request->code !== $token->code 
           // || (int)session()->get("therapist_id") !== $token->therapist->id
        ) {
            return response()->json(['error' => 'Invalid token'], 422);
        }else{
            return response()->json(['message' => 'Verification code accepted']);
        }
    }

    public function updatePasswordTherapist(Request $request){
        $request->validate([
            'password' => 'required',
            'confirm_password' => 'required'
            ]);
        if($request->password == $request->confirm_password && $request->password !== ''){
            if (! $request->session()->has("token_id", "therapist_id")) {
                return response()->json(['error' => 'Invalid Operation'], 422);
            }
            $token = Token::find($request->session()->get("token_id"));
            if (! $token ||
                ! $token->isValid() ||
                $request->code !== $token->code 
               // || (int)session()->get("therapist_id") !== $token->therapist->id
            ) {
                return response()->json(['error' => 'Invalid token'], 422);
            }else{
                 $therapist = \DB::table('therapists')->where('id', $token->therapist_id)->update(['password'=>Hash::make($request->password)]);
                // $therapist = Therapist::find($request->session()->get("therapist_id"));
                // $therapist->password = Hash::make($request->password);
                // $therapist->save();

                $token->used = true;
                $token->save();

                return response()->json(['message' => 'Password updated successfully']);

            }
        }
        return response()->json(['error' => 'Password should be same and can not be empty'], 422);


    }

    public function updatePasswordAdmin(Request $request){
        
         $validate = $request->validate([
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'password_confirmation' => 'required|min:8'
            ]);
           // dd($request->all());
            $admin = Admin::where('email', $request->email)->first();
       
            $admin->password = Hash::make($request->password);
            $admin->save();

            return redirect('/login/admin');
       
 
     }


}































