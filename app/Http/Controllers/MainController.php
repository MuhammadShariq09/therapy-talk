<?php

namespace App\Http\Controllers;

use App\AgeGroup;
use App\Filters\TherapistFilter;
use App\Ethnicity;
use App\Language;
use App\Modality;
use App\Payment;
use App\Session as SessionLog;
use App\SessionReshedule;
use App\Speciality;
use App\Feedback;
use App\Review;
use App\Therapist;
use App\PackageUsage;
use App\Subscription;
use App\TreatmentOrientation;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Cartalyst\Stripe\Stripe;
// use App\Mail\User\SessionsReshedule;
// use App\Mail\Therapist\ResheduleRequests;
use App\Mail\RescheduleApprovedAppointments;
use App\Mail\AdminMail;
use App\Notifications\AdminNotification;
use App\Chat\Soachat;


use App\Notifications\User\SessionEvents;
use App\Notifications\Therapist\SessionRequests;

use App\Notifications\User\SessionsReshedule;
use App\Notifications\Therapist\ResheduleRequests;

use Srmklive\PayPal\Services\ExpressCheckout;

class MainController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(TherapistFilter $filters)
    {

        $therapists = Therapist::where('is_active', "1")->where('is_verified', "1")->orderBy('id', 'DESC')->filter($filters)->paginate(10);

        $specialities = Speciality::orderBy('name', 'ASC')->get();
        $modality = Modality::orderBy('name', 'ASC')->get();
        $ethnicity = Ethnicity::orderBy('name', 'ASC')->get();
        $agegroup = AgeGroup::orderBy('name', 'ASC')->get();
        $treatment = TreatmentOrientation::orderBy('name', 'ASC')->get();
        $language = Language::orderBy('name', 'ASC')->get();

        return view('user.find-a-therapist',
            compact( 'therapists', 'specialities','modality','ethnicity','agegroup','treatment', 'language'));
    }

    public function therapistProfile(Request $request, $id){
        try {
            $therapist_id = decrypt($id);
            $therapist = Therapist::find($therapist_id);
            $specialities = Speciality::orderBy('name', 'ASC')->get();
            $modality = Modality::orderBy('name', 'ASC')->get();
            $ethnicity = Ethnicity::orderBy('name', 'ASC')->get();
            $agegroup = AgeGroup::orderBy('name', 'ASC')->get();
            $treatment = TreatmentOrientation::orderBy('name', 'ASC')->get();
            $language = Language::orderBy('name', 'ASC')->get();
            $rating = \DB::select("SELECT IFNULL(AVG(r.rating), 0) AS rating FROM reviews as r WHERE r.therapist_id=".$therapist_id."");
            $therapist_rating = $rating[0]->rating;
            
            return view('user.therapist_profile',
                compact('id', 'therapist', 'specialities','modality','ethnicity','agegroup','treatment', 'language', 'therapist_rating'));

        }
        catch(\Exception $e){
            return redirect()->route('user.find-a-therapist');
        }
    }

    public function rescheduleApprovedAppointment(Request $request, $id){
        $user = auth()->user();
        $session = SessionLog::with('therapist')->where('user_id', $user->id)->where('id', $id)->first();
        if($session->payment){
            $from = Carbon::parse($session->payment->created_at);
            $to = Carbon::now();
            $diff_in_hours = $to->diffInHours($from);

            if($diff_in_hours < 48){

                $session->reschedule()->save(new SessionReshedule([
                    'session_id' => $session->id,
                    'suggested_date' =>  Carbon::parse($request->suggested_date)->format('Y-m-d'),
                    'suggested_time' => $request->suggested_time,
                    'action_by' => 1,
                    'status' => 0
                ]));
                
                $r = SessionReshedule::where('session_id', $session->id)->first();
              
                // \Mail::to($user->email)->send(new SessionsReshedule($user, $r));
                // \Mail::to($session->therapist->email)->send(new ResheduleRequests($session->therapist, $r));

                auth()->user()->notify(new SessionsReshedule($user, $r));
                $session->therapist->notify(new ResheduleRequests($session->therapist, $r));

                $message = "Session has been rescheduled between the User:".$user->name." and Therapist: ".$session->therapist->name." on Date: ".$r->suggested_date." at Time: ".$r->suggested_time.".";
                \Mail::to(\App\Admin::all())->send(new AdminMail($message, 'Reschedule Session'));

                return redirect('therapist/session/logs/'.$id)->with(['success' => 'Appointment is reschedules waiting for user to accept or reject']);
            }
        }
        return redirect('session/logs/');
    }

    public function approveRejectedRescAppointment(Request $request, $id){
        $session = SessionReshedule::where('id', $id)->first();
        $session->status = 1;
        $session->save();
        return redirect()->back()->with(['success' => 'Appointment reschedule request is accepted']);
    }

    public function bookSession(Request $request){
        $request->validate([
            'trainer_id' => 'required',
            'requested_time' => 'required',
            'requested_date' => 'required',
            //'duration' => 'required|numeric',
            'reason' => 'required',
            'type' => 'required',
        ]);

        try{
            $therapist = decrypt($request->trainer_id);
        }catch(\Exception $e){
            return $this->responseWithError(['therapist' => 'Therapist do not exist in our record']);
        }

        $session = SessionLog::with('therapist', 'user')->create([
            'trainer_id' => $therapist,
            'user_id' => auth()->user()->id,
            'duration' => config('app.duration'),
            'type' => $request->type,
            'session_type' => $request->session_type,
            'reason' => $request->reason,
           // 'amount' => config('app.fee')[config('app.duration')] ,
            'requested_date' => Carbon::parse($request->requested_date)->format('Y-m-d'),
            'requested_time' => $request->requested_time,
            'month' => date('m'),
            'year' => date('Y'),
            'status' => 0,
        ]);
       
        $userData = auth()->user();

      //  \Mail::to($userData->email)->send(new SessionEvents($userData, $session));
       // \Mail::to($session->therapist->email)->send(new SessionRequests($session));

        auth()->user()->notify(new SessionEvents($session->user,$session));
        $session->therapist->notify(new SessionRequests($session));

        $message = "Session has been booked between the User:".$userData->name." and Therapist: ".$session->therapist->name." on Date: ".$session->requested_date." at Time: ".$session->requested_time.".";
       // \Mail::to(\App\Admin::all())->send(new AdminMail($message, 'Session Booked'));
       \Notification::send(\App\Admin::all(), new AdminNotification($message, 'Session Booked'));

        return $this->responseSuccess('Your appointment has been sent to therapist successfully');
    }


    public function sessionLogs(Request $request){
        $sessionLogs = SessionLog::with('therapist', 'reschedule')->where('user_id', auth()->user()->id)->orderBy('id', 'desc');
        if($request->filled('from') && $request->filled('to')) {
          
            $sessionLogs->where(function ($query) use ($request) {
                $query->whereBetween('requested_date', [date('Y-m-d', strtotime($request->from)), date('Y-m-d', strtotime($request->to))]);
            });
        }
  
        if($request->filled('duration')) {
            $sessionLogs->where(function ($query) use ($request) {
                $query->where('duration', $request->duration);
            });
        }

        if($request->filled('status')) {
            $sessionLogs->where(function ($query) use ($request) {
                $query->where('status', $request->status);
            });
        }

        $sessionLogs = $sessionLogs->get();
        return view('user.sessionLogs', compact('sessionLogs'));
    }

    public function sessionLogDetail(Request $request, $id){
        $session = SessionLog::with('therapist', 'reschedule')->withCount('payment')
            ->where('user_id', auth()->user()->id)
            ->where('id', $id)
            ->first();
        $diff_in_hours = 0;
        if($session->payment){
            $from = Carbon::parse($session->payment->created_at);
            $to = Carbon::now();
            $diff_in_hours = $to->diffInHours($from);
        }

        $session->load('reschedule');

        $subscription = Subscription::where('user_id', auth()->user()->id)->first();
        $pkg_usage = PackageUsage::where('user_id', auth()->user()->id)->first();

        return view('user.sessionLogDetail', compact('session', 'diff_in_hours', 'pkg_usage', 'subscription'));
    }

    public function acceptReschedule(Request $request, $id){
        $user = auth()->user();
        $r = SessionReshedule::find($id);
        $r->status = 1;
        $r->save();

        $session = SessionLog::with('user', 'therapist')->find($r->session_id);
        $t_msg = "User has accepted the appointment reschedule request. Appointment has been booked on ".$r->suggested_date." at ".$r->suggested_time;
        $u_msg = "You have accepted the appointment reschedule request. Your appointment for ".$r->suggested_date." at ".$r->suggested_time." with therapist has been booked.";

        \Mail::to($user->email)->send(new RescheduleApprovedAppointments($user, $u_msg));
        \Mail::to($session->therapist->email)->send(new RescheduleApprovedAppointments($session->therapist, $t_msg));
        
        return redirect()->back()->with(['success' => 'The Appointment has been rescheduled successfully']);

    }

    public function askForRefund(Request $request, $id){
        $user = auth()->user();
        $r = SessionReshedule::find($id);
        $r->status = 2;
        $r->save();

        $session = SessionLog::with('user', 'therapist')->find($r->session_id);
        $session->is_refund = 1;
        $session->save();

        $t_msg = "User has asked for refund following your appointment reschedule request. The appointment has been cancelled.";
        $u_msg = "Your appointment has been cancelled. Your amount for the appointment for ".$r->suggested_date." at ".$r->suggested_time." with ".$session->therapist->first_name." ".$session->therapist->last_name." will be refunded.";

        \Mail::to($user->email)->send(new RescheduleApprovedAppointments($user, $u_msg));
        \Mail::to($session->therapist->email)->send(new RescheduleApprovedAppointments($session->therapist, $t_msg));

        $message = "Session has been cancelled between the User:".$user->name." and Therapist: ".$session->therapist->name." on Date: ".$r->suggested_date." at Time: ".$r->suggested_time.". The user has requested for a refund.";
        \Mail::to(\App\Admin::all())->send(new AdminMail($message, 'Session Cancelled'));

        return redirect()->back()->with(['success' => 'The Appointment has been cancelled, our representative will soon contact your regarding your refund']);

    }

    public function appointmentsLogs(Request $request){
       // $sessionLogs = SessionLog::with('therapist')->where('user_id', auth()->user()->id)->where('is_paid', 1)->whereNotIn('status', [3,4])->get();
        $sessionLogs = SessionLog::with('therapist')->where('user_id', auth()->user()->id)->where('is_paid', 1)->whereNotIn('status', [0])->orderBy('id', 'DESC');

        if($request->filled('from') && $request->filled('to')) {
            $sessionLogs->where(function ($query) use ($request) {
                $query->whereBetween('requested_date', [date('Y-m-d', strtotime($request->from)), date('Y-m-d', strtotime($request->to))]);
            });
        }

        if($request->filled('duration')) {
            $sessionLogs->where(function ($query) use ($request) {
                $query->where('duration', $request->duration);
            });
        }

        if($request->filled('status')) {
            $sessionLogs->where(function ($query) use ($request) {
                $query->where('status', $request->status);
            });
        }

        $sessionLogs = $sessionLogs->get();
        
        return view('user.appointmentsLogs', compact('sessionLogs'));
    }

    public function appointmentsLogDetail(Request $request, $id){
        $session = SessionLog::with('therapist')
                ->where('user_id', auth()->user()->id)
                ->where('id', $id)
                ->first();
        $therapist_id = $session->therapist->id; 
        
        $from = Carbon::parse($session->requested_date);
        $to = Carbon::now();
        $diff_in_days = $to->diffInDays($from);
       
        return view('user.appointmentsLogDetail', compact('session', 'therapist_id', 'diff_in_days'));
    }

    public function chat($id){
        $session = SessionLog::with('therapist')
                ->where('user_id', auth()->user()->id)
                ->where('id', $id)
                ->first();
        if($session->status==3 || $session->status==1){
            $therapist_id = $session->therapist->id; 

            if($session->session_type==1){
                Soachat::updateUser($session->user_id, auth()->user()->full_name, NULL, 0);
            }else{
                Soachat::updateUser($session->user_id, auth()->user()->full_name, NULL, 1);
            }      
            
            return view('user.chat', compact('session', 'therapist_id', 'diff_in_hours'));
        }else{
            return redirect()->back();
        }
        
    }

    public function payForSession(Request $request){
        $request->validate([
            'card' => 'required|min:16|max:16',
            'cvv' => 'required',
            'expiry' => 'required',
            'card_holder' => 'required',
        ]);
        $sessionLog = SessionLog::with('therapist')
            ->where('user_id', auth()->user()->id)
            ->where('id', $request->id)
            ->first();
           
        if($sessionLog){
            if(!$request->stripe_token){
               // return redirect()->back()->withErrors(['stripe' => 'stripe token not found']);
                return response()->json(['errors' => 'Stripe token not found. Please check your card details or may be card expired.']);
            }
            $amount = $sessionLog->session_type==3 ? ($sessionLog->session_type==2 ? config('app.voice_call') : config('app.video_call')) : config('app.live_chat');
            $response = $this->pay($request, $amount, 0, $sessionLog->id);

            $sessionLog->is_paid = 1;
            $sessionLog->save();
            
            $userData = auth()->user();
            //\ \Mail::to($userData->email)->send(new SessionEvents($userData, $sessionLog));
            // \Mail::to($sessionLog->therapist->email)->send(new SessionRequests($sessionLog));

             auth()->user()->notify(new SessionEvents($userData,$sessionLog));
             $sessionLog->therapist->notify(new SessionRequests($sessionLog));
            
             return response()->json(['message' => 'Payment is done successfully']);
           //  return redirect()->route('user.session.logs')->with(['success' => 'Payment is done successfully']);
        }
        return redirect('session.logs');
    }

    public function pay(Request $request, $amount, $type, $id){
        $stripe = Stripe::make(config('services.stripe.SECRET_KEY'));

        $order_id = rand(1, 10000);
        try{
            $charge = $stripe->charges()->create([
                'amount' => $amount,
                'currency' => 'usd',
                'source' => $request->stripe_token,
                'transfer_group' => $order_id,
            ]);
        }
        catch (\Exception $e){
            return $e->getMessage();
           // return redirect()->back()->withErrors(['stripe' => $e->getMessage()]);
        }

        $payment = Payment::create([
            'user_id' => auth()->user()->id,
            'transaction_id' => $id,
            'status' => 0,
            'type' => $type,
            'payload' => $charge
        ]);

        return ['id' => $payment->id, 'payload' => $charge];
    }

    public function report(Request $request){
        $request->validate([
            'message' => 'required',
        ]);

        $feedback = new Feedback();
        $feedback->type = 1;
        $feedback->user_id = auth()->user()->id;
        $feedback->therapist_id = $request->therapist_id;
        $feedback->session_id = $request->session_id;
        $feedback->message = $request->message;
        $feedback->email = auth()->user()->email;
        $feedback->save();

        return response()->json(['success'=>true]);
    }

    public function feedback(Request $request){
        $request->validate([
            'message' => 'required',
        ]);
       

        $review = new Review();
        $review->therapist_id = $request->therapist_id;
        $review->session_id = $request->session_id;
        $review->feedback = $request->message;
        $review->rating = $request->rating;
        $review->save();

         return response()->json(['success'=>true]);
    }

    // paypal methods
    public function payment($id)
    {
        $session = SessionLog::find($id);
        $data = [];
        $data['items'] = [
            [
                'name' => 'Therapy Talk',
                'price' => $session->amount,
                'desc'  => 'Description for Therapy Talk',
                'qty' => 1
            ]
        ];
  
        $data['invoice_id'] = 1;
        $data['invoice_description'] = "Order #{$data['invoice_id']} Invoice";
        $data['return_url'] = route('user.payment.success', ['id'=> $session->id]);
        $data['cancel_url'] = route('user.payment.cancel');
        $data['total'] = $session->amount;
        
        $provider = new ExpressCheckout;
  
        $response = $provider->setExpressCheckout($data);
        
        $response = $provider->setExpressCheckout($data, true);
      
        return redirect($response['paypal_link']);
    }
   
    public function cancel()
    {
        return redirect()->route('user.session.logs')->with(['error' => 'Your payment is canceled. You can create cancel page here.']);
    }
  
    public function success(Request $request, $id)
    {
        $provider = new ExpressCheckout;
        $response = $provider->getExpressCheckoutDetails($request->token);

        $sessionLog = SessionLog::with('therapist')->where('user_id', auth()->user()->id)->where('id', $id)->first();

        if (in_array(strtoupper($response['ACK']), ['SUCCESS', 'SUCCESSWITHWARNING'])) {
            $payment = Payment::create([
                'user_id' => auth()->user()->id,
                'transaction_id' =>$id,
                'status' => 0,
                'type' => 1,
                'payload' =>$response
            ]);
           

            auth()->user()->notify(new SessionEvents(auth()->user(),$sessionLog));
            $sessionLog->therapist->notify(new SessionRequests($sessionLog));

            return redirect()->route('user.session.logs')->with(['success' => 'Payment is done successfully.']);
        }
  
        return redirect()->route('user.session.logs')->with(['error' => 'Something went wrong, Please try again.']);
    }
}





















