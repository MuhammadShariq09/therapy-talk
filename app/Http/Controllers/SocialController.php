<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Therapist;
use App\TherapistAddress;
use Auth;
use Socialite;
use App\Chat\Soachat;

class SocialController extends Controller
{

    public function redirect($provider)
    {
        session(['callback' => 'user']);

        return Socialite::driver($provider)->redirect();
    }
    

    public function Callback($provider)
    {
        if(session('callback', 'web') === 'therapist'){
            return $this->CallbackTherapist($provider);
        }


        $userSocial =   Socialite::driver($provider)->stateless()->user();
        $users       =   User::where(['email' => $userSocial->getEmail()])->first();
        if($users)
        {
            Auth::login($users);
            return redirect()->route('user.profile');
        }else{
        $exist = Therapist::where('email', $userSocial->getEmail())->first();
       
        if(is_null($exist)){
            $user = User::create([
                'full_name'     => $userSocial->getName(),
                'email'         => $userSocial->getEmail(),
                'image'         => $userSocial->getAvatar(),
                'provider_id'   => $userSocial->getId(),
                'provider'      => $provider,
            ]);
            Soachat::addUser($user->id, $user->name, NULL, 1);
            return redirect()->route('user.profile');
        }else{
            return redirect()->route('login')->with(['error' => 'Account with this email id is already registered.']);
        }
       
         
        }
    }

    public function redirectTherapist($provider)
    {
        session(['callback' => 'therapist']);

        return Socialite::driver($provider)->redirect();
    }
    

    public function CallbackTherapist($provider)
    {
        $userSocial =   Socialite::driver($provider)->stateless()->user();
        $users       =   Therapist::where(['email' => $userSocial->getEmail()])->first();
       
        if($users)
        {
            Auth::guard('therapist')->login($users);
            return redirect()->route('therapist.dashboard');
        }else{
            $exist = User::where('email', $userSocial->getEmail())->first();
            if(is_null($exist)){
               
                $user = Therapist::create([
                                'full_name'     => $userSocial->getName(),
                                'email'         => $userSocial->getEmail(),
                                'image'         => $userSocial->getAvatar(),
                                'provider_id'   => $userSocial->getId(),
                                'provider'      => $provider,
                                'is_active'      => 1,
                                'provider'      => $provider,
                            ]);
                Soachat::addUser($user->id, $user->name, NULL, 1);
                TherapistAddress::create(['therapist_id' => $user->id]);
                Auth::guard('therapist')->login($users);
                return redirect()->route('therapist.dashboard');
            }else{
          
                return redirect()->route('therapist.login')->with(['error' => 'Account with this email id is already registered.']);
            }
        }
    }
}
