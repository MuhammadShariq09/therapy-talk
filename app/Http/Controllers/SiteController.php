<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\UserQueryEmail;
use App\Mail\NewsletterMail;
use App\Package;
use Validator;
use Redirect;
use Session;

class SiteController extends Controller
{
    public function pricing(){
        $packages = Package::whereType(1)->orderBy('id','desc')->get();
        return view('frontend.pricing', compact('packages'));
    }

    public function submitQuery(Request $request)
    {
        
        
        if($request->type == 'quote'){
            $vArr = [
                'name' => ['required', 'string', 'max:100'],
                'email' => ['required', 'string', 'email', 'max:255'],
                'number' => ['required', 'string', 'max:255'],
                'message' => 'required'
            ];            
        }else{
            $vArr = [
                'name' => ['required', 'string', 'max:100'],
                'email' => ['required', 'string', 'email', 'max:255'],
                'subject' => ['required', 'string', 'max:255'],
                'message' => 'required'
            ];            
            
        }
        $validator = Validator::make($request->all(), $vArr);
        
        if ($validator->fails()) {
            Session::flash('error',$validator->errors()->first());
            return redirect()->back();
        }
        
        //Mail::to('muhammad.sohaib@salsoft.net')->send(new UserQueryEmail($request->all()));
        Mail::to('info@mytherapytalk.com')->send(new UserQueryEmail($request->all()));

        Session::flash('message','Your Query is submitted to our representative, We will get back to you shortly');
        return ($request->type == 'quote') ? redirect('/talk-to-mtt') : redirect('/contact-us');
    }
    
    public function submitNewsletter(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => ['required', 'string', 'email', 'max:255'],
        ]);
        
        if ($validator->fails()) {
            return response()->json(['error' => 'Please enter a valid email address'], 422);
        }
        
        Mail::to('info@mytherapytalk.com')->send(new NewsletterMail($request->all()));

        return response()->json(['message' => 'Thanks for subscribing to our newsletter']);
    }

    public function getCities($id){
        $cities = \DB::table('cities')->where('state_id', $id)->get();
 
         return response()->json($cities);
     }
}



















