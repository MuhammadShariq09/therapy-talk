<?php

namespace App\Filters;



class TherapistFilter extends Filters {

    protected $filters = ['name', 'zipcode', 'specialities','modality','treatment','agegroup','ethnicity','language'];

    public function name($name)
    {
        if($name)
            $this->builder->where("full_name", 'LIKE', "%{$name}%");
    }

    public function zipcode($zipcode)
    {
        if($zipcode)
            $this->builder->where("zipcode", 'LIKE', "%{$zipcode}%");
    }

    public function specialities($specialities){
        if(count($specialities)){
            $this->builder->whereIn("specialities_id", $specialities);

        }
    }
    public function modality($modality){
        if(count($modality)){
            $this->builder->whereIn("modalities_id", $modality);

        }
    }
    public function treatment($treatment){
        if(count($treatment)){
            $this->builder->whereIn("treatments_id", $treatment);

        }
    }
    public function agegroup($agegroup){
        if(count($agegroup)){
            $this->builder->whereIn("age_groups_id", $agegroup);

        }
    }
    public function ethnicity($ethnicity){
        if(count($ethnicity)){
            $this->builder->whereIn("ethnicities_id", $ethnicity);

        }
    }
    public function language($language){
        if(count($language)){
            $this->builder->whereIn("languages_id", $language);

        }
    }


}
