<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Traits\StoreImageTrait;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes, StoreImageTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'full_name', 'last_name', 'email', 'password', 'gender', 'image', 'phone', 'date_of_birth', 'address', 'latitude', 'longitude', 'city', 'country', 'state', 'zipcode', 'is_active', 'provider_id', 'provider', 'auth_code', 'is_assessment'
    ];

  //  protected $appends = ['name'];

    protected $imageFolder = "/media/user/";

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

  //  protected $dates = ['date_of_birth'];


    public function getNameAttribute(){
        return $this->full_name;
    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function enable()
    {
        $this->is_active = "1";
    }

    // public function setDateOfBirthAttribute($value) {
    //     $this->attributes['date_of_birth'] = $value;
    //     try{
    //        $dob = date_create_from_format('m/d/Y', $value);
    //         $this->attributes['date_of_birth'] = $dob->format('Y-m-d');
    //     }
    //     catch (\Exception $e){

    //     }
    // }


    public function disable()
    {
        $this->is_active = "0";
    }

    public function toggleActivation()
    {
        if($this->is_active)
            $this->disable();
        else
            $this->enable();
    }

    public function states(){
        return $this->hasOne(State::class, 'id', 'state');
    }

    public function cities(){
        return $this->hasOne(City::class, 'id', 'city');
    }

    public function assessment(){
        return $this->hasMany(Assesment::class);
    }

    public function subscriptions(){
        return $this->hasOne(Subscription::class, 'user_id', 'id');
    }

}
