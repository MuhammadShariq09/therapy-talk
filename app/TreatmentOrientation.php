<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TreatmentOrientation extends Model
{
    protected $fillable = [
        'name',
    ];
}
