<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Subscription extends Model
{
    protected $fillable = [
        'user_id', 'therapist_id', 'package_id', 'status', 'type', 'expiry_date', 'amount'
    ];

    protected $table = 'subscriptions';

    public function package(){
        return $this->belongsTo(Package::class, 'package_id' );
    }

    public function user(){
        return $this->hasOne(User::class, 'id', 'user_id' );
    }

    public function therapist(){
        return $this->hasOne(Therapist::class, 'id', 'therapist_id' );
    }

    public function getCreatedAtAttribute(){
        return $this->attributes['created_at'] =  (new Carbon($this->attributes['created_at']))->format('m/d/y');
    }

    public function getExpiryDateAttribute(){
        return $this->attributes['expiry_date'] =  (new Carbon($this->attributes['expiry_date']))->format('m/d/y');
    }




}
