<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class SubscriptionLog extends Model
{
    protected $fillable = [
        'type','user_id', 'therapist_id', 'package_id','title','amount','subscribe_date','expiry_date', 'status'
    ];

    public function getSubscribeDateAttribute(){
        return $this->attributes['subscribe_date'] =  (new Carbon($this->attributes['subscribe_date']))->format('m/d/y');
    }

    public function getExpiryDateAttribute(){
        return $this->attributes['expiry_date'] =  (new Carbon($this->attributes['expiry_date']))->format('m/d/y');
    }
}
