<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SessionRejection extends Model
{
    protected $fillable = [
        'session_id',  'suggested_date', 'suggested_time', 'status', 'reason'
    ];

    protected $table = 'session_rejections';

    protected $casts = [
        'suggested_date' => 'date'
    ];
}
