<?php

namespace App;

use App\Core\Traits\ImageableFill;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class Payment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'therapist_id', 'type', 'transaction_id', 'status', 'payload'
    ];

    protected $casts = [
        'payload' => 'json',
    ];

    protected $table = 'payments';


}














