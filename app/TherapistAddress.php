<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TherapistAddress extends Model
{
    protected $fillable = [
        'therapist_id', 'practice_name', 'practice_website', 'phone', 'address_2', 'address', 'city', 'state', 'zipcode', 'country'
    ];

    protected $table = 'therapist_office';

    public function states(){
        return $this->hasOne(State::class, 'id', 'state');
    }

    public function cities(){
        return $this->hasOne(City::class, 'id', 'city');
    }
}
