<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Traits\StoreImageTrait;
class Admin extends Authenticatable
{
    use Notifiable, StoreImageTrait;
    protected $guard = 'admin';

    protected $fillable = [
        'full_name', 'last_name', 'email', 'image', 'password', 'phone', 'address', 'country', 'city', 'state', 'zipcode'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $imageFolder = "/uploads/admin/";
}
