<?php


use Intervention\Image\Image;
use Carbon\Carbon;
//use DB;

if(! function_exists('session_type_count'))
{
    function session_type_count($trainer_id, $month, $year, $type)
    {
        if($type===3){ //video call
            $count = \DB::table('sessions')->where('trainer_id', $trainer_id)->whereStatus(4)->where('month', $month)->where('year', $year)->where('session_type',3)->where('is_therapist_paid',0)->count();
        }
        else if($type===2){ //voice call
            $count = \DB::table('sessions')->where('trainer_id', $trainer_id)->whereStatus(4)->where('month', $month)->where('year', $year)->where('session_type',2)->where('is_therapist_paid',0)->count();
        }
        else{ //live chat
            $count = \DB::table('sessions')->where('trainer_id', $trainer_id)->whereStatus(4)->where('month', $month)->where('year', $year)->where('session_type',1)->where('is_therapist_paid',0)->count();
        }
        
        return $count;
    }
}

if (!function_exists('format_number_in_notation')) {
    function format_number_in_notation(int $number): string
    {
        $suffixByNumber = function () use ($number) {
            if ($number < 1000) {
                return sprintf('%d', $number);
            }

            if ($number < 1000000) {
                return sprintf('%d%s', floor($number / 1000), 'K+');
            }

            if ($number >= 1000000 && $number < 1000000000) {
                return sprintf('%d%s', floor($number / 1000000), 'M+');
            }

            if ($number >= 1000000000 && $number < 1000000000000) {
                return sprintf('%d%s', floor($number / 1000000000), 'B+');
            }

            return sprintf('%d%s', floor($number / 1000000000000), 'T+');
        };

        return $suffixByNumber();
    }
}