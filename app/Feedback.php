<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    public $fillable = ['type', 'user_id', 'therapist_id', 'session_id', 'subject', 'email', 'message'];

    public $table = "feedbacks";

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function therapist(){
        return $this->belongsTo(Therapist::class, 'therapist_id');
    }

    public function getCreatedAtAttribute(){
        return $this->attributes['created_at'] =  (new Carbon($this->attributes['created_at']))->format('m/d/y');
    }
}
