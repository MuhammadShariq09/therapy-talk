<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    public $fillable = ['session_id', 'therapist_id', 'message', 'rating'];

    public $table = "reviews";

    public function therapist(){
        return $this->belongsTo(Therapist::class, 'therapist_id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
}
