<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\MassAssignmentException;
use Illuminate\Database\Eloquent\Model;
use Helper;

trait StoreImageTrait
{
	public function fill(array $attributes)
	{
        //dd('here');

		$totallyGuarded = $this->totallyGuarded();

		foreach ($this->fillableFromArray($attributes) as $key => $value) {
			$key = $this->removeTableFromKey($key);

			if($key == 'image')
			{
                if(!$value) continue;

				try {
        			$value = Helper::upload_base_64_image($value, property_exists($this, 'imageFolder')? $this->imageFolder : '');
				}catch (\Exception $e) {
					continue;
				}
			}

			// The developers may choose to place some attributes in the "fillable" array
			// which means only those attributes may be set through mass assignment to
			// the model, and all others will just get ignored for security reasons.
			if ($this->isFillable($key)) {
				$this->setAttribute($key, $value);
			} elseif ($totallyGuarded) {
				throw new MassAssignmentException(sprintf(
					'Add [%s] to fillable property to allow mass assignment on [%s].',
					$key, get_class($this)
				));
			}
		}

		return $this;
    }


	public function getImageAttribute($value)
	{
        $background = "00B7D5";

		$color = "FFFFFF";

		// To use UI Avatars this is the parameter structure in sequence from documentation
		// $url = urlencode("https://ui-avatars.com/api/name/size/background/color/length/font-size/rounded/uppercase/bold");
		return $value
				? asset($value)
				: asset('images/student-pro-girl.png');
	}

}




// namespace App\Traits;
// use Illuminate\Http\Request;
// trait StoreImageTrait {

//     public function StoreImage( $image = null, $directory = null) {

//             $image64 = $image;
//             if(!empty($image64) || $image64 != ''){
//               list($type, $image64) = explode(';', $image64);
//               list(,$extension) = explode('/',$type);
//               list(, $image64)      = explode(',', $image64);
//             }
//             $path = $directory;
//             $directoryPath=$destination=public_path($directory);
//             $image64 = base64_decode($image64);
//             $time = time();
//             $filename = md5(rand(11111111,99999999)).'.png';
//             file_put_contents($destination.$filename, $image64);
//             // User::where('id',$user->id)->update([
//             //     'image' => $path.$filename
//             // ]);
//             $uploadedImage = $path.$filename;
//             //dd($uploadedImage);

//             return $uploadedImage;


//     }

// }
