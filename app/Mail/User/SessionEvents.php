<?php

namespace App\Mail\User;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SessionEvents extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $session)
    {
        $this->user = $user;
        $this->session = $session;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if($this->session->status == 0 && $this->session->is_paid == 0){
            $data = [
                'title' => 'Session Booking',
                'message' => 'Your appointment request at '.$this->session->requested_date.' '.$this->session->requested_time.' has been sent to the therapist “'.$this->session->therapist['first_name'].' '.$this->session->therapist['last_name'].'”.',
                'name' => $this->user->first_name . " ".$this->user->last_name
            ];
        }
        else if($this->session->status == 1 && $this->session->is_paid == 0){
            $data = [
                'title' => 'Session Request Approved',
                'message' => 'Therapist has accepted your appointment request. Please pay to book the appointment.',
                'name' => $this->user->first_name . " ".$this->user->last_name
            ];
        }
        else if($this->session->status == 2 && $this->session->is_paid == 0){
            $data = [
                'title' => 'Session Request Rejected',
                'message' => 'Your appointment request has been rejected by the therapist, View your session log and see Therapist`s suggested date and time.',
                'name' => $this->user->first_name . " ".$this->user->last_name
            ];
        }
        else {
            $data = [
                'title' => 'Appointment Booked',
                'message' => 'Your appointment at '.$this->session->requested_date.' '.$this->session->requested_time.' with “'.$this->session->therapist['first_name'].' '.$this->session->therapist['last_name'].'” has been booked.',
                'name' => $this->user->first_name . " ".$this->user->last_name
            ];
        }
        return $this->view('email.session')->subject('My Therapy talk - '.$data['title'].'')->with([
            'data'=> $data
        ]);
    }
}
