<?php

namespace App\Mail\User;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SessionsReshedule extends Mailable
{
    use Queueable, SerializesModels;

    public function __construct($user, $session)
    {
        $this->user = $user;
        $this->session = $session;
    }

    
    public function build()
    {
        if($this->session->status == 0){
            $data = [
                'title' => 'Session Reshedule Request',
                'message' => 'Your appointment reschedule request has been sent to the therapist.',
                'name' => $this->user->first_name . " ".$this->user->last_name
            ];
        }
        else if($this->session->status == 1){
            $data = [
                'title' => 'Session Reshedule Request Approved',
                'message' => 'Your appointment reschedule request has been accepted by the therapist.',
                'name' => $this->user->first_name . " ".$this->user->last_name
            ];
        }
        else if($this->session->status == 2){
            $data = [
                'title' => 'Session Reshedule Request Rejected',
                'message' => 'Therapist has rejected your appointment request. Please see the suggested time and date and book again',
                'name' => $this->user->first_name . " ".$this->user->last_name
            ];
        }
        return $this->view('email.session')->subject('My Therapy talk - '.$data['title'].'')->with([
            'data'=> $data
        ]);
    }
}
