<?php

namespace App\Mail\Therapist;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ResheduleRequests extends Mailable
{
    use Queueable, SerializesModels;


    public function __construct($user, $session)
    {
        $this->user = $user;
        $this->session = $session;
    }


    public function build()
    {
        if($this->session->status == 0){
            $data = [
                'title' => 'Session Reshedule Request',
                'message' => 'User has requested to reschedule the appointment. Please view details to respond.',
                'action' => 'View Session Logs',
                'action_url' => route('therapist.session.logs'),
                'name' => $this->user->first_name . " ".$this->user->last_name
            ];
        }
        else if($this->session->status == 1){
            $data = [
                'title' => 'Session Reshedule Request Approved',
                'message' => 'You have accepted the appointment reschedule request. Appointment has been booked on “'.$this->session->suggested_date.'” at “'.$this->session->suggested_time.'”',
                'action' => 'View Session Logs',
                'action_url' => route('therapist.session.logs'),
                'name' => $this->user->first_name . " ".$this->user->last_name
            ];
        }
        else if($this->session->status == 2){
            $data = [
                'title' => 'Session Reshedule Request Rejected',
                'message' => 'You have rejected the appointment reschedule request. Suggested date and time are sent to the user.',
                'action' => 'View Session Logs',
                'action_url' => route('therapist.session.logs'),
                'name' => $this->user->first_name . " ".$this->user->last_name
            ];
        }
        return $this->view('email.session')->subject('Therapy talk - '.$data['title'].'')->with([
            'data'=> $data
        ]);
    }
}
