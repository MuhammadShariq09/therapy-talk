<?php

namespace App\Mail\Therapist;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SessionRequests extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($session)
    {
        $this->session = $session;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if($this->session->status == 0 && $this->session->is_paid == 0){
            $data = [
                'title' => 'Session Appointment Request',
                'message' => 'You have received an appointment booking request. Please view the details and respond.',
                'action' => 'View Session Logs',
                'action_url' => route('therapist.session.logs'),
                'name' => $this->session->therapist->first_name . " ".$this->session->therapist->last_name
            ];
        }
        else if($this->session->status == 1 && $this->session->is_paid == 0){
            $data = [
                'title' => 'Session Appointment Request',
                'message' => 'You have accepted the appointment booking request. Waiting for the user to pay for the appointment.',
                'action' => 'View Session Logs',
                'action_url' => route('therapist.session.logs'),
                'name' => $this->session->therapist->first_name . " ".$this->session->therapist->last_name
            ];
        }
        else {
            $data = [
                'title' => 'Appointment Booked',
                'message' => 'User has booked appointment with you on '.$this->session->requested_date.' '.$this->session->requested_time.' .',
                'action' => 'View Session Logs',
                'action_url' => route('therapist.session.logs'),
                'name' => $this->session->therapist->first_name . " ".$this->session->therapist->last_name
            ];
        }
       
        return $this->view('email.session')->subject('Therapy talk - '.$data['title'].'')->with([
            'data'=> $data
        ]);
    }
}
