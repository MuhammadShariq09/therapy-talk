<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RescheduleApprovedAppointments extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $message)
    {
        $this->user = $user;
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = [
            'title' => 'Session Reshedule Request',
            'message' => $this->message,
            'name' => $this->user->first_name . " ".$this->user->last_name
        ];
        return $this->view('email.session')->subject('Therapy talk - '.$data['title'].'')->with([
            'data'=> $data
        ]);
    }
}
