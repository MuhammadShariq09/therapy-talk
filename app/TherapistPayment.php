<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TherapistPayment extends Model
{
    protected $fillable = [
        'therapist_id', 'sessions', 'live_chat', 'voice_call', 'video_call', 'month', 'year', 'amount', 'status'
    ];
}
