<?php
namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class SessionReshedule extends Model
{
    protected $fillable = [
        'session_id', 'suggested_date', 'suggested_time', 'status', 't_suggested_time', 't_suggested_date', 'action_by'
    ];

    protected $table = 'session_reshedule';

    protected $casts = [
        'suggested_date' => 'date',
        't_suggested_date' => 'date'
    ];

    public function getSuggestedDateAttribute($value){
        return Carbon::parse($value)->format('m/d/y');
    }

    public function getTSuggestedDateAttribute($value){
        return Carbon::parse($value)->format('m/d/y');
    }

}
