<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageUsage extends Model
{
    protected $fillable = [
        'user_id', 'live_chat', 'voice_call','video_call','live_chat_availed','voice_call_availed','video_call_availed', 'status'
    ];


    public function user(){
        return $this->hasOne(User::class, 'id', 'user_id' );
    }

}
