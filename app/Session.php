<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Carbon\Carbon;

class Session extends Model
{
    protected $fillable = [
        'user_id',  'trainer_id', 'session_type', 'reason', 'type', 'duration', 'requested_date', 'requested_time', 'amount', 'status', 'is_paid', 'month', 'year', 'payment_type', 'is_therapist_paid'
    ];

    protected $with = ['user', 'rejections'];

    protected $table = 'sessions';

    public function rejections(){
        return $this->hasMany(SessionRejection::class, 'session_id')->orderBy('id','desc');
    }

    public function payment(){
        return $this->hasOne(Payment::class, 'transaction_id');
    }

    public function reschedule(){
        return $this->hasMany(SessionReshedule::class, 'session_id')->orderBy('id','desc');
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function therapist(){
        return $this->belongsTo(Therapist::class, 'trainer_id');
    }

    public function getRequestedDateAttribute(){
        return $this->attributes['requested_date'] =  (new Carbon($this->attributes['requested_date']))->format('m/d/y');
    }

}
