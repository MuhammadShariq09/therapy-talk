<?php
namespace App;

class Helper {

    public static function upload_base_64_image($image, $path = '', $name = null) {
        if(! $image || empty($image))
            return;
        $name = $name?? time();

        list($type, $image) = explode(';', $image);
        list(, $extension) = explode('/', $type);
        list(, $image) = explode(',', $image);

        $_filename = sprintf('%s/%s.%s', (trim($path, '/')), $name, $extension);
        $filename = sprintf('%s/%s.%s', public_path(trim($path, '/')), $name, $extension);
        
        (!\File::isDirectory(public_path($path))) ?  \File::makeDirectory(public_path($path), 0777, true, true) : '';

        file_put_contents(($filename), base64_decode($image));

        return $_filename;

    }

}
