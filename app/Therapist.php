<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Traits\StoreImageTrait;

class Therapist extends Authenticatable
{
    use Notifiable, SoftDeletes, StoreImageTrait;

    protected $guard = 'therapist';

    protected $fillable = [
        'full_name', 'image', 'email', 'password', 'practice_name',
        'practice_website', 'phone', 'address', 'latitude', 'longitude', 'city', 'country', 'state',
        'zipcode', 'therapist_title', 'therapist_type', 'license_type', 'years_in_practice', 'license_in',
        'undergraduate_institute', 'postgraduate_institute', 'specialities_id',
        'treatments_id', 'modalities_id', 'age_groups_id', 'ethnicities_id', 'languages_id',
        'statement', 'time_availability', 'reference_name', 'reference_title',
        'reference_email', 'is_active','is_verified', 'rejected_reason', 'is_rejected'
    ];

    protected $imageFolder = "/uploads/therapists/";
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    protected $appends = ['name'];

    public function getNameAttribute(){
        return $this->full_name;
    }

    

    public function offices(){
        return $this->hasMany(TherapistAddress::class, 'therapist_id');
    }

    public function reviews(){
        return $this->hasMany(Review::class, 'therapist_id');
    }

    public function activeAll($per_page){
        return self::where('is_active', "1")->where('is_verified', "1")->paginate($per_page);
    }


    public function scopeFilter($query, $filters){
        return $filters->apply($query);
    }


    public function setSpecialitiesIdAttribute($value) {
        $this->attributes['specialities_id'] = implode(',', $value);
    }

    public function setTreatmentsIdattribute($value) {
        $this->attributes['treatments_id'] = implode(',', $value);
    }

    public function setModalitiesIdAttribute($value) {
        $this->attributes['modalities_id'] = implode(',', $value);
    }

    public function setAgeGroupsIdAttribute($value) {
        $this->attributes['age_groups_id'] = implode(',', $value);
    }

    public function setEthnicitiesIdAttribute($value) {
        $this->attributes['ethnicities_id'] = implode(',', $value);
    }

    public function setLanguagesIdAttribute($value) {
        $this->attributes['languages_id'] = implode(',', $value);
    }

    public function subscriptions(){
        return $this->hasOne(Subscription::class, 'user_id', 'id');
    }

   
    public function sessions(){
        return $this->hasMany(Session::class, 'trainer_id', 'id');
    }

    public function my_payments(){
        return $this->hasMany(TherapistPayment::class, 'therapist_id', 'id');
    }


    public function enable()
    {
        $this->is_active = "1";
    }

    public function disable()
    {
        $this->is_active = "0";
    }

    public function toggleActivation()
    {
        if($this->is_active)
            $this->disable();
        else
            $this->enable();
    }

    public function verified(){
        $this->is_verified = "1";
        $this->is_active = "1";
    }

    public function nonVerified(){
        $this->is_verified = "0";
    }

    public function toggleVerification()
    {
        if($this->is_verified)
            $this->nonVerified();
        else
            $this->verified();
    }

}
