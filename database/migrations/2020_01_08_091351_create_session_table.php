<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSessionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sessions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user_id');
            $table->string('trainer_id');
            $table->text('reason');
            $table->string('type');
            $table->string('duration');
            $table->string('requested_date');
            $table->string('requested_time');
            $table->float('amount', 5, 2)->nullable();
            $table->tinyInteger('status')->default(0);
            $table->tinyInteger('is_refund')->default(0);
            $table->tinyInteger('is_paid')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sessions');
    }
}
