<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name', 25);
            $table->string('last_name', 25);
            $table->string('image', 500);
            $table->string('email')->unique();
            $table->string('password');
            $table->string('phone', 25);
            $table->string('address', 500);
            $table->string('latitude',50);
            $table->string('longitude', 50);
            $table->string('country', 50);
            $table->string('city', 50);
            $table->string('state', 50);
            $table->string('zipcode', 50);
            $table->boolean('is_super')->default(false);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
