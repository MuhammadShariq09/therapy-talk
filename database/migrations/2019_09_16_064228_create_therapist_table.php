<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTherapistTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('therapists', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name', 25);
            $table->string('last_name', 25);
            $table->string('image', 500);
            $table->string('email')->unique();
            $table->string('password');
            $table->string('phone', 25);
            $table->string('address', 500);
            $table->string('latitude',50);
            $table->string('longitude',50);
            $table->string('address_2', 500);
            $table->string('country', 50);
            $table->string('city', 50);
            $table->string('state', 50);
            $table->string('zipcode', 50);
            $table->string('therapist_title', 50);
            $table->string('therapist_type');
            $table->string('license_type');
            $table->string('years_in_practice');
            $table->string('license_in');
            $table->string('undergraduate_institute');
            $table->string('postgraduate_institute');
            $table->string('speciality_id');
            $table->string('treatment_orientation_id');
            $table->string('modality_id');
            $table->string('age_group_id');
            $table->string('ethnicity_id');
            $table->string('language_id');
            $table->string('statement');
            $table->string('time_availability');
            $table->string('reference_name', 50);
            $table->string('reference_title', 50);
            $table->string('reference_email');
            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->enum('is_active', array('0', '1'))->default('0')->comment = "0 for Inactive, 1 for active";
            $table->enum('is_verified', array('0', '1'))->default('0')->comment = "0 for Inactive, 1 for active";
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('therapists');
    }
}
