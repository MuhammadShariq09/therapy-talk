<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSessionResheduleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('session_reshedule', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('session_id');
            $table->string('suggested_date');
            $table->string('suggested_time');
            $table->string('t_suggested_date')->nullable();
            $table->string('t_suggested_time')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('session_reshedule');
    }
}
