<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name', 25);
            $table->string('last_name', 25);
            $table->string('image', 500);
            $table->string('email')->unique();
            $table->string('password');
            $table->string('phone',25);
            $table->date('date_of_birth');
            $table->enum('gender', array('0', '1', '2'))->default('0')->comment = "0 for Male, 1 for Female, 2 for Transgender";
            $table->string('address',500);
            $table->string('latitude',50);
            $table->string('longitude',50);
            $table->string('country',50);
            $table->string('state',50);
            $table->string('city',50);
            $table->string('postal_code',50);
            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->enum('is_active', array('0', '1'))->default('1')->comment = "0 for Inactive, 1 for active";
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
