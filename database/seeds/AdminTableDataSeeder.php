<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Admin;
class AdminTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i <= 1; $i++) { 
	    	Admin::create([
                'first_name' => 'Denny',
                'last_name' => 'Hpokin',
	            'email' => 'dennyhopkin@gmail.com',
                'password' => bcrypt('Admin123'),
                'phone' => '46464654',
	        ]);
    	}
    }
}
