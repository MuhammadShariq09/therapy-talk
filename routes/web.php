<?php
use Illuminate\Notifications\DatabaseNotification;

/*
|--------------------------------------------------------------------------
| FrontEnd Routes
|--------------------------------------------------------------------------
|
*/
Route::view('/', 'frontend.index');
Route::view('/about-us', 'frontend.aboutus');
Route::get('/pricing', 'SiteController@pricing');
Route::view('/how-it-works', 'frontend.how-it-works');
Route::view('/faq', 'frontend.faq');
Route::view('/blogs', 'frontend.blogs');
Route::get('/blog/{slug}', function($slug){
    if($slug == 'depression-a-quick-guide')
        return view('frontend.blog1');
    else if($slug == 'building-healthy-self-esteem')
        return view('frontend.blog2');
    else if($slug == 'your-personal-virtual-office')
        return view('frontend.blog3');
    else if($slug == 'how-effective-is-online-counselling-and-therapy')
        return view('frontend.blog4');
    else if($slug == 'reason-for-getting-mental-health-therapy-online')
        return view('frontend.blog5');
    else if($slug == 'feeling-overwhelmed-during-the-coronavirus-crisis')
        return view('frontend.blog6');
    else if($slug == 'five-ways-to-stay-active-and-healthy-during-the-quarantine')
        return view('frontend.blog7');
    else if($slug == 'protecting-your-mental-health-during-the-coronavirus-outbreak')
        return view('frontend.blog8');
    
    else
        return redirect('blogs');


});
Route::view('/contact-us', 'frontend.contact-us');
Route::view('/join-mtt', 'frontend.talk-to-mtt');
Route::post('submit-query', 'SiteController@submitQuery');
Route::post('subscribe', 'SiteController@submitNewsletter');
Route::get('/terms-and-conditions', function(){
    return view('frontend.terms');
})->name('terms-and-conditions');

Route::get('user/login/{provider}', 'SocialController@redirect');
Route::get('login/{provider}/callback','SocialController@Callback');

Route::get('therapist/login/{provider}', 'SocialController@redirectTherapist');
Route::get('therapist/login/{provider}/callback','SocialController@CallbackTherapist');

Route::get('become-a-member','Auth\RegisterController@becomeAMember')->name('become-a-member');

Route::get('/logout', function(){ 
    Auth::guard('web')->logout();
    return redirect('/login');
 })->name('logout');
 

 // Notifications
    Route::get('/markasread/{id?}', function($id=Null){
        
    if($id){
        DatabaseNotification::find($id)->markAsRead();
        $notification =  DatabaseNotification::where('id',$id)->first();
        return ['message' => 'Notifications marked as read', 'url' => $notification->data['url'] ?? ""];
        }
        else{
            if(Auth::guard('therapist')->check() == true){
                Auth::guard('therapist')->user()->notifications->markAsRead();
            }
            else{
                auth()->user()->notifications->markAsRead();
            }
            return ['message' => 'Notifications marked as read', 'url' => url('/')];
        }    
    });
        

    Route::get('get-cities/{id}','SiteController@getCities')->name('get-cities');

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
*/
/* User Auth */
Auth::routes();
Route::post('/password-reset/send-verification-code', 'Auth\ForgotPasswordController@sendVerificationCode')->name('user.send-verification-code');
Route::post('/password-reset/verify-verification-code', 'Auth\ForgotPasswordController@verifyVerificationCode')->name('user.verify-verification-code');
Route::post('/password-reset/update-password', 'Auth\ForgotPasswordController@updatePassword')->name('user.update-password');
Route::get('show-assesment-form', 'Auth\RegisterController@showAssesmentForm');
Route::get('user/register', 'Auth\RegisterController@showRegisterForm')->name('user.register');
Route::post('submit-assesment-form', 'Auth\RegisterController@submitAssesmentForm')->name('submit-assesment-form');
Route::post('verify-account', 'Auth\RegisterController@verifyAccount')->name('verify-account');
Route::get('resend-code', 'Auth\RegisterController@resendCode')->name('resend-code');
Route::get('complete-your-registration/{id}', 'Auth\RegisterController@completeRegistration')->name('complete-your-registration');

/* Admin Auth */
Route::get('/login/admin' , 'Auth\LoginController@showAdminLoginForm');
Route::post('/login/admin', 'Auth\LoginController@adminLogin');
Route::get('/admin/logout', function(){ 
    Auth::guard('admin')->logout();
    return redirect('/login/admin');
 })->name('admin.logout');
 
/* Therapist Auth */
Route::get('/login/therapist' , 'Auth\LoginController@showTherapistLoginForm')->name('therapist.login');
Route::post('/login/therapist', 'Auth\LoginController@TherapistLogin');
Route::get('/register/therapist' , 'Auth\RegisterController@showTherapistRegisterForm')->name('therapist.register');
Route::post('/register/therapist', 'Auth\RegisterController@registerTherapist');
Route::any('/therapist-data-modal', 'Auth\RegisterController@getPartials');
Route::get('/therapist/logout' , 'Auth\LoginController@showTherapistLoginForm')->name('therapist.logout');
Route::post('/therapist/password-reset/send-verification-code', 'Auth\ForgotPasswordController@sendVerificationCodeTherapist')->name('therapist.send-verification-code');
Route::post('/therapist/password-reset/verify-verification-code', 'Auth\ForgotPasswordController@verifyVerificationCodeTherapist')->name('therapist.verify-verification-code');
Route::post('/therapist/password-reset/update-password', 'Auth\ForgotPasswordController@updatePasswordTherapist')->name('therapist.update-password');

Route::post('/admin/password-reset/update-password', 'Auth\ForgotPasswordController@updatePasswordAdmin')->name('admin.update-password');
Route::get('/logout', function(){ 
    Auth::logout();
    return redirect('/login');
 })->name('logout');
 Route::get('/therapist/logout', function(){ 
    Auth::guard('therapist')->logout();
    return redirect('/login/therapist');
 })->name('therapist.logout');
Route::group(['middleware' => 'prevent-back-history'],function(){
/* Admin Routes */
Route::group(['prefix' => 'admin', 'middleware'=> ['auth:admin'] ] , function(){
    Route::get('/', 'Admin\AdminController@index');
    Route::get('/edit-profile', 'Admin\AdminController@editProfile')->name('edit-profile');
    Route::post('/update-profile', 'Admin\AdminController@updateProfile')->name('update-profile');
    Route::post('/change-password', 'Admin\AdminController@updatePassword')->name('change-password');

    // Assesments
    Route::get('assesment-forms', 'Admin\AssesmentController@index')->name('assesments');
    Route::get('assesment-forms/{id}', 'Admin\AssesmentController@downloadAssesment')->name('assesments.pdf');
    Route::post('reject-assement-form', 'Admin\AssesmentController@rejectAssesment')->name('assesments.reject');
    Route::post('approve-assement-form/{id}', 'Admin\AssesmentController@approveAssesment')->name('assesments.approve');

    // Users
    Route::get('/users', 'Admin\UserController@getActiveUsers')->name('active-users');
    Route::get('/blocked-users', 'Admin\UserController@getBlockedUsers')->name('blocked-users');
    Route::get('/add-user', 'Admin\UserController@addUser')->name('add-user');
    Route::post('/submit-user', 'Admin\UserController@submitUser')->name('submit-user');
    Route::post('/update-user', 'Admin\UserController@updateUser')->name('update-user');
    Route::get('/view-user/{id}', 'Admin\UserController@viewUser')->name('view-user');
    Route::get('/edit-user/{id}', 'Admin\UserController@editUser')->name('edit-user');
    Route::post('/update-user', 'Admin\UserController@updateUser')->name('update-user');
    Route::get('/update-user-status', 'Admin\UserController@updateUserStatus')->name('update-user-status');
    Route::delete('/delete-user/{id}', 'Admin\UserController@deleteUser')->name('delete-user');


    // Therapists
    Route::get('approve-therapist/{id}', 'Admin\TherapistController@approveTherapist')->name('admin.therapist.approve');
    Route::post('reject-therapist}', 'Admin\TherapistController@rejectTherapist')->name('admin.therapist.reject');

    Route::get('/active-therapists', 'Admin\TherapistController@getActiveTherapists')->name('active-therapists');
    Route::get('/unverified-therapists', 'Admin\TherapistController@getUnVerifiedTherapists')->name('unverified-therapists');
    Route::get('/blocked-therapists', 'Admin\TherapistController@getBlockedTherapists')->name('blocked-therapists');
    Route::get('/add-therapist', 'Admin\TherapistController@addTherapist')->name('add-therapist');
    Route::post('/submit-therapist', 'Admin\TherapistController@submitTherapist')->name('submit-therapist');
    Route::get('/view-therapist/{id}', 'Admin\TherapistController@viewTherapist')->name('view-therapist');
    Route::post('/update-therapist', 'Admin\TherapistController@updateTherapist')->name('update-therapist');
    Route::get('/update-therapist-status', 'Admin\TherapistController@updateTherapistStatus')->name('update-therapist-status');
    Route::get('/block-therapist/{id}', 'Admin\TherapistController@blockTherapist')->name('block-therapist');
    Route::get('/unblock-therapist/{id}', 'Admin\TherapistController@unblockTherapist')->name('unblock-therapist');
    Route::get('/update-therapist-verified', 'Admin\TherapistController@updateTherapistVerified')->name('update-therapist-verified');
    Route::delete('/delete-therapist/{id}', 'Admin\TherapistController@deleteTherapist')->name('delete-therapist');

    // Session logs
    Route::get('session-logs', 'Admin\SessionController@index')->name('session-logs');
    Route::get('session-logs/{id}', 'Admin\SessionController@view')->name('session-logs.view');

    // Payment logs
    Route::get('payment-logs', 'Admin\PaymentController@index')->name('payment-logs');
    Route::get('payment-logs/{id}', 'Admin\PaymentController@view')->name('payment-logs.view');
    Route::get('subscriptions', 'Admin\PaymentController@subscriptions')->name('subscriptions');
    Route::post('package/{id}', 'Admin\PaymentController@editPackage')->name('package.edit');

    Route::post('refund-paid/{id}', 'Admin\PaymentController@refund')->name('refund');
    Route::post('pay-therapist', 'Admin\PaymentController@payTherapist')->name('pay-therapist');
    Route::post('pay', 'Admin\PaymentController@payTherapist')->name('pay.therapist');
    Route::get('payables', 'Admin\PaymentController@payables')->name('payment-logs.payables');
    Route::get('payment-logs/payables/{id}', 'Admin\PaymentController@payablesView')->name('payment-logs.payables.view');

    // Feedbacks
    Route::get('feedbacks', 'Admin\FeedbackController@index')->name('feedbacks');
    Route::get('feedbacks/{id}', 'Admin\FeedbackController@view')->name('feedbacks.view');

     // Reports
     Route::get('reports', 'Admin\FeedbackController@reports')->name('reports');
     Route::get('reports/{id}', 'Admin\FeedbackController@reportView')->name('reports.view');

     // Feedbacks
     Route::get('package-management', 'Admin\PackageController@index')->name('package-management');
     Route::get('package-management/{id}', 'Admin\PackageController@view')->name('package-management.view');
     Route::get('package-management/edit/{id}', 'Admin\PackageController@edit')->name('package-management.edit');
     Route::post('package-management/update', 'Admin\PackageController@update')->name('package-management.update');
});

Route::group(['middleware' => 'prevent-back-history'],function(){
/* Therapist Routes */
Route::name('therapist.')->group(function (){
    Route::group(['prefix' => 'therapist', 'namespace' => 'Therapist', 'middleware'=> ['auth:therapist'] ] , function(){
      //  Route::group(['middleware' => ['subscription']], function (){
            Route::get('/', 'DashboardController@index');
            Route::get('dashboard', 'DashboardController@index')->name('dashboard');

            Route::post('change-password', 'HomeController@changePassword');
            Route::get('profile', 'HomeController@index')->name('profile');
            Route::get('edit-profile', 'HomeController@editProfile')->name('profile.edit');
            Route::post('update-profile', 'HomeController@updateProfile')->name('profile.update');

            Route::get('session/logs', 'SessionController@index')->name('session.logs');
            Route::get('session/logs/{id}', 'SessionController@view')->name('session.logs.view');

            Route::post('/session/reschedule-approved-appointment/{id}', 'SessionController@rescheduleApprovedAppointment')->name('reschedule-approved-appointment');

            Route::get('/session/approve-reschedule-request/{id}', 'SessionController@approveRescheduleRequest')->name('approve-resedule-request');
            Route::post('/session/reject-reschedule-request/{id}', 'SessionController@rejectRescheduleRequest')->name('reject-resedule-request');


            Route::post('session/logs/{id}/approve', 'SessionController@approveSessionLog')->name('session.logs.approve');
            Route::post('session/logs/{id}/reject', 'SessionController@rejectSessionLog')->name('session.logs.reject');
            Route::get('create-random-session', 'SessionController@createRandomRecord');

            Route::get('appointments', 'SessionController@myAppointments')->name('appointments');
            Route::get('appointments/{id}', 'SessionController@myAppointmentsDetail')->name('appointments.detail');

            Route::get('change-status/{id}/{status}', 'SessionController@changeStatus')->name('change-status');

            Route::get('payment/logs', 'SessionController@paymentLogs')->name('payment');
       // });

        Route::get('subscribe-package','HomeController@subscribePackage')->name('subscribe-package');
        Route::post('subscribe-package','HomeController@submitSubscribePackage')->name('submit.subscribe-package');

    });
});
});

Route::group(['middleware' => 'prevent-back-history'],function(){
/* User Routes */
Route::name('user.')->group(function () {
    Route::group(['prefix' => '', 'middleware' => ['auth']], function () {
        Route::get('/home', 'HomeController@index')->name('home');
        Route::get('/profile', 'HomeController@profile')->name('profile');
        Route::get('/edit-profile', 'HomeController@editProfile')->name('profile.edit');
        Route::post('/update-password', 'HomeController@updatePassword')->name('password.update');
        Route::post('/update-profile', 'HomeController@updateProfile')->name('profile.update');

        Route::get('/find-a-therapist', 'MainController@index')->name('find-a-therapist');
        Route::get('/find-a-therapist/therapist/{id}', 'MainController@therapistProfile')->name('therapist.profile');

        Route::post('/book-appointment', 'MainController@bookSession')->name('book-appointment');
       // Route::post('/session/payment/{id}', 'MainController@payForSession')->name('appointment.payment');
        Route::post('/session/payment', 'MainController@payForSession')->name('appointment.payment');

        Route::get('/my-session-logs', 'MainController@sessionLogs')->name('session.logs');
        Route::get('/my-session-logs/{id}', 'MainController@sessionLogDetail')->name('session.log.details');

        Route::get('/my-appointments', 'MainController@appointmentsLogs')->name('appointments.logs');
        Route::get('/my-appointments/{id}', 'MainController@appointmentsLogDetail')->name('appointments.log.details');
        Route::get('/my-appointments-session/{id}', 'MainController@chat')->name('appointments.session');

        // accept-reschedule
        ROute::post('session/accept-reschedule/{id}',  'MainController@acceptReschedule')->name('accept-reschedule');
        ROute::post('session/ask-for-refund/{id}',  'MainController@askForRefund')->name('ask-for-refund');

        ROute::post('/report',  'MainController@report')->name('report');
        ROute::post('/feedback',  'MainController@feedback')->name('feedback');

        Route::post('/session/reschedule-approved-appointment/{id}', 'MainController@rescheduleApprovedAppointment')->name('reschedule-approved-appointment');

        Route::get('/session/approve-rejected-res-appointment/{id}', 'MainController@approveRejectedRescAppointment')->name('approve_rejected_therapist_suggested_datetime_modal');

        Route::get('payment/{id}', 'MainController@payment')->name('payment');
        Route::get('cancel', 'MainController@cancel')->name('payment.cancel');
        Route::get('payment/success/{id}', 'MainController@success')->name('payment.success');

        Route::post('/create-payment', 'PaypalController@createPayment' );

        Route::post('/execute-payment', 'PaypalController@createPayment' );

        Route::get('assessment-form','HomeController@becomeAMember')->name('assessment-form');
        Route::post('assessment-form', 'HomeController@submitAssesmentForm')->name('assessment-form');

        Route::post('/user/subscribe-package','SubscriptionController@userSubscription')->name('subscribe-package');

        Route::get('/subscription-logs', 'SubscriptionController@subscriptionLogs')->name('subscription-logs');
        Route::get('/use-package/{id}', 'SubscriptionController@usePackage')->name('use-package');
        Route::post('/renew-package', 'SubscriptionController@renewPackage')->name('renew-package');
        Route::post('/renew-package-subscription', 'SubscriptionController@renewPackageSubscription')->name('renew-package-subscription');
        Route::post('/subscribe-new-package', 'SubscriptionController@subscribeNewPackage')->name('subscribe-new-package');
    });
});
});


});



