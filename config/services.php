<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],
    'stripe' => [
        // 'PUBLISH_KEY' => env('MIX_STRIPE_PUBLISHABLE_KEY', 'pk_test_HAorAMBpZdnSy3XeSoEc7EHZ00GmySthxL'),
        // 'SECRET_KEY' => env('MIX_STRIPE_SECRET_KEY', 'sk_test_6mb3q2e2DC5qsrTVlkX82lkQ00RqzFBxvP'),
        'PUBLISH_KEY' => 'pk_test_HAorAMBpZdnSy3XeSoEc7EHZ00GmySthxL',
        'SECRET_KEY' => 'sk_test_6mb3q2e2DC5qsrTVlkX82lkQ00RqzFBxvP',
    ],

    'facebook' => [
        'client_id'     => env('FACEBOOK_CLIENT_ID'),
        'client_secret' => env('FACEBOOK_CLIENT_SECRET'),
        'redirect'      => env('FACEBOOK_URL'),
    ],

    'google' => [
            'client_id'     => env('GOOGLE_CLIENT_ID'),
            'client_secret' => env('GOOGLE_CLIENT_SECRET'),
            'redirect'      => env('GOOGLE_URL'),
        ],

];
