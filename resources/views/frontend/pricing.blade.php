@extends('frontend.layout')
@section('title','Affordable Online Counseling Therapy | Video Conferencing')
@section('description','If you are looking for affordable online counseling and therapy services, My Therapy Talk offers mental health therapy using audio, video and text. Contact now.')
@section('body-col',"2-columns")
@section('css')
@endsection
@section('content')
<div class="about-us">
   <!--------------------------banner start------------------------>
   <section class="inner-page-banner">
      <!-------------------header section---------------------------->
      @include('frontend.include.topbanner')
      <!-----------------header end--------------------------->
      <!----------------banner content start-------------->
      <section class="banner-content">
         <div class="container">
            <div class="row wow fadeInDown" data-wow-duration="0.5s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 0.5s; animation-delay: 0.5s; animation-name: fadeInDown;">
               <div class="col-md-7 col-sm-12 ">
                  <h1>Pricing</h1>
               </div>
               <div class="col-md-5 col-sm-12"></div>
            </div>
         </div>
      </section>
      <!----------------banner content end-------------->
   </section>
   <!----------------banner end---------------------->
   <section class="pricing">
      <div class="container">
         <div class="row">
            <div class="col-lg-8 col-sm-12">
               <h1 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.5s; animation-name: fadeInUp;">Therapist Package</h1>
               <p class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.8s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.8s; animation-name: fadeInUp;">MyTherapyTalk’s cost-effective packages make counseling accessible and easy and work in favor of all types of people. We’d love to liberate you from your challenges and optimize your performance.</p>
               <p>Take a look at our affordable packages for quicker recovery.</p>
            </div>
         </div>
         <div class="row">
         @foreach($packages as $package)
            <div class="col-lg-4 col-md-6 col-sm-12 d-flex">
               <div class="price-box wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.8s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.8s; animation-name: fadeInLeft;">
                  <div class="price-tag" style="margin-bottom: 35px;">
                     <!-- <h3>3 Months<span>Package</span></h3> -->
                     <h2>${{number_format($package->amount, 2)}}</h2>
                     <h3><span>{{$package->title}}</span></h3>
                  </div>
                  <div class="price-list" >
                     <ul>
                     <li class="check">Text messaging - {{$package->live_chat }}</li>
                        <li class="check">Video messaging - {{$package->video_call }}</li>
                        <li class="check">Audio messaging - {{$package->voice_call }}</li>
                        <li class="check">Therapist available &nbsp;<strong>5 days/week</strong></li>
                        <li class="check">validity is 1 month</li>
                     </ul>
                     @if(auth()->user()==null)
                        <a href="#" class="btn-a bkappnmt" data-toggle="modal" data-target="#login">Select</a>
                     @else 
                        @php $user=auth()->user(); $user->load('subscriptions'); @endphp 
                        {{-- @if($user->subscriptions)
                        <a href="#" class="btn-a bkappnmt" data-toggle="modal" data-target="#alreadySubscribed">Select</a>
                        @else --}}
                           <div class="price-btn">
                              <a class="subpkg" data-packageId="{{$package->id}}" type="button"
                                       data-packageName="{{$package->title}}"
                                       data-packageAmount="{{$package->amount}}"
                                       data-packageMonth="{{$package->month}}"
                                       data-toggle="modal"
                                       data-target="#subscribeModal">Select</a>
                           </div>
                        {{-- @endif --}}
                     @endif
                  </div>
               </div>
            </div>
         @endforeach
         </div>
      </div>
   </section>
  
</div>

<div class="login-fail-main user admin-profile profile-view" >
        <div class="featured inner">
            <div class="modal fade" id="subscribeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="forget-pass">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                            <div class="modal-body">
                                <div class="payment-main-popup">
                                    <!-- <form action="{{ route('user.book-appointment') }}" id="bookAppointment" method="post" id="loader">
                                        @csrf
                                        -->
                                        <div class="payment-modal-main">
                                            <div class="payment-modal-inner">
                                            <div class="row">
                                                   <div class="col-12">
                                                      <div class="pkg-payment-title">
                                                         <h3 class="package_title">Package: </h3>
                                                         <h3 class="package_fee">Fee: $ 30</h3>
                                                      </div>
                                                   </div>
                                             </div>
                                             <div class="row">
                                                   <div class="col-12 form-group">
                                                      <div class="pro-inner-row">
                                                         <label><i class="fa fa-address-card" aria-hidden="true"></i> Card Holder Number</label>
                                                         <input type="text" id="card" required="required" spellcheck="true" class="form-control" >
                                                      </div>
                                                   </div>
                                                   <div class="col-12 form-group">
                                                      <div class="pro-inner-row">
                                                         <label><i class="fa fa-user-circle" aria-hidden="true"></i> Card Holder Name</label>
                                                         <input type="text" id="card_holder" required="required" spellcheck="true" class="form-control" >
                                                      </div>
                                                   </div>
                                                   <div class="col-lg-6 col-sm-12 form-group">
                                                      <div class="pro-inner-row">
                                                         <label><i class="fa fa-address-card" aria-hidden="true"></i> CVV</label>
                                                         <input type="text" id="cvv" required="required" spellcheck="true" class="form-control" >
                                                      </div>
                                                   </div>
                                                   <div class="col-lg-6 col-sm-12 form-group">
                                                      <div class="pro-inner-row">
                                                         <label><i class="fa fa-calendar"></i> Expiry Date</label>
                                                         <input id="expiry" type="text" required="required" spellcheck="true" class="form-control">
                                                      </div>
                                                   </div>
                                                   <div class="col-12 text-center blockui">
                                                      <button type="button"  class="can" data-dismiss="modal">Cancel</button>
                                                      <button type="button"  class="can" onclick="saveUserPackage()" id="cont">Confirm</button>
                                                   </div>
                                             </div>
                                                
                                            </div>
                                        </div>
                                    <!-- </form> -->
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="login-fail-main user admin-profile profile-view">
                <div class="featured inner">
                  <div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                      <div class="modal-content">
                        <div class="forget-pass">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                          <div class="modal-body">
                            <div class="payment-main-popup">
                              <form action="">
                                <div class="payment-modal-main">
                                  <div class="payment-modal-inner">
                                    <div class="row">
                                      <div class="col-12">
                                        <h1 class="text-center">Please login first to continue subscription.</h1>
                                      </div>
              
                                      <div class="col-12 text-center">
                                      <a href="{{url('/login')}}" class="btn-a bkappnmt can" >Login</a>
                                      </div>
              
                                    </div>
              
                                  </div>
                                </div>
                              </form>
                            </div>
              
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="modal fade" id="alreadySubscribed" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                      <div class="modal-content">
                        <div class="forget-pass">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                          <div class="modal-body">
                            <div class="payment-main-popup">
                              <form action="">
                                <div class="payment-modal-main">
                                  <div class="payment-modal-inner">
                                    <div class="row">
                                      <div class="col-12">
                                        <h1 class="text-center">You are already subcribed. You want to renew package.</h1>
                                      </div>
              
                                      <div class="col-12 text-center">
                                      <a role="button" onclick="subscribeNew()" class="btn-a bkappnmt can" >Subscribe new package</a>
                                      </div>
              
                                    </div>
              
                                  </div>
                                </div>
                              </form>
                            </div>
              
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
           
        </div>
    </div>
      
@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
    <script src="{{ asset('js/package.js') }}"></script>
@endsection
