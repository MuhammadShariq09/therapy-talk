@extends('frontend.layout')
@section('title','FAQs | Clinical Psychologist Online Therapy | Emotion Therapy ')
@section('description','Our therapists at My Therapy Talk focus on emotion based therapy to ensure the wellbeing of anyone in need of mental therapy. Consult our clinical psychologist today!')
@section('body-col',"2-columns")
@section('css')
@endsection
@section('content')
<div class="about-us">
   <!--------------------------banner start------------------------>
   <section class="inner-page-banner">
      <!-------------------header section---------------------------->
      @include('frontend.include.topbanner')
      <!-----------------header end--------------------------->
      <!----------------banner content start-------------->
      <section class="banner-content">
         <div class="container">
            <div class="row wow fadeInDown" data-wow-duration="0.5s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 0.5s; animation-delay: 0.5s; animation-name: fadeInDown;">
               <div class="col-md-7 col-sm-12 ">
                  <h1>FAQ</h1>
               </div>
               <div class="col-md-5 col-sm-12"></div>
            </div>
         </div>
      </section>
      <!----------------banner content end-------------->
   </section>
   <!----------------banner end---------------------->
   <section class="accordian-main">
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <div class="accordian-one">
                  <h1 class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 0.8s; animation-delay: 0.5s; animation-name: fadeInUp;">Frequently Asked Questions</h1>
                  <p class="accordian-top-p">Below is a list of a few frequently asked questions. If there are further questions in your mind, don’t hesitate to contact us.</p>
                  <div class="accordion_container">
                     <div class="accordian-content">
                        <div class="accordion_head">
                           <p>What is My Therapy Talk?</p>
                           <span class="plusminus">+</span>
                        </div>
                        <div class="accordion_body" style="display: none;">
                           <p>It is an online therapy platform for people who cannot seek counseling in a traditional office. It addresses mental health care and helps members online through video, chat, audio, and text.</p>
                        </div>
                     </div>
                     <div class="accordian-content">
                        <div class="accordion_head">
                           <p>What is the role of My Therapy Talk?</p>
                           <span class="plusminus">+</span>
                        </div>
                        <div class="accordion_body" style="display: none;">
                           <p>Therapy Talk helps you deal with a range of problems, including depression, stress, anxiety, relationships, addictions, and self-esteem affordably and easily. Help can be accessed anytime, anywhere.</p>
                        </div>
                     </div>
                     <div class="accordian-content">
                        <div class="accordion_head">
                           <p>Who will be helping me?</p>
                           <span class="plusminus">+</span>
                        </div>
                        <div class="accordion_body" style="display: none;">
                           <p>After signing up, your licensed counselor will be assigned to you based on your preferences and the type of issues you are dealing with. Different counselors have different approaches and areas of focus, so it's essential to find the right person who can obtain the best results for you. If at any point, you feel it isn't a good fit, you may switch to a different counselor.</p>
                        </div>
                     </div>
                     <div class="accordian-content">
                        <div class="accordion_head">
                           <p>Who are the counselors?</p>
                           <span class="plusminus">+</span>
                        </div>
                        <div class="accordion_body" style="display: none;">
                           <p>All are counselors and therapists are licensed, trained, experienced, and accredited psychologists (Ph.D./ PsyD), licensed marriage and family therapists (LMFT), licensed clinical social workers (LCSW) or licensed professional counselors (LPC or LPCC). All of them have a Master’s Degree or a Doctoral Degree in their field. They have been certified by their state's professional board after completing the necessary education, exams, training, and practice. They all possess at least three years of experience.</p>
                        </div>
                     </div>
                     <div class="accordian-content">
                        <div class="accordion_head">
                           <p>Can I trust you with my information?</p>
                           <span class="plusminus">+</span>
                        </div>
                        <div class="accordion_body" style="display: none;">
                           <p>Of course. You can sign up with any name you want which will recognize you in the system. As soon as your counseling process is initiated, your emergency contact information will be requested, which is kept safe with us in our system. It can be accessed by the therapist only if we think you or someone else is in danger.</p>
                        </div>
                     </div>
                     <div class="accordian-content">
                        <div class="accordion_head">
                           <p>How does online counseling work?</p>
                           <span class="plusminus">+</span>
                        </div>
                        <div class="accordion_body" style="display: none;">
                           <p>You sign up as a member, fill out the questionnaire, get the therapist, and start communicating whenever and wherever you want.</p>
                        </div>
                     </div>
                     <div class="accordian-content">
                        <div class="accordion_head">
                           <p>Can I unsubscribe?</p>
                           <span class="plusminus">+</span>
                        </div>
                        <div class="accordion_body" style="display: none;">
                           <p>Canceling your subscription can quickly be done online. You can cancel future subscription payments online by following these steps: log in to your account, go to “Billing Settings,” then “Quit Counseling.” Your account will be closed, and you will see a message box stating, “You have unsubscribed.”</p>
                        </div>
                     </div>
                     <div class="accordian-content">
                        <div class="accordion_head_2">
                           <p>What is the pricing?</p>
                           <span class="plusminus">+</span>
                        </div>
                        <div class="accordion_body_2" style="display: none;">
                           <p>Therapy pricing ranges from $30 to $100 per month and includes all your messaging, chats, phone, and video sessions.</p>
                        </div>
                     </div>
                     <div class="accordian-content">
                        <div class="accordion_head_2">
                           <p>How long will my therapist take to reply to my message?</p>
                           <span class="plusminus">+</span>
                        </div>
                        <div class="accordion_body_2" style="display: none;">
                           <p>The speed at which the therapist responds depends on the interaction between you and your therapist. Some people have short, frequent interactions monthly, while others may have long, thorough, and less regular sessions.</p>
                        </div>
                     </div>
                     <div class="accordian-content">
                        <div class="accordion_head_2">
                           <p>Is there a way to stay anonymous?</p>
                           <span class="plusminus">+</span>
                        </div>
                        <div class="accordion_body_2" style="display: none;">
                           <p>When you sign up, your full name is not required. You can pick any name which will identify you in our system.</p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>

</div>
@endsection('content')
@section('js')
<script>
    $(document).ready(function() {
    //toggle the component with class accordion_body
    $(".accordion_head").click(function() {
      if ($('.accordion_body').is(':visible')) {
        $(".accordion_body").slideUp(300);
        $(".plusminus").text('+');
      }
      if ($(this).next(".accordion_body").is(':visible')) {
        $(this).next(".accordion_body").slideUp(300);
        $(this).children(".plusminus").text('+');
        $(this).children(".plusminus").removeClass('customMinus');
      } else {
        $(this).next(".accordion_body").slideDown(300);
        $(this).children(".plusminus").text('-');
        $(this).children(".plusminus").addClass('customMinus');
      }
    });
    });

    $(document).ready(function() {
    //toggle the component with class accordion_body
    $(".accordion_head_2").click(function() {
      if ($('.accordion_body_2').is(':visible')) {
        $(".accordion_body_2").slideUp(300);
        $(".plusminus").text('+');
      }
      if ($(this).next(".accordion_body_2").is(':visible')) {
        $(this).next(".accordion_body_2").slideUp(300);
        $(this).children(".plusminus").text('+');
        $(this).children(".plusminus").removeClass('customMinus');
      } else {
        $(this).next(".accordion_body_2").slideDown(300);
        $(this).children(".plusminus").text('-');
        $(this).children(".plusminus").addClass('customMinus');
      }
    });
    });
</script>
@endsection
