@extends('frontend.layout')
@section('title','Building Healthy Self-Esteem')
@section('body-col',"2-columns")
@section('css')
@endsection
@section('content')
<div class="blog-detail">
   <!--------------------------banner start------------------------>
   <section class="inner-page-banner">
      <!-------------------header section---------------------------->
      @include('frontend.include.topbanner')
      <!-----------------header end--------------------------->
      <!----------------banner content start-------------->
		<section class="banner-content">
		<div class="container">
		  <div class="row wow fadeInDown" data-wow-duration="0.5s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 0.5s; animation-delay: 0.5s; animation-name: fadeInDown;">
		    <div class="col-md-7 col-sm-12 ">
		      <h1>Blog</h1>
		    </div>
		    <div class="col-md-5 col-sm-12"></div>
		  </div>
		</div>
		</section>
      <!----------------banner content end-------------->
   </section>
   <!----------------banner end---------------------->
<section class="match">
      <div class="container">
        <a href="#">
          <div class="video-sec">
            <img src="{{asset('site/images/box2.png')}}">
          </div>
        </a>
        <div class="mission-vision">
          <h2 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.5s; animation-name: fadeInUp;">Building Healthy Self-Esteem</h2>
          <h3 class="wow fadeInUp animated" data-wow-duration="1s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.5s; animation-name: fadeInUp;">Oct 1, 2019</h3>
          <p class="wow fadeInUp animated" data-wow-duration="1s" data-wow-delay="0.9s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.9s; animation-name: fadeInUp;">What does it mean when people ask you to build your self-esteem? Healthy self-esteem is essential for a person to achieve his goals and purposes in life. Self-esteem plays a major role in every aspect of life be it an achievement or a failure. It is the feeling of being happy with your traits and capabilities.
          </p>
          <p class="text-center"><span>It is about balance, acceptance, and respect towards your self.</span></p>
          <p class="text-center"><span>Why It’s Important</span></p>
          <p>
              Positive self-esteem is essential for every individual because it is based on our opinions and beliefs about ourselves, which sometimes might be a challenge to change. It is about knowing who we are and leading life in peace with ourselves without desiring anyone’s approval. When we are satisfied and don’t care about what others say or think, it is easier to live a happier and successful life. Happier self helps relax cells and helps us control stress, which leads to an extended harmonious life.
            <br>
            <br>
          It’s not hard to break a person’s self-esteem than to regain it once it's wounded. There are many ways to rebuild it if you are an individual who can manage it.
          <br>
          <br>
         <span>Never compare yourself to others</span> if you don’t want to shatter your self-esteem. Comparing yourself to another individual will not make anything about you better, particularly your self-esteem. It is best to remember that what others choose to share about their lives isn't the full picture and comparing yourselves to them isn't realistic.
          <br>
          <br>
        <span>Do not allow contradictory ideas to disturb your peace of mind.</span> When they start to arrive, it is crucial to notify yourself that you are a valuable individual. Saying positive things to yourself will make you feel more comfortable the more you do it. Being affected by someone’s negative thoughts and ideas will give them the power to have control over your thoughts and emotions.
          <br>
          <br>
         <span>Learning new things can increase your self-confidence.</span> Think about the things that you always wanted to do and would like to discover more, or certain things that you believe you'd relish doing. By signing up for those things, you will not only divert your mind but will be exposed to new adventures that will elevate your confidence and build your self-esteem.
          <br>
          <br>
         <span>Surrounding yourself with positivity</span> is always a good idea instead of putting too much weight on negatives. It will automatically lift your mood and reduce stress.
          <br>
          <br>
         Although every one functions differently, a lot of the times, these methods work for people. Building self-esteem needs some effort and works on your part, but it will be worth it. You will notice the change in yourself and feel better as you start to look at yourself differently. You can always give an attempt to what you’re comfortable with, without putting too much pressure on yourself.</p>
        </div>
        <div class="footer footer-4 wow fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.5s" style="visibility: hidden; animation-duration: 0.6s; animation-delay: 0.5s; animation-name: none;">
                <h6>Share this post</h6>

                <ul class="social-icon">
                  <li><a href="#"><i class="fa fa-facebook-f"></i></a></li>
                  <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                  <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-vimeo" aria-hidden="true"></i></a></li>
                </ul>
              </div>
      </div>
    </section>


</div>
@endsection('content')
@section('js')

@endsection
