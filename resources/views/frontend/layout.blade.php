<!DOCTYPE html>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>{{config('app.name') }} - @yield('title')</title>
    <meta name="description" content="@yield('description')">
    <link rel="canonical" href="https://www.mytherapytalk.com/">

    <link rel="shortcut icon" href="{{URL::asset('assets/admin/images/favicon.ico')}}?v=1.0">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <input id="url" type="hidden" value="{{ url('/')}}" >
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="google-site-verification" content="ZvjUgGHJn_UDtR5ecwEsZmzaCXfMiLs-59aBkYi_nVA" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Roboto+Slab&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Barlow:300,400,500,600,700&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('site/css/owl.theme.default.min.css') }}">
  <link rel="stylesheet" href="{{ asset('site/css/owl.carousel.min.css') }}">
  <link rel="stylesheet" href="{{ asset('site/css/animate.css') }}">
  <link rel="stylesheet" href="{{ asset('site/css/slicknav.css') }}" />
  <link rel="stylesheet" type="text/css" href="{{ asset('site/css/style.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('/assets/css/u_style.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('site/css/responsive.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('site/css/responsive-web.css') }}">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-154884619-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-154884619-1');
</script>
  <style>.pointer { cursor: pointer; }</style>
  <style>
    .sticky {
      position: fixed;
      top: 0;
      width: 100%;
       background-color: #bac1c1;
     z-index: 999;
     padding-bottom: 30px;
     }
     .sticky #menu ul li.active a{
        color:#fff;
     }
     </style>
      <style>
        .blockUI.blockMsg.blockElement {
            width: 100px !important;
            height: 100px !important;
            border-radius: 50%;
            color: transparent !important;
            background-color: transparent !important;
            transform: translateX(-50%) translateY(-50%);
            top: 50% !important;
            left: 50% !important;
            border-color: white !important;
            border-top-color: #00b7d5 !important;
            animation: spinner 1.2s ease-in-out infinite;
        }

        @keyframes  spinner {
            0% {
                transform: translateX(-50%) translateY(-50%) rotate(0deg);
            }
            100% {
                transform: translateX(-50%) translateY(-50%) rotate(360deg);
            }
        }
    </style>
  @yield('css')
</head>
<body>
    @yield('content')
    @include('frontend.include.footer')
    <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.6.2/modernizr.min.js"></script>
    <script src="{{ asset('site/js/owl.carousel.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.6.2/modernizr.min.js"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="{{ asset('site/js/function.js') }}"></script>
    <script src="{{ asset('site/js/datatables.min.js') }}"></script>
    <script src="{{ asset('site/js/jquery.slicknav.min.js') }}"></script>
    <script src="{{ asset('site/js/wow.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js"></script>

    <script>
      var baseUrl = "{{url('/')}}";
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
      function redirect(path){
          window.location = baseUrl + '/' + path;
      }
      $('#subscribe').on('click', function(){
            $.ajax({
                url: baseUrl + "/subscribe",
                data: {email: $('#email').val() },
                type: 'post',
                success: function(response){
                    toastr.success(response.message,'Success');
                    $('#email').val('');
                },
                error: function(error){
                    toastr.error(error.responseJSON.error,'Error');
                }
            })
      });
      $(document).ready(function() { $('#menu').slicknav(); });
      new WOW().init();
      jQuery( '#home' ).owlCarousel( {
        loop: true,
        margin: 0,
        autoplay: true,
        autoplayTimeout: 3000,
        autoplayHoverPause: false,
        responsiveClass: true,
        responsive: {
          0: {
            items: 1,
            nav: false
          },
          600: {
            items: 1,
            nav: false
          },
          1000: {
            items: 1,
            nav: false,
          }
        }
      } );
    </script>
    <script>
    
      function markRead(id=""){
        var url = $('#url').val() + '/markasread/'+id;
        $.ajax({
            url: url,
            type: 'get',
            success: function(response){
              console.log(response.url);
              toastr.success(response.message, 'Success');
              setTimeout(function () {
                  window.location.href = response.url;
              }, 1000);
            }
        })
      }
      </script>
      <script>
        window.onscroll = function() {myFunction()};
        
        var header = document.getElementById("myHeader");
        var sticky = header.offsetTop;
        
        function myFunction() {
         if (window.pageYOffset > sticky) {
           header.classList.add("sticky");
         } else {
           header.classList.remove("sticky");
         }
        }
        
        </script>
    @yield('js')
    <!--Start of Tawk.to Script-->
<script type="text/javascript">
  var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
  (function(){
  var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
  s1.async=true;
  s1.src='https://embed.tawk.to/5ebd9a66967ae56c5219deff/default';
  s1.charset='UTF-8';
  s1.setAttribute('crossorigin','*');
  s0.parentNode.insertBefore(s1,s0);
  })();
</script>
<!--End of Tawk.to Script-->
  </body>
  </html>
