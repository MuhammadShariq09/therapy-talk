@extends('layouts.therapist')
@section('title', ' Register')
@section('body-class',"vertical-layout vertical-menu 2-columns menu-expanded  modal-open")
@section('body-col',"2-column")
@section('content')
<section class="register loginn">
<div class="container"></div>
<div class="login-fail-main user admin-profile profile-view loginn reg-form">
    <div class="featured inner ">
    <div class="modal" style="display: block" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
    <div class="forget-pass register-form">
    <div class="modal-body">
    <div class="payment-main-popup">
        <div action="">
            <div class="payment-modal-main">
                <div class="">
                    <div class="row">
                        <div class="col-12">
                            <h3 style="text-align: left;font-size: 25px;">Introduction</h3>
                            <p>Welcome to My Therapy Talk. If you continue to use this website, you are agreeing to comply with and be bound by the following terms and conditions of use, which together with our privacy policy govern My Therapy Talk relationship with you in relation to this website. If you disagree with any part of these terms and conditions, please do not use our website.
                                The term ‘My Therapy Talk’ or ‘us’ or ‘we’ refer to the owner of the website.<br> The term ‘you’ ‘customer’ or ‘user’ refers to the user of our website.</p>
                            <h3 style="text-align: left;font-size: 25px;">Third Party</h3>
                            <p>The use of this website is subject to the following terms of use: <br>
                                The content of the pages of this website is for your general information and use only. It is subject to change without notice.<br>
                                Neither we nor any third parties provide any warranty or guarantee as to the accuracy, timeliness, performance, completeness or suitability of the information and materials found or offered on this website for any particular purpose. You acknowledge that such information and materials may contain inaccuracies or errors, and we expressly exclude liability for any such inaccuracies or errors to the fullest extent permitted by law.</p>
                            <h3 style="text-align: left;font-size: 25px;">Using My Therapy Talk</h3>
                            <p>We are not responsible for any financial losses or any other damages caused whatsoever. When using My Therapy Talk you agree to and understand that you will not:</p>
                            <ul>
                                <li>Break or violate any laws</li>
                                <li>Post anything that breaks our rules</li>
                                <li>Infringe any third party right</li>
                                <li>Distribute viruses or harm anything of our interest</li>
                                <li>Respect all users who use the website</li>
                            </ul>
                            <p>By using our site you agree that you are above the age of 18 or have the legal consent to therapy or have the consent of a parent or guradian. <br>
                                My Therapy Talk grant you limited access and use to the website and users must not download, modify or copy any part of our website including information and content. The user agrees not to collect or store any product information or resell, copy, distribute or sell any part of our website or software. <br>
                                To use our site and services you are required to create an account with My Therapy Talk and provide certain personal information. You must only create one account and only create an account for yourself. If you are creating an account for someone else, you must obtain permission from them beforehand.<br>
                                By creating an acoount with us you agree that you will provide accurate and complete information. If My Therapy Talk have reason to believe that your account information is incorrect, false inaccurate or incomplete we may transition you to other service providers.</p>
                            <h3 style="text-align: left;font-size: 25px;">Content</h3>
                            <p>You are completely responsible for the material and content you submit to our website. We are not to be held liable for any content posted nor are we to be held responsible. <br>
                                We may monitor and record traffic to our site and collect the following information including but not limited to the IP address of your computer and the website which referred you to our website. We do this, so we can make ongoing improvements on our site based on what our customers actions are.<br>
                                All content including but not limited to pictures, videos and text are copyrighted by My Therapy Talk unless stated. The pictures on our website for visual representation only.<br>
                                You may not copy, reproduce, distribute or store any material on our website unless given express written permission by us.<br>
                                Do not use this service for medical or emergency needs. If you experience a medical emergency please get in touch with a professional or your local health services.<br>
                                You should not constitute any of the advice as professional medical advice and should always consult a licensed professional.</p>
                            <h3 style="text-align: left;font-size: 25px;">Personal Information</h3>
                            <p>We may collect and store your personal information for reasons such as improving our site and making the user experience better. We may use them for advertisement purposes. We will never sell, rent or give your personal data to any third party company without your consent.<br>
                                If we do, we will only collect personal identification information from users only if they themselves submit such information to us. Users can refuse to supply such information but it may stop them from using certain sites or accessing specific parts of our website.<br>
                                We are not to be held responsible for any breaches to our security caused by third party. Keeping your personal information is of the utmost importance to us however we are not to be held liable for any data breaches.<br>
                                This website contains material which is owned by or licensed to us. This material includes, but is not limited to, the design, layout, look, appearance and graphics. Reproduction is prohibited other than in accordance with the copyright notice, which forms part of these terms and conditions.<br>
                                All trademarks reproduced in this website, which are not the property of, or licensed to the operator, are acknowledged on the website.<br>
                                If you opt to sign up to our newsletter we may use your email address to send you information about our products or services. You do have the option to opt out of our mailing list and you can ask for personal data to stop being recorded at any time.<br>
                                We reserve the right to disclose user information and personal information to law enforcement or authorities should we feel the need to. <br>
                                The user agrees to be bound by the My Therapy Talk privacy policy which are available here.<br>
                                My Therapy Talk are not liable for any failure or delay in performance or its obligations which is due to the result of any act, event, terrorism act, acts of God, governmental regulations, war, riots, fire, flood or anything else which is beyond our control. <br>
                                We may suspend your access to our service at any time for any or no reason at all. </p>
                            <h3 style="text-align: left;font-size: 25px;">General</h3>
                            <p>Unauthorized use of this website may give rise to a claim for damages and/or be a criminal offense.<br>
                                This website may also include links to other websites. These links are provided for your convenience to provide further information. They do not signify that we endorse the website(s). We have no responsibility for the content of the linked website(s).<br>
                                We do not accept any liability or responsibility for any delays caused by third parties. Your use of this website and any dispute arising out of such use of the website is subject to the laws of your specific country.<br>
                                This website is owned and operated by My Therapy Talk <br>
                                PO Box 928876 <br>
                                San Diego, CA 92192   </p>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>


</section>


@endsection
