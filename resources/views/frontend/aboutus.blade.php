@extends('frontend.layout')
@section('title','Anxiety & Depression Therapy Services | Treat Depression')
@section('description','My Therapy Talk offers the best online therapy services. We give you the solution to anxiety, depression, marriage and family online through our online services.')
@section('body-col',"2-columns")
@section('css')
@endsection
@section('content')
<div class="about-us">
<!--------------------------banner start------------------------>
<section class="inner-page-banner">
   <!-------------------header section---------------------------->
      @include('frontend.include.topbanner')
   <!-----------------header end--------------------------->
   <!----------------banner content start-------------->
   <section class="banner-content">
      <div class="container">
         <div class="row wow fadeInDown" data-wow-duration="0.5s" data-wow-delay="0.5s">
            <div class="col-md-7 col-sm-12 ">
               <h1>About Us</h1>
            </div>
            <div class="col-md-5 col-sm-12"></div>
         </div>
      </div>
   </section>
   <!----------------banner content end-------------->
</section>
<!----------------banner end---------------------->
<!-----------------my therapy start here------------>
<section class="myTherapy">
   <div class="container">
      <div class="row">
         <div class="col-md-5 col-sm-12 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.9s">
            <h1>Seeking to <br><span class="grey-text">Enhance Your </span><br><span class="blue-text">  Health and Mind?</span></h1>
         </div>
         <div class="col-md-7 col-sm-12 align-self-center wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.9s">
            <p>My Therapy Talk was established in June 2019 as an online therapy platform for people who could not seek counseling in a traditional office. Based in California, USA, My Therapy Talk addresses mental health care; helping members online through video, chat, audio, and text. Catering to all people, including, couples, groups, students, teenagers, as well as various modalities, we initiated a platform focused on helping individuals towards achieving their true selves.</p>
         </div>
      </div>
   </div>
</section>
<!-----------------my therapy end here------------>
<!-----------------sub therapy start here------------>
<section class="subTherapy">
   <div class="container">
      <div class="row wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.9s">
         <div class="col-lg-4 col-md-6 col-sm-12 d-flex">
            <div class="inner-box">
               <div>
                  <h3>Effective Online Therapy</h3>
                  <p>So many members have been helped through online therapy</p>
               </div>
               <div>
                  <img src='{{ asset("site/images/ic1.png") }}' class="img-fluid img-1" >
               </div>
            </div>
         </div>
         <div class="col-lg-4 col-md-6 col-sm-12 d-flex">
            <div class="inner-box">
               <div>
                  <h3>Many Qualified Licensed Therapists</h3>
                  <p>Be matched with the right therapist to address your needs</p>
               </div>
               <div>
                  <img src='{{ asset("site/images/ic2.png") }}' class="img-fluid" >
               </div>
            </div>
         </div>
         <div class="col-lg-4 col-md-6 col-sm-12 d-flex">
            <div class="inner-box">
               <div>
                  <h3>Numerous Satisfied Members</h3>
                  <p>Visit My Therapy Talk to gain a new perspective through the best therapeutic methods</p>
               </div>
               <div><img src='{{ asset("site/images/ic3.png") }}' class="img-fluid"></div>
            </div>
         </div>
      </div>
   </div>
</section>
<!-----------------sub therapy end here------------>
<section class="match">
   <div class="container">
      <a href="#" onclick="lightbox_open();">
         <div class="video-sec">
                <!--<video width="100%" poster=""  controls>-->
                <!--  <source src="{{ asset("site/videos/my theraphy_finalcut.mp4") }}" type="video/mp4">-->
                <!--  Your browser does not support HTML5 video.-->
                <!--</video>-->

            <img src='{{ asset("site/images/video-image2.jpg") }}'>
         </div>
      </a>
      <div class="light_main">
        <div id="light">
          <a class="boxclose" id="boxclose" onclick="lightbox_close();"></a>
          <video id="VisaChipCardVideo" width="100%" controls>
              <source src="{{ asset('site/videos/my theraphy_finalcut.mp4') }}" type="video/mp4">
              <!--Browser does not support <video> tag -->
            </video>
        </div>
        </div>    
        <div id="fade" onClick="lightbox_close();"></div>
      <div class="mission-vision">
         <h2 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">Mission & Vision</h2>
         <h3>We Take Your Recovery Seriously</h3>
         <p class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.9s">My Talk Therapy intends to create a safe space for people suffering from mental health illnesses to enhance their quality of lives. We realized that more online therapy platforms should be available to help people who were not able to seek counseling in a traditional office. Therefore, we established a platform to support an initiative focused on helping individuals. We offer the same kind of professionalism and guidance that you would expect from an in-office therapist for individuals at affordable rates.
            <br>
            <br>
            Based in California, USA, My Therapy Talk addresses mental health care; helping members online through video, chat, audio, and text that is affordable and easy to access anytime anywhere. Catering to all people including, couples, groups, students, teenagers, as well as various modalities, My Therapy Talk helps you deal with a range of problems including depression, stress, anxiety, relationships, addictions, and self-esteem.
            <br>
            <br>
            We are passionate about revolutionizing mental health care by finding the right professionals for suitable treatments. My Therapy Talk wants to guide the people struggling with challenges by listening to them and supporting as many people as possible. We aim to create a positive change and continuously strive towards the wellbeing and empowerment of the community.
         </p>
      </div>
   </div>
</section>
<!----------------------------signuptoday start here--------------->
@include('frontend.include.join')


<!-----------------Notified end here---------------->
@endsection('content')
@section('js')
<script>
    window.document.onkeydown = function(e) {
  if (!e) {
    e = event;
  }
  if (e.keyCode == 27) {
    lightbox_close();
  }
}

function lightbox_open() {
  var lightBoxVideo = document.getElementById("VisaChipCardVideo");
  window.scrollTo(0, 0);
  document.getElementById('light').style.display = 'block';
  document.getElementById('fade').style.display = 'block';
  lightBoxVideo.play();
}

function lightbox_close() {
  var lightBoxVideo = document.getElementById("VisaChipCardVideo");
  document.getElementById('light').style.display = 'none';
  document.getElementById('fade').style.display = 'none';
  lightBoxVideo.pause();
}
</script>

@endsection
