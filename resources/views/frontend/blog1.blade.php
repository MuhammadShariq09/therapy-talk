@extends('frontend.layout')
@section('title','Depression: A Quick Guide')
@section('body-col',"2-columns")
@section('css')
@endsection
@section('content')
<div class="blog-detail">
   <!--------------------------banner start------------------------>
   <section class="inner-page-banner">
      <!-------------------header section---------------------------->
      @include('frontend.include.topbanner')
      <!-----------------header end--------------------------->
      <!----------------banner content start-------------->
		<section class="banner-content">
		<div class="container">
		  <div class="row wow fadeInDown" data-wow-duration="0.5s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 0.5s; animation-delay: 0.5s; animation-name: fadeInDown;">
		    <div class="col-md-7 col-sm-12 ">
		      <h1>Blog</h1>
		    </div>
		    <div class="col-md-5 col-sm-12"></div>
		  </div>
		</div>
		</section>
      <!----------------banner content end-------------->
   </section>
   <!----------------banner end---------------------->
<section class="match">
      <div class="container">
        <a href="#">
          <div class="video-sec">
            <img src="{{asset('site/images/box1.png')}}">
          </div>
        </a>
        <div class="mission-vision">
          <h2 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.5s; animation-name: fadeInUp;">Depression: A Quick Guide</h2>
          <h3 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.5s; animation-name: fadeInUp;">Oct 1, 2019</h3>
          <p class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.9s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.9s; animation-name: fadeInUp;">With more and more people suffering from mental health issues today, depression is the most predominant among them. The occurrence of mental illnesses has widely increased over the last decade due to our lifestyles. More and more people are stressed with no time for relaxation. Depression, however, can be a consequence of many reasons. It can set in as a result of lack of relaxation, loss of someone close, post-pregnancy, failures, a dissolved marriage, and many more. 
          <br>
          <br>
          Significantly affecting emotional health, a depressed state of mind can have a vital impact on physical health as well. Individuals diagnosed with depression suffer severely with more extended periods of despondency and sadness. This state can be transferred into making poor decisions for the future, making it worse. Different genders and ages experience depression differently. However, some people are unable to recognize these feelings as a mental health problem and refuse to seek help.
          <br>
          <br>
         Curing depression is possible, and it can be killed through availing mental health care. Mental health care is revolved around the idea of instilling positive energy within individuals and overpowering positivity over negativity. It is for people who find it hard to manage the arising difficulties in life; therefore, the helplessness and despair takes charge and makes it tough to lead a healthy, meaningful life. However, like any other illness, such imbalanced state can be dealt with, if taken proper care.
          <br>
          <br>
         <span>A good support network</span> is crucial. People suffering from depression will not be able to drag themselves out of it. Intervention often plays a major role where family and friends boost up confidence and self-esteem. Most of the times, support and guidance work perfectly in getting the person out of the depressed state.
          <br>
          <br>
          <span>Mental health care treatment</span> is necessary. While few seek help for a breakthrough, the majority tend to hide it to stay away from the stigma that can be problematic in many areas of life. Mental health care is essential if the person wants to restore his health and save himself timely, while waiting to get to the lowest point to visit a therapist or a counselor. Therapy can help the person speak his heart out that may result in the overcoming of his sad state.
          <br>
          <br>
           <span>Meditation</span> is also a part of the mental health care to obtain a balanced viewpoint. Performing meditation as a regular practice affects the brain positively and helps manage stress and anxiety that leads to depression. Meditation has been found to change certain regions of the brain that are precisely linked to depression.
          <br>
          <br>
            <span>Proper nutrition</span> is often a factor not paid heed to. Mental health care covers the aspect and importance of nutrition. Moods and emotions are a result of what you consume. When experiencing an unhappy state of mind, the person generally tends to overlook the diet and avoid taking meals, which further worsens the health.</p>
        </div>
        <div class="footer footer-4 wow fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.5s; animation-name: fadeInUp;">
                <h6>Share this post</h6>

                <ul class="social-icon">
                  <li><a href="#"><i class="fa fa-facebook-f"></i></a></li>
                  <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                  <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-vimeo" aria-hidden="true"></i></a></li>
                </ul>
              </div>
      </div>
    </section>
    

</div>
@endsection('content')
@section('js')

@endsection
