@extends('frontend.layout')
@section('title','Join MTT')
@section('body-col',"2-columns")
@section('css')
@endsection
@section('content')
<div class="blog-detail">
   <!--------------------------banner start------------------------>
   <section class="inner-page-banner">
      <!-------------------header section---------------------------->
      @include('frontend.include.topbanner')
      <!-----------------header end--------------------------->
      <!----------------banner content start-------------->
		<section class="banner-content">
		<div class="container">
		  <div class="row wow fadeInDown" data-wow-duration="0.5s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 0.5s; animation-delay: 0.5s; animation-name: fadeInDown;">
		    <div class="col-md-7 col-sm-12 ">
		      <h1>Blog</h1>
		    </div>
		    <div class="col-md-5 col-sm-12"></div>
		  </div>
		</div>
		</section>
      <!----------------banner content end-------------->
   </section>
   <!----------------banner end---------------------->
<section class="match ">
      <div class="container">
        <a href="#">
          <div class="video-sec">
            <img src="{{asset('site/images/box6.png')}}">
          </div>
        </a>
        <div class="mission-vision">
          <h2 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.5s; animation-name: fadeInUp;">Join MTT</h2>
          <h3 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.5s; animation-name: fadeInUp;">Nov 11, 2019</h3>
          <p><span>Your Personal Virtual Office.</span></p>
          <p><span>Be a part of MyTherapyTalk platform to join thousands of therapists!</span></p>

          <p>Having high-quality therapists in our community with their attention, professionalism, skills, and desire to help people can retain our purpose and values forward of a meaningful life for everyone.</p>

          <p><span>Why MyTherapyTalk?</span></p>
          <p><span>Volume</span></p>
          <p>With thousands of members having access to MyTherapyTalk, they can search the platform and seek help from counselors in tackling their challenges and obstacles in life.</p>
          <p><span>Flexibility</span></p>
          <p>By joining MyTherapyTalk, it can be a key source or a full-time source, or serve as a beneficial supplement to your current work. You will have the power to choose how many people you want to work with as well as selecting clients.</p>
<p>With MyTherapyTalk, communication takes place when two or more individuals connect at a different or same time without being present physically. The time will be decided by the member or the therapist.</p>
<p><span>Requirements</span></p>
         
            <ul>
              <li>Must be licensed or registered by a state board to offer counseling (e.g., LCSW, LMFT, LPC, PsyD or related field).</li>
              <li>If you are a registered or intern counselor, you are required to submit a copy of your supervisor’s authorization form with a pledge to supervise, a copy of your supervisor’s current state license, and a copy of malpractice insurance. (You must be aware of the laws in your state regarding supervision. Please be in accordance with your state board of psychology to confirm if practicing online is allowed. My Therapy Talk will not be responsible.)</li>
              <li>MyTherapyTalk does not accept counselors subjected to school, career and substance abuse, as well as business coaches, life coaches and registered nurses (except for an added mental health license and/or credential).</li>
              <li>Must have experience in individual counseling for adults and/or group counseling.</li>
              <li>Musty have excellent writing skills.</li>
              <li>Must have a reliable internet connection.</li>
              <li>Must be a US resident.</li>
              </ul>
          
<p>Note: Counselors are not MyTherapyTalk employees.<span> Counselors are independent providers.</span>
You can avail our monthly membership to manage your practice and bill your clients directly on MyTherapyTalk’s platform.
</p>
        </div>
        <div class="footer footer-4 wow fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.5s" style="visibility: hidden; animation-duration: 0.6s; animation-delay: 0.5s; animation-name: none;">
                <h6>Share this post</h6>

                <ul class="social-icon">
                  <li><a href="#"><i class="fa fa-facebook-f"></i></a></li>
                  <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                  <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-vimeo" aria-hidden="true"></i></a></li>
                </ul>
              </div>
      </div>
    </section>
    
    


</div>
@endsection('content')
@section('js')

@endsection
