@extends('frontend.layout')
@section('title','Feeling Overwhelmed During the Coronavirus Crisis? Get Help by Getting Online Therapy')
@section('body-col',"2-columns")
@section('css')
<style>
  .dot {
    height: 10px;
    width: 10px;
    background-color: #bbb;
    border-radius: 50%;
    display: inline-block;
  }
  </style>
@endsection
@section('content')
<div class="blog-detail">
   <!--------------------------banner start------------------------>
   <section class="inner-page-banner">
      <!-------------------header section---------------------------->
      @include('frontend.include.topbanner')
      <!-----------------header end--------------------------->
      <!----------------banner content start-------------->
		<section class="banner-content">
		<div class="container">
		  <div class="row wow fadeInDown" data-wow-duration="0.5s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 0.5s; animation-delay: 0.5s; animation-name: fadeInDown;">
		    <div class="col-md-7 col-sm-12 ">
		      <h1>Blog</h1>
		    </div>
		    <div class="col-md-5 col-sm-12"></div>
		  </div>
		</div>
		</section>
      <!----------------banner content end-------------->
   </section>
   <!----------------banner end---------------------->
<section class="match">
      <div class="container">
        <a href="#">
          <div class="video-sec">
            <img src="{{asset('site/images/blog6.jpg')}}">
          </div>
        </a>
        <div class="mission-vision">
          <h2 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.5s; animation-name: fadeInUp;">Feeling Overwhelmed During the Coronavirus Crisis? <span style="color: #00b7d5;"> Get Help by Getting Online Therapy.</span></h2>
          <h3 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.5s; animation-name: fadeInUp;">April 17, 2020</h3>
          <p class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.9s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.9s; animation-name: fadeInUp;">It is an entirely different experience altogether to be talking to someone about your innermost feelings, yet this person isn’t sitting in front of you physically but rather virtually. Given the current situation, if you are looking to start online therapy or if your therapist has moved their practice to a digital platform, it can be quite a nuisance for some individuals. However, you can still make the most of it by considering a few tips to make your transition easier to teletherapy or, in simple words, online mental therapy via video call. 
          <br>
          <br>
          <span>Find a quiet and safe corner for your self and set a particular time for your therapy.</span>
          <br>
          <br>
          One of the biggest benefits of online therapy is that you can do it at any time of your choice. Although this approach is not exactly recommended because it is impossible to find a distraction-free environment at all times and therapy requires space without interferences as it is a rigorous task for the therapist. Try to find the right time and space and make sure you don’t miss out on your online mental health therapy sessions.
          <br>
          <br>
          <span>You might feel some awkwardness at first</span>
          <br>
          <br>
           Don’t feel alarmed if you don’t think that you and your therapist are in-sync at the very start of the therapy because no matter how tech-savvy you or your therapist are, it will not be the same experience as in-person counseling. It can also be tempting to think that because there is some initial discomfort, the therapy itself isn’t working. Some frustration, fear, and sadness are to be expected during these online sessions, but you can talk to your therapist about it. 
          <br>
          <br>
          <span>Take advantage of the unique parts of teletherapy</span>
          <br>
          <br>
          There are a few things that you can do with online therapy that you can’t do with in-person therapy. For instance, you cannot bring your pet, who might be a source of comfort to you, with yourself into your in-person therapy sessions, but you can do that during an online therapy session. 
          <br>
          <br>
          You can use your online medium to share articles that might have resonated with you, which might help you and your therapist better understand and cater to each other’s needs during an online therapy session. Also, the various online-tools available can help you to make online therapy a lot more engaging. It can also be a way for you to learn to practice specifying your feelings more explicitly, as there is a lack of bodily cues in an online session. 
          <br>
          <br>
         
          <a href="https://www.mytherapytalk.com/">Online mental health counseling services</a> can be a powerful tool to help you with your mental health during these trying times of self-isolation. Don’t be afraid to try something new and be vocal about your needs while meeting your therapist halfway and working together with him/her. Fight coronavirus from your homes and take care of your mental health. 
          <br>
          <br>
          
          <br>
          
         
          </p>
        </div>
        <div class="footer footer-4 wow fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.5s; animation-name: fadeInUp;">
                <h6>Share this post</h6>

                <ul class="social-icon">
                  <li><a href="#"><i class="fa fa-facebook-f"></i></a></li>
                  <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                  <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-vimeo" aria-hidden="true"></i></a></li>
                </ul>
              </div>
      </div>
    </section>
    

</div>
@endsection('content')
@section('js')

@endsection
