@extends('frontend.layout')
@section('title','Online Mental Therapy & Counseling Services | My Therapy Talk')
@section('description','Talk to an online licensed therapist and get mental health therapy at an affordable cost. Enhance your mental health through our mental health counseling services.')
@section('body-col',"2-columns")
@section('css')

@endsection
@section('content')
    <section class="banner">
    <!-------------------header section---------------------------->
        @include('frontend.include.topbanner')

{{--    <header class="main-top-banner">--}}
{{--        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">--}}
{{--      <div class="container"><!--container-->--}}
{{--        <div class="row">--}}
{{--          <div class="col-md-12">--}}
{{--            <div class="btn-menu wow fadeInDown" data-wow-duration="0.5s" data-wow-delay="0.3s">--}}
{{--              <ul>--}}
{{--                <li>--}}
{{--                           <a href="{{ url('talk-to-mtt') }}">Join MTT</a>--}}
{{--                </li>--}}
{{--                <!-- <li>--}}
{{--                  <a href="register.html">Register</a>--}}
{{--                </li>--}}
{{--                <li>--}}
{{--                  <a href="#">Login</a>--}}
{{--                </li> -->--}}
{{--              </ul>--}}
{{--            </div>--}}
{{--          </div>--}}
{{--        </div>--}}
{{--        <div class="row"><!--row-->--}}
{{--          <div class="col-md-3 col-sm-12"><!--col-1-->--}}
{{--            <a href="{{ url('/') }}"><img src='{{ asset("site/images/logo.png") }}' alt="logo"  class="img-fluid"/></a>--}}
{{--          </div>--}}
{{--          <!--col1-->--}}
{{--          <div class="col-md-9 col-sm-12 align-self-center"><!--col-2-->--}}
{{--            <div id="menu"><!--mnu bar-->--}}
{{--              <ul>--}}
{{--                @include('frontend.include.nav')--}}
{{--              </ul>--}}
{{--            </div>--}}
{{--            <!--mnu bar-->--}}
{{--          </div>--}}
{{--          <!--col2-->--}}
{{--        </div>--}}
{{--        <!--row-->--}}
{{--      </div>--}}
{{--      <!--container-->--}}
{{--    </header>--}}

    <!-----------------header end--------------------------->

    <!----------------banner content start-------------->
    <section class="banner-content">
      <div class="container">
        <div class="row wow fadeInDown" data-wow-duration="0.5s" data-wow-delay="0.5s">
          <div class="col-md-7 col-sm-12 ">
            <h1>My Therapy Talk<br> A Space for Retrieval</h1>
<!--            <p>Move Beyond to A New and Greater you with My Therapy Talk.</p>-->
            <a href="#" onclick="lightbox_open();" class="btn">Learn More <i class="fa fa-play-circle" aria-hidden="true"></i></a>

            <!--<button type="button" class="btn">Video Overview <i class="fa fa-play-circle" aria-hidden="true"></i></button>-->
          </div>
          <div class="col-md-5 col-sm-12"></div>
        </div>
      </div>
    </section>

    <div class="light_main">
        <div id="light">
          <a class="boxclose" id="boxclose" onclick="lightbox_close();"></a>
          <video id="VisaChipCardVideo" width="100%" controls>
              <source src="{{ asset('site/videos/my theraphy_finalcut.mp4') }}" type="video/mp4">
              <!--Browser does not support <video> tag -->
            </video>
        </div>
        </div>
        <div id="fade" onClick="lightbox_close();"></div>
    <!----------------banner content end-------------->
  </section>
    <section class="myTherapy">
    <div class="container">
      <div class="row">
        <div class="col-md-5 col-sm-12 wow fadeInLeft" data-wow-duration="0.9" data-wow-delay="0.9s">
          <h2>Welcome to <br>
           <span>My  Therapy Talk</span></h2>
          </div>
          <div class="col-md-7 col-sm-12 align-self-center wow fadeInRight" data-wow-duration="0.9" data-wow-delay="0.9s">
            <p>As an online therapy platform, we ensure people’s goals to seek wellness regardless of how busy their schedule is. We offer therapeutic hope to individuals that help them move forward and give them the ability to cope up with the obstacles holding them back. My Therapy Talk understands that emotions can come at any particular time and that help can be needed and offered anytime, anywhere; therefore, our licensed therapists can help you whenever you need them in making you feel better. My Therapy Talk will efficiently help you move beyond to a new and greater you.</p>
          </div>
        </div>
      </div>
    </section>
    <section class="subTherapy">
   <div class="container">
      <div class="row wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.9s">
         <div class="col-lg-4 col-md-6 col-sm-12 d-flex">
            <div class="inner-box">
               <div>
                  <h3>Effective Online Therapy</h3>
                  <p>So many members have been helped through online therapy</p>
               </div>
               <div>
                  <img src='{{ asset("site/images/ic1.png") }}' alt="" class="img-fluid img-1" >
               </div>
            </div>
         </div>
         <div class="col-lg-4 col-md-6 col-sm-12 d-flex">
            <div class="inner-box">
               <div>
                  <h3>Many Qualified Licensed Therapists</h3>
                  <p>Be matched with the right therapist to address your needs</p>
               </div>
               <div>
                  <img src='{{ asset("site/images/ic2.png") }}'  alt="" class="img-fluid" >
               </div>
            </div>
         </div>
         <div class="col-lg-4 col-md-6 col-sm-12 d-flex">
            <div class="inner-box">
               <div>
                  <h3>Numerous Satisfied Members</h3>
                  <p>Visit My Therapy Talk to gain a new perspective through the best therapeutic methods</p>
               </div>
               <div><img src='{{ asset("site/images/ic3.png") }}' alt="" class="img-fluid"></div>
            </div>
         </div>
      </div>
   </div>
</section>

    <section class="licensed ">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-12 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.5s">
            <h2>Meet Our Licensed Therapists</h2>
            <p>When you sign up, you enter the most prominent space of licensed therapists who are anxiously waiting to help you. Our therapists are highly trained to guide you towards the road to recovery. Whether you are struggling with depression, anxiety, stress, addiction, self-esteem, or relationships, we are just a message away! We offer the same kind of professionalism you would expect from an in-office therapist for individuals at affordable rates.</p>
            @if(!Auth::guard('therapist')->check())
            <a href="{{ route('user.find-a-therapist') }}" class="new-btn-a">Find a Therapist</a>
            @endif
            <!--<button class="btn" type="button">Match a Therapist</button>-->
          </div>
          <div class="col-md-6 col-sm-12 wow bounceIn" data-wow-duration="1s" data-wow-delay="0.5s" > <img src='{{ asset("site/images/meet-right-img.png") }}' alt="" class="img-fluid"> </div>
        </div>
      </div>
    </section>
    <section class="match">
      <div class="container">
        <h4 class="wow fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.5s" >We'll match you to a counselor that can help with...</h4>
        <div class="row wow fadeInDown" data-wow-duration="0.8s" data-wow-delay="0.5s">
          <div class="col-md-4 col-sm-12 ">
            <a href="{{ route('user.find-a-therapist') }}">
              <div class="box"> <img src='{{ asset("site/images/box1.png") }}' alt="" class="img-fluid">
                <h5>Depression</h5>
              </div>
            </a>
          </div>
          <div class="col-md-4 col-sm-12">
            <a href="{{ route('user.find-a-therapist') }}">
              <div class="box"> <img src='{{ asset("site/images/box2.png") }}' alt="" class="img-fluid">
                <h5>Stress</h5>
              </div>
            </a>
          </div>
          <div class="col-md-4 col-sm-12">
            <a href="{{ route('user.find-a-therapist') }}">
              <div class="box"> <img src='{{ asset("site/images/box3.png") }}' alt="" class="img-fluid">
                <h5>Anxiety</h5>
              </div>
            </a>
          </div>
        </div>
        <div class="row wow fadeInDown" data-wow-duration="0.8s" data-wow-delay="0.8s">
          <div class="col-md-4 col-sm-12">
            <a href="{{ route('user.find-a-therapist') }}">
              <div class="box"> <img src='{{ asset("site/images/box4.png") }}' alt="" class="img-fluid">
                <h5>Addiction</h5>
              </div>
            </a>
          </div>
          <div class="col-md-4 col-sm-12">
            <a href="{{ route('user.find-a-therapist') }}">
              <div class="box"> <img src='{{ asset("site/images/box5.png") }}' alt="" class="img-fluid">
                <h5>Relationships</h5>
              </div>
            </a>
          </div>
          <div class="col-md-4 col-sm-12">
            <a href="{{ route('user.find-a-therapist') }}">
              <div class="box"> <img src='{{ asset("site/images/box6.png") }}' alt="" class="img-fluid">
                <h5>Self-Esteem</h5>
              </div>
            </a>
          </div>
        </div>
      </div>
      <section class="work">
        <div class="container">
          <h4 class="wow fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.5s">How does it work?</h4>
          <div class="workbg">
            <div class="row">
              <div class="col-md-4 col-12">
                <div class="image-box">
                  <img src='{{ asset("site/images/wk-icon1.png") }}' alt="" class="img-fluid wow rotateIn" data-wow-duration="0.5s" data-wow-delay="0.8s">
                  <h3>Answer A Few Questions</h3>
                  <p>To get the best therapist to fulfill your requirements, all you need to do is answer a few questions. You are in the most prominent online community of proficient and licensed providers.</p>
                </div>
              </div>

              <div class="col-md-4 col-12">
                <div class="image-box">
                  <img src='{{ asset("site/images/wk-icon2.png") }}' alt="" class="img-fluid wow rotateIn" data-wow-duration="0.5s" data-wow-delay="0.8s">
                  <h3>Start Communication</h3>
                  <p>After entering the network, you can talk to your therapist however you feel comfortable. The options available are messaging, chat, phone, or video.</p>
                </div>
              </div>
              <div class="col-md-4 col-12">
                <div class="image-boxx">
                  <img src='{{ asset("site/images/wk-icon3.png") }}' alt="" class="img-fluid wow rotateIn" data-wow-duration="0.5s" data-wow-delay="0.8s">
                  <h3>Get Ready to Feel Better</h3>
                  <p>You can then message your therapist whenever you feel the need. You can get the connection via phone, tablet, or computer, and start a session.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </section>
    @include('frontend.include.join')
    <section class="testimonial">
      <div class="container">
        <div class="row">
          <div class="offset-md-1  col-md-10">
            <div class="owl-carousel owl-theme" id="home">
              <div class="item"> <img class="img-fluid" alt="" src='{{ asset("site/images/user-img.png") }}'>
                <h5>Michael A</h5>
                <h6><br></h6>
                <p>My therapist is extremely thoughtful and considerate. She is the one person who I can trust to listen to me whenever I need someone. I always feel better after sharing my feelings with someone, and I am glad I have her for that purpose. My Therapy Talk has taught me how to be okay.</p>
              </div>
              <div class="item"> <img class="img-fluid" alt="" src='{{ asset("site/images/user-img.png") }}'>
                <h5>Anthony H</h5>
                <h6><br></h6>

                <p>I love how My Therapy Talk is an online platform where we can ask for help anywhere and anytime. It has helped me so much. My random attacks make me feel so helpless, and my counselor always guides me. I not only feel better after talking to him but so much happier and lighter too. They help me find solutions to my problems and help me deal with them for the future</p>
              </div>
              <div class="item"><img class="img-fluid" alt="" src='{{ asset("site/images/user-img.png") }}'>
                <h5>Brenton J</h5>
                <h6><br></h6>

                <p>I am the kind of a person who likes to keep her emotions bottled up, which often leads to more stress and anxiety. Often, I did not understand how to deal with it. I found My Therapy Talk very helpful and finally felt heard and hopeful when my life felt quite terrible.</p>
              </div>
              <div class="item"><img class="img-fluid" alt="" src='{{ asset("site/images/user-img.png") }}'>
                <h5>Lizette V</h5>
                <h6><br></h6>
                <p>“My therapist treated me so personable. I’ve been dealing with a heavy load with my divorce and being a single mother. It has been so helpful to have access to my therapist when I am feeling depressed and experiencing anxiety. This platform is so easy to navigate and use”</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>



@endsection('content')
@section('js')
<script>
    window.document.onkeydown = function(e) {
  if (!e) {
    e = event;
  }
  if (e.keyCode == 27) {
    lightbox_close();
  }
}

function lightbox_open() {
  var lightBoxVideo = document.getElementById("VisaChipCardVideo");
  window.scrollTo(0, 0);
  document.getElementById('light').style.display = 'block';
  document.getElementById('fade').style.display = 'block';
  lightBoxVideo.play();
}

function lightbox_close() {
  var lightBoxVideo = document.getElementById("VisaChipCardVideo");
  document.getElementById('light').style.display = 'none';
  document.getElementById('fade').style.display = 'none';
  lightBoxVideo.pause();
}
</script>

 
  
@endsection
