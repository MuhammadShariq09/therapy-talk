@extends('frontend.layout')
@section('title','Contact')
@section('body-col',"2-columns")
@section('css')
@endsection
@section('content')
<div class="about-us">
   <!--------------------------banner start------------------------>
   <section class="inner-page-banner">
      <!-------------------header section---------------------------->
      @include('frontend.include.topbanner')
      <!-----------------header end--------------------------->
      <!----------------banner content start-------------->
		<section class="banner-content">
		<div class="container">
		  <div class="row wow fadeInDown" data-wow-duration="0.5s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 0.5s; animation-delay: 0.5s; animation-name: fadeInDown;">
		    <div class="col-md-7 col-sm-12 ">
		      <h1>Contact</h1>
		    </div>
		    <div class="col-md-5 col-sm-12"></div>
		  </div>
		</div>
		</section>
      <!----------------banner content end-------------->
   </section>
   <!----------------banner end---------------------->
<section class="contact-main">
      <div class="container">
        <div class="row">
          <div class="col-lg-7 col-sm-12">
            <div class="row">
              <div class="col-lg-12 col-xl-12 col-sm-12">
                <h1 class="wow fadeInUp" data-wow-duration="0.9s" data-wow-delay="0.4s" style="visibility: visible; animation-duration: 0.9s; animation-delay: 0.4s; animation-name: fadeInUp;">Get in Touch</h1>
                <p class="wow fadeInUp" data-wow-duration="0.9s" data-wow-delay="0.6s" style="visibility: visible; animation-duration: 0.9s; animation-delay: 0.6s; animation-name: fadeInUp;">Need Help in Facing Challenges? Move Beyond to A New and Greater you with My Therapy Talk. Let us offer a path to a happier and empowered you!</p>
                <p>At My Therapy Talk, you always have the support you need from one of our therapists.</p>
              </div>
         

              <div class="col-lg-12 col-xl-12 col-sm-12">
                <form action="{{ url('submit-query') }}" method="post" class="wow fadeInLeft" data-wow-duration="0.9s" data-wow-delay="0.6s" style="visibility: visible; animation-duration: 0.9s; animation-delay: 0.6s; animation-name: fadeInLeft;">
                    <input type="hidden" name="type" value="contact">
                    @csrf
                    
                    @if(Session::has('message'))
                        <div class="alert alert-success">
                            <strong>{{ Session::get('message')  }}</strong>
                        </div>
                    @endif                    
                    
                  <div class="row">
                    <div class="col-12 form-group">
                      <label><i class="fa fa-user-circle"></i> Name</label>
                      <input type="text" name="name" required="required" spellcheck="true" class="form-control" placeholder="Enter Name" autocomplete="off" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAAAXNSR0IArs4c6QAAAPhJREFUOBHlU70KgzAQPlMhEvoQTg6OPoOjT+JWOnRqkUKHgqWP4OQbOPokTk6OTkVULNSLVc62oJmbIdzd95NcuGjX2/3YVI/Ts+t0WLE2ut5xsQ0O+90F6UxFjAI8qNcEGONia08e6MNONYwCS7EQAizLmtGUDEzTBNd1fxsYhjEBnHPQNG3KKTYV34F8ec/zwHEciOMYyrIE3/ehKAqIoggo9inGXKmFXwbyBkmSQJqmUNe15IRhCG3byphitm1/eUzDM4qR0TTNjEixGdAnSi3keS5vSk2UDKqqgizLqB4YzvassiKhGtZ/jDMtLOnHz7TE+yf8BaDZXA509yeBAAAAAElFTkSuQmCC&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%;">
                    </div>
                    
                    <div class="col-12 form-group">
                      <label><i class="fa fa-envelope"></i> Email</label>
                      <input type="email" name="email" required="required" spellcheck="true" class="form-control" placeholder="Enter Email">
                    </div>
                    
                    <div class="col-12 form-group">
                      <label><i class="fa fa-file-text" aria-hidden="true"></i>Subject</label>
                      <input type="text" name="subject" required="required" spellcheck="true" class="form-control" placeholder="Enter Subject">
                    </div>
                    <div class="col-12 form-group">
                      <label><i class="fa fa-comments" aria-hidden="true"></i>Message</label>
                      <textarea type="text" name="message" required="required" class="form-control" placeholder="Enter message here"></textarea>
                    </div>
                    <div class="col-6 form-group">
                      <button type="submit" class="can" data-dismiss="modal">Send</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            
          </div>
          <div class="col-lg-5 col-sm-12">
            <div class="row contact-info-main">
              <div class="col-lg-12 col-xl-12 col-sm-12">
                <h1 class="wow fadeInUp" data-wow-duration="0.9s" data-wow-delay="0.4s" style="visibility: visible; animation-duration: 0.9s; animation-delay: 0.4s; animation-name: fadeInUp;">Reach out with any questions you have</h1>
              </div>
              <div class="col-lg-12 col-xl-12 col-sm-12">
                <div class="contact-info wow fadeInRight" data-wow-duration="0.9s" data-wow-delay="0.6s" style="visibility: visible; animation-duration: 0.9s; animation-delay: 0.6s; animation-name: fadeInRight;">
                  <ul>
                    <li><i class="fa fa-map-marker"></i> Address<br><a href="https://www.google.com/maps/place/San+Diego,+CA+92192,+USA/@32.8511698,-117.2240367,15z/data=!3m1!4b1!4m5!3m4!1s0x80dc00dd5c602cc1:0xd822249bf92f9ad2!8m2!3d32.8511526!4d-117.2152819%22" target="_blank">My Theraphy Talk<br>PO Box 928876<br> San Diego, CA 92192</a></li>
                    <li><i class="fa fa-phone" aria-hidden="true"></i> Call us directly<br><a href="Tel:888-544-5865">888-544-5865</a></li>
                    <li><i class="fa fa-envelope" aria-hidden="true"></i> Contact email<br><a href="mailto:support@mytherapytalk.com">support@mytherapytalk.com</a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
	


</div>
@endsection('content')
@section('js')

@endsection
