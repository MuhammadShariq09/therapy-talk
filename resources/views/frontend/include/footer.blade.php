    <section class="notified">
       <div class="container">
          <div class="row">
             <div class="col-md-4 col-sm-12">
                <h3>Be Notified</h3>
                <p>Stay updated to know more about us. We hope to be of help to you in any way possible.</p>
             </div>
             <div class="col-md-5 col-sm-12 align-self-center">
                <form>
                   <div class="form-group">
                      <input type="email" class="form-control" id="email" placeholder="Enter email to receive our newsletter" name="email" required="" autocomplete="off" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR4nGP6zwAAAgcBApocMXEAAAAASUVORK5CYII=&quot;);">
                   </div>
                </form>
             </div>
             <div class="col-md-3 col-sm-12 align-self-center">
                <button type="button" id="subscribe" class="btn">Subscribe</button>
             </div>
          </div>
          <!--row-->
       </div>
    </section>

    <footer>
      <div class="container">
        <div class="footer-top">
          <div class="row">
            <div class="col-md-3 col-sm-6 col-12">
              <div class="footer footer-1"> 
              <a href="{{ url('/') }}"><img src="{{ asset("site/images/footer-logo.png") }}" class="img-fluid" alt=""></a>
               <p>Disclaimer: If you are in a life-threatening situation, do not use this site. Call the National Suicide Prevention Lifeline, a free, 24-hour hotline, at 1-800-273-8255. Your call will be routed to the crisis center near you. If your issue is an emergency, call 911 or go to your nearest emergency room. </p>
              </div>
            </div>
            <div class="col-md-3 col-sm-6 col-12">
              <div class="footer footer-2">
                <h6>QUICK LINKS</h6>
                <ul>
                  <li><a href="{{url('/')}}">Home</a></li>
                  <li><a href="{{url('/about-us')}}">About Us</a></li>
                  <li><a href="{{url('/pricing')}}">Pricing</a></li>
                  <li><a href="{{url('/how-it-works')}}">How It Works</a></li>
                  <li><a href="{{url('/faq')}}">FAQs</a></li>
                  <li><a href="{{url('/blogs')}}">Blogs</a></li>
                  <li><a href="{{url('/contact-us')}}">Contact</a></li>
                </ul>
              </div>
            </div>
            <div class="col-md-3 col-sm-6 col-12">
              <div class="footer footer-3">
                <h6>Company</h6>
                <ul>
                  <li>My Therapy Talk<br>PO Box 928876<a href="https://www.google.com/maps/place/San+Diego,+CA+92192,+USA/@32.8511698,-117.2240367,15z/data=!3m1!4b1!4m5!3m4!1s0x80dc00dd5c602cc1:0xd822249bf92f9ad2!8m2!3d32.8511526!4d-117.2152819" target="_blank" ><br>
                    San Diego, CA 92192</a></li>
                </ul>
              </div>
            </div>
            <div class="col-md-3 col-sm-6 col-12">
              <div class="footer footer-4">
                <h6>Contact Us</h6>
                <ul>
                  <li>Support Chat</li>
                  <li><a href="tel:888-544-5865">888-544-5865</a></li>
                  <ul class="social-icon">
                    <li><a href="https://www.facebook.com/MyTherapyTalk/"><i class="fa fa-facebook-f"></i></a></li>
                    <li><a href="https://www.youtube.com/channel/UCjqwvbrd7-bVpEZfk1Qzbeg"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                    <li><a href="https://www.instagram.com/mytherapytalk/"><i class="fa fa-instagram"></i></a></li>
                    <li><a href="https://twitter.com/MyTherapyTalk1"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                  </ul>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="footer-bottom">
        <p>© My Therapy Talk 2019, All Rights Reserved</p>
      </div>
    </footer>
