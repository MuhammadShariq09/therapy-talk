<li class="{{ (Request::segment(1) == '') ? 'active' : '' }}"><a href="{{ url('/') }}"><i class="fa fa-home" aria-hidden="true"></i>  </a></li>
<li class="{{ (Request::segment(1) == 'about-us') ? 'active' : '' }}"><a href="{{ url('about-us') }}">About Us </a></li>
<li class="{{ (Request::segment(1) == 'pricing') ? 'active' : '' }}"><a href="{{ url('pricing') }}">Pricing </a></li>
<li class="{{ (Request::segment(1) == 'how-it-works') ? 'active' : '' }}"><a href="{{ url('how-it-works') }}">How It Works</a></li>
<li class="{{ (Request::segment(1) == 'faq') ? 'active' : '' }}"><a href="{{ url('faq') }}">FAQs</a></li>
<li class="{{ (Request::segment(1) == 'blogs') ? 'active' : '' }}"><a href="{{ url('blogs') }}">Blogs</a></li>
<li class="{{ (Request::segment(1) == 'contact-us') ? 'active' : '' }}"><a href="{{ url('contact-us') }}">Contact</a></li>