@if(auth()->user())
    <header class="main-top-banner" id="myHeader" >
    <div class="container"><!--container-->
        <div class="row">
            <div class="col-md-12">
                <div class="header-top-right">
                    <ul class="right-main">
                        <li>
                            <div class="dropdown notification-dropdown">
                                <a class="dropdown-toggle" href="#" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img src="{{ asset('images/bell-icon-g.png') }}" class="bell-g">
                                    <img src="{{ asset('images/bell-icon.png') }}" class="bell-w"><span class="badge">{{ auth()->user()->unreadNotifications->count() }}</span>
                                </a>
                                @if(auth()->user()->unreadNotifications->count() > 0)
                                <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                                    <li class="dropdown-menu-header">
                                        <h6 class="dropdown-header m-0"> <span class="grey darken-2">Notifications</span> <span class="notification-tag badge-default badge-danger float-right m-0">{{ auth()->user()->unreadNotifications->count() }} New</span> </h6>
                                    </li>
                                    <li class="scrollable-container media-list ps-container ps-theme-dark ps-active-y" data-ps-id="cbae8718-1b84-97ac-6bfa-47d792d8ad89">
                                        @foreach (auth()->user()->unreadNotifications as $notification)
                                        <a onclick="markRead('{{$notification->id}}')" role="button">
                                            <div class="media">
                                                <div class="media-left align-self-center"><i class="fa fa-bell" aria-hidden="true"></i>
                                                </div>
                                                <div class="media-body">
                                                    <p class="notification-text font-small-3 text-muted">{{ $notification->data['message'] }}</p>
                                                    <small>
                                                        <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($notification->created_at))->diffForHumans() }}</time>
                                                    </small>
                                                </div>
                                            </div>
                                        </a>
                                        @endforeach
                                    </li>
                                    <li class="dropdown-menu-footer"><a onclick="markRead()" role="button" class="dropdown-item text-muted text-center" {{ \Carbon\Carbon::createFromTimeStamp(strtotime($notification->created_at))->diffForHumans() }}>Read all notifications</a></li>
                                </ul>
                                @endif
                            </div>
                        </li>
                     
                        <li class="profile-menu">
                            <div class="avatar-img">
                                <img src="{{ auth()->user()->image }}" alt="{{ucfirst(Auth::user()->name)}}" onerror="this.style.display='none'">
                            </div>
                            <div class="dropdown">
                                <a class="dropdown-toggle" href="#" id="dropdownMenuLink" data-toggle="dropdown"
                                   aria-haspopup="true" aria-expanded="false">
                                    {{  auth()->user()->name }}
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                                    <a class="dropdown-item" href="{{ route('user.profile') }}"><i class="fa fa-chevron-right" aria-hidden="true"></i>My Profile</a>
                                    @if(auth()->user()->is_assessment == 0)
                                    <a class="dropdown-item" href="{{ route('user.assessment-form') }}"><i class="fa fa-chevron-right" aria-hidden="true"></i>Fill Assessment</a>
                                    @elseif(auth()->user()->assessment->last() && auth()->user()->assessment->last()->status ==2)
                                    <a class="dropdown-item" href="{{ route('user.assessment-form') }}"><i class="fa fa-chevron-right" aria-hidden="true"></i>Fill Assessment</a>
                                    @elseif(auth()->user()->assessment->last() && auth()->user()->assessment->last()->status ==1)
                                        <a class="dropdown-item" href="{{ route('user.session.logs') }}">
                                            <i class="fa fa-chevron-right" aria-hidden="true"></i>My Sessions</a>
                                        <a class="dropdown-item" href="{{ route('user.appointments.logs') }}">
                                            <i class="fa fa-chevron-right" aria-hidden="true"></i>My Appointments</a>
                                        @php $user=auth()->user(); $user->load('subscriptions'); @endphp 
                                            @if($user->subscriptions)
                                            <a class="dropdown-item" href="{{ route('user.subscription-logs') }}">
                                                    <i class="fa fa-chevron-right" aria-hidden="true"></i>My Subscriptions</a>
                                            @endif
                                    @else

                                   
                                    @endif
                                    <a onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="dropdown-item" href="{{ route('logout') }}"><i class="fa fa-chevron-right" aria-hidden="true"></i>Logout</a>
                                    {{-- <a class="dropdown-item" href="#"><i class="fa fa-chevron-right" aria-hidden="true"></i>Logout</a> --}}
                                    
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>

            </div>
        </div>
        <div class="row"><!--row-->
            <div class="col-md-3 col-sm-12"><!--col-1-->
            <a href="{{ url('/') }}">
                <img src="{{ asset('images/logo-w.png') }}" alt="logo" class="img-fluid logo-w">
                <img src="{{ asset('images/logo.png') }}" alt="logo" class="img-fluid logo-g">
            </a>
            </div>
            <!--col1-->
            <div class="col-md-9 col-sm-12 align-self-center"><!--col-2-->
                <div id="menu"><!--mnu bar-->
                    @include('includes.user.nav')
                </div>
                <!--mnu bar-->
            </div>
            <!--col2-->
        </div>
        <!--row-->
    </div>
    <!--container-->
</header>
    @else
    <header id="myHeader" class="main-top-banner">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <div class="container">
            <!--container-->
            <div class="row">
                <div class="col-md-12">
                    <div class="btn-menu wow fadeInDown" data-wow-duration="0.5s" data-wow-delay="0.3s" style="visibility: visible; animation-duration: 0.5s; animation-delay: 0.3s; animation-name: fadeInDown;">
                        <ul>
                            <li>
                               <a href="mailto:support@mytherapytalk.com">Support@mytherapytalk.com</a>
                            </li>
                            @if(!Auth::guard('therapist')->check())
                            <li>
                                <a href="{{ url('assessment-form') }}">Become a Member</a>
                            </li>
                             <li>
                                <a href="{{ route('therapist.login') }}">Therapist</a>
                            </li>
                            <li>
                                <a href="{{ url('login') }}">Login</a>
                            </li>
                          
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <!--row-->
                <div class="col-md-3 col-sm-12">
                    <!--col-1-->
                    <a href="{{ url('/') }}">
                        <img src="{{ asset('images/logo-w.png') }}" alt="logo" class="img-fluid logo-w">
                <img src="{{ asset('images/logo.png') }}" alt="logo" class="img-fluid logo-g">
                    </a>
                </div>
                <!--col1-->
                <div class="col-md-9 col-sm-12 align-self-center">
                    <!--col-2-->
                    <div id="menu">
                        <!--mnu bar-->
                        <ul>
                            @include('frontend.include.nav')
                        </ul>
                    </div>
                    <!--mnu bar-->
                </div>
                <!--col2-->
            </div>
            <!--row-->
        </div>
        <!--container-->
    </header>
<script>
window.onscroll = function() {myFunction()};

var header = document.getElementById("myHeader");
var sticky = header.offsetTop;

function myFunction() {
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky");
  } else {
    header.classList.remove("sticky");
  }
}
	
</script>
<script>
window.onscroll = function() {myFunction()};

var header = document.getElementById("myHeader2");
var sticky = header.offsetTop;

function myFunction() {
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky");
  } else {
    header.classList.remove("sticky");
  }
}
	
</script>
<style>
.sticky {
  position: fixed;
  top: 0;
  width: 100%;
   background-color: #bac1c1;
	z-index: 999;
	padding-bottom: 30px;
}
.sticky	#menu ul li.active a{
    color:#fff;
}
	
</style>
@endif
