@extends('frontend.layout')
@section('title','How Effective Is Online Counseling & Therapy?')
@section('body-col',"2-columns")
@section('css')
<style>
  .dot {
    height: 10px;
    width: 10px;
    background-color: #bbb;
    border-radius: 50%;
    display: inline-block;
  }
  </style>
@endsection
@section('content')
<div class="blog-detail">
   <!--------------------------banner start------------------------>
   <section class="inner-page-banner">
      <!-------------------header section---------------------------->
      @include('frontend.include.topbanner')
      <!-----------------header end--------------------------->
      <!----------------banner content start-------------->
		<section class="banner-content">
		<div class="container">
		  <div class="row wow fadeInDown" data-wow-duration="0.5s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 0.5s; animation-delay: 0.5s; animation-name: fadeInDown;">
		    <div class="col-md-7 col-sm-12 ">
		      <h1>Blog</h1>
		    </div>
		    <div class="col-md-5 col-sm-12"></div>
		  </div>
		</div>
		</section>
      <!----------------banner content end-------------->
   </section>
   <!----------------banner end---------------------->
<section class="match">
      <div class="container">
        <a href="#">
          <div class="video-sec">
            <img src="{{asset('site/images/blog4.jpeg')}}">
          </div>
        </a>
        <div class="mission-vision">
          <h2 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.5s; animation-name: fadeInUp;">How Effective Is Online Counseling & Therapy?</h2>
          <h3 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.5s; animation-name: fadeInUp;">April 10, 2020</h3>
          <p class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.9s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.9s; animation-name: fadeInUp;">A 2018 study published in the Journal of Psychological Disorders found that online cognitive behavioural therapy is <b>“effective, acceptable and practical health care.”</b> 
          <br>
          <br><span>Effectiveness</span>
          while online therapy has its own challenges, it has received support from several patients who have utilized online mental health treatments. Research demonstrates that therapy from a distance can be just as effective as in-person therapy.
          <br>
          <br>
          In a study, researchers revealed that online cognitive-behavioural therapy (CBT) combined with clinical care proved to be effective in the treatment of anxiety, depression, and illness-related emotional distress. In a few cases, the outcome was better through online therapy than those who had traditional in-person CBT.
          <br>
          <br>
          Often, clients want to stick with the person they trust the most, who might not be someone they can visit in person. There are situations where online therapy may even be considered as the best option. For example, some clients may not want to spend the time travelling to a therapist’s office because of their busy schedule, or certain people have difficulty leaving the house due to their mental health issues. Therefore, voice or video chat will be highly suitable for such people without needing to spend additional time on their way, while fulfilling their professional support needs.
          <br>
          <br>
          “In a review of studies published in the journal ‘World Journal of Psychiatry’, patients receiving mental health treatment through video conferencing reported <b>“high levels of satisfaction”</b>.
          <br>
          <br>
          Moreover, <a href="https://www.mytherapytalk.com/"><mark>affordable online counselling and therapy</mark></a>, effective in the treatment of plenty of health issues, is great news for people who live in rural areas where access to mental health services may be limited.
          <br>
          <br>
          The effectiveness of online counselling and therapy can be proved by the following points:
          <br>
          <br>
          <span class="dot"></span>   The security offered by many online therapy sites allowing users to sign up with “nicknames” entice people to take the therapy online. They are more open with hidden identities that make the process more involving and helpful. It becomes easier for them to reveal private information when they’re sharing it online, rather than the traditional face-to-face process.
          <br>
          <br>
          <span class="dot"></span>   It suits many people because most online therapy services cost less than face-to-face treatment.
          <br>
          <br>
          <span class="dot"></span>   People belonging to rural areas or those experiencing transportation difficulties get easier access. Hence, understanding the option of being treated and getting better is available to more people.
          <br>
          <br>
          <span class="dot"></span>   Studies demonstrate that online therapy requires less of a therapist’s time than face-to-face treatment, which means therapists can treat more people online than they can in-person.
          <br>
          <br>
          <span class="dot"></span>   Highly beneficial for individuals with anxiety, especially social anxiety.</li>
         
          <br>
          <br>
          
          Since the relationship between therapists and clients require reliability and trust, online therapy is a valuable way to expand your options. If you decide to talk to an <a href="https://www.mytherapytalk.com/how-it-works"><mark>online licensed therapist</mark></a> remotely, it’s best to make sessions mimic face-to-face interactions as much as possible for an effective outcome.
          <br>
          <br>
          You can choose the most suitable form of communication from the many options available. Reflect on the type of services you require the most – phone therapy, live chats, video chats, audio messaging, or text messaging, and go ahead.
          <br>
          <br>
          </p>
        </div>
        <div class="footer footer-4 wow fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.5s; animation-name: fadeInUp;">
                <h6>Share this post</h6>

                <ul class="social-icon">
                  <li><a href="#"><i class="fa fa-facebook-f"></i></a></li>
                  <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                  <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-vimeo" aria-hidden="true"></i></a></li>
                </ul>
              </div>
      </div>
    </section>
    

</div>
@endsection('content')
@section('js')

@endsection
