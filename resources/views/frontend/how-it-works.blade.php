@extends('frontend.layout')
@section('title','Therapy & Counseling For Men & Women | Relationship Therapy')
@section('description','All you have to do is answer a few questions regarding the therapy you want. Be it for a relationship, substance abuse or for PTSD. We have all the answers.')
@section('body-col',"2-columns")
@section('css')
@endsection
@section('content')
<div class="how-it-works">
   <!--------------------------banner start------------------------>
   <section class="inner-page-banner">

      @include('frontend.include.topbanner')
      <section class="banner-content">
         <div class="container">
            <div class="row wow fadeInDown" data-wow-duration="0.5s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 0.5s; animation-delay: 0.5s; animation-name: fadeInDown;">
               <div class="col-md-7 col-sm-12 ">
                  <h1>How It Works</h1>
               </div>
               <div class="col-md-5 col-sm-12"></div>
            </div>
         </div>
      </section>
      <!----------------banner content end-------------->
   </section>
   <!----------------banner end---------------------->
   <section class="match">
      <section class="work">
         <div class="container">
            <h4 class="wow fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.5s; animation-name: fadeInUp;">How does it work?</h4>
            <div class="workbg">
               <div class="row">
                  <div class="col-md-4 col-12">
                     <div class="image-box">
                        <img src='{{asset("site/images/wk-icon1.png")}}' class="img-fluid wow rotateIn" data-wow-duration="0.5s" data-wow-delay="0.8s" style="visibility: visible; animation-duration: 0.5s; animation-delay: 0.8s; animation-name: rotateIn;">
                        <h3>Answer A Few Questions</h3>
                        <p>To get the best therapist to fulfill your requirements, all you need to do is answer a few questions. You are in the most prominent online community of proficient and licensed providers.</p>
                     </div>
                  </div>
                  <div class="col-md-4 col-12">
                     <div class="image-box">
                        <img src='{{asset("site/images/wk-icon2.png")}}' class="img-fluid wow rotateIn" data-wow-duration="0.5s" data-wow-delay="0.8s" style="visibility: visible; animation-duration: 0.5s; animation-delay: 0.8s; animation-name: rotateIn;">
                        <h3>Start Communication</h3>
                        <p>After entering the network, you can talk to your therapist however you feel comfortable. The options available are messaging, chat, phone, or video.</p>
                     </div>
                  </div>
                  <div class="col-md-4 col-12">
                     <div class="image-boxx">
                        <img src='{{asset("site/images/wk-icon3.png")}}' class="img-fluid wow rotateIn" data-wow-duration="0.5s" data-wow-delay="0.8s" style="visibility: visible; animation-duration: 0.5s; animation-delay: 0.8s; animation-name: rotateIn;">
                        <h3>Get Ready to Feel Better</h3>
                        <p>You can then message your therapist whenever you feel the need. You can get the connection via phone, tablet, or computer, and start a session.</p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
   </section>
   <!-----------------licensed start here------------>
   <section class="licensed ">
      <div class="container">
         <div class="row">
            <div class="col-md-6 col-sm-12 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.5s" style="visibility: hidden; animation-duration: 1s; animation-delay: 0.5s; animation-name: none;">
               <h2>Meet Our Licensed Therapists</h2>
               <p>When you sign up, you enter the most prominent space of licensed therapists who are anxiously waiting to help you. Our therapists are highly trained to guide you towards the road to recovery. Whether you are struggling with depression, anxiety, stress, addiction, self-esteem, or relationships, we are just a message away! We offer the same kind of professionalism that you would expect from an in-office therapist for individuals at affordable rates.</p>
               <!--<button class="btn" type="button">Match a Therapist</button>-->
            </div>
            <div class="col-md-6 col-sm-12 wow bounceIn" data-wow-duration="1s" data-wow-delay="0.5s" style="visibility: hidden; animation-duration: 1s; animation-delay: 0.5s; animation-name: none;"> <img src='{{asset("site/images/meet-right-img.png")}}' class="img-fluid"> </div>
         </div>
      </div>
   </section>
   <!-----------------licensed end here------------>
   <section class="find-out-more work">
      <div class="container">
         <h4 class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.5s" style="visibility: hidden; animation-duration: 0.8s; animation-delay: 0.5s; animation-name: none;">Find Out More?</h4>
         <p class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.8s" style="visibility: hidden; animation-duration: 0.8s; animation-delay: 0.8s; animation-name: none;">My Therapy Talk helps people reach their maximum potential and embrace their true personalities. Through our high-quality therapies and therapists, we wish a meaningful life for everyone and want to create a relationship of trust and loyalty for our customers to believe in us. We offer therapeutic hope to individuals that makes them move forward and give them the ability to cope up with the obstacles that were holding them back.</p>
      </div>
   </section>
   <!----------------testimonial start here -------------------->
    <section class="testimonial">
      <div class="container">
        <div class="row">
          <div class="offset-md-1  col-md-10">
            <div class="owl-carousel owl-theme" id="home">
              <div class="item"> <img class="img-fluid" src='{{ asset("site/images/user-img.png") }}'>
                <h5>Michael A</h5>
                <h6><br></h6>
                <p>My therapist is extremely thoughtful and considerate. She is the one person who I can trust to listen to me whenever I need someone. I always feel better after sharing my feelings with someone, and I am glad I have her for that purpose. My Therapy Talk has taught me how to be okay.</p>
              </div>
              <div class="item"> <img class="img-fluid" src='{{ asset("site/images/user-img.png") }}'>
                <h5>Anthony H</h5>
                <h6><br></h6>

                <p>I love how My Therapy Talk is an online platform where we can ask for help anywhere and anytime. It has helped me so much. My random attacks make me feel so helpless, and my counselor always guides me. I not only feel better after talking to him but so much happier and lighter too. They help me find solutions to my problems and help me deal with them for the future</p>
              </div>
              <div class="item"><img class="img-fluid" src='{{ asset("site/images/user-img.png") }}'>
                <h5>Brenton J</h5>
                <h6><br></h6>

                <p>I am the kind of a person who likes to keep her emotions bottled up, which often leads to more stress and anxiety. Often, I did not understand how to deal with it. I found My Therapy Talk very helpful and finally felt heard and hopeful when my life felt quite terrible.</p>
              </div>
              <div class="item"><img class="img-fluid" src='{{ asset("site/images/user-img.png") }}'>
                <h5>Lizette V</h5>
                <h6><br></h6>
                <p>“My therapist treated me so personable. I’ve been dealing with a heavy load with my divorce and being a single mother. It has been so helpful to have access to my therapist when I am feeling depressed and experiencing anxiety. This platform is so easy to navigate and use”</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
   <!----------------testimonial end here -------------------->
   <!-----------------Notified Start here---------------->

   <!--footer end here-->
</div>
@endsection('content')
@section('js')

@endsection
