@extends('frontend.layout')
@section('title','Talk to My Theraphy Talk')
@section('body-col',"2-columns")
@section('css')
@endsection
@section('content')
<div class="how-it-works">
   <!--------------------------banner start------------------------>
   <section class="inner-page-banner">

      @include('frontend.include.topbanner')
      <section class="banner-content">
         <div class="container">
            <div class="row wow fadeInDown" data-wow-duration="0.5s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 0.5s; animation-delay: 0.5s; animation-name: fadeInDown;">
               <div class="col-md-7 col-sm-12 ">
                  <h1>Join MTT</h1>
               </div>
               <div class="col-md-5 col-sm-12"></div>
            </div>
         </div>
      </section>
      <!----------------banner content end-------------->
   </section>
<section class="contact-main">
      <div class="container">
        <div class="row">
          <div class="col-lg-7 col-sm-12">
            <div class="row">
              <div class="col-xl-12 col-lg-12 offset-lg-4  col-sm-12 text-center">
                <p class="wow fadeInUp" data-wow-duration="0.9s" data-wow-delay="0.6s" style="visibility: visible; animation-duration: 0.9s; animation-delay: 0.6s; animation-name: fadeInUp;">Need Help in Facing Challenges? Move Beyond to A New and Greater you with My Therapy Talk. Let us offer a path to a happier and empowered you!
                  <br>
                At My Therapy Talk, you always have the support you need from one of our therapists.</p>

              </div>


              <div class="col-xl-12  col-lg-12 offset-lg-4 col-sm-12">
                <form action="{{ url('submit-query') }}" method="post" class="wow fadeInLeft" data-wow-duration="0.9s" data-wow-delay="0.6s" style="visibility: visible; animation-duration: 0.9s; animation-delay: 0.6s; animation-name: fadeInLeft;">
                    <input type="hidden" name="type" value="quote">
                    @csrf

                    @if(Session::has('message'))
                        <div class="alert alert-success">
                            <strong>{{ Session::get('message')  }}</strong>
                        </div>
                    @endif

                  <div class="row">
                    <div class="col-12 form-group">
                      <label><i class="fa fa-user-circle"></i> Name</label>
                      <input type="text" name="name" required="required" spellcheck="true" class="form-control" placeholder="Enter Name" autocomplete="off" >
                    </div>
                    
                    <div class="col-12 form-group">
                      <label><i class="fa fa-envelope"></i> Email</label>
                      <input type="email" name="email" required="required" spellcheck="true" class="form-control" placeholder="Enter Email">
                    </div>
                    
                    <div class="col-12 form-group">
                      <label><i class="fa fa-file-text" aria-hidden="true"></i>Contact Number:</label>
                      <input type="number" name="number" required="required" spellcheck="true" class="form-control" placeholder="Enter Contact Number">
                    </div>
                    <div class="col-12 form-group">
                      <label><i class="fa fa-comments" aria-hidden="true"></i>Therapy Reason:</label>
                      <textarea type="text" name="message" required="required" class="form-control" placeholder="Enter message here"></textarea>
                    </div>
                    <div class="col-12 form-group text-center">
                      <button type="submit" class="can" data-dismiss="modal">Send</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            
          </div>
          <div class="col-lg-5 col-sm-12">
            
          </div>
        </div>
      </div>
    </section>


   <!--footer end here-->
</div>
@endsection('content')
@section('js')

@endsection
