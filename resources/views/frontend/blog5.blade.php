@extends('frontend.layout')
@section('title','Reasons for Getting Mental Health Therapy Online')
@section('body-col',"2-columns")
@section('css')
<style>
  .dot {
    height: 10px;
    width: 10px;
    background-color: #bbb;
    border-radius: 50%;
    display: inline-block;
  }
  </style>
@endsection
@section('content')
<div class="blog-detail">
   <!--------------------------banner start------------------------>
   <section class="inner-page-banner">
      <!-------------------header section---------------------------->
      @include('frontend.include.topbanner')
      <!-----------------header end--------------------------->
      <!----------------banner content start-------------->
		<section class="banner-content">
		<div class="container">
		  <div class="row wow fadeInDown" data-wow-duration="0.5s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 0.5s; animation-delay: 0.5s; animation-name: fadeInDown;">
		    <div class="col-md-7 col-sm-12 ">
		      <h1>Blog</h1>
		    </div>
		    <div class="col-md-5 col-sm-12"></div>
		  </div>
		</div>
		</section>
      <!----------------banner content end-------------->
   </section>
   <!----------------banner end---------------------->
<section class="match">
      <div class="container">
        <a href="#">
          <div class="video-sec">
            <img src="{{asset('site/images/blog5.jpg')}}">
          </div>
        </a>
        <div class="mission-vision">
          <h2 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.5s; animation-name: fadeInUp;">Reasons for Getting Mental Health Therapy Online: <span> It Is Right for You.</span></h2>
          <h3 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.5s; animation-name: fadeInUp;">April 13, 2020</h3>
          <p class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.9s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.9s; animation-name: fadeInUp;">Online mental health therapy utilizes technology to improve therapy. This use of technology is a part of a growing shift in the healthcare industry, enabling more convenient, affordable, and on-demand benefits. 
          <br>
          <br>
          If you’re considering getting therapy from an <a href="https://www.mytherapytalk.com/"><mark>online therapy platform</mark></a> and having a tough time deciding if it is right for you, following are some of its positive benefits to help you make the right decision.
          <br>
          <br>
          <span>Comfortable Communication</span>
          <br>
          <br>
          The experience with traditional therapy has been constant since its inception. In essence, you arrive at a location, sit on the couch, and talk to your therapist about any possible or potential struggles in your life. After a week or two, the same process occurs.
          <br>
          <br>
          However, with online therapy, new ways of communication are introduced. It doesn’t require you to leave your comfort zones, either office, or home. If you want a session with your therapist in your pyjamas, it's totally up to you.
          <br>
          <br><span>Types Of Communications</span><br>
          <br>
          <b>You can have a text-based therapy</b>, giving you the freedom to have a therapeutic discussion throughout the day. You get passed the process of waiting for weeks for an appointment, and you get quick and straightforward responses. It’s a good option for the socially anxious as it won’t involve having face-to-face conversations.
          <br>
          <br>
          <b>If you prefer audio</b>, you can easily exchange voice messages with your therapist. It is a convenient choice if you need to get something off your chest immediately or need quick help, along with the comfort of hearing a therapist’s voice without the need to schedule a session.
          <br>
          <br>
          <b>Full video chat sessions are another option</b> for people who want to experience a therapist’s reaction and have them react to body language. You can have that quality face-to-face session entirely on your terms.
          <br>
          <br>
           The security offered by many online therapy sites allowing users to sign up with “nicknames” entice people to take the therapy online. They are more open with hidden identities that make the process more involving and helpful. It becomes easier for them to reveal private information when they’re sharing it online, rather than the traditional face-to-face process.
          <br>
          <br>
          <span>It’s Cost-Effective</span>
          <br>
          <br>
          Today, when many people are struggling for basic coverage before seeking help, <a href="https://www.mytherapytalk.com/"><mark>online mental health counseling services</mark></a> offers an excellent platform for a way to get assistance without hurdles. Online therapy costs less than the insurance co-pay for a weekly traditional therapy session. Even in the case when costs are almost the same, traditional therapy sessions are conducted once or twice a week, and any communication outside those sessions is not possible or will become an extra addition to the cost.
          <br>
          <br>
          Online therapy offers infinite opportunities to contact your therapist without having to pay any additional cost. This enables people to share more and get the right help at the right time.
          <br>
          <br>
          <span>It’s Confidential</span>
          <br>
          <br>
          In some communities, being seen in a therapist’s office will make you a subject of judgment, which is also a common reason why some people refuse therapy.
          <br>
          <br>
          On the other hand, in online therapy, you have an option to keep your name hidden for your satisfaction and privacy. It may not be an issue for some, but it’s an advantage that you can avail of to make yourself feel comfortable with the process. Therapy is always about opening up when you feel it’s right; therefore, you should never feel like you have to share more information than you are comfortable sharing.
         
          <br>
          <br>
          
          Confidentiality and security provided through online therapy also solves the problem in terms of technology and is on par with major world banks. Communications occurred, and discussions conducted between the two parties are fully encrypted. Even in the case of a breach, attackers would not be able to trace the data back to the user.
          <br>
          <br>
         
          </p>
        </div>
        <div class="footer footer-4 wow fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.5s; animation-name: fadeInUp;">
                <h6>Share this post</h6>

                <ul class="social-icon">
                  <li><a href="#"><i class="fa fa-facebook-f"></i></a></li>
                  <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                  <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-vimeo" aria-hidden="true"></i></a></li>
                </ul>
              </div>
      </div>
    </section>
    

</div>
@endsection('content')
@section('js')

@endsection
