@extends('frontend.layout')
@section('title','Five Ways to Stay Active & Healthy During the Quarantine')
@section('body-col',"2-columns")
@section('css')
<style>
  .dot {
    height: 10px;
    width: 10px;
    background-color: #bbb;
    border-radius: 50%;
    display: inline-block;
  }
  </style>
@endsection
@section('content')
<div class="blog-detail">
   <!--------------------------banner start------------------------>
   <section class="inner-page-banner">
      <!-------------------header section---------------------------->
      @include('frontend.include.topbanner')
      <!-----------------header end--------------------------->
      <!----------------banner content start-------------->
		<section class="banner-content">
		<div class="container">
		  <div class="row wow fadeInDown" data-wow-duration="0.5s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 0.5s; animation-delay: 0.5s; animation-name: fadeInDown;">
		    <div class="col-md-7 col-sm-12 ">
		      <h1>Blog</h1>
		    </div>
		    <div class="col-md-5 col-sm-12"></div>
		  </div>
		</div>
		</section>
      <!----------------banner content end-------------->
   </section>
   <!----------------banner end---------------------->
<section class="match">
      <div class="container">
        <a href="#">
          <div class="video-sec">
            <img src="{{asset('site/images/blog7.png')}}">
          </div>
        </a>
        <div class="mission-vision">
          <h2 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.5s; animation-name: fadeInUp;">Five Ways to Stay Active & Healthy During the <span style="color: #00b7d5;"> Quarantine</span></h2>
          <h3 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.5s; animation-name: fadeInUp;">April 20, 2020</h3>
          <p class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.9s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.9s; animation-name: fadeInUp;">The current <a href="https://www.who.int/emergencies/diseases/novel-coronavirus-2019/events-as-they-happen">global pandemic of COVID-19 </a> is an unsettling stretch where things can feel very much out of control. Most people and their routines have been thrown into complete disarray, and as the experts say that the future is far from certain. Most of the cities are still in total lockdowns, and people are distancing themselves to minimize the spread of the COVID-19 virus. While it can get a little dull, there are still a lot of things that you can do - aside from practicing social distancing and washing your hands.
          <br>
          <br>
          <span>Eat Well</span>
          <br>
          <br>
          A vaccine is one sheer short way for us to be sure that we are safe from the virus, but because even the experts aren’t sure how far away, we are from creating a vaccine, the one thing we can do to take care of ourselves. This will not only help with our physical health but also our physiological wellbeing. It is a known fact that a healthy diet reduces our risk of contracting chronic illnesses such as diabetes, cardiovascular diseases as well as depression and anxiety disorders. By avoiding processed food, you can take a healthy and nutritious diet. The complex carbohydrates found in fruits, vegetables, and whole grains are said to be great for our mental health.
          <br>
          <br>
          <span>Sleep </span>
          <br>
          <br>
          Sleep is an integral part of our everyday life as it is one thing that is essential for cell repair and clear toxins out of our system. Research shows that sleep deprivation can have a significant effect on one’s health. Going to bed and waking up at the same time every day can help bring a sense of normality and help individuals to carry out their day to day plans. Try limiting your screen time and exposure to the news right before to help yourself with sleep. Also, remember that sleep improves immune system response, which is critical to combat the virus. 
          <br>
          <br>
          <span>Exercise </span>
          <br>
          <br>
          Exercising helps release chemicals into the body that make us feel good. It has also been linked to helping sleep better, reduce stress and anxiety while providing improvement in cognitive memory. Well, a gym or running course may be off the table, there are still ways to exercise at home and on your own to take care of yourself. WHO recommends 30 mins of daily activity to stay fit and active in these hard times. 
          <br>
          <br>
          <span>Calming Activities  </span>
          <br>
          <br>
          It is easy to feel overwhelmed and swept under the weather with the world talking about only one thing that is the ‘Coronavirus.’  But this forced isolation can also be an excellent opportunity to take a break from the hectic daily life and invest some time in activities that have a calming effect on you. We don’t usually get time for things like meditation, cooking, gardening, etc. but right now is the perfect opportunity to do that. The WHO has also advised, to “draw on skills you have used in the past that have helped you manage previous adversities.” 
          <br>
          <br>
          <span>Don’t Forget About Your Mental Health</span>
          <br>
          <br>
          This pandemic is an especially difficult time for people and their mental health, and it is particularly hard for those who already suffer from some sort of mental health issue. At such times when one is limited to staying at home, it is crucial not to miss out on those therapist appointments. Whether you are a new or existing patient seeking professional help, there are a lot of places that provide <a href="https://mytherapytalk.com/pricing">affordable online counseling therapy.</a> These services are available for everyone, and people who suffer from mental issues can stay on track in these difficult times. 
          <br>
          <br>
          
          <br>
          
         
          </p>
        </div>
        <div class="footer footer-4 wow fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.5s; animation-name: fadeInUp;">
                <h6>Share this post</h6>

                <ul class="social-icon">
                  <li><a href="#"><i class="fa fa-facebook-f"></i></a></li>
                  <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                  <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-vimeo" aria-hidden="true"></i></a></li>
                </ul>
              </div>
      </div>
    </section>
    

</div>
@endsection('content')
@section('js')

@endsection
