@extends('frontend.layout')
@section('title','Protecting Your Mental Health During the Coronavirus Outbreak')
@section('body-col',"2-columns")
@section('css')
<style>
  .dot {
    height: 10px;
    width: 10px;
    background-color: #bbb;
    border-radius: 50%;
    display: inline-block;
  }
  </style>
@endsection
@section('content')
<div class="blog-detail">
   <!--------------------------banner start------------------------>
   <section class="inner-page-banner">
      <!-------------------header section---------------------------->
      @include('frontend.include.topbanner')
      <!-----------------header end--------------------------->
      <!----------------banner content start-------------->
		<section class="banner-content">
		<div class="container">
		  <div class="row wow fadeInDown" data-wow-duration="0.5s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 0.5s; animation-delay: 0.5s; animation-name: fadeInDown;">
		    <div class="col-md-7 col-sm-12 ">
		      <h1>Blog</h1>
		    </div>
		    <div class="col-md-5 col-sm-12"></div>
		  </div>
		</div>
		</section>
      <!----------------banner content end-------------->
   </section>
   <!----------------banner end---------------------->
<section class="match">
      <div class="container">
        <a href="#">
          <div class="video-sec">
            <img src="{{asset('site/images/blog8.jpg')}}">
          </div>
        </a>
        <div class="mission-vision">
          <h2 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.5s; animation-name: fadeInUp;">Protecting Your Mental Health During the Coronavirus Outbreak</h2>
          <h3 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.5s; animation-name: fadeInUp;">April 22, 2020</h3>
          <p class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.9s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.9s; animation-name: fadeInUp;">The world is currently in the middle of a global crisis where there is a mass outbreak of a new virus called COVID-19 or simply the Coronavirus. The outbreak was declared by WHO in January 2020, to be a Public Health Emergency Of International Concern as the virus was spreading at an exponential rate worldwide. WHO made assessments in March and declared <a href="https://www.who.int/emergencies/diseases/novel-coronavirus-2019/events-as-they-happen">COVID-19 to be a ‘Pandemic.’</a>
          <br>
          <br>
          <span>The spread of Coronavirus has brought quite a bit of uncertainty and restlessness into the world. This uncertainty and disorder have taken a heavy toll on people’s mental health and on especially those individuals who are already living with mental health disorders.</span>
          <br>
          <br>
          The fear of not being able to control things and the uncertainty that looms in the air are common characteristics of anxiety disorder. Therefore, it is understandable that a lot of individuals with pre-existing anxiety and co-morbid depression are facing a myriad of problems. The good news is that even in these times of uncertainty and plight, there are ways to protect your mental health and stay positive.
          <br>
          <br>
          <span>Limit your view of news and be careful of what you read </span>
          <br>
          <br>
          Reading or watching a lot of news about pandemic can lead to anxiety attacks. It is a fact that during uncertain times people tend to rely on the news outlets in order to get information and stay connected with the outside world. It can, however, get overwhelming with all the negative news coming in and often leads to one feeling helpless and without any control. This results in anxiety and can quickly lead to a spiraling panic attack, especially during lockdown situations. Therefore it is better to try and limit your exposure to the news.
          <br>
          <br>
          <span>Take breaks from social media and mute things that might be trigging </span>
          <br>
          <br>
          In a world ruled by twitter and Instagram, it is easy to get lost in the world of hashtags and watching unverified made-up conspiracy theories, which can cause distress and can be very triggering. It is advised to mute specific words and hashtags on twitter and avoid checking those on social media apps for the sake of your mental health and sanity. Stay away from Paranoia Period. 
          <br>
          <br>
          <span>Wash your hands, but don’t be too excessive. </span>
          <br>
          <br>
          There has been a spike in the cases of OCD in these difficult times as people who suffer from obsessive-compulsive disorder are prone to washing hands. People with OCD often suffer from the fear of contamination; therefore, the advice about washing hands can be triggering for them. It is advised that people with OCD especially focus on the function of washing hands instead of the action. For instance, they should focus on washing hands for the recommended amount of time instead of several times for it to be effective. It is also recommended that these people especially switch to <a href="http://mytherapytalk.com/">online mental health therapy</a>, as regular access to a therapist can be of huge help in this situation.
          <br>
          <br>
          <br>
          These are a few tips to help you be mindful of your mental health during this global crisis. One way to defeat this virus is by practicing social distancing, but at the same time, it is especially important not to let it affect your mental health. Stay strong. Stay Positive. 
          <br>
          <br>
          
          <br>
          
         
          </p>
        </div>
        <div class="footer footer-4 wow fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.5s; animation-name: fadeInUp;">
                <h6>Share this post</h6>

                <ul class="social-icon">
                  <li><a href="#"><i class="fa fa-facebook-f"></i></a></li>
                  <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                  <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-vimeo" aria-hidden="true"></i></a></li>
                </ul>
              </div>
      </div>
    </section>
    

</div>
@endsection('content')
@section('js')

@endsection
