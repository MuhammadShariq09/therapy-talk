@extends('frontend.layout')
@section('title','Blogs')
@section('body-col',"2-columns")
@section('css')
@endsection
@section('content')
<div class="blog">
   <!--------------------------banner start------------------------>
   <section class="inner-page-banner">
      <!-------------------header section---------------------------->
      @include('frontend.include.topbanner')
      <!-----------------header end--------------------------->
      <!----------------banner content start-------------->
		<section class="banner-content">
		<div class="container">
		  <div class="row wow fadeInDown" data-wow-duration="0.5s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 0.5s; animation-delay: 0.5s; animation-name: fadeInDown;">
		    <div class="col-md-7 col-sm-12 ">
		      <h1>Blog</h1>
		    </div>
		    <div class="col-md-5 col-sm-12"></div>
		  </div>
		</div>
		</section>
      <!----------------banner content end-------------->
   </section>
   <!----------------banner end---------------------->
	<section class="match">
	  <div class="container">
	      

	    <div class="row wow fadeInDown" data-wow-duration="0.9s" data-wow-delay="0.6s" style="visibility: visible; animation-duration: 0.9s; animation-delay: 0.6s; animation-name: fadeInDown;">
		 
			<div class="col-md-6 col-sm-12 content" style="display: block;">
				<div class="box"> <img src='{{asset("site/images/blog8.jpg")}}' style="width: 554px;height: 265px;" class="img-fluid">
				</div>
				<div class="box-text">
					<h3>Protecting Your Mental Health During the Coronavirus Outbreak</h3>
				    <p>The world is currently in the middle of a global crisis where there is a mass outbreak of a new virus called COVID-19 or simply the Coronavirus. The outbreak was declared by WHO in January 2020, to be a Public Health Emergency Of International Concern as the virus was spreading at an exponential rate worldwide. WHO made assessments in March and declared COVID-19 to be a ‘Pandemic.’</p>
					<a href="{{ url('blog/protecting-your-mental-health-during-the-coronavirus-outbreak') }}">Read More</a>
				 </div>
			</div>

			<div class="col-md-6 col-sm-12 content" style="display: block;">
				<div class="box"> <img src='{{asset("site/images/blog7.png")}}' style="width: 554px;height: 265px;" class="img-fluid">
				</div>
				<div class="box-text">
					<h3>Five Ways to Stay Active & Healthy During the Quarantine</h3>
				    <p>The current global pandemic of COVID-19 is an unsettling stretch where things can feel very much out of control. Most people and their routines have been thrown into complete disarray, and as the experts say that the future is far from certain. Most of the cities are still in total lockdowns, and people are distancing themselves to minimize the spread of the COVID-19 virus. While it can get a little dull, there are still a lot of things that you can do - aside from practicing social distancing and washing your hands. </p>
					<a href="{{ url('blog/five-ways-to-stay-active-and-healthy-during-the-quarantine') }}">Read More</a>
				 </div>
			</div>

			<div class="col-md-6 col-sm-12 content" style="display: block;">
				<div class="box"> <img src='{{asset("site/images/blog6.jpg")}}' style="width: 554px;height: 265px;" class="img-fluid">
				  
				</div>
				<div class="box-text">
					<h3>Feeling Overwhelmed During the Coronavirus Crisis? Get Help by Getting Online Therapy.</h3>
				  <p>It is an entirely different experience altogether to be talking to someone about your innermost feelings, yet this person isn’t sitting in front of you physically but rather virtually. Given the current situation, if you are looking to start online therapy or if your therapist has moved their practice to a digital platform, it can be quite a nuisance for some individuals. </p>
					<a href="{{ url('blog/feeling-overwhelmed-during-the-coronavirus-crisis') }}">Read More</a>
				 </div>
			  </div>
			
			  <div class="col-md-6 col-sm-12 content" style="display: block;">
				<div class="box"> <img src='{{asset("site/images/blog4.jpeg")}}' style="width: 554px;height: 265px;" class="img-fluid">
				  
				</div>
				<div class="box-text">
					<h3>How Effective Is Online Counseling & Therapy?</h3>
				  <p>Increasingly popular and proven to be as effective as face-to-face therapy, online counselling and therapy introduce significant advantages to the modern era. For many, traditional therapy is the choice because they haven’t yet experienced the freedom offered by the online therapy.</p>
					<a href="{{ url('blog/how-effective-is-online-counselling-and-therapy') }}">Read More</a>
				 </div>
			  </div>
			  
			  <div class="col-md-6 col-sm-12 content" style="display: block;">
				<div class="box"> <img src='{{asset("site/images/blog5.jpg")}}' style="width: 554px;height: 265px;" class="img-fluid">
				  
				</div>
				<div class="box-text">
					<h3>Reasons for Getting Mental Health Therapy Online</h3>
				  <p>Online mental health therapy utilizes technology to improve therapy. This use of technology is a part of a growing shift in the healthcare industry, enabling more convenient, affordable, and on-demand benefits.</p>
					<a href="{{ url('blog/reason-for-getting-mental-health-therapy-online') }}">Read More</a>
				 </div>
			  </div>
			
			<div class="col-md-6 col-sm-12 content" style="display: block;">
	        <div class="box">
	          <img src='{{asset("site/images/box1.png")}}' class="img-fluid">
	        </div>
	        <div class="box-text">
	          <h3>Depression: A Quick Guide</h3>
	          <p>With more and more people suffering from mental health issues today, depression is the most predominant among them. The occurrence of mental illnesses has widely increased over the last decade due to our lifestyles. More and more people are stressed with no time for relaxation. Depression, however, can be a consequence of many reasons. It can set in as a result of lack of relaxation, loss of someone close, post-pregnancy, failures, a dissolved marriage, and many more.</p>
	          <a href="{{ url('blog/depression-a-quick-guide') }}">Read More</a>
	        </div>
	      </div>

	      <div class="col-md-6 col-sm-12 content" style="display: block;">
	        <div class="box"> <img src='{{asset("site/images/box2.png")}}' class="img-fluid">
	          
	        </div>
	        <div class="box-text">
	          <h3>Building Healthy Self-Esteem</h3>
	          <p>What does it mean when people ask you to build your self-esteem? Healthy self-esteem is essential for a person to achieve his goals and purposes in life. Self-esteem plays a major role in every aspect of life be it an achievement or a failure. It is the feeling of being happy with your traits and capabilities.</p>
	          <a href="{{ url('blog/building-healthy-self-esteem') }}">Read More</a>
	        </div>
	      </div>
	    
	      <div class="col-md-6 col-sm-12 content" style="display: block;">
	       <div class="box"> <img src='{{asset("site/images/box6.png")}}' class="img-fluid">
	          
	        </div>
	       <div class="box-text">
	          <h3>Join MTT</h3>
	        <p>Your Personal Virtual Office.
	Be a part of MyTherapyTalk platform to join thousands of therapists!
	Having high-quality therapists in our community with their attention, professionalism, skills, and desire to help people can retain our purpose and values forward of a meaningful life for everyone.
	</p>
	          <a href="{{ url('blog/your-personal-virtual-office') }}">Read More</a>
		   </div>
		 </div>
		 

		 
		  
		  
	  

	  </div>

	</div>
	</section>

</div>
@endsection('content')
@section('js')

@endsection
