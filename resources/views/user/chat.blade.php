@extends('layouts.app')
@section('title',' Chat')
@section('body-class',"vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar")
@section('body-col',"2-columns")

@section('content')
<section class="contact-main all-notifications">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-sm-12">
                <input type="hidden" id="type" value="{{ ($session->session_type == 1) ? '0' : ($session->session_type == 2 ? '1' : '') }}" />
                <div id="chat"></div>
            </div>
        </div>
    </div>


</section>
@endsection('content')

@section('js')
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script uid="{{ Auth::user()->id }}" src="https://dev28.onlinetestingserver.com/soachatcentralizedWeb/js/ocs.js"></script>
<script >
    $(document).ready(function() {
      var type = $('#type').val();
      console.log('type', type);
    //   ocs.init({
    //         appid: "ffdf711c6f4e05f6860314918df47def",
    //         appkey: "36a06ece5863a3af3bceb8ce7247ee56",
    //         domain: "therapytalk.com",
    //         global: '0',
    //         id: "{{ auth()->user()->id }}", 
    //      //   toid: "3",
    //         colorScheme: '00b7d5',
    //         element: "#chat",
    //   });

      if(type==""){
        ocs.init({
            appid: "ffdf711c6f4e05f6860314918df47def",
            appkey: "36a06ece5863a3af3bceb8ce7247ee56",
            domain: "therapytalk.com",
            global: '0',
            id: "{{ auth()->user()->id }}", 
            toid: "{{ $session->trainer_id }}",
            colorScheme: '00b7d5',
         
            element: "#chat",
      });
      }
      else{
        ocs.init({
            appid: "ffdf711c6f4e05f6860314918df47def",
            appkey: "36a06ece5863a3af3bceb8ce7247ee56",
            domain: "therapytalk.com",
            global: '0',
            id: "{{ auth()->user()->id }}", 
            toid: "{{ $session->trainer_id }}",
            colorScheme: '00b7d5',
            onlyAudio: type==2 ? 1 : 0, // will be given if you need audio chat
           // onlyAudio: 1, // will be given if you need audio chat
            element: "#chat",
      });
     }
      

    });
</script>
@endsection('js')
