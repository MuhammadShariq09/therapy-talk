@extends('layouts.app')
@section('title',' Edit Profile')
@section('body-class',"vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar")
@section('body-col',"2-columns")
@section('css')
    <link rel='stylesheet' href='https://foliotek.github.io/Croppie/croppie.css'>
@endsection
@section('content')
    <section class="contact-main all-notifications u-profile u-profile-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12">
                    <div class="row align-items-center">
                        <div class="col-lg-6 col-xl-6 col-md-6 col-sm-12">
                            <div class="left">
                                <h1 class="pull-left">Edit Profile</h1>
                            </div>

                        </div>
                        <div class="col-lg-6 col-xl-6 col-md-6 col-sm-12">

                            <div class="right text-right">
                            </div>
                        </div>
                    </div>

                    <form action="{{ route('user.profile.update') }}" method="post"  class="view-t">
                        @csrf
                    <div class="row">
                        <div class="col-md-12">
                            <div class="attached">
                                <img src="{{ $user->image ? $user->image : asset('images/student-pro-girl.png') }}" id="personal_img" class="img-full" alt="">
                                <button type="button" class="camera-btn" onclick="document.getElementById('upload').click()"><i class="fa fa-camera"></i></button>
                                <input type="file" id="upload" name="file" accept=".image/*,.jpg, .jpeg, .png" >
                                <input type="hidden" id="image" name="image">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-xl-12 col-sm-12">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 form-group">
                                        <label><i class="fa fa-user-circle"></i>Full Name</label>
                                        <input type="text" name="full_name" value="{{ $user->full_name }}"  class="form-control" >
                                        @if ($errors->has('full_name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('full_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <!-- <div class="col-md-6 col-sm-12 form-group">
                                        <label for=""><i class="fa fa-user-circle"></i> Last Name</label>
                                        <input type="text" name="last_name" value="{{ $user->last_name }}" id="" class="form-control" >
                                        @if ($errors->has('last_name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('last_name') }}</strong>
                                            </span>
                                        @endif
                                    </div> -->
                                    <div class="col-md-6 col-sm-12 form-group">
                                        <label for=""><i class="fa fa-envelope"></i> Email</label>
                                        <input type="email" disabled="" placeholder="{{ $user->email }}"  class="form-control" >
                                    </div>
                                    <div class="col-md-6 col-sm-12 form-group">
                                        <label for=""><i class="fa fa-phone"></i> Phone </label>
                                        <input type="number" name="phone" value="{{ $user->phone }}"  class="form-control" >
                                        @if ($errors->has('phone'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="col-md-6 col-sm-12 form-group">
                                        <label for=""><i class="fa fa-calendar" aria-hidden="true"></i> Date of Birth</label>
                                        <input type="text" name="date_of_birth" value="{{ $user->date_of_birth }}"  class="form-control" >
                                        @if ($errors->has('date_of_birth'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('date_of_birth') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="col-md-6 col-sm-12 form-group">
                                        <label for="" class="mb-3"><i class="fa fa-transgender" aria-hidden="true"></i> Gender</label>
                                        <label class="radio-container">Male
                                            <input type="radio" name="gender" {{ ($user->gender==0) ? '' : 'checked="checked"' }}  value="0" name="radio">
                                            <span class="checkmark"></span>
                                        </label>
                                        <label class="radio-container">Female
                                            <input type="radio" name="gender" {{ ($user->gender==1) ? 'checked="checked"' : "" }} value="1" name="radio">
                                            <span class="checkmark"></span>
                                        </label>
                                        <label class="radio-container">Non Binary
                                            <input type="radio" name="gender" {{ ($user->gender==2) ? 'checked="checked"' : "" }} value="2" name="radio">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                    <div class="col-md-12 col-sm-12 form-group">
                                        <label for=""><i class="fa fa-globe" aria-hidden="true"></i> Address</label>
                                        <input type="text" name="address" value="{{ $user->address }}" id="autocomplete" onFocus="geolocate()" class="form-control" >
                                        @if ($errors->has('address'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('address') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="col-md-6 col-sm-12 form-group">
                                        <label for=""><i class="fa fa-globe" aria-hidden="true"></i> Country</label>
                                        <input type="text" name="country" value="{{ $user->country }}" id="country" class="form-control" >
                                        @if ($errors->has('country'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('country') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="col-md-6 col-sm-12 form-group">
                                        <label for=""><i class="fa fa-map" aria-hidden="true"></i> State </label>
                                        <select id="state" class="form-control" name="state">
                                            <option>Select State...</option>
                                            @foreach($states as $row)
                                            <option value="{{$row->id}}" {{$row->id==$user->state ? 'selected' : ''}}>{{$row->name}}</option>
                                            @endforeach
                                          
                                        </select>
                                        <input  id="administrative_area_level_1" type="hidden" />
                                        {{-- <input type="text" name="state" value="{{ $user->state }}" id="administrative_area_level_1" class="form-control" > --}}
                                        @if ($errors->has('state'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('state') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="col-md-6 col-sm-12 form-group">
                                        <label for=""><i class="fa fa-map-marker" aria-hidden="true"></i> City </label>
                                        <select id="city" class="form-control" name="city" placeholder="Enter City">
                                            <option value="{{$user->city ?? ''}}">{{ $user->cities->city ?? '' }}</option>
                                        </select>
                                        {{-- <input type="text" name="city" value="{{ $user->city }}" id="city" class="form-control" > --}}
                                        @if ($errors->has('city'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('city') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                   
                                    <div class="col-md-6 col-sm-12 form-group">
                                        <label for=""><i class="fa fa-map-marker" aria-hidden="true"></i> Zip Code</label>
                                        <input type="text" name="zipcode" value="{{ $user->zipcode }}" id="zipcode" class="form-control" >
                                        @if ($errors->has('zipcode'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('zipcode') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                                        <button type="submit">Update</button>
                                    </div>
                                </div>
                        </div>
                    </div>
                    </form>

                </div>

            </div>
        </div>
    </section>
    @include('includes.user.cropper')
@endsection('content')

@section('js')
    <script src="https://foliotek.github.io/Croppie/croppie.js"></script>
    <script src="{{ asset('assets/js/profile.js') }}"></script>
    <script src="{{ asset('js/address_autocomplete.js') }}"></script>
    <script>
        $(document).ready(function() {
        var city = '{{$user->city}}';
        $('#state').on('change', function() {
          
                var stateID = $(this).val();
                var url = $('#url').val() + '/get-cities/';
                if(stateID) {
                    $.ajax({
                        url: url+stateID,
                        type: "GET",
                        dataType: "json",
                        success:function(data) {
                            $('#city').empty();
                            $.each(data, function(key, value) {
                                //  console.log(key);
                                //  console.log(value); return;
                                $('#city').append('<option value="'+ value.id +'" >'+ value.city +'</option>');
                            });
    
    
                        }
                    });
                }else{
                    $('#city]').empty();
                }
        });
    });
        </script>
@endsection
