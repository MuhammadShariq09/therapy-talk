@extends('layouts.app')
@section('title',' Appointment Log Detail')
@section('body-class',"vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar")
@section('body-col',"2-columns")
@section('css')
    <style>
        .alert-danger {
            display: none;
        }
        .show {
            display: block;
        }

    </style>
@endsection
@section('content')
    <section class="contact-main">
        <div class="container">
            <div class="row">
                <input type="hidden" id="session_id" value="{{$session->id}}" />
                <div class="col-lg-12 col-sm-12">
                    <div class="row">
                        <div class="col-lg-12 col-xl-12 col-sm-12">
                            <h1 class="wow fadeInUp" data-wow-duration="0.9s" data-wow-delay="0.4s"
                                style="visibility: visible; animation-duration: 0.9s; animation-delay: 0.4s; animation-name: fadeInUp;">
                                Appointment Log Details</h1>
                        </div>
                        @error('stripe')
                             <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="col-lg-12 col-xl-12 col-sm-12">
                            <div class="session-details-main u-profile appointment-accepted">
                                <div class="session-details-top">
                                    <div class="media">
                                        <div class="img-box">
                                            <img src="{{ $session->therapist->image }}" alt="{{ucfirst($session->therapist->full_name)}}" onerror="this.style.display='none'">
                                        </div>
                                        <div class="media-body">
                                            <div class="media justify-content-between align-items-center">
                                                <div class="left">
                                                    <h2>{{ $session->therapist->name }}</h2>
                                                    <h3>Therapist</h3>
                                                    <ul>
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                    </ul>
                                                </div>
                                                <div class="right">
                                                    <a href="{{ route('user.therapist.profile', ['id' => encrypt($session->therapist->id)]) }}"
                                                       class="btn-a">View Profile</a>
                                                </div>
                                            </div>
                                            <p class="p-italic">{{ $session->therapist->statement }}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="session-details-bottom">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-12 col-sm-12">
                                            <div class="left">
                                                <h2>Session Type: {{$session->type}}</h2>
                                                <h3>Session Duration: {{ $session->duration }}</h3>
                                                <h3>Reason for seeking therapy:</h3>
                                                <p>{{ $session->reason }}</p>
                                                @if($session->status == 4)
                                                <a href="#" class="btn-a" data-toggle="modal" data-target="#exampleModalCenter">Report</a>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-12 col-sm-12">
                                            <div class="right">
                                                <h3>The appointment details are</h3>
                                                {{-- <p> Session will going to start in {{ $diff_in_hours }}         </p> --}}
                                                <div class="view-t">
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                                                            <label for=""><i class="fa fa-calendar"
                                                                             aria-hidden="true"></i>Date</label>
                                                            <input type="text" name=""
                                                                   placeholder="{{ isset($session->reschedule[0]) ? $session->reschedule[0]->suggested_date : $session->requested_date }}"
                                                                   id="" class="form-control" disabled="">
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                                                            <label for=""><i class="fa fa-clock-o"
                                                                             aria-hidden="true"></i>Time</label>
                                                            <input type="text" name=""
                                                                   placeholder="{{ isset($session->reschedule[0]) ? $session->reschedule[0]->suggested_time : $session->requested_time }}"
                                                                   id="" class="form-control" disabled="">
                                                        </div>
                                                        {{-- <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                                                            <label for=""><i class="fa fa-money"
                                                                             aria-hidden="true"></i>Fee</label>
                                                            <input type="text" name=""
                                                                   placeholder="{{ isset(config('app.fee')[$session->duration ]) ? config('app.fee')[$session->duration ] : "" }}"
                                                                   id="" class="form-control" disabled="">
                                                        </div> --}}

                                                        @php
                                                            $date = isset($session->reschedule[0]) ? new DateTime($session->reschedule[0]->suggested_date) : new DateTime($session->requested_date);
                                                            $now = new DateTime('now');
                                                            $interval = $date->diff($now);
                                                         
                                                        @endphp
                                                       
                                                        @if($session->status!==4)
                                                            <div class="col-lg-12 col-md-6 col-sm-12 form-group">
                                                                @if($interval->days <= 0)
                                                                <a style="padding: 15px 50px;font-size: 17px;min-width: 215px;margin: 30px 0 0;background: #00b7d5; border-radius: 100px;border-top-left-radius: 1px;letter-spacing: 0.5px;text-transform: uppercase;transition-duration: .5s;font-family: 'Barlow', sans-serif;font-weight: 600;border: none;color: #fff;"
                                                                    href="{{route('user.appointments.session', $session->id)}}">
                                                                    Start Session
                                                                </a>
                                                                @else 
                                                                <label class="white badge badge-secondary" style="color: #fff;">Session Expired</label>
                                                                @endif
                                                            </div>
                                                        @else 
                                                        <div class="col-lg-12 col-md-6 col-sm-12 form-group">
                                                            <label class="white badge badge-success" style="color: #fff;">Session Completed</label>
                                                        </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <div class="login-fail-main user admin-profile profile-view">
        <div class="featured inner">
            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                    <div class="forget-pass register-form">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                      <div class="modal-body">
                        <div class="payment-main-popup">
                          <form action="">
                        <div class="payment-modal-main">
                          <div class="payment-modal-inner">
                            <div class="row">
                              <div class="col-12">
                                <h1 class="text-center">Are you sure you want to report this session?</h1>
                              </div>
                                <div class="col-12">
                                  <textarea type="text" name="" placeholder="Enter message" id="report-message" class="form-control"></textarea>
                                </div>
                              
                              <div class="col-12 text-center">
                                <button type="button" class="can" data-dismiss="modal">Cancel</button>
                                
                                                   
                                <button type="button" class="can con" onclick="reportTherapist({{$session->therapist->id}})" >Report</button>
    
                                   </div>
                            </div>
                            
                          </div>
                        </div>
                      </form>
                        </div>
                        
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <div class="modal fade" id="bookAppointmentModal" tabindex="-1" role="dialog"
                 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="forget-pass">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">×</span></button>
                            <div class="modal-body">
                                <div class="payment-main-popup">
                                    <form action="{{ route('user.book-appointment') }}" id="bookAppointment"
                                          method="post">
                                        @csrf
                                        <input type="hidden" id="redirectTo" value="1">
                                        <input type="hidden" name="trainer_id"
                                               value="{{encrypt($session->therapist->id)}}">
                                        <div class="payment-modal-main">
                                            <div class="payment-modal-inner">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <h1>Book A Session</h1>
                                                    </div>
                                                    <!-- <div class="col-12 form-group">
                                                        <label>Duration of Session</label>
                                                        <select id="duration" name="duration">
                                                            <option value="1">1 hour</option>
                                                            <option value="2">2 hour</option>
                                                            <option value="3">3 hour</option>
                                                        </select>
                                                    </div> -->
                                                    <div class="col-12 form-group">
                                                        <label for="">Select Date</label>
                                                        <input id="datepicker" name="requested_date"/>
                                                    </div>
                                                    <div class="col-12 form-group">
                                                        <label for="">Select Time</label>
                                                        <input id="timepicker" name="requested_time"/>
                                                    </div>
                                                    <div class="col-12 form-group">
                                                        <label for="">Select Therapy Type</label>
                                                        <select id="type" name="type">
                                                            <option value="Individual">Individual</option>
                                                            <option value="Group">Group</option>
                                                            <option value="Couple">Couple</option>
                                                            <option value="Kids">Kids</option>
                                                            <option value="Family">Family</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-12 form-group">
                                                        <label for="">Reason</label>
                                                        <textarea type="text" id="reason" name="reason"
                                                                  placeholder="Enter Reason"></textarea>
                                                    </div>

                                                    <div class="col-12 text-center blockui">
                                                        <button type="submit" class="can">Request Appointment</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="forget-pass  register-form">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">×</span></button>
                            <div class="modal-body">
                                <div class="payment-main-popup payment-view-popup">
                                    <div class="payment-modal-main">
                                        <div class="payment-modal-inner">
                                            <div class="row">
                                                <div class="col-12 text-center">
                                                    <img src="{{ asset('images/appointment-schedule-icon.png') }}"
                                                         class="img-fluid" alt="">
                                                    <h3>Appointment Scheduled</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection

    @section('js')
    <script src="{{ asset('assets/js/book_appointment.js') }}"></script>

    @endsection

