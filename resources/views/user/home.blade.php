@extends('layouts.app')
@section('title',' Dashboard')
@section('body-class',"vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar")
@section('body-col',"2-columns")

@section('content')

    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-body">
                <!-- Basic form layout section start -->
                <section id="combination-charts">
                    <div class="row">
                        <div class="col-12">
                            <div class="admin-overview-main">
                                <div class="row">
                                    <div class="col-12">
                                        <h1>dashboard</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

@endsection('content')

@section('js')

    <script type="text/javascript">

    </script>
    <!-- END STACK JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <!-- END PAGE LEVEL JS-->

@endsection('js')
