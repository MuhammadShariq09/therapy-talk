@extends('layouts.app')
@section('title',' Session Log Details')
@section('body-class',"vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar")
@section('body-col',"2-columns")
@section('css')
    <style>
        .alert-danger {
            display: none;
        }
        .show {
            display: block;
        }

    </style>
@endsection
@section('content')
    <section class="contact-main">
        <div class="container">
            <div class="row">
                <input type="hidden" id="session_id" value="{{$session->id}}" />
                <div class="col-lg-12 col-sm-12">
                    <div class="row">
                        <div class="col-lg-12 col-xl-12 col-sm-12">
                            <h1 class="wow fadeInUp" data-wow-duration="0.9s" data-wow-delay="0.4s"
                                style="visibility: visible; animation-duration: 0.9s; animation-delay: 0.4s; animation-name: fadeInUp;">
                                Session Log Details</h1>
                        </div>
                        @error('stripe')
                             <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="col-lg-12 col-xl-12 col-sm-12">
                            <div class="session-details-main u-profile appointment-accepted">
                                <div class="session-details-top">
                                    <div class="media">
                                        <div class="img-box">
                                            <img src="{{ $session->therapist->image }}">
                                        </div>
                                        <div class="media-body">
                                            <div class="media justify-content-between align-items-center">
                                                <div class="left">
                                                    <h2>{{ $session->therapist->name }}</h2>
                                                    <h3>Therapist</h3>
                                                    <ul>
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                    </ul>
                                                </div>
                                                <div class="right">
                                                    <a href="{{ route('user.therapist.profile', ['id' => encrypt($session->therapist->id)]) }}"
                                                       class="btn-a">View Profile</a>
                                                </div>
                                            </div>
                                            <p class="p-italic">{{ $session->therapist->statement }}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="session-details-bottom">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-12 col-sm-12">
                                            <div class="left">
                                                <h2>Session Type: {{$session->type}}</h2>
                                                <h3>Session Duration: {{ $session->duration }}</h3>
                                                <h3>Reason for seeking therapy:</h3>
                                                <p>{{ $session->reason }}</p>
                                                @if($session->status == 4)
                                                <a href="#" class="btn-a" data-toggle="modal" data-target="#exampleModalCenter">Report</a>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-12 col-sm-12">
                                            @if($session->status == 0)
                                                <div class="right">
                                                    <h2>Waiting for therapist's reply</h2>
                                                    <h3>You Booked Appointment For</h3>
                                                    <div action="" class="view-t">
                                                        <div class="row">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                                                                <label for=""><i class="fa fa-calendar"
                                                                                 aria-hidden="true"></i>Date</label>
                                                                <input type="text" name=""
                                                                       placeholder="{{ $session->requested_date }}"
                                                                       class="form-control" disabled="">
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                                                                <label for=""><i class="fa fa-clock-o"
                                                                                 aria-hidden="true"></i>Time</label>
                                                                <input type="text" name=""
                                                                       placeholder="{{ $session->requested_time }}"
                                                                       class="form-control" disabled="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @elseif($session->status == 1)
                                                @include('includes.user.sessionApproval')
                                                @if($session->is_paid==0)
                                                    @include('includes.user.paymentPartial')
                                                @endif
                                                @include('includes.user.reschedule')
                                            @elseif($session->status == 2)
                                                @include('includes.user.sessionRejection')
                                            @elseif($session->status == 3)
                                                @include('includes.user.sessionApproval')
                                            @elseif($session->status == 4)
                                                @include('includes.user.sessionCompleted')
                                            @endif
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <div class="login-fail-main user admin-profile profile-view ">
        <div class="featured inner">
            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                    <div class="forget-pass register-form">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                      <div class="modal-body">
                        <div class="payment-main-popup">
                          <form action="" class="load">
                        <div class="payment-modal-main">
                          <div class="payment-modal-inner">
                            <div class="row">
                              <div class="col-12">
                                <h1 class="text-center">Are you sure you want to report this session?</h1>
                              </div>
                                <div class="col-12">
                                  <textarea type="text" name="" placeholder="Enter message" id="report-message" class="form-control"></textarea>
                                </div>
                              
                              <div class="col-12 text-center blockui">
                                <button type="button" class="can" data-dismiss="modal">Cancel</button>
                                
                                                   
                                <button type="button" class="can con" onclick="reportTherapist({{$session->therapist->id}})" >Report</button>
    
                                   </div>
                            </div>
                            
                          </div>
                        </div>
                      </form>
                        </div>
                        
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <div class="modal fade" id="bookAppointmentModal" tabindex="-1" role="dialog"
                 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="forget-pass">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">×</span></button>
                            <div class="modal-body">
                                <div class="payment-main-popup">
                                    <form action="{{ route('user.book-appointment') }}" id="bookAppointment"
                                          method="post">
                                        @csrf
                                        <input type="hidden" id="redirectTo" value="1">
                                        <input type="hidden" name="trainer_id"
                                               value="{{encrypt($session->therapist->id)}}">
                                        <div class="payment-modal-main">
                                            <div class="payment-modal-inner">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <h1>Book A Session</h1>
                                                    </div>
                                                    <!-- <div class="col-12 form-group">
                                                        <label>Duration of Session</label>
                                                        <select id="duration" name="duration">
                                                            <option value="1">1 hour</option>
                                                            <option value="2">2 hour</option>
                                                            <option value="3">3 hour</option>
                                                        </select>
                                                    </div> -->
                                                    <div class="col-12 form-group">
                                                        <label for="">Select Date</label>
                                                        <input id="datepicker" name="requested_date"/>
                                                    </div>
                                                    <div class="col-12 form-group">
                                                        <label for="">Select Time</label>
                                                        <input id="timepicker" name="requested_time"/>
                                                    </div>
                                                    <div class="col-12 form-group">
                                                        <label for="">Select Therapy Type</label>
                                                        <select id="type" name="type">
                                                            <option value="Individual">Individual</option>
                                                            <option value="Group">Group</option>
                                                            <option value="Couple">Couple</option>
                                                            <option value="Kids">Kids</option>
                                                            <option value="Family">Family</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-12 form-group">
                                                        <label for="">Select Therapy Medium</label>
                                                        <select name="session_type">
                                                          <option value="3">Video Call</option>
                                                          <option value="2">Voice Call</option>
                                                          <option value="1">Live Chat</option>
                                                        </select>
                                                      </div>
                                                    <div class="col-12 form-group">
                                                        <label for="">Reason</label>
                                                        <textarea type="text" id="reason" name="reason"
                                                                  placeholder="Enter Reason"></textarea>
                                                    </div>

                                                    <div class="col-12 text-center blockui">
                                                        <button type="submit" class="can">Request Appointment</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="forget-pass  register-form">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">×</span></button>
                            <div class="modal-body">
                                <div class="payment-main-popup payment-view-popup">
                                    <div class="payment-modal-main">
                                        <div class="payment-modal-inner">
                                            <div class="row">
                                                <div class="col-12 text-center">
                                                    <img src="{{ asset('images/appointment-schedule-icon.png') }}"
                                                         class="img-fluid" alt="">
                                                    <h3>Appointment Scheduled</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection('content')
@section('js')
    <script src="{{ asset('js/rater.min.js') }}"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <script>

        $(function () {
            $('#select-date').datepicker({
                uiLibrary: 'bootstrap4'
            });
            $('#timepicker').timepicker({
                uiLibrary: 'bootstrap4'
            });
            var options = {
                max_value: 5,
                step_size: 1,
            }
            $(".rating").rate(options);
        })

    </script>
    @if(!$session->payment_count)
    <script src="https://js.stripe.com/v3/"></script>
    <script>
        var stripe = Stripe('{{ config('services.stripe.PUBLISH_KEY') }}');
    </script>
    <script src="{{ asset('assets/js/stripe_init.js') }}"></script>
    <script>
        (function () {
           
            'use strict';

            var elements = stripe.elements({
                fonts: [
                    {
                        cssSrc: 'https://fonts.googleapis.com/css?family=Source+Code+Pro',
                    },
                ],
                // Stripe's examples are localized to specific languages, but if
                // you wish to have Elements automatically detect your user's locale,
                // use `locale: 'auto'` instead.
                locale: window.__exampleLocale
            });

            // Floating labels
            var inputs = document.querySelectorAll('.cell.example.example2 .input');
            Array.prototype.forEach.call(inputs, function (input) {
                input.addEventListener('focus', function () {
                    input.classList.add('focused');
                });
                input.addEventListener('blur', function () {
                    input.classList.remove('focused');
                });
                input.addEventListener('keyup', function () {
                    if (input.value.length === 0) {
                        input.classList.add('empty');
                    } else {
                        input.classList.remove('empty');
                    }
                });
            });

            var elementStyles = {
                base: {
                    color: '#32325D',
                    fontWeight: 500,
                    fontFamily: 'Source Code Pro, Consolas, Menlo, monospace',
                    fontSize: '16px',
                    fontSmoothing: 'antialiased',

                    '::placeholder': {
                        color: '#CFD7DF',
                    },
                    ':-webkit-autofill': {
                        color: '#e39f48',
                    },
                },
                invalid: {
                    color: '#E25950',

                    '::placeholder': {
                        color: '#FFCCA5',
                    },
                },
            };

            var elementClasses = {
                focus: 'focused',
                empty: 'empty',
                invalid: 'invalid',
                class: 'form-control'
            };

            var cardNumber = elements.create('cardNumber', {
                style: elementStyles,
                classes: elementClasses,
            });
            cardNumber.mount('#session-card-number');

            var cardExpiry = elements.create('cardExpiry', {
                style: elementStyles,
                classes: elementClasses,
            });
            cardExpiry.mount('#session-card-expiry');

            var cardCvc = elements.create('cardCvc', {
                style: elementStyles,
                classes: elementClasses,
            });
            cardCvc.mount('#session-card-cvc');

            registerElements([cardNumber, cardExpiry, cardCvc], 'pay_for_booking');
        })();
    </script>
    @endif
    <script src="{{ asset('assets/js/book_appointment.js') }}"></script>
   
@endsection
