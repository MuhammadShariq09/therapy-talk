@extends('layouts.app')
@section('title',' Profile')
@section('body-class',"vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar")
@section('body-col',"2-columns")

@section('content')
    <section class="contact-main all-notifications u-profile u-profile-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12">
                    <div class="row align-items-center">
                        <div class="col-lg-6 col-xl-6 col-md-6 col-sm-12">
                            <div class="left">
                                <h1 class="pull-left">Profile</h1>
                            </div>

                        </div>
                        <div class="col-lg-6 col-xl-6 col-md-6 col-sm-12">

                            <div class="right text-right">
                                <a href="#" class="btn-a" data-toggle="modal" data-target="#updatePasswordModal">Change Password</a>
                                <a href="{{ route('user.profile.edit') }}" class="btn-a">Edit Profile</a>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="attached">
                              
                                <img src="{{ $user->image ? $user->image : asset('images/student-pro-girl.png') }}" class="img-full" alt="">

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-xl-12 col-sm-12">
                            @if(session('success'))
                                <div class="alert alert-success">{{ session('success') }}</div>
                            @endif
                         
                            <form action="" class="view-t">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 form-group">
                                        <label><i class="fa fa-user-circle"></i>Full Name</label>
                                        <input type="text" name="" placeholder="{{ $user->full_name }}" id="" class="form-control" disabled="">
                                    </div>
                                    <!-- <div class="col-md-6 col-sm-12 form-group">
                                        <label for=""><i class="fa fa-user-circle"></i> Last Name</label>
                                        <input type="text" name="" placeholder="{{ $user->last_name }}" id="" class="form-control" disabled="">
                                    </div> -->
                                    <div class="col-md-6 col-sm-12 form-group">
                                        <label for=""><i class="fa fa-envelope"></i> Email</label>
                                        <input type="email" name="" placeholder="{{ $user->email }}" id="" class="form-control" disabled="">
                                    </div>
                                    <div class="col-md-6 col-sm-12 form-group">
                                        <label for=""><i class="fa fa-phone"></i> Phone </label>
                                        <input type="number" name="" placeholder="{{ $user->phone ?? "" }}" id="" class="form-control" disabled="">
                                    </div>
                                    <div class="col-md-6 col-sm-12 form-group">
                                        <label for=""><i class="fa fa-calendar" aria-hidden="true"></i> Date of Birth</label>
                                        <input type="text" name="" placeholder="{{ $user->date_of_birth ?? "" }}" id="" class="form-control" disabled="">
                                    </div>
                                    <div class="col-md-6 col-sm-12 form-group">
                                        <label for=""><i class="fa fa-transgender" aria-hidden="true"></i> Gender</label>
                                        
                                        <input type="text" name="" placeholder="{{ ($user->gender== 1) ? ($user->gender== 2 ? "Non Binary" : "Female") : 'Male'  }}" id="" class="form-control" disabled="">
                                    </div>
                                    <div class="col-md-12 col-sm-12 form-group">
                                        <label for=""><i class="fa fa-globe" aria-hidden="true"></i> Address</label>
                                        <input type="text" name="" placeholder="{{ $user->address ?? "" }}" id="" class="form-control" disabled="">
                                    </div>
                                    <div class="col-md-6 col-sm-12 form-group">
                                        <label for=""><i class="fa fa-globe" aria-hidden="true"></i> Country</label>
                                        <input type="text" name="" placeholder="{{ $user->country ?? "" }}" id="" class="form-control" disabled="">
                                    </div>
                                    <div class="col-md-6 col-sm-12 form-group">
                                        <label for=""><i class="fa fa-map" aria-hidden="true"></i> State </label>
                                        <input type="text" name="" placeholder="{{ $user->states->name ?? '' }}" id="" class="form-control" disabled="">
                                    </div>
                                    <div class="col-md-6 col-sm-12 form-group">
                                        <label for=""><i class="fa fa-map-marker" aria-hidden="true"></i> City </label>
                                        <input type="text" name="" placeholder="{{ $user->cities->city ?? '' }}" id="" class="form-control" disabled="">
                                    </div>
                                  
                                    <div class="col-md-6 col-sm-12 form-group">
                                        <label for=""><i class="fa fa-map-marker" aria-hidden="true"></i> Zip Code</label>
                                        <input type="text" name="" placeholder="{{ $user->zipcode ?? "" }}" id="" class="form-control" disabled="">
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>

                </div>

            </div>
        </div>
    </section>
    <div class="login-fail-main user admin-profile profile-view">
        <div class="featured inner">
            <div class="modal fade bd-example-modal-lg" id="updatePasswordModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-lgg conf">

                    <div class="modal-content">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <form action="{{ route('user.password.update') }}" id="updatePassword">
                            @csrf
                            <div class="payment-modal-main">
                                <div class="payment-modal-inner">
                                    <div class="row">
                                        <div class="col-12">
                                            <h1>Change Password</h1>
                                        </div>
                                        <div class="col-12 form-group">
                                            <label for=""><i class="fa fa-unlock-alt" aria-hidden="true"></i> Current Password</label>
                                            <input type="password" name="oldpassword" class="form-control" spellcheck="true" placeholder="Current Password">
                                        </div>
                                        <div class="col-12 form-group">
                                            <label for=""><i class="fa fa-unlock-alt" aria-hidden="true"></i> New Password</label>
                                            <input type="password" name="newPassword" class="form-control" spellcheck="true" placeholder="New Password">
                                        </div>
                                        <div class="col-12 form-group">
                                            <label for=""><i class="fa fa-unlock-alt" aria-hidden="true"></i> Confirm New Password</label>
                                            <input type="password" name="confirmNewPassword" class="form-control" spellcheck="true" placeholder="Retype Password">
                                        </div>
                                        <div class="col-12 text-center">
                                            <button type="submit" class="can">save</button>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>



    </div>
@endsection('content')

@section('js')
    <script src="{{ asset('assets/js/profile.js') }}"></script>
@endsection('js')
