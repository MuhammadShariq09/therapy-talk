@extends('layouts.app')
@section('title',' My Appointments')
@section('body-class',"vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar")
@section('body-col',"2-columns")

@section('content')
    <section class="contact-main all-notifications">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12">
                    <div class="row">
                        <div class="col-lg-12 col-xl-12 col-sm-12">
                            <h1 class="wow fadeInUp" data-wow-duration="0.9s" data-wow-delay="0.4s">My Appointments</h1>
                        </div>
                    </div>

                    <form method="GET">
                        <div class="row align-items-end">
                            <div class="col-lg-6 col-md-12 col-sm-12 profile-view">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <label  for="">Sort By:</label>
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 form-group">
                                                <input id="select-date" name="from" readonly placeholder="From Date">
                                            </div>
                                            <div class="col-lg-6 col-md-6 form-group">
                                                <input id="select-date2" name="to" readonly placeholder="To Date">
                                            </div>
                                        </div>
                                    </div>
    
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12 profile-view filter-main">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 form-group d-flex align-items-center">
                                        <label class="w-100" for="">Filter By Session Duration:</label>
                                        <select name="duration">
                                            <option value="">Select...</option>
                                            <option value="0.5">1/2 hour</option>
                                            <option value="1">1 hour</option>
                                            <option value="2">2 hour</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 form-group d-flex align-items-center">
                                        <label class="w-100" for="">Filter By Status:</label>
                                        <select name="status">
                                            <option value="">Select...</option>
                                            <option value="0">Pending Reply</option>
                                            <option value="1">Appointment Cancelled/Refunded</option>
                                            <option value="1">Appointment reschedule Pending</option>
                                            <option value="1">Appointment accepted</option>
                                            <option value="2">Appointment rejected</option>
                                            <option value="3">Session Started</option>
                                            <option value="3">Session Expired</option>
                                            <option value="4">Session Completed</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 form-group d-flex align-items-center">
                                    <button type="submit" class="btn btn-sm" style="padding: 10px">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>

                    <div class="table-responsive">
                        <div class="row maain-tabble">
                            <table class="table  table-striped table-bordered zero-configuration">
                                <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Therapist Name</th>
                                    <th>Date</th>
                                    <th>Time</th>
                                    <th>Session Duration</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                    @if(count($sessionLogs) <= 0)
                                    <tr><td>No record found.</td></tr>
                                    @else 
                                        @foreach($sessionLogs as $index => $s)
                                        @php
                                            $date = isset($s->reschedule[0]) ? new DateTime($s->reschedule[0]->suggested_date) : new DateTime($s->requested_date);
                                            $now = new DateTime('now');
                                            $interval = $date->diff($now);
                                           
                                        @endphp
                                        <tr>
                                            <td>{{ ++$index }}</td>
                                            <td>
                                                <img src="{{ $s->therapist->image }}" class="avatar" alt="">{{ $s->therapist->name }}
                                            </td>
                                         
                                            <td>{{ isset($s->reschedule[0]) ? $s->reschedule[0]->suggested_date : $s->requested_date }}</td>
                                            <td>{{ isset($s->reschedule[0]) ? $s->reschedule[0]->suggested_time : $s->requested_time }}</td>
                                            <td>{{ $s->duration }} Hour</td>
                                            @if($s->status == 0)
                                                <td>Pending</td>
                                            @elseif($s->status == 1)
                                                <td>Approved</td>
                                            @elseif($s->status == 2)
                                                <td>Rejected</td>
                                            @elseif($s->status == 3)
                                            <td>{{ ($interval->days >0) ? "Session Expired" : "Session Started"}}</td>
                                        
                                            @elseif($s->status == 4)
                                                <td>Session Completed</td>
                                            @endif
                                            <td>
                                                <div class="btn-group mr-1 mb-1">
                                                    <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v"></i>
                                                    </button>
                                                    <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(4px, 23px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                        <a class="dropdown-item" href="{{ route('user.appointments.log.details', ['id' => $s->id]) }}"><i class="fa fa-eye"></i>View </a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach

                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        </div>

    </section>
@endsection('content')

@section('js')
<script>
    $('#select-date').datepicker({
      uiLibrary: 'bootstrap4',
      format: 'mm/dd/yyyy'
    });
    $('#select-date2').datepicker({
                uiLibrary: 'bootstrap4',
                format: 'mm/dd/yyyy'
              });
  </script>
@endsection
