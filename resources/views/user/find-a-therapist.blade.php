@extends('layouts.app')
@section('title',' Find a Therapist')
@section('body-class',"vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar")
@section('body-col',"2-columns")
@section('content')
@section('css')

    <link rel="stylesheet" type="text/css" href="{{ asset('css/select2.min.css') }}">

@endsection('css')
    <section class="contact-main therapist-profile find-therapist">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12">
                    <div class="row">
                        <div class="col-lg-4 col-xl-4 col-md-5 col-sm-12">
                            <div class="side-panel">
                                <h3>Filter by</h3>
                                <form method="GET">
                                <div class="side-panel-inner">
                                    <div class="row m-0">
                                        <div class="col-12 form-group">
                                            <label>Search by Zip Code</label>
                                            <div class="search-area">
                                                <input type="search" value="{{ request()->query('zipcode') }}" name="zipcode" placeholder="Zip Code" />
                                                <button type="submit">Search</button>
                                            </div>
                                        </div>

                                        <div class="col-12 form-group">

                                            <label>Search By Full, First or Last Name</label>
                                            <div class="search-area">
                                                <input type="search" name="name" value="{{ request()->query('name') }}" placeholder="Name" />
                                                <button type="submit">Search</button>

                                            </div>
                                        </div>

                                        <div class="col-12 form-group">

                                            <h4>Specialities</h4>
                                        </div>

                                        <div class="col-12 form-group">

                                            <label>Specialise in</label>
                                            <select class="select2 form-control" name="specialities[]" multiple="multiple">
                                                <optgroup>
                                                    @foreach($specialities as $s)
                                                    <option {{  ( request('specialities') &&  in_array($s->id, request('specialities')) ) ? "Selected" : ""   }} value="{{$s->id}}">{{ $s->name }}</option>
                                                    @endforeach
                                                </optgroup>
                                            </select>
                                        </div>

                                        <div class="col-12 form-group">

                                            <label>Modality</label>
                                            <select class="select2 form-control" name="modality[]" multiple="multiple">
                                                <optgroup>
                                                    @foreach($modality as $m)
                                                        <option {{  ( request('modality') &&  in_array($m->id, request('modality')) ) ? "Selected" : ""   }} value="{{$m->id}}">{{ $m->name }}</option>
                                                    @endforeach
                                                </optgroup>
                                            </select>
                                        </div>
                                        <div class="col-12 form-group">

                                            <label>Treatement Orientation</label>
                                            <select class="select2 form-control" name="treatment[]"  multiple="multiple">
                                                <optgroup>
                                                    @foreach($treatment as $t)
                                                        <option {{  ( request('treatment') &&  in_array($t->id, request('treatment')) ) ? "Selected" : ""   }} value="{{$t->id}}">{{ $t->name }}</option>
                                                    @endforeach
                                                </optgroup>
                                            </select>
                                        </div>

                                        <div class="col-12 form-group">

                                            <label>Age specialty</label>
                                            <select class="select2 form-control" name="agegroup[]"  multiple="multiple">
                                                <optgroup>
                                                    @foreach($agegroup as $ag)
                                                        <option {{  ( request('agegroup') &&  in_array($ag->id, request('agegroup')) ) ? "Selected" : ""   }} value="{{$ag->id}}">{{ $ag->name }}</option>
                                                    @endforeach
                                                </optgroup>
                                            </select>
                                        </div>
                                        <div class="col-12 form-group">

                                            <label>Ethnic Specialty</label>
                                            <select class="select2 form-control" name="ethnicity[]"  multiple="multiple">
                                                <optgroup>
                                                    @foreach($ethnicity as $eth)
                                                        <option {{  ( request('ethnicity') &&  in_array($eth->id, request('ethnicity')) ) ? "Selected" : ""   }} value="{{$eth->id}}">{{ $eth->name }}</option>
                                                    @endforeach
                                                </optgroup>
                                            </select>
                                        </div>

                                        <div class="col-12 form-group">

                                            <label>Language Specialty</label>
                                            <select class="select2 form-control" name="language[]"  multiple="multiple">
                                                <optgroup>
                                                    @foreach($language as $lang)
                                                        <option {{  ( request('language') &&  in_array($lang->id, request('language')) ) ? "Selected" : ""   }} value="{{$lang->id}}">{{ $lang->name }}</option>
                                                    @endforeach
                                                </optgroup>
                                            </select>
                                        </div>
                                        <div class="col-12 form-group">
                                            <button type="submit" class="btn-a">Search</button>
                                        </div>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-8 col-xl-8 col-md-7 col-sm-12">
                            <div class="session-details-top first border-0">
                                <div class="left title">
                                    <h2>Find a Therapist</h2>
                                </div>
                                @if(count($therapists)>0)
                                @foreach($therapists as $index => $therapist)
                                <div class="session-details-top second">
                                    <div class="media">
                                        <div class="img-box">
                                            @if($therapist->image == "" || $therapist->image == Null)
                                                <span data-toggle="popover" data-content="johny" class="circle" style="background: #3C5088;">{{ucfirst(substr($therapist->full_name,0,1))}}</span>
                                            @else
                                            <img src="{{ $therapist->image }}" alt="{{ucfirst($therapist->full_name)}}" onerror="this.style.display='none'">
                                            @endif
                                        </div>
                                        <div class="media-body">
                                            <div class="media justify-content-between align-items-center">
                                                <div class="left">
                                                    <h2>{{ $therapist->full_name }}</h2>
                                                    <h3>Therapist</h3>
                                                </div>
                                            </div>
                                            <p>{{ $therapist->statement }}</p>
                                            <a href="{{ route('user.therapist.profile', ['id' => encrypt($therapist->id)]) }}" class="btn-a">View profile </a>
                                            {{-- <a href="#" class="btn-a" data-toggle="modal" data-target="#exampleModalCenter">Request Appointment</a> --}}
                                        </div>
                                    </div>
                                </div>
                                @endforeach

                                <nav aria-label="Page navigation example">
                                    {{ $therapists->links() }}
                                </nav>
                                @else 
                                <h3>No search results found</h3>
                                @endif
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>

        
        
    </section>
@endsection('content')

@section('js')
    <script src="{{ asset('js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/form-select2.js') }}" type="text/javascript"></script>
@endsection('js')
