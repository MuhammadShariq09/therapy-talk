@extends('layouts.app')
@section('title',' Therapist Profile')
@section('body-class',"vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar")
@section('body-col',"2-columns")
@section('content')
    <section class="contact-main therapist-profile" >
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12">
                    <div class="row">
                        <div class="col-lg-3 col-xl-3 col-md-6 col-sm-12">
                            <div class="session-details-main u-profile appointment-accepted">
                                <div class="session-details-top session-details-bottom">
                                    <div class="img-box">
                                        <img src="{{ $therapist->image }}" alt="{{ucfirst($therapist->full_name)}}" onerror="this.style.display='none'">
                                    </div>

                                   @if(auth()->user()->assessment->last()==null)
                                   <a href="#" class="btn-a bkappnmt" data-toggle="modal" data-target="#redirectToAssessment">Request Appointment</a>
                                    @elseif(auth()->user()->assessment->last() !=null && auth()->user()->assessment->last()->status==1)

                                    <a href="#" class="btn-a bkappnmt" data-toggle="modal" data-target="#bookAppointmentModal">Request Appointment</a>
                                    @else 
                                    <a href="#" class="btn-a bkappnmt" data-toggle="modal" data-target="#fillAssessment">Request Appointment</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-xl-6 col-md-12 col-sm-12">
                            <div class="session-details-top first">
                                <div class="left">
                                    <h2>{{ $therapist->name }}</h2>
                                    <h3>Therapist</h3>
                                    {{-- @if($therapist_rating) > 0) --}}
                                    <ul><div class="rating" ></div></ul>
                                    
                                    {{-- @endif  --}}
                                    {{-- <ul>
                                        <li class="active"><i class="fa fa-star"></i></li>
                                        <li class="active"><i class="fa fa-star"></i></li>
                                        <li class="active"><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                    </ul> --}}
                                </div>
                                <p class="p-italic">{{ $therapist->statement }}</p>
                            </div>
                            <div class="session-details-top second">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class=" therpaist-info">
                                            <h4>Treatment Orientation</h4>
                                            <ul>
                                            @foreach($treatment as $t)
                                                @if(in_array($t->id, explode(',', $therapist->treatments_id)))
                                                    <li>{{ $t->name }}</li>
                                                @endif
                                            @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class=" therpaist-info">
                                            <h4>Age group</h4>
                                            <ul>
                                                @foreach($agegroup as $ag)
                                                    @if(in_array($ag->id, explode(',', $therapist->age_groups_id)))
                                                        <li>{{ $ag->name }}</li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class=" therpaist-info">
                                            <h4>Language Specialty</h4>
                                            <ul>
                                                @foreach($language as $lang)
                                                    @if(in_array($lang->id, explode(',', $therapist->languages_id)))
                                                        <li>{{ $lang->name }}</li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-lg-3 col-xl-3 col-md-12 col-sm-12">
                            <div class="session-details-top third pt-0">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="therpaist-info">
                                            <h4>Availability</h4>
                                            <ul class="availability">
                                                <li>{{ $therapist->time_availability }}</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                             <div class="session-details-top third">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="therpaist-info">
                                            <h4>Skills &amp; Expertise</h4>
                                            <ul>
                                                @foreach($specialities as $s)
                                                    @if(in_array($s->id, explode(',', $therapist->specialities_id)))
                                                       <li>{{ $s->name }}</li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="session-details-top third">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="therpaist-info">
                                            <h4>Modality</h4>
                                            <ul>
                                                @foreach($modality as $m)
                                                    @if(in_array($m->id, explode(',', $therapist->modalities_id)))
                                                        <li>{{ $m->name }}</li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="session-details-top third">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="therpaist-info">
                                            <h4>Ethnic Group</h4>
                                            <ul>
                                                @foreach($ethnicity as $e)
                                                    @if(in_array($e->id, explode(',', $therapist->ethnicities_id)))
                                                        <li>{{ $e->name }}</li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-8 offset-lg-3">
                            <div class="session-details-top third feedback-sec">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <div class="login-fail-main user admin-profile profile-view" >
        <div class="featured inner">
            <div class="modal fade" id="bookAppointmentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="forget-pass">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                            <div class="modal-body">
                                <div class="payment-main-popup">
                                    <form action="{{ route('user.book-appointment') }}" id="bookAppointment" method="post" id="loader">
                                        @csrf
                                        <input type="hidden" name="trainer_id" value="{{$id}}">
                                        <div class="payment-modal-main">
                                            <div class="payment-modal-inner">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <h1>Book A Session</h1>
                                                    </div>
                                                    {{-- <div class="col-12 form-group">
                                                        <label>Duration of Session</label>
                                                        <select name="duration">
                                                            <option value="0.5">1/2 hour</option>
                                                            <option value="1">1 hour</option>
                                                            <option value="2">2 hour</option>
                                                        </select>
                                                    </div> --}}
                                                    <div class="col-12 form-group">
                                                        <label for="">Select Date</label>
                                                        <input id="datepicker" readonly name="requested_date" />
                                                    </div>
                                                    <div class="col-12 form-group">
                                                        <label for="">Select Time</label>
                                                        <input id="timepicker" readonly name="requested_time" />
                                                    </div>
                                                    <div class="col-12 form-group">
                                                        <label for="">Select Therapy Type</label>
                                                        <select name="type">
                                                            <option value="Individual">Individual</option>
                                                            <option value="Group">Group</option>
                                                            <option value="Couple">Couple</option>
                                                            <option value="Kids">Kids</option>
                                                            <option value="Family">Family</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-12 form-group">
                                                        <label for="">Select Therapy Medium</label>
                                                        <select name="session_type">
                                                          <option value="3">Video Call</option>
                                                          <option value="2">Voice Call</option>
                                                          <option value="1">Live Chat</option>
                                                        </select>
                                                      </div>
                                                    <div class="col-12 form-group">
                                                        <label for="">Reason</label>
                                                        <textarea type="text" name="reason" placeholder="Enter Reason"></textarea>
                                                    </div>

                                                    <div class="col-12 text-center blockui">
                                                        <button type="submit" class="can">Request Appointment</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="forget-pass  register-form">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                            <div class="modal-body">
                                <div class="payment-main-popup payment-view-popup">
                                    <div class="payment-modal-main">
                                        <div class="payment-modal-inner">
                                            <div class="row">
                                                <div class="col-12 text-center">
                                                    <img src="{{ asset('images/appointment-schedule-icon.png') }}" class="img-fluid" alt="">
                                                    <h3>Appointment Scheduled</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="login-fail-main user admin-profile profile-view">
                <div class="featured inner">
                  <div class="modal fade" id="fillAssessment" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                      <div class="modal-content">
                        <div class="forget-pass">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                          <div class="modal-body">
                            <div class="payment-main-popup">
                              <form action="">
                                <div class="payment-modal-main">
                                  <div class="payment-modal-inner">
                                    <div class="row">
                                      <div class="col-12">
                                        <h1 class="text-center">Your assessment is still under evaluation 
                                            process from admin Once it is verified, you 
                                            will be able to request appointment in 
                                            the application </h1>
                                      </div>
              
                                      <div class="col-12 text-center">
                                        <button type="button" class="can" data-dismiss="modal">Okay</button>
                                      </div>
              
                                    </div>
              
                                  </div>
                                </div>
                              </form>
                            </div>
              
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>

            <div class="login-fail-main user admin-profile profile-view">
                <div class="featured inner">
                  <div class="modal fade" id="redirectToAssessment" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                      <div class="modal-content">
                        <div class="forget-pass">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                          <div class="modal-body">
                            <div class="payment-main-popup">
                              <form action="">
                                <div class="payment-modal-main">
                                  <div class="payment-modal-inner">
                                    <div class="row">
                                      <div class="col-12">
                                        <h1 class="text-center">You haven't submitted your assessment form till now, please submit the assessment form now and get it approved in order to avail the services.</h1>
                                      </div>
              
                                      <div class="col-12 text-center">
                                      <a href="{{url('/assessment-form')}}" class="btn-a bkappnmt can" >Submit Now</a>
                                      </div>
              
                                    </div>
              
                                  </div>
                                </div>
                              </form>
                            </div>
              
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>

        </div>
    </div>


    {{-- <div class="modal fade" id="fillAssessment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="forget-pass  register-form">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                    <div class="modal-body">
                        <div class="payment-main-popup payment-view-popup">
                            <div class="payment-modal-main">
                                <div class="payment-modal-inner">
                                    <div class="row">
                                        <div class="col-12 text-center">
                                            <h4>Fill Assessment first in order to request therapist appointment.</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}

    
@endsection

@section('js')
    <script src="{{ asset('assets/js/book_appointment.js') }}"></script>
    <script>
        $(function () {
            var ratingValue = {{ $therapist_rating }}, rounded = (ratingValue | 0);
        
            for (var j = 0; j < 5; j++) {
            str = '<li class="active"><i class="fa ';
            if (j < rounded) {
                str += "fa-star";
            } else if ((ratingValue - j) > 0 && (ratingValue - j) < 1) {
                str += "fa-star-half-o";
            } else {
                str += "fa-star-o";
            }
            str += '" aria-hidden="true"></i><li>';
            $(".rating").append(str);
            }
        
        
        })
 </script>
@endsection
