@extends('layouts.app')
@section('title',' My Subscription Logs')
@section('body-class',"vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar")
@section('body-col',"2-columns")
@section('css')
<link rel="stylesheet" href="{{ asset('css/datatables.min.css')}}">
<link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css')}}">
@endsection
@section('content')

     
    <section class="contact-main all-notifications">
    <div class="container">
          <div class="row wow fadeInDown" data-wow-duration="0.5s" data-wow-delay="0.5s">
            <div class="col-md-7 col-sm-12 ">
              <h1>Subscription Log</h1>
            </div>
            <div class="col-md-5 col-sm-12"></div>
          </div>
        </div>
      <div class="container">
        <div class="row wow fadeInDown">
          <div class="col-lg-12 col-sm-12">
            <div class="row">
              <div class="col-lg-8 col-12">
                <h1>Current packages</h1>
                 <div class="owl-carousel owl-theme tab-menu" id="package-slider">
                            {{-- <div class="item">
                              <li><a href="#" class="tab-a active-a" data-id="tab1">{{$package->title}}</a></li>
                            </div> --}}
                        
                          </div>

                          <div class="tab-content">
                            <div  class="tab tab-active" data-id="tab1">
                              <div class="table-responsive">
                                <table class="table">
                                  <div class="table-head">
                                    <h3>{{$package->title}}</h3>
                                    <label class="blue-label">{{$user->subscriptions->expiry_date}}</label>
                                  </div>
                                  <tbody>

                                    <tr>
                                      <td>Live Chats</td>
                                      <td>Total {{$usage->live_chat}}</td>
                                      <td>Used {{$usage->live_chat_availed}}</td>
                                      <td>Left {{$usage->live_chat - $usage->live_chat_availed}}</td>
                                    </tr>
                                    <tr>
                                      <td>Voice Call</td>
                                      <td>Total {{$usage->voice_call}}</td>
                                      <td>Used {{$usage->voice_call_availed}}</td>
                                      <td>Left {{$usage->voice_call-$usage->voice_call_availed}}</td>
                                    </tr>
                                    <tr>
                                      <td>Video Call</td>
                                      <td>Total {{$usage->video_call}}</td>
                                      <td>Used {{$usage->video_call_availed}}</td>
                                      <td>Left {{$usage->video_call-$usage->video_call_availed}}</td>
                                    </tr>
                                  </tbody>
                                </table>
                              </div>
                            </div>
                           
                            <div class="text-center">
                              <button class="can" data-packageId="{{$package->id}}" type="button"
                                data-packageName="{{$package->title}}"
                                data-packageAmount="{{$package->amount}}"
                                data-toggle="modal"
                                data-target="#subscribeModal">Renew Package</button>
                                <button class="can" data-toggle="modal" data-target="#subscribeNew" >subscribe a new package</a>
                            </div>
                          </div>
              </div>
              
            </div>
            <div class="row mt-2">
              <div class="col-lg-12 col-xl-12 col-sm-12">
                <h1 class="wow fadeInUp" data-wow-duration="0.9s" data-wow-delay="0.4s">Subscription Log</h1>
              </div>
            </div>
            {{-- <form method="GET">
                <div class="row align-items-end">
                <div class="col-lg-6 col-md-12 col-sm-12 profile-view">
                    <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <label  for="">Sort By:</label>
                        <div class="row">
                        <div class="col-lg-6 col-md-6 form-group">
                        <input id="select-date" name="from" readonly placeholder="From Date">
                        </div>
                        <div class="col-lg-6 col-md-6 form-group">
                        <input id="select-date2" name="to" readonly placeholder="To Date">
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 form-group d-flex align-items-center">
                            <button type="submit" class="btn btn-sm" style="padding: 10px">Search</button>
                            </div>
                        </div>
                    </div>

                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 profile-view filter-main">
                    <div class="row">
                    <!-- <div class="col-lg-12 col-md-12 col-sm-12 form-group d-flex align-items-center">
                        <label class="w-100" for="">Filter By Session Duration:</label>
                        <select>
                        <option>1 hour</option>
                        <option>2 hour</option>
                        <option>3 hour</option>
                        </select>
                    </div> -->
                    <div class="col-lg-12 col-md-12 col-sm-12 form-group d-flex align-items-center">
                        <label class="w-100" for="">Filter By Status:</label>
                        <select>
                        <option>Waiting for Therapist's Response</option>
                        <option>Waiting for User To Pay</option>
                        <option>Appointment Rejected By Therapist</option>
                        <option>Appointment Scheduled</option>
                        <option>Session Completed</option>
                        </select>
                    </div>
                    </div>
                </div>

                </div>
            </form> --}}
            
                          <div class="table-responsive">
                            <div class="row maain-tabble">
                              <table class="table  table-striped table-bordered zero-configuration">
                                <thead>
                                  <tr>
                                    <th>S.No</th>
                                    <th>Package Name</th>
                                    <th>Amount</th>
                                    <th>Subscription date</th>
                                    <th>Expiration date</th>
                                  </tr>
                                </thead>
                                <tbody>
                                    @if(count($subcriptionLogs) <= 0)
                                        <tr><td>No record found.</td></tr>
                                    @else 
                                    @foreach($subcriptionLogs as $key=>$row)
                                   
                                    <tr>
                                      <td>{{++$key}}</td>
                                      <td>{{$row->title}}</td>
                                      <td>${{$row->amount}}</td>
                                      <td>{{$row->subscribe_date}}</td>
                                      <td>{{$row->expiry_date}}</td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

            </section>
            <div class="login-fail-main user admin-profile profile-view" >
         <div class="featured inner">
            <div class="modal fade" id="subscribeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
               <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                     <div class="forget-pass">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                        <div class="modal-body">
                           <div class="payment-main-popup">
                              <div class="payment-modal-main">
                                 <div class="payment-modal-inner">
                                    <div class="row">
                                       <div class="col-12">
                                          <div class="pkg-payment-title">
                                             <h3 class="package_title">Package: {{$package->title}}</h3>
                                             <h3 class="package_fee">Fee: ${{$package->amount}}</h3>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-12 form-group">
                                          <div class="pro-inner-row">
                                             <label><i class="fa fa-address-card" aria-hidden="true"></i> Card Holder Number</label>
                                             <input type="text" id="card" required="required" spellcheck="true" class="form-control" >
                                          </div>
                                       </div>
                                       <div class="col-12 form-group">
                                          <div class="pro-inner-row">
                                             <label><i class="fa fa-user-circle" aria-hidden="true"></i> Card Holder Name</label>
                                             <input type="text" id="card_holder" required="required" spellcheck="true" class="form-control" >
                                          </div>
                                       </div>
                                       <div class="col-lg-6 col-sm-12 form-group">
                                          <div class="pro-inner-row">
                                             <label><i class="fa fa-address-card" aria-hidden="true"></i> CVV</label>
                                             <input type="text" id="cvv" required="required" spellcheck="true" class="form-control" >
                                          </div>
                                       </div>
                                       <div class="col-lg-6 col-sm-12 form-group">
                                          <div class="pro-inner-row">
                                             <label><i class="fa fa-calendar"></i> Expiry Date</label>
                                             <input id="expiry" type="text" required="required" spellcheck="true" class="form-control">
                                          </div>
                                       </div>
                                       <div class="col-12 text-center blockui">
                                          <button type="button"  class="can" data-dismiss="modal">Cancel</button>
                                          <button type="button"  class="can" onclick="renewUserPackage()" id="cont">Pay</button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="modal fade" id="subscribeNew" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
               <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                     <div class="forget-pass ">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                        <div class="modal-body">
                           <div class="payment-main-popup payment-view-popup">
                              <div class="payment-modal-main">
                                 <div class="payment-modal-inner">
                                    <div class="row">
                                       <div class="col-12">


                                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                                          @foreach($all_packages as $key=>$row)
                                            <li class="nav-item" role="presentation">
                                              <a class="nav-link pkg-tab {{ $loop->index === 0 ? 'active': '' }}" id="tab{{ $loop->index }}-tab" data-toggle="tab" href="#tab{{ $loop->index }}" role="tab" aria-controls="tab{{ $loop->index }}" aria-selected="{{ $loop->index == 0? 'true': 'false' }}"
                                                data-pkg-id="{{$row->id}}"
                                                data-pkg-title="{{$row->title}}"
                                                data-pkg-amount="{{$row->amount}}"
                                                data-id="tab{{++$key}}"
                                              >{{$row->title}}</a>
                                            </li>
                                          @endforeach
                                        </ul>
                                        <div class="tab-content" id="myTabContent">
                                          @foreach($all_packages as $row)
                                            <div class="tab-pane fade {{ $loop->index === 0? 'show active': '' }}" id="tab{{ $loop->index }}" role="tabpanel" aria-labelledby="tab{{ $loop->index }}-tab">
                                              <table class="table">
                                                <tbody>
                                                   <tr>
                                                      <td>Live Chats</td>
                                                      <td>Total {{$row->live_chat}}</td>
                                                   </tr>
                                                   <tr>
                                                      <td>Voice Call</td>
                                                      <td>Total {{$row->voice_call}}</td>
                                                   </tr>
                                                   <tr>
                                                      <td>Video Call</td>
                                                      <td>Total {{$row->video_call}}</td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                            </div>
                                          @endforeach
                                          <div class="text-center">
                                         <button class="can" id="selectPkg">Select package</button>
                                         </div>
                                        </div>

                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="modal fade" id="payment-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
               <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                     <div class="forget-pass ">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                        <div class="modal-body">
                           <div class="payment-main-popup">
                              <input type="hidden" id="stripKey" value="{{env('STRIPE_KEY')}}">
                              <input type="hidden" value="" id="stripe_token" class="stripe_token">
                              <div class="payment-modal-main">
                                 <div class="payment-modal-inner">
                                    <div class="row">
                                       <div class="col-12">
                                          <div class="pkg-payment-title">
                                             <h3 class="package_title">Package: </h3>
                                             <h3 class="package_fee">Fee:</h3>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-12 form-group">
                                          <label for="">Credit Card Number</label>
                                          <input type="number" id="card-number" class="form-control" spellcheck="true" placeholder="Credit Card Number">
                                       </div>
                                       <div class="col-12 form-group">
                                          <label for="">Card Holder Name</label>
                                          <input type="text" id="card-holder" class="form-control" spellcheck="true" placeholder="Card Holder Name">
                                       </div>
                                       <div class="col-lg-6 col-12 form-group">
                                          <label for="">CVV</label>
                                          <input type="number" id="card-cvc" class="form-control" spellcheck="true" placeholder="CVV">
                                       </div>
                                       <div class="col-lg-6 col-12 form-group">
                                          <label for="">Expiry Date</label>
                                          <input type="text" id="card-expiry" id="select-date" onchange="getStripeToken()" placeholder="Expiry Date">
                                       </div>
                                       <div class="col-12 text-center blockui">
                                          <button type="button" class="can" data-dismiss="modal">go back</button>
                                          <button type="button" onclick="buyNew()" class="can con" >Pay </button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

@endsection('content')

@section('js')

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>

<script>
    $('#select-date').datepicker({
      uiLibrary: 'bootstrap4',
      format: 'mm/dd/yyyy'
    });
    $('#select-date2').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'mm/dd/yyyy'
      });

  $('.pkg-tab').click(function (event) {        
    var id = $(this).attr('data-pkg-id');
    var title = $(this).attr('data-pkg-title');
    var amount = $(this).attr('data-pkg-amount');
        // alert(activeTab)
        let package = {
              packageid: id,
              packagename: title,
              packageamount: amount
          }
          console.log('package',package);
          $('.package_title').text(package.packagename+': 1 Month');
          $('.package_fee').text('Fee: $'+package.packageamount);
          localStorage.setItem('package', JSON.stringify(package));
});

$('#selectPkg').on('click', function(){
  let package = localStorage.getItem('package');
  console.log('package',package);
  if(package==null){
    alert('Please select any package.')
  }else{
    $('#subscribeNew').modal('hide');
    $('#payment-modal').modal('show');
  }
 
});

$('.pkg-tab').click(function (event) {        
    var id = $(this).attr('data-pkg-id');
    var title = $(this).attr('data-pkg-title');
    var amount = $(this).attr('data-pkg-amount');
        // alert(activeTab)
        let package = {
              packageid: id,
              packagename: title,
              packageamount: amount
          }
          console.log('package',package);
          $('.package_title').text(package.packagename+': 1 Month');
          $('.package_fee').text('Fee: $'+package.packageamount);
          localStorage.setItem('package', JSON.stringify(package));
});
  </script>

   
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
    <script src="{{ asset('js/package.js') }}"></script>
    {{-- <script src="{{ asset('js/owl.carousel.min.js') }}"></script> --}}

    
     
    <script type="text/javascript">
  $( document ).ready(function() {
    $(".dataTables_filter input").attr("placeholder", "Search");
  });
</script>
@endsection
