@extends('layouts.therapist')
@section('title',' Appointment Details')
@section('body-class',"vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar")
@section('body-col',"2-columns")

@section('content')
<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-body">
        <!-- Basic form layout section start -->
        <section id="configuration" class="search view-cause ">
          <div class="row">
            <div class="col-12">
              <div class="card rounded pad-20">
                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">
                    <div class="row">
                      <div class="col-md-6 col-sm-6">
                        <h1 class="pull-left" style="margin-bottom: 15px;">My Appointments</h1>
                      </div>
                    
                      <div class="col-md-6 col-sm-6">
                      @if($diff_in_days <= 0)
                        <div class="actions clearfix text-right">
                          <button class="btn btn-info ">Your Session will start on {{ $date }} {{ $time }}</button>
                          @if($session->status==1)
                          <button class="btn btn-info" id="start" onclick="changeStatus({{ $session->id }}, 3)">Start Session</button>
                          <button class="btn btn-info" id="complete" style="display:none" onclick="changeStatus({{ $session->id }}, 4)">Mark Complete</button>
                          @elseif($session->status==3)
                          <button class="btn btn-info" id="complete" onclick="changeStatus({{ $session->id }}, 4)">Mark Complete</button>
                          @else 
                          <button class="btn btn-info" >{{ $session->status==4 ? "Session Completed" : "Session Expired"}}</button>
                          @endif
                        </div>
                        </div>
                        @elseif($diff_in_days>0)
                      <div class="actions clearfix text-right">
                      <button class="btn btn-info ">Your Session will start on {{ $date }} {{ $time }}</button>
                      </div>
                      </div>
                      @else
                     
                        <div class="actions clearfix text-right">
                        <button class="btn btn-info" >{{ $session->status==4 ? "Session Completed" : "Session Expired"}}</button>
                        </div>
                      </div> 
                      @endif 
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                      <input type="hidden" id="type" value="{{ ($session->session_type == 1) ? '0' : ($session->session_type == 2 ? '1' : '') }}" />
                      @if($diff_in_days<=0 && $session->status!==4)
                        <div id="chat"></div>
                      @endif
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
</div>


@endsection

@section('js')
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script uid="{{ Auth::user()->id }}" src="https://dev28.onlinetestingserver.com/soachatcentralizedWeb/js/ocs.js"></script>
    <script >
        $(document).ready(function() {
          var type = $('#type').val();
          console.log('type', type);
          if(type==""){
            ocs.init({
                appid: "ffdf711c6f4e05f6860314918df47def",
                appkey: "36a06ece5863a3af3bceb8ce7247ee56",
                domain: "therapytalk.com",
                global: '0',
                id: "{{ auth()->user()->id }}", 
                toid: "{{ $session->user_id }}",
                colorScheme: '00b7d5',
               // onlyAudio: , // will be given if you need audio chat
              //  onlyAudio: 0, // will be given if you need audio chat
                element: "#chat",
          });
          }
          else{
            ocs.init({
                appid: "ffdf711c6f4e05f6860314918df47def",
                appkey: "36a06ece5863a3af3bceb8ce7247ee56",
                domain: "therapytalk.com",
                global: '0',
                id: "{{ auth()->user()->id }}", 
                toid: "{{ $session->user_id }}",
                colorScheme: '00b7d5',
                onlyAudio: type==2 ? 1 : 0, // will be given if you need audio chat
               // onlyAudio: 1, // will be given if you need audio chat
                element: "#chat",
          });
          }
          
    
        });
    </script>
<script src="{{ asset('js/main.js') }}" type="text/javascript"></script>
@endsection
