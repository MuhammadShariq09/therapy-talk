@extends('layouts.therapist')
@section('title',' Subscribe Package')
@section('body-class',"vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar")
@section('body-col',"2-columns")

@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="">
                <div class="content-body">
                    <section id="configuration" class="justify-content-center u-c-p u-profile a-edit row">
                        <div class="col-md-9 col-lg-9 text-center">
                            <div class="card pro-main">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 text-center">
                                        <h1>Subscribe Package</h1>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <p>Please select a package to continue</p>
                                    </div>
                                </div>
                                <div class="row">
                                    @foreach($packages as $package)
                                    <div class="col-lg-4 col-md-6 col-sm-12">
                                        <div class="price-box">
                                            <div class="price-tag">
                                                <h3>{{$package->month}} Months<span>Package</span></h3>
                                                <h2>${{number_format($package->amount, 2)}}</h2>
                                            </div>
                                            <div class="price-list">
                                                <ul>
                                                    <li class="check">Lorem ipsum dolor sit amet</li>
                                                    <li class="check">Exea commodo consequat duise</li>
                                                    <li class="check">Sunt in culpa qui officia deserunt warsdas.</li>
                                                    <li class="check">Exea commodo consequat duise</li>
                                                    <li class="cross">Sunt in culpa qui officia deserunt warsdas.</li>
                                                    <li class="cross">Exea commodo consequat duise</li>
                                                    <li class="cross">Sunt in culpa qui officia deserunt warsdas.</li>
                                                </ul>
                                                <!-- <a href="#">Select</a> -->
                                            </div>
                                            <div class="price-btn">
                                                <button data-packageId="{{$package->id}}" type="button" class="subpkg big-center-cta"
                                                        data-packageName="{{$package->title}}"
                                                        data-packageAmount="{{$package->amount}}"
                                                        data-packageMonth="{{$package->month}}"
                                                        data-toggle="modal"
                                                        data-target="#subscribeModal">Select</button>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
    <div class="login-fail-main user u-c-p  change-profile">
        <div class="featured inner ">
            <div class="modal fade pkg-payment" id="subscribeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                        <div class="payment-main-popup payment-view-popup">
                            <div class="payment-modal-main">
                                <div class="payment-modal-inner">
                                    <div class="row">
                                        <div class="col-12">
                                            <h1>Payment</h1>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="pkg-payment-title">
                                                <h3 class="package_title">Package: 3 Months</h3>
                                                <h3 class="package_fee">Fee: $ 30</h3>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 form-group">
                                            <div class="pro-inner-row">
                                                <label><i class="fa fa-address-card" aria-hidden="true"></i> Card Holder Number</label>
                                                <input type="text" id="card" required="required" spellcheck="true" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="col-12 form-group">
                                            <div class="pro-inner-row">
                                                <label><i class="fa fa-user-circle" aria-hidden="true"></i> Card Holder Name</label>
                                                <input type="text" id="card_holder" required="required" spellcheck="true" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-sm-12 form-group">
                                            <div class="pro-inner-row">
                                                <label><i class="fa fa-address-card" aria-hidden="true"></i> CVV</label>
                                                <input type="text" id="cvv" required="required" spellcheck="true" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-sm-12 form-group">
                                            <div class="pro-inner-row">
                                                <label><i class="fa fa-calendar"></i> Expiry Date</label>
                                                <input id="expiry" type="text" required="required" spellcheck="true" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-12 text-center">
                                            <button type="button"  class="can" data-dismiss="modal">Cancel</button>
                                            <button type="button"  class="can" onclick="savePackage()" id="cont">Confirm</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>



@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
    <script src="{{ asset('js/package.js') }}"></script>

@endsection
