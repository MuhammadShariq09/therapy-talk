@extends('layouts.therapist')
@section('title',' Session Logs')
@section('body-class',"vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar")
@section('body-col',"2-columns")

@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="">
                <div class="content-body">
                    <section id="configuration" class="search view-cause ">
                        <div class="row">
                            <div class="col-12">
                                <div class="card rounded pad-20">
                                    <div class="card-content collapse show">
                                        <div class="card-body table-responsive card-dashboard">
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12">
                                                    <h1 class="pull-left">Session Log</h1>
                                                </div>
                                                <div class="col-lg-6 col-md-12 col-sm-12">

                                                </div>
                                                <div class="col-lg-6 col-md-12 col-sm-12">
                                                <form method="GET">
                                                    <div class="u-c-p subs-log-filter">
                                                        <div class="col-lg-12 form-group d-flex align-items-center">
                                                        <label>From: Date</label>
                                                        <input id="select-date" name="from" readonly placeholder="From Date">
                                                        </div>
                                                        <div class="col-lg-12 form-group d-flex align-items-center">
                                                        <label>To: Date</label>
                                                        <input id="select-date2" name="to" readonly placeholder="To Date">
                                                        </div>
                                                        <div class="add-user date-input d-flex align-items-center form-group">
                                                            <button type="submit" class="btn btn-sm" style="padding: 10px">Search</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                </form>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="maain-tabble">
                                                <table class="table table-striped table-bordered zero-configuration">
                                                    <thead>
                                                    <tr>
                                                        <th>S.no</th>
                                                        <th>User Name</th>
                                                        <th>Date</th>
                                                        <th>Status</th>
                                                        <th>Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                        @if(count($sessions) <= 0)
                                                        <tr><td>No record found.</td></tr>
                                                        @else 
                                                            @foreach($sessions as $key => $session)

                                                            <tr>
                                                                <td>{{ ++$key }}</td>
                                                                <td>
                                                                    @if(isset($session->user->image) &&  $session->user->image == "")
                                                                        <span data-toggle="popover" data-content="johny" class="circle"
                                                                            style="background: #3C5088;">{{ucfirst(substr($session->user->full_name,0,1))}}</span>
                                                                    @else
                                                                        <span class="avatar avatar-online">
                                                                                <img class="tab-img" src="{{ isset($session->user->image) ? asset($session->user->image) : ''  }}" alt="{{ucfirst($session->user->full_name)}}" onerror="this.style.display='none'">
                                                                            </span>
                                                                    @endif
                                                                {{ucfirst($session->user->full_name)}}
                                                                </td>
                                                                <td>{{ $session->requested_date }} {{$session->requested_time}}</td>
                                                                @if($session->status == 0)
                                                                    <td><span class="badge badge-default">Appointment request received</span></td>
                                                                @elseif($session->status == 1)
                                                                    @if($session->is_refund)
                                                                        <td><span class="badge badge-danger">Appointment Cancelled/Refunded</span></td>
                                                                    @elseif($session->is_paid==1)
                                                                        <td><span class="badge badge-success">Appointment scheduled</span></td>
                                                                    @else
                                                                        <td><span class="badge badge-primary">Appointment accepted waiting for response</span></td>
                                                                    @endif
                                                                @elseif($session->status == 3)
                                                                    <td><span class="badge badge-info">Session Started</span></td>
                                                                @elseif($session->status == 4)
                                                                    <td><span class="badge badge-success">Session Completed</span></td>
                                                                @else
                                                                    <td><span class="badge badge-secondary">Appointment request Rejected</span></td>
                                                                @endif
                                                                <td>
                                                                    <div class="btn-group mr-1 mb-1">
                                                                        <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i></button>
                                                                        <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(4px, 23px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                                            <a class="dropdown-item"
                                                                            href="{{ route('therapist.session.logs.view', ['id' => $session->id]) }}"><i class="fa fa-eye"></i>View </a>
                                                                        </div>
                                                                </td>
                                                            </tr>
                                                            @endforeach
                                                        @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <!--main card end-->
                                </div>
                            </div>
                        </div>
                    </section>

                </div>
            </div>
        </div>
    </div>



@endsection

@section('js')
<script>
    $('#select-date').datepicker({
      uiLibrary: 'bootstrap4',
      format: 'mm/dd/yyyy'
    });
    $('#select-date2').datepicker({
                uiLibrary: 'bootstrap4',
                format: 'mm/dd/yyyy'
              });
  </script>
@endsection
