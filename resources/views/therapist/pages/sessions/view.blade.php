@extends('layouts.therapist')
@section('title',' Session Logs')
@section('body-class',"vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar")
@section('body-col',"2-columns")
@section('css')
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css"/>
<style>
.modal {
z-index: 1203;
top: 0;
}

</style>
@endsection
@section('content')
<div class="app-content content">
<div class="content-wrapper">
<div class="">
<div class="content-body">
<section id="configuration" class="u-c-p u-profile a-edit">
<div class="row">
<div class="col-12">
<div class="card pro-main">
<div class="row">
<div class="col-md-6 col-sm-12">
    <h1>session log</h1>
</div>
</div>
<div class="row">
<div class="col-lg-12 col-12">
    <div class="profile-frm">
        <div class="row">
            <div class="col-12 text-center">
                <div class="pro-img-main">
                <img src="{{ $session->user->image ? $session->user->image : asset('images/student-pro-girl.png') }}" alt="{{$session->user->full_name}}" onerror="this.style.display='none'">
                </div>
            </div>
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <!--col end-->
            <div class="col-12">
                <div class="row">
                    <div class="col-lg-6 col-sm-12">
                        <div class="pro-inner-row">
                            <label><i class="fa fa-user" aria-hidden="true"></i>
                                User Name</label>
                            <input type="text" required="required"
                                   spellcheck="true"
                                   disabled class="form-control"
                                   placeholder="{{ $session->user->name }}">
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-12">
                        <div class="pro-inner-row">
                            <label><i class="fa fa-users"
                                      aria-hidden="true"></i> Session
                                type</label>
                            <input type="text" required="required"
                                   spellcheck="true"
                                   disabled class="form-control"
                                   placeholder="{{ $session->type }}">
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-12">
                        <div class="pro-inner-row">
                            <label><i class="fa fa-calendar"
                                      aria-hidden="true"></i> Date of
                                Birth</label>
                            <input type="text" required="required"
                                   spellcheck="true"
                                   disabled class="form-control"
                                   placeholder="{{ $session->user->date_of_birth }}">
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-12">
                        <div class="pro-inner-row">
                            <label><i class="fa fa-transgender"
                                      aria-hidden="true"></i> Gender</label>
                            <input type="text" required="required"
                                   spellcheck="true"
                                   disabled class="form-control"
                                   placeholder="{{ ($session->user->gender) ? "Female" : "Male" }}">
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-12">
                        <div class="pro-inner-row">
                            <label><i class="fa fa-file-text"
                                      aria-hidden="true"></i> Reason For seeking
                                therapy</label>
                            <textarea class="form-control"
                                      disabled>{{ $session->reason }}</textarea>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-12">
                        <div class="pro-inner-row">
                            <label><i class="fa fa-clock-o"
                                      aria-hidden="true"></i> Session
                                Duration</label>
                            <input type="text" required="required"
                                   spellcheck="true" disabled
                                   class="form-control"
                                   placeholder="{{$session->duration}} Hour">
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-12">
                        <div class="pro-inner-row">
                            <label><i class="fa fa-clock-o"
                                      aria-hidden="true"></i> Session
                                Medium</label>
                            <input type="text" required="required"
                                   spellcheck="true" disabled
                                   class="form-control"
                                   placeholder="{{ ($session->session_type == 1) ? 'Live Chat' : ($session->session_type == 2 ? "Voice Call" : "Video Call") }}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-lg-8 col-md-12 col-sm-12">
                <div class="card rounded">
                    <div class="card-content">
                        <div class="card-body">
                            <label class="blue-label">
                                Appointment scheduled Requested On
                            </label>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="pro-inner-row">
                                        <label><i class="fa fa-calendar"
                                                  aria-hidden="true"></i>
                                            Date</label>
                                        <input type="text" required="required"
                                               spellcheck="true"
                                               disabled class="form-control"
                                               placeholder="{{ $session->requested_date }}">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="pro-inner-row">
                                        <label><i class="fa fa-clock-o"
                                                  aria-hidden="true"></i>
                                            Time</label>
                                        <input type="text" required="required"
                                               spellcheck="true"
                                               disabled class="form-control"
                                               placeholder="{{ $session->requested_time }}">
                                    </div>
                                </div>
                                 @if($session->status == 0)
                                    <div class="col-12 text-center">
                                        <button type="button"
                                                class="big-center-cta bg-secondary"
                                                data-toggle="modal"
                                                data-target="#datamodal2">Reject
                                        </button>
                                        <button type="button"
                                                class="big-center-cta mr-1"
                                                data-toggle="modal"
                                                data-target=".bd-example-modal-lg">
                                            Accept
                                        </button>

                                    </div>
                                 @else
                                     @if($session->reschedule)
                                         <div class="col-12">
                                         @foreach($session->reschedule as $index => $reschedule)
                                            <label class="blue-label">
                                                Appointment Reschedule
                                            </label>
                                            <div class="row">
                                                <div class="col-lg-12 col-sm-12">
                                                    <h6>Rescheduled By {{ ($reschedule->action_by) ? "User" : "Therapist" }}:</h6>
                                                    <p>Suggested Date &amp; Time:</p>
                                                </div>
                                                <div class="col-lg-6 col-sm-12">
                                                    <div class="pro-inner-row">
                                                        <label><i class="fa fa-calendar" aria-hidden="true"></i> Date</label>
                                                        <input type="text" required="required" spellcheck="true"
                                                               disabled="" class="form-control" value="{{ $reschedule->suggested_date }} ">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-sm-12">
                                                    <div class="pro-inner-row">
                                                        <label><i class="fa fa-clock-o" aria-hidden="true"></i> Time</label>
                                                        <input type="text" required="required" spellcheck="true" disabled=""
                                                               class="form-control" value="{{ $reschedule->suggested_time }} ">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-sm-12">
                                                    @if($reschedule->status == 0)
                                                        <p>Status: Waiting for reply</p>
                                                        @if($reschedule->action_by == 1)
                                                        <div class="row">
                                                            <div class="col-12 text-center">
                                                                <a href="{{route('therapist.approve-resedule-request', ['id' => $reschedule->id])}}"
                                                                   type="button" class="big-center-cta" >Accept</a>
                                                            </div>
                                                            <div class="col-12 text-center">
                                                                <button type="button" class="big-center-cta" data-toggle="modal" data-target="#reject-rq-modal">Reject</button>
                                                            </div>
                                                        </div>
                                                        <div class="login-fail-main user change-pass">
                                                            <div class="featured inner">
                                                                <div class="modal fade bd-example-modal-lg" id="reject-rq-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                                                    <div class="modal-dialog modal-dialog-centered">
                                                                        <div class="modal-content">
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                                                                            <div class="payment-modal-main">
                                                                                <div class="payment-modal-inner card-body"> <img src="images/request-rejected.png" class="img-fluid" alt="">
                                                                                    <h6>Are you sure you want reject reschedule request?</h6>
                                                                                    <form method="post"
                                                                                          action="{{ route('therapist.reject-resedule-request', ['id' => $reschedule->id]) }}">
                                                                                        @csrf
                                                                                    <div class="row">
                                                                                        <div class="col-12 form-group">
                                                                                            <div class="pro-inner-row">
                                                                                                <input id="select-date" name="t_suggested_date" type="text" readonly="" required="required" spellcheck="true" class="form-control" placeholder="Suggested Date">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-12 form-group">
                                                                                            <div class="pro-inner-row">
                                                                                                <input type="text" name="t_suggested_time" id="timepicker" class="form-control" placeholder="Suggested Time">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-12 text-center">
                                                                                            <button type="button" data-dismiss="modal" class="can">Cancel</button>
                                                                                            <button type="submit" class="can">Send to client</button>
                                                                                        </div>
                                                                                    </div>

                                                                                    </form>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @endif
                                                    @elseif($reschedule->status == 1)
                                                        <p>Status: Approved</p>
                                                    @else
                                                        <p>Status: Rejected</p>
                                                        @if($session->is_refund)
                                                            <h2 style="color:red"><span>Appointment Cancelled</span></h2>
                                                            <p>Our representative will soon contact you regarding your refund</p>
                                                        @endif
                                                        @if($reschedule->t_suggested_time)
                                                        <div class="row">
                                                            <div class="col-lg-12 col-sm-12">
                                                                <h6>Suggested Date By Therapist:</h6>
                                                                <p>Suggested Date &amp; Time:</p>
                                                            </div>
                                                            <div class="col-lg-6 col-sm-12">
                                                                <div class="pro-inner-row">
                                                                    <label><i class="fa fa-calendar" aria-hidden="true"></i> Date</label>
                                                                    <input type="text" required="required" spellcheck="true"
                                                                           placeholder="{{ date('m/d/Y', strtotime($reschedule->t_suggested_date)) }}" class="form-control" >
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6 col-sm-12">
                                                                <div class="pro-inner-row">
                                                                    <label><i class="fa fa-clock-o" aria-hidden="true"></i> Time</label>
                                                                    <input type="text" required="required" spellcheck="true"
                                                                           placeholder="{{ $reschedule->t_suggested_time }}"
                                                                           disabled="" class="form-control" >
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @endif

                                                    @endif
                                                </div>
                                            </div>
                                         @endforeach
                                         </div>
                                     @endif
                                     @if($session->status == 1 && $session->payment && $diff_in_hours < 48)
                                         @if(!count($session->reschedule)) <div class="col-12">
                                                <label class="red-label">
                                                    You can reschedule this appointment within 48 Hours
                                                </label>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <button type="button" class="big-center-cta" data-toggle="modal"
                                                                data-target="#reschedule_appointment">Reschedule Appointment</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="login-fail-main user change-pass">
                                                <div class="featured inner">
                                                    <div class="modal fade bd-example-modal-lg" id="reschedule_appointment" tabindex="-1" role="dialog"
                                                         aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog modal-dialog-centered">
                                                            <div class="modal-content">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                                                                <div class="payment-modal-main">
                                                                    <div class="payment-modal-inner"> <img src="{{ asset('images/reschedule.png') }}" class="img-fluid" alt="">
                                                                        <h3>Reschedule Appointment</h3>
                                                                        <form method="post" action="{{ route('therapist.reschedule-approved-appointment', ['id' => $session->id ]) }}">
                                                                        @csrf
                                                                        <div class="row">
                                                                            <div class="col-12 form-group">
                                                                                <div class="pro-inner-row">
                                                                                    <input id="select-date"
                                                                                           type="text" name="suggested_date" required="required" spellcheck="true"
                                                                                           class="form-control" placeholder="Suggested Date">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-12 form-group">
                                                                                <div class="pro-inner-row">
                                                                                    <input type="text" name="suggested_time" id="timepicker" class="form-control" placeholder="Suggested Time">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-12 text-center">
                                                                                <button type="submit" class="can">Send to client</button>
                                                                            </div>
                                                                        </div>
                                                                        </form>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                         @endif
                                     @endif
                                 @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if($session->status == 2)
                <div class="col-xl-6 col-lg-8 col-sm-12">
                    <div class="card rounded">
                        <div class="card-content">
                            @foreach($session->rejections as $rej)
                            <div class="card-body">
                                <label class="blue-label">
                                    Appointment Rejected
                                </label>

                                <div class="row">
                                    <div class="col-lg-12 col-sm-12">
                                        <div class="pro-inner-row">
                                            <label><i class="fa fa-file-text" aria-hidden="true"></i> Reason</label>
                                            <textarea class="form-control" disabled="">{{ $rej->reason }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <label class="blue-label">
                                    Suggested Time & Date
                                </label>
                                <div class="row">
                                    <div class="col-lg-6 col-sm-12">
                                        <div class="pro-inner-row">
                                            <label><i class="fa fa-calendar" aria-hidden="true"></i> Date</label>
                                            <input type="text" required="required"
                                                   spellcheck="true" disabled=""
                                                   class="form-control" placeholder="{{ $rej->suggested_date }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-sm-12">
                                        <div class="pro-inner-row">
                                            <label><i class="fa fa-clock-o" aria-hidden="true"></i> Time</label>
                                            <input type="text" required="required" spellcheck="true" disabled=""
                                                   class="form-control" placeholder="{{ $rej->suggested_time }}">
                                        </div>
                                    </div>

                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>

                </div>
            @endif
            <div class="login-fail-main user">
                <div class="featured inner">
                    <div class="modal fade bd-example-modal-lg" tabindex="-1"
                         role="dialog" aria-labelledby="myLargeModalLabel"
                         aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                                <button type="button" class="close"
                                        data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <div class="payment-modal-main">
                                    <div class="payment-modal-inner">
                                        <form
                                            action="{{ route('therapist.session.logs.approve', ['id' => $session->id ]) }}"
                                            method="post">
                                            @csrf
                                            <img
                                                src="{{ asset('images/request-accepted.png') }}"
                                                class="img-fluid" alt="">
                                            <h3>Accept Appointment</h3>
                                            <p>Are you sure you want accept the
                                                appointment?</p>
                                            <div class="row mt-3 mb-2">
                                                <div class="col-lg-6 col-sm-12">
                                                    <div class="pro-inner-row">
                                                        <label><i
                                                                class="fa fa-clock-o"
                                                                aria-hidden="true"></i>
                                                            Session
                                                            Duration</label>
                                                        <input type="text"
                                                               required="required"
                                                               spellcheck="true"
                                                               disabled
                                                               class="form-control"
                                                               placeholder="{{$session->duration}} Hour">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-sm-12">
                                                    <div class="pro-inner-row">
                                                        <label><i
                                                                class="fa fa-money"
                                                                aria-hidden="true"></i>
                                                            Session Fee </label>
                                                        <input type="text"
                                                               required="required"
                                                               spellcheck="true"
                                                               disabled
                                                               class="form-control"
                                                               placeholder="${{ ($session->session_type == 1) ? config('app.live_chat') : ($session->session_type == 2 ?config('app.voice_call') : config('app.video_call')) }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12 text-center">
                                                    <button type="submit"
                                                            class="can">Accept
                                                    </button>
                                                </div>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="login-fail-main user change-pass">
                <div class="featured inner">
                    <div class="modal fade bd-example-modal-lg" id="datamodal2"
                         tabindex="-1" role="dialog"
                         aria-labelledby="myLargeModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                                <button type="button" class="close"
                                        data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <div class="payment-modal-main">
                                    <div class="payment-modal-inner"><img
                                            src="{{ asset('images/request-rejected.png') }}"
                                            class="img-fluid" alt="">
                                        <form
                                            action="{{ route('therapist.session.logs.reject', ['id' => $session->id ]) }}"
                                            method="post">
                                            @csrf
                                            <h3>Reject Appointment</h3>
                                            <div class="row mt-3 mb-2">
                                                <div class="col-12">
                                                    <div class="pro-inner-row">
                                                        <i class="fa fa-lock"></i>
                                                        <textarea
                                                            class="form-control"
                                                            name="reason"
                                                            placeholder="Enter Reject Reason"></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-12 form-group">
                                                    <div class="pro-inner-row">
                                                        <i class="fa fa-calendar"></i>
                                                        <input id="select-date"
                                                               type="text"
                                                               required="required"
                                                               name="suggested_date"
                                                               spellcheck="true"
                                                               class="form-control"
                                                               placeholder="Suggested Date">
                                                    </div>
                                                </div>
                                                <div class="col-12 form-group">
                                                    <div class="pro-inner-row">
                                                        <i class="fa fa-clock-o"></i>
                                                        <input type="text"
                                                               id="timepicker"
                                                               name="suggested_time"
                                                               class="form-control"
                                                               placeholder="Suggested Time">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12 text-center">
                                                    <button type="submit"
                                                            class="can">Send to
                                                        client
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!--row end-->
</div>
<!--profile form end-->
</div>
</div>
</div>
</div>
</section>
</div>
</div>
</div>
</div>



@endsection

@section('js')
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<script>

$(function () {
$('#select-date').datepicker({
uiLibrary: 'bootstrap4'
});
$('#timepicker').timepicker({
uiLibrary: 'bootstrap4'
});
})

</script>
@endsection
