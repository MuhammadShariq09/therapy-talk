@extends('layouts.therapist')
@section('title',' Payment Logs')
@section('body-class',"vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar")
@section('body-col',"2-columns")

@section('content')

<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-body">
      <!-- Basic form layout section start -->
      <section id="configuration" class="search view-cause ">
        <div class="row">
          <div class="col-12">
            <div class="card rounded pad-20">
              <div class="card-content collapse show">
                <div class="card-body table-responsive card-dashboard">
                  <div class="row">
                    <div class="col-md-12 col-sm-12">
                      <h1 class="pull-left">Payment Log</h1>
                    </div>
                    <div class="col-xl-12 col-lg-12 col-md-12">
                      
                        <div class="card-content">
                          <div class="card-body db-pad">

                            <div class="media home-chart align-items-center">
                              <h5 class="text-center">Total Earning</h5>
                              <div class="media-body">
                                
                                <h5 class="text-center">Months</h5>
                                <div id="basic-column" class="height-400 echart-container"></div>

                              </div>
                            </div>
                          </div>
                        </div>
                      
                    </div>
                  </div>
                 
                  <div class="row">
                    
                    <div class="col-lg-6 col-md-4 col-sm-12">
                    </div>
                    <div class="col-lg-6 col-md-8 col-sm-12">
                    <form method="GET">
                        <div class="u-c-p subs-log-filter">
                            <div class="col-lg-12 form-group d-flex align-items-center">
                            <label>From: Date</label>
                            <input id="select-date" name="from" readonly placeholder="From Date">
                            </div>
                            <div class="col-lg-12 form-group d-flex align-items-center">
                            <label>To: Date</label>
                            <input id="select-date2" name="to" readonly placeholder="To Date">
                            </div>
                            <div class="add-user date-input d-flex align-items-center form-group">
                                <button type="submit" class="btn btn-sm" style="padding: 10px">Search</button>
                            </div>
                        </div>
                    </div>
                    </form>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="maain-tabble mt-3">
                    <table class="table table-striped table-bordered zero-configuration">
                      <thead>
                        <tr>
                          <th>S.no</th>
                          <th>User Name</th>
                          <th>Session Conducted On</th>
                          <th>Total Session Fee</th>
                          <th>My Earning</th>
                          <th>Status</th>
                        </tr>
                      </thead>
                      <tbody>
                        @if(count($sessions) <= 0)
                        <tr><td>No record found.</td></tr>
                        @else 
                            @foreach($sessions as $key => $session)
                          
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>
                                @if(isset($session->user->image) &&  $session->user->image == "")
                                      <span data-toggle="popover" data-content="johny" class="circle"
                                          style="background: #3C5088;">{{ucfirst(substr($session->user->full_name,0,1))}}</span>
                                  @else
                                      <span class="avatar avatar-online">
                                              <img class="tab-img" src="{{ isset($session->user->image) ? asset($session->user->image) : ''  }}" alt="{{ucfirst($session->user->full_name)}}" onerror="this.style.display='none'">
                                          </span>
                                  @endif
                              {{ucfirst($session->user->full_name)}}
                                  </td>
                                  <td>{{ $session->requested_date }} {{$session->requested_time}}</td>
                                  <td>{{$session->amount }}</td>
                                  <td>{{$session->amount }}</td>
                                  @if($session->is_paid == 1)
                                    <td>Paid</td>
                                  @else
                                      <td>Un Paid</td>
                                  @endif
                                  
                              </tr>
                          @endforeach
                        @endif
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <!--main card end-->
            </div>
          </div>
        </div>
      </section>
      <!-- // Basic form layout section end -->
    </div>
  </div>
</div>




@endsection



@section('js')
<script>

  $(function () {
      $('#select-date').datepicker({
          uiLibrary: 'bootstrap4'
      });
      $('#select-date2').datepicker({
          uiLibrary: 'bootstrap4'
      });
      $('#timepicker').timepicker({
          uiLibrary: 'bootstrap4'
      });
  })

</script>

<script src="{{ asset('app-assets/vendors/js/charts/echarts/echarts.js') }}" type="text/javascript"></script>
<script src="{{ asset('app-assets/js/scripts/charts/echarts/bar-column/basic-column.js') }}" type="text/javascript"></script> 

<script type="text/javascript">
  // Set paths
// ------------------------------
require.config({
paths: {
    echarts: 'app-assets/vendors/js/charts/echarts'
}
});
// Configuration
// ------------------------------
require(
[
'echarts',
'echarts/chart/bar',
'echarts/chart/line',
'echarts/chart/scatter',
'echarts/chart/pie'
],
// Charts setup
function (ec) {
// Initialize chart
// ------------------------------
var myChart = ec.init(document.getElementById('basic-column'));
// Chart Options
// ------------------------------
chartOptions = {
// Setup grid
grid: {
x: 50,
x2: 40,
y: 45,
y2: 25
},
// Add tooltip
tooltip: {
trigger: 'axis'
},
// Add legend

// Add custom colors
color: ['#191919', '#0053a8', '#ff8b45'],
// Enable drag recalculate
calculable: true,
// Horizontal axis
xAxis: [{
type: 'category',
data: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
}],
// Vertical axis
yAxis : [
{
type : 'value',
name : 'Water',
axisLabel : {
    formatter: '{value} ml'
},
padding:10
},
{
type : 'value',
name : 'Temperature',
axisLabel : {
    formatter: '{value} °C'
},
padding:10
}
],
// Add series
series : [
{
name:'Evaporation',
type:'bar',
data:[2.0, 4.9, 7.0, 23.2, 25.6, 76.7, 135.6, 162.2, 32.6, 20.0, 6.4, 3.3]
},
{
name:'Precipitation',
type:'bar',
data:[2.6, 5.9, 9.0, 26.4, 28.7, 70.7, 175.6, 182.2, 48.7, 18.8, 6.0, 2.3]
},
{
name:'Average temperature',
type:'line',
yAxisIndex: 1,
data:[2.0, 2.2, 3.3, 4.5, 6.3, 10.2, 20.3, 23.4, 23.0, 16.5, 12.0, 6.2]
}
]
};
// Apply options
// ------------------------------
myChart.setOption(chartOptions);
// Resize chart
// ------------------------------
$(function () {
// Resize chart on menu width change and window resize
$(window).on('resize', resize);
$(".menu-toggle").on('click', resize);
// Resize function
function resize() {
setTimeout(function() {
// Resize chart
myChart.resize();
}, 200);
}
});
}
);
</script>

<script>
    $('#select-date').datepicker({
      uiLibrary: 'bootstrap4',
      format: 'mm/dd/yyyy'
    });
    $('#select-date2').datepicker({
                uiLibrary: 'bootstrap4',
                format: 'mm/dd/yyyy'
              });
  </script>


@endsection
