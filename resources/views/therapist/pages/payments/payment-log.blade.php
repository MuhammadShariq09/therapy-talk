@extends('layouts.therapist')
@section('title',' Payment Logs')
@section('body-class',"vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar")
@section('body-col',"2-columns")

@section('content')

<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-body">
    <section id="configuration" class="search view-cause payment-log">
        <form method="GET" >
            <div class="row">
              <div class="col-12">
                <div class="card rounded pad-20">
                  <div class="card-content collapse show">
                    <div class="card-body table-responsive card-dashboard">
                      <div class="row">
                        <div class="col-md-12 col-sm-12">
                          <h1 class="pull-left">Payment Log</h1>
                        </div>
                      </div>
                      <div class="row align-items-center justify-content-between mt-2">
                        <div class="col-lg-4 col-md-6 col-sm-12 form-group">
                          <div class="card rounded grey-card m-0">
                            <div class="card-content">
                                <div class="card-body top-card cleartfix">
                                    
                                    <div class="media align-items-stretch">
                                        
                                        <div class="media-body align-self-center">
                                            <span>Total Earning</span>
                                        </div>
                                        <div class="media-body align-self-center">
                                            <h4>${{$total}}</h4>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                      
                        
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 form-group u-c-p subs-log-filter">
                              <select name="month">
                                <option>Select Month</option>
                                <option value="01">January</option>
                                <option value="02">Feburary</option>
                                <option value="03">March</option>
                                <option value="04">April</option>
                                <option value="05">May</option>
                                <option value="06">June</option>
                                <option value="07">July</option>
                                <option value="08">August</option>
                                <option value="09">September</option>
                                <option value="10">Octuber</option>
                                <option value="11">November</option>
                                <option value="12">December</option>
                              </select>
                      </div>
                    </div>
                    <div class="row u-c-p mt-2">
                      <div class="col-12">
                        <label class="blue-label">Payment Information</label>
                      </div>
                       <div class="col-lg-3 col-md-4 col-sm-12">

                                  <div class="pro-inner-row">
                                    <label class="m-0"><i class="fa fa-money" aria-hidden="true"></i> Amount Payable</label>
                                    <input type="text" value="{{isset($payments) ? '$'.$payments->amount : 0}}" spellcheck="true" disabled class="form-control"  placeholder="$40">
                                  </div>
                                </div>
                                <div class="col-lg-3 col-md-4 col-sm-12">
                                  <div class="pro-inner-row">
                                    <label class="m-0"><i class="fa fa-money" aria-hidden="true"></i> Status</label>
                                    <input type="text" value="{{(isset($payments) && $payments->status==1) ? 'Paid' : 'Un Paid' }}" spellcheck="true" disabled class="form-control"  placeholder="Paid">
                                  </div>
                                </div>
                                <div class="col-12 mt-2">
                                    <label class="blue-label">Session Information</label>
                                </div>
                                <div class="col-lg-3 col-md-4 col-sm-12">

                                  <div class="pro-inner-row">
                                    <label class="m-0"><i class="fa fa-money" aria-hidden="true"></i> Total Sessions</label>
                                    <input type="text" value="{{count($sessions)}}" spellcheck="true" disabled class="form-control"  placeholder="40">
                                  </div>
                                </div>
                                <div class="col-lg-3 col-md-4 col-sm-12">
                                  <div class="pro-inner-row">
                                    <label class="m-0"><i class="fa fa-money" aria-hidden="true"></i> Live Chats</label>
                                    <input type="text" value="{{isset($payments) ? $payments->live_chat : 0}}" spellcheck="true" disabled class="form-control"  placeholder="10">
                                  </div>
                                </div>
                                <div class="col-lg-3 col-md-4 col-sm-12">

                                  <div class="pro-inner-row">
                                    <label class="m-0"><i class="fa fa-money" aria-hidden="true"></i> Voice Calls</label>
                                    <input type="text" value="{{isset($payments) ? $payments->voice_call : 0}}" spellcheck="true" disabled class="form-control"  placeholder="10">
                                  </div>
                                </div>
                                <div class="col-lg-3 col-md-4 col-sm-12">
                                  <div class="pro-inner-row">
                                    <label class="m-0"><i class="fa fa-money" aria-hidden="true"></i> Video Calls</label>
                                    <input type="text" value="{{isset($payments) ? $payments->video_call : 0}}" spellcheck="true" disabled class="form-control"  placeholder="10">
                                  </div>
                                </div>
                                </div>
                                <div class="row mt-3">
                                    
                                    <div class="col-lg-6 col-md-4 col-sm-12">
                                    </div>
                                    <div class="col-lg-6 col-md-8 col-sm-12">
                                    <div class="row u-c-p subs-log-filter">
                                        <div class="col-lg-12 form-group d-flex align-items-center">
                                        <label class="w-100" for="">To Date:</label>
                                        <input  type="text" id="select-date" name="from" readonly class="form-control">
                                        </div>
                                        <div class="col-lg-12 form-group d-flex align-items-center">
                                        <label class="w-100" for="">From Date:</label>
                                        <input  type="text" id="select-date2" name="to" readonly class="form-control">
                                        </div>
                                        <div class="add-user date-input d-flex align-items-center form-group">
                                            <button type="submit" class="btn-primary" style="padding: 10px">Search</button>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="maain-tabble mt-3">
                                    <table class="table table-striped table-bordered zero-configuration">
                                    <thead>
                                        <tr>
                                        <th>S.no</th>
                                        <th>User Name</th>
                                        <th>Session Type</th>
                                        <th>Session Conducted On</th>
                                        <th>Total Session Fee</th>
                                        <th>My Earning</th>
                                        <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($sessions as $key=>$row)
                                        <tr>
                                        <td>{{++$key}}</td>
                                        <td>
                                            <span class="avatar avatar-online">
                                                <img class="tab-img" src="{{ isset($row->user->image) ? asset($row->user->image) : ''  }}" alt="" onerror="this.style.display='none'">
                                            </span>
                                            {{ isset($row->user) ? ucfirst($row->user->full_name) : ''}}
                                        </td>
                                        <td>{{($row->session_type == 1) ? 'Live Chat' : ($row->session_type == 2 ? "Voice Call" : "Video Call")}}</td>
                                        <td>{{$row->requested_date}}</td>
                                        <td>${{$row->amount}}</td>
                                        <td>${{ ($row->session_type == 1) ? config('app.live_chat') : ($row->session_type== 2) ? config('app.voice_call') : config('app.video_call') }}</td>
                                        <td>Completed</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    </table>
                                </div>
                                </div>
                            </div>
                            <!--main card end-->
                            </div>
                        </div>
                        </div>
                    </form>
                    </section>
                </div>
            </div>
            </div>




@endsection



@section('js')
<script>
    $('#select-date').datepicker({
      uiLibrary: 'bootstrap4',
      format: 'mm/dd/yyyy'
    });
    $('#select-date2').datepicker({
                uiLibrary: 'bootstrap4',
                format: 'mm/dd/yyyy'
              });
  </script>

@endsection
