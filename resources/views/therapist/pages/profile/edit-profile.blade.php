@extends('layouts.therapist')
@section('title',' Edit Profile')
@section('body-class',"vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar")
@section('body-col',"2-columns")
@section('css')
    <link rel='stylesheet' href='https://foliotek.github.io/Croppie/croppie.css'>

@endsection
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="">
                <div class="content-body">
                    <section id="configuration" class="u-c-p u-profile a-edit">
                        <div class="row">
                            <div class="col-12">
                                <div class="card pro-main">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">
                                            <h1>Edit Profile</h1>
                                        </div>
                                    </div>
                                    @if ($errors->any())
                                        @foreach ($errors->all() as $error)
                                            <div class="alert alert-danger">
                                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    @endif
                                    <div class="row">
                                        <div class="col-12">
                                            <form action="{{ route('therapist.profile.update') }}" enctype="multipart/form-data"
                                                  method="post" id="epf" class="number-tab-steps wizard-circle"><!-- Step 1 -->
                                                @csrf
                                                <h6>Account</h6>
                                                <fieldset>
                                                    <div class="row">
                                                        <div class="col-lg-12 col-sm-12">
                                                            <div class="pro-inner-row">
                                                                <label><i class="fa fa-user-circle"></i> Full Name</label>
                                                                <input type="text" value="{{$user->full_name}}" name="full_name" required="required" spellcheck="true" class="form-control">
                                                            </div>
                                                        </div>
                                                        {{-- <div class="col-lg-6 col-sm-12">
                                                            <div class="pro-inner-row">
                                                                <label><i class="fa fa-user-circle"></i> Last Name</label>
                                                                <input type="text" value="{{$user->last_name}}"  name="last_name" required="required" spellcheck="true" class="form-control">
                                                            </div>
                                                        </div> --}}
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-12 col-sm-12">
                                                            <div class="pro-inner-row">
                                                                <label><i class="fa fa-envelope"></i>Email</label>
                                                                <input type="email" value="{{$user->email}}"  disabled name="email" required="required" spellcheck="true" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <!-- Step 2 -->
                                                <h6>Office Location</h6>
                                                <fieldset>
                                                    <div class="form-group mb-2 contact-repeater">
                                                        <div data-repeater-list="repeater-group">
                                                            @foreach($user->offices as $key => $office)
                                                            <h3>Office # {{++$key}}</h3>
                                                            <div class="mb-1" data-repeater-item>
                                                                <div class="row">
                                                                    <div class="col-lg-6 col-sm-12">
                                                                        <div class="pro-inner-row">
                                                                            <label><i class="fa fa-id-card" aria-hidden="true"></i> Name of
                                                                                Practice (optional)</label>
                                                                            <input type="text" value="{{$office->practice_name }}" name="practice_name" required="required" spellcheck="true"
                                                                                   class="form-control">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-6 col-sm-12">
                                                                        <div class="pro-inner-row">
                                                                            <label><i class="fa fa-globe" aria-hidden="true"></i> Practice
                                                                                Website</label>
                                                                            <input type="text" value="{{$office->practice_website }}" name="practice_website" required="required" spellcheck="true"
                                                                                   class="form-control">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-lg-6 col-sm-12">
                                                                        <div class="pro-inner-row">
                                                                            <label><i class="fa fa-phone"></i> Work Phone #</label>
                                                                            <input type="number" value="{{$office->phone }}" onkeydown="javascript: return event.keyCode == 69 ? false : true"   name="phone" required="required" spellcheck="true"
                                                                                   class="form-control">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-6 col-sm-12">
                                                                        <div class="pro-inner-row">
                                                                            <label><i class="fa fa-map-marker" aria-hidden="true"></i> Address
                                                                                (line 1)</label>
                                                                            <input type="text"  value="{{$office->address }}"  name="address" required="required" spellcheck="true"
                                                                                   class="form-control">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-lg-6 col-sm-12">
                                                                        <div class="pro-inner-row">
                                                                            <label><i class="fa fa-map-marker" aria-hidden="true"></i> Address
                                                                                (line 2)</label>
                                                                            <input type="text"  value="{{$office->address_2 }}"  name="address_2" required="required" spellcheck="true"
                                                                                   class="form-control">
                                                                        </div>
                                                                    </div>
                                                                    <!--pro inner row end-->
                                                                    <div class="col-lg-6 col-sm-12">
                                                                        <div class="pro-inner-row">
                                                                            <label><i class="fa fa-map" aria-hidden="true"></i> State of
                                                                                Practice</label>
                                                                            <select id="state" class="selectSearch form-control" name="state">
                                                                                @foreach($states as $row)
                                                                                <option value="{{$row->id}}" {{ ($row->id == $office->state) ? 'selected' : '' }}>{{$row->name}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                            {{-- <input type="text" value="{{$office->states->name }}"  name="state" required="required" spellcheck="true"
                                                                                   class="form-control"> --}}
                                                                        </div>
                                                                    </div>
                                                                   
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-lg-6 col-sm-12">
                                                                        <div class="pro-inner-row">
                                                                            <label><i class="fa fa-building" aria-hidden="true"></i>
                                                                                City</label>
                                                                            {{-- <input type="text"  value="{{ $office->cities->city }}" name="city" required="required" spellcheck="true"
                                                                                   class="form-control"> --}}
                                                                             <select id="city" class="selectSearch form-control" name="city">
                                                                             <option value="{{$office->city}}">{{ $office->cities->city ?? '' }}</option>
                                                                             </select>
                                                                        </div>
                                                                    </div>
                                                                    <!--pro inner row end-->
                                                                    <div class="col-lg-6 col-sm-12">
                                                                        <div class="pro-inner-row">
                                                                            <label><i class="fa fa-map-marker"></i> Zip Code</label>
                                                                            <input type="text" value="{{$office->zipcode }}"  name="zipcode" required="required" spellcheck="true"
                                                                                   class="form-control">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-lg-6 col-sm-12">
                                                                        <div class="pro-inner-row">
                                                                            <label><i class="fa fa-globe"></i> Country</label>
                                                                            <input type="text" value="{{$office->country }}"  name="country" required="required" spellcheck="true"
                                                                                   class="form-control">
                                                                        </div>
                                                                    </div>
                                                                    
                                                                    <div class="col-lg-6 col-sm-12">
                                                                        <button type="button" data-repeater-delete class="btn btn-default btn-margin">
                                                                            <i class="fa fa-trash-o" aria-hidden="true"></i> Remove
                                                                        </button>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            @endforeach
                                                        </div>
                                                        <button type="button" data-repeater-create class="btn btn-primary btn-margin">
                                                            <i class="fa fa-plus-circle" aria-hidden="true"></i> Add New Office
                                                        </button>
                                                    </div>
                                                </fieldset>
                                                <!-- Step 3 -->
                                                <h6>Public Profile</h6>
                                                <fieldset>
                                                    <div class="row">
                                                        <div class="col-lg-6 col-sm-12">
                                                            <div class="pro-inner-row">
                                                                <label><i class="fa fa-user-circle" aria-hidden="true"></i> Title
                                                                    (optional)</label>
                                                                <input type="text" value="{{$user->therapist_title}}" name="therapist_title" required="required" spellcheck="true" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-sm-12">
                                                            <div class="pro-inner-row">
                                                                <label><i class="fa fa-user-md" aria-hidden="true"></i> Therapist Type</label>
                                                                <input type="text"  value="{{$user->therapist_title}}" name="therapist_type" required="required" spellcheck="true" class="form-control">
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-6 col-sm-12">
                                                            <div class="pro-inner-row">
                                                                <label><i class="fa fa-id-card" aria-hidden="true"></i> Type of License</label>
                                                                <input type="text" value="{{$user->license_type}}"  name="license_type" required="required" spellcheck="true" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-sm-12">
                                                            <div class="pro-inner-row">
                                                                <label><i class="fa fa-id-card" aria-hidden="true"></i> Years In
                                                                    Practice</label>
                                                                <input type="text"  value="{{$user->years_in_practice}}" name="years_in_practice" required="required" spellcheck="true" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-6 col-sm-12">
                                                            <div class="pro-inner-row">
                                                                <label><i class="fa fa-id-card" aria-hidden="true"></i> License In</label>
                                                                <input type="text"  value="{{$user->license_in}}" name="license_in" required="required" spellcheck="true" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-sm-12">
                                                            <div class="pro-inner-row">
                                                                <label><i class="fa fa-university" aria-hidden="true"></i> Undergraduate
                                                                    Institution Name</label>
                                                                <input type="text" value="{{$user->undergraduate_institute}}"  name="undergraduate_institute" required="required" spellcheck="true" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-6 col-sm-12">
                                                            <div class="pro-inner-row">
                                                                <label><i class="fa fa-university" aria-hidden="true"></i> Postgraduate
                                                                    Institiution Name</label>
                                                                <input type="text"  value="{{$user->postgraduate_institute}}" name="postgraduate_institute" required="required" spellcheck="true" class="form-control">
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-6 col-sm-12">
                                                            <label class="blue-label">Skills & Expertise</label>
                                                            <select id="specialities" data-type="specialities" name="specialities_id[]"  class="select-modal select2 form-control" multiple="multiple">
                                                                @foreach($speciality as $spec)
                                                                    <option
                                                                        {{  (in_array($spec->id, explode(',', $user->specialities_id))) ? "selected" : "" }}
                                                                        value="{{$spec->id}}">{{ ucfirst($spec->name)}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="col-lg-6 col-sm-12">
                                                            <label class="blue-label">Treatment orientation</label>
                                                            <select id="treatment"  data-type="treatment"  name="treatments_id[]" class="select-modal select2 form-control" multiple="multiple">
                                                                @foreach($treatment as $treat)
                                                                    <option
                                                                        {{  (in_array($treat->id, explode(',', $user->treatments_id))) ? "selected" : "" }}
                                                                        value="{{$treat->id}}">{{ ucfirst($treat->name)}}</option>

                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="col-lg-6 col-sm-12">
                                                            <label class="blue-label">Modality</label>
                                                            <select id="modality" data-type="modality"  name="modalities_id[]" class="select-modal select2 form-control" multiple="multiple">
                                                                @foreach($modality as $modal)
                                                                    <option
                                                                        {{  (in_array($modal->id, explode(',', $user->modalities_id))) ? "selected" : "" }}
                                                                        value="{{$modal->id}}">{{ ucfirst($modal->name)}}</option>

                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="col-lg-6 col-sm-12">
                                                            <label class="blue-label">Focus for clients age group</label>
                                                            <select id="agegroup" data-type="agegroup"  name="age_groups_id[]" class="select-modal select2 form-control" multiple="multiple">
                                                                @foreach($agegroup as $age)
                                                                    <option
                                                                        {{  (in_array($age->id, explode(',', $user->age_groups_id))) ? "selected" : "" }}
                                                                        value="{{$age->id}}">{{ ucfirst($age->name)}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="col-lg-6 col-sm-12">
                                                            <label class="blue-label">Focus for clients ethnicity</label>
                                                            <select id="ethnicity" data-type="ethnicity"  name="ethnicities_id[]" class="select-modal select2 form-control"  multiple="multiple">
                                                                @foreach($ethnicity as $eth)
                                                                    <option
                                                                        {{  (in_array($eth->id, explode(',', $user->ethnicities_id))) ? "selected" : "" }}
                                                                        value="{{$eth->id}}">{{ ucfirst($eth->name)}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="col-lg-6 col-sm-12">
                                                            <label class="blue-label">Focus for clients spoken language</label>
                                                            <select id="language" data-type="language"  name="languages_id[]" class="select-modal select2 form-control" multiple="multiple">
                                                                @foreach($language as $lang)
                                                                    <option
                                                                        {{  (in_array($lang->id, explode(',', $user->languages_id))) ? "selected" : "" }}
                                                                        value="{{$lang->id}}">{{ ucfirst($lang->name)}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                </fieldset>
                                                <!-- Step 4 -->
                                                <h6>Personal Details</h6>
                                                <fieldset>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="pro-inner-row">
                                                                <label><i class="fa fa-file-text" aria-hidden="true"></i> Personal
                                                                    Statement</label>
                                                                <textarea name="statement" class="form-control">{{ $user->statement }}</textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-12">
                                                            <div class="pro-img-main">
                                                                <img src="{{ $user->image ? $user->image : asset('/images/student-pro-girl.png') }}" id="user_image" alt="">
                                                                <button type="button" name="file" class="change-cover" onclick="document.getElementById('upload').click()"><i class="fa fa-camera"></i></button>
                                                                <input type="file"  accept="image/*" onchange="readURL(this, 'user_image')" id="upload" name="user_image">
                                                                <input type="hidden" id="personal_img_base64" name="image">
                                                            </div>
                                                        </div>
                                                        <div class="col-12">
                                                            <div class="pro-inner-row">
                                                                <label><i class="fa fa-clock-o" aria-hidden="true"></i> Your Availability Timing
                                                                    for Your Clients</label>
                                                                <textarea name="time_availability" class="form-control">{{$user->time_availability}}</textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <h6>Professional Reference</h6>
                                                <fieldset>
                                                    <div class="row mt-3 mb-5">
                                                        <div class="col-lg-6 col-sm-12">
                                                            <div class="pro-inner-row">
                                                                <label><i class="fa fa-user-circle"></i> Full Name of Reference</label>
                                                                <input type="text" value="{{$user->reference_name}}" name="reference_name" required="required" spellcheck="true" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-sm-12">
                                                            <div class="pro-inner-row">
                                                                <label><i class="fa fa-user-circle"></i> Professional Title of Reference</label>
                                                                <input type="text" value="{{$user->reference_title}}" name="reference_title" required="required" spellcheck="true" class="form-control">
                                                            </div>
                                                        </div>
                                                        <!--pro inner row end-->
                                                        <!--pro inner row end-->
                                                        <div class="col-lg-12 col-sm-12">
                                                            <div class="pro-inner-row">
                                                                <label><i class="fa fa-envelope"></i> Email of Reference</label>
                                                                <input type="email" value="{{$user->reference_email}}" name="reference_email" required="required" spellcheck="true" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
    <div class="modal" id="cropImagePop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <!--<div class="modal-body" style="min-height:400px">-->
                <div class="modal-body" style="">
                    <div id="upload-demo" style="width:350px"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="cropImageBtn" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
    <div class="login-fail-main user change-profile t-registration cat-popup">
        <div class="featured inner">
            <div class="modal fade bd-example-modal-lg" id="dataModal" tabindex="-1" role="dialog"
                 aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <div id="dataModalRes"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="login-fail-main user change-pass change-profile">
        <div class="featured inner">
            <div class="modal fade bd-example-modal-lg" id="submitProfileModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                        <div class="payment-modal-main">
                            <div class="payment-modal-inner">
                                <h3>Users would not be able to book appointments with you until your details are verified. </h3>
                                <p>Are you sure you want to update your details?</p>
                                <div class="row">
                                    <div class="col-12 text-center">
                                        <button type="button" class="big-center-cta" data-dismiss="modal">Cancel</button>
                                        <button type="button" onclick="document.getElementById('epf').submit()" class="submit_form big-center-cta">Update</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection

@section('js')
    <script>
        var edit_profile = true;
    </script>
    <script src="https://foliotek.github.io/Croppie/croppie.js"></script>
    <script src="{{ asset('js/therapist-auth.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
    
        $('#state').on('change', function() {
           
                var stateID = $(this).val();
                var url = $('#url').val() + '/get-cities/';
                if(stateID) {
                    $.ajax({
                        url: url+stateID,
                        type: "GET",
                        dataType: "json",
                        success:function(data) {
                            $('#city').empty();
                            $.each(data, function(key, value) {
                                // console.log(key);
                                // console.log(value); return;
                                $('#city').append('<option value="'+ value.id +'">'+ value.city +'</option>');
                            });
    
    
                        }
                    });
                }else{
                    $('#city]').empty();
                }
        });
    });
        </script>
    <script>
    (function($) {
        $.fn.inputFilter = function(inputFilter) {
            return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
            if (inputFilter(this.value)) {
                this.oldValue = this.value;
                this.oldSelectionStart = this.selectionStart;
                this.oldSelectionEnd = this.selectionEnd;
            } else if (this.hasOwnProperty("oldValue")) {
                this.value = this.oldValue;
                this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
            } else {
                this.value = "";
            }
            });
        };
        }(jQuery));

        $("input[type=number]").inputFilter(function(value) {
        return /^-?\d*$/.test(value); });
    </script>

@endsection
