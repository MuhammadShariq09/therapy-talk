@extends('layouts.therapist')
@section('title',' Dashboard')
@section('body-class',"vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar")
@section('body-col',"2-columns")

@section('content')

    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-body">
                <!-- Basic form layout section start -->
                <section id="combination-charts">
                    <div class="row">
                        <div class="col-12">
                            <div class="admin-overview-main">
                                <div class="row">
                                    <div class="col-12">
                                        <h1>dashboard</h1>
                                    </div>

                                    <div class="col-xl-4 col-lg-6 col-md-12 col-sm-12 d-flex">
                                        <div class="card rounded w-100 justify-content-center">
                                            <div class="card-content">
                                                <div class="card-body top-card cleartfix">
                                                    <div class="media align-items-stretch">
                                                        <div class="align-self-center">
                                                            <i class="mr-2"><img src="{{ asset('images/notes-icon.png') }}"></i>
                                                        </div>
                                                        <div class="media-body align-self-center">
                                                            <span>Total Completed<br>Appointments</span>
                                                        </div>
                                                        <div class="align-self-center">
                                                            <h4>{{ $completed }}</h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-4 col-lg-6 col-md-12 col-sm-12 d-flex">
                                        <div class="card rounded w-100 justify-content-center">
                                            <div class="card-content">
                                                <div class="card-body top-card cleartfix">
                                                    <div class="media align-items-stretch">
                                                        <div class="align-self-center">
                                                            <i class="mr-2"><img src="{{ asset('images/doctor-icon.png') }}"></i>
                                                        </div>
                                                        <div class="media-body align-self-center">
                                                            <span>New Appointment<br> Requests</span>
                                                        </div>
                                                        <div class="align-self-center">
                                                            <h4>{{ $new }}</h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-4 col-lg-6 col-md-12 col-sm-12 d-flex">
                                        <div class="card rounded w-100 justify-content-center">
                                            <div class="card-content">
                                                <div class="card-body top-card cleartfix">
                                                    <div class="media align-items-stretch">
                                                        <div class="align-self-center">
                                                            <i class="mr-2"><img src="{{ asset('images/dollar-icon.png') }}"></i>
                                                        </div>
                                                        <div class="media-body align-self-center">
                                                            <span>Total<br>Earning</span>
                                                        </div>
                                                        <div class="align-self-center">
                                                            <h4>${{ $earning  }}</h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-12">
                                        <div class="card rounded">
                                            <div class="card-content">
                                                <div class="card-body db-pad">
                                                    <div class="media home-chart align-items-center">
                                                        <h5 class="text-center">Number of Appointments</h5>
                                                        <div class="media-body">
                                                            <h5>Total appointments per month</h5>
                                                            <canvas id="canvas" height="280" width="600"></canvas>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

@endsection('content')

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.bundle.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.js"></script>
<script>
    const d = @json($totalAppointments);
  
    window.onload = function() {
        var ctx = document.getElementById("canvas").getContext("2d");
        window.myBar = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: d.map( m => m.key ),
                datasets: [{
                    label: '',
                    data: d.map( m => m.value ),
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                elements: {
                    rectangle: {
                        borderWidth: 2,
                        borderColor: 'rgb(0, 255, 0)',
                        borderSkipped: 'bottom'
                    }
                },
                responsive: true,
                title: {
                    display: true,
                    text: 'TOTAL APPOINTMENTS PER MONTH'
                }
            }
        });
        
        
       

    };
</script>

@endsection('js')
