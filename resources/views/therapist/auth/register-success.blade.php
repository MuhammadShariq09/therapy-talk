@extends('layouts.therapist')
@section('title', ' Registration Successful')
@section('body-class',"vertical-layout vertical-menu 2-columns menu-expanded")
@section('body-col',"2-column")
@section('content')
@section('css')

@endsection
    <section class="app-content u-c-p  t-registration-thankyou">
        <div class="container">
            <div class="card pro-main">
                <div class="row">
                    <div class="col-md-12 col-sm-12 text-center">
                        <h1>Therapist Registration</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <h3>Thank you for applying</h3>
                        <p>Your profile is queued for review.</p>
                        <p>We will get back to you on your email once we have reviewed your details.</p>
                        <p>We look forward to working with you.</p>
                    </div>
                    <div class="col-12 text-center">
                        <a href="{{ route('therapist.profile') }}" class="big-center-cta" data-dismiss="modal">Profile</a>
                        <a onclick="event.preventDefault();document.getElementById('logout-form').submit();" href="{{ route('logout') }}" class="big-center-cta" data-dismiss="modal">Logout</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection
@section('js')

@endsection
