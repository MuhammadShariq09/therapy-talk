@extends('layouts.master')
@section('title',  ' - Forgot Password')
@section('body-class',"vertical-layout vertical-menu 2-columns menu-expanded")
@section('body-col',"2-column")
@section('content')

@section('content')
<section class="register forget-pass">
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-xl-3"></div>
            <div class="col-md-8 col-xl-6 col-12">
                <div class="register-main">
                        <a href="index.html"><img src="{{ asset('assets/admin/images/login-logo.png') }}" class="img-full" alt="logo"></a>
                        <div class="form-main">
                            <div class="row">
                                <div class="col-md-12">
                                    <h2>Forgot Password</h2>
                                </div>
                                @if (session('status'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session('status') }}
                                    </div>
                                @endif
                            </div>

                            <!--fields start here-->
                            <form method="POST" action="{{ route('password.email') }}">
                            @csrf
                                <div class="fields">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <i class="fa fa-envelope" aria-hidden="true"></i>
                                            <input id="email" type="email" placeholder="Email Address" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                    </div>
                                    <button type="submit" class="form-control">{{ __('Continue') }}</button>
                                    <div class="new-user">
                                        <p><a href="{{ route('login')}}"><i class="fa fa-arrow-left" aria-hidden="true"></i>  Back To Login</a></p>
                                    </div>
                                </div>
                            </form>    
                            <!--fields end here-->
                        </div>
                </div>

            </div>
            <div class="col-md-2 col-xl-3"></div>
        </div>
    </div>
</section>
@endsection
