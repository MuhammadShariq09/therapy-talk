<div class="payment-modal-main">
    <div class="payment-modal-inner"> 
        @if($type=='agegroup')
        <h2>AGE GROUP</h2>
        @else 
        <h2>{{ strtoupper($type) }}</h2>
        @endif
        <div class="row">
            <div class="col-12">
                <div class="search-area">
                    <input type="search" id="search" name="search" placeholder="Search {{ $type }}">
                    <button type="submit">Search</button>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <ul class="cat-sorting">
                    <li class="active"><a href="#">All</a></li>
                    <li><a href="javascript:;">(0-9)</a></li>
                    <li><a href="javascript:;">A</a></li>
                    <li><a href="javascript:;">B</a></li>
                    <li><a href="javascript:;">C</a></li>
                    <li><a href="javascript:;">D</a></li>
                    <li><a href="javascript:;">E</a></li>
                    <li><a href="javascript:;">F</a></li>
                    <li><a href="javascript:;">G</a></li>
                    <li><a href="javascript:;">H</a></li>
                    <li><a href="javascript:;">I</a></li>
                    <li><a href="javascript:;">J</a></li>
                    <li><a href="javascript:;">K</a></li>
                    <li><a href="javascript:;">L</a></li>
                    <li><a href="javascript:;">M</a></li>
                    <li><a href="javascript:;">N</a></li>
                    <li><a href="javascript:;">O</a></li>
                    <li><a href="javascript:;">P</a></li>
                    <li><a href="javascript:;">Q</a></li>
                    <li><a href="javascript:;">R</a></li>
                    <li><a href="javascript:;">S</a></li>
                    <li><a href="javascript:;">T</a></li>
                    <li><a href="javascript:;">U</a></li>
                    <li><a href="javascript:;">V</a></li>
                    <li><a href="javascript:;">W</a></li>
                    <li><a href="javascript:;">X</a></li>
                    <li><a href="javascript:;">Y</a></li>
                    <li><a href="javascript:;">Z</a></li>
                </ul>
            </div>
        </div>
        <div class="row" id="filter">
            @foreach($data as $d)
                <div class="col-sm-4">
                    <ul class="cat-list">
                    <li>
                        <label class="check">{{ $d->name }}
                            <input value="{{$d->id}}" {{ (in_array($d->id, $selected)) ? "checked" : "" }} spellcheck="true" name="datamodal[]" type="checkbox">
                            <span class="checkmark"></span>
                        </label>
                    </li>
                    </ul>
                </div>
            @endforeach
        </div>
        <div class="row">
            <div class="col-12 text-center">
                <button type="submit" data-type="{{$type}}" class="submitDataModal big-center-cta" data-dismiss="modal">Submit</button>
            </div>
        </div>
    </div>
</div>
