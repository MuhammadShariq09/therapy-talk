@extends('layouts.therapist')
@section('title', ' Register')
@section('body-class',"vertical-layout vertical-menu 2-columns menu-expanded")
@section('body-col',"2-column")
@section('content')
@section('css')
    <link rel='stylesheet' href='https://foliotek.github.io/Croppie/croppie.css'>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
<style>
.back {
    display: table;
    margin: 0 auto;
    color: #333;
    background: transparent !important;
    border: 0px;
    text-transform: capitalize;
    min-width: 130px;
    height: auto;
    border-bottom: 1px solid #333;
    font-size: 16px;
    border-radius: 0;
    padding: 0;
    float: none;
    transition: .3s;
    font-family: 'Conv_Harabara';
    letter-spacing: 1px;
}
</style>
@endsection
<section class="app-content u-c-p edit-profile t-registration">
    <div class="container">
        <div class="card pro-main">
            <div class="row">
                <div class="col-md-12 col-sm-12 text-center">
                    <h1>Therapist Registration</h1><br/>
                </div>
            </div>
            @if ($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {{$error}}
                    </div>
                @endforeach
            @endif
            <div class="row">
                <div class="col-12">
                    <form action="{{ route('therapist.register') }}" enctype="multipart/form-data" method="post" class="number-tab-steps  wizard-circle"><!-- Step 1 -->
                        @csrf
                        <h6>Account</h6>
                        <fieldset>
                            <div class="row">
                                <div class="col-lg-12 col-sm-12">
                                    <div class="pro-inner-row">
                                        <label><i class="fa fa-user-circle"></i> Full Name</label>
                                        <input type="text" name="full_name" required="required" spellcheck="true" class="form-control required">
                                        @if ($errors->has('name'))
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('full_name') }}</strong>
                                                    </span>
                                            @endif
                                    </div>
                                </div>
                                {{-- <div class="col-lg-6 col-sm-12">
                                    <div class="pro-inner-row">
                                        <label><i class="fa fa-user-circle"></i> Last Name</label>
                                        <input type="text" name="last_name" required="required" spellcheck="true" class="form-control required">
                                    </div>
                                </div> --}}
                            </div>
                            <div class="row">
                                <div class="col-lg-12 col-sm-12">
                                    <div class="pro-inner-row">
                                        <label><i class="fa fa-envelope"></i>Email</label>
                                        <input type="email" name="email" required="required" spellcheck="true" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-sm-12">
                                    <div class="pro-inner-row">
                                        <label><i class="fa fa-unlock-alt" aria-hidden="true"></i> Password</label>
                                        <input type="password" name="password" required="required" spellcheck="true" class="form-control">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <div class="pro-inner-row">
                                        <label><i class="fa fa-unlock-alt" aria-hidden="true"></i> Confirm
                                            Password</label>
                                        <input type="password" name="password_confirmation" required="required" spellcheck="true" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <!-- Step 2 -->
                        <h6>Office Location</h6>
                        <fieldset>
                            <div class="form-group mb-2 contact-repeater">
                                <div data-repeater-list="repeater-group">
                                    <div class="mb-1" data-repeater-item>
                                        <div class="row">
                                            <div class="col-lg-6 col-sm-12">
                                                <div class="pro-inner-row">
                                                    <label><i class="fa fa-id-card" aria-hidden="true"></i> Name of
                                                        Practice (optional)</label>
                                                    <input type="text" name="practice_name" required="required" spellcheck="true"
                                                           class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-sm-12">
                                                <div class="pro-inner-row">
                                                    <label><i class="fa fa-globe" aria-hidden="true"></i> Practice
                                                        Website</label>
                                                    <input type="text" name="practice_website" required="required" spellcheck="true"
                                                           class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6 col-sm-12">
                                                <div class="pro-inner-row">
                                                    <label><i class="fa fa-phone"></i> Work Phone #</label>
                                                    <input type="number" name="phone" required="required" spellcheck="true"
                                                           class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-sm-12">
                                                <div class="pro-inner-row">
                                                    <label><i class="fa fa-map-marker" aria-hidden="true"></i> Address
                                                        (line 1)</label>
                                                    <input type="text" name="address" required="required" spellcheck="true"
                                                           class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6 col-sm-12">
                                                <div class="pro-inner-row">
                                                    <label><i class="fa fa-map-marker" aria-hidden="true"></i> Address
                                                        (line 2)</label>
                                                    <input type="text" name="address_2" required="required" spellcheck="true"
                                                           class="form-control">
                                                </div>
                                            </div>
                                            <!--pro inner row end-->
                                            <div class="col-lg-6 col-sm-12">
                                                <div class="pro-inner-row">
                                                    <label><i class="fa fa-map" aria-hidden="true"></i> State of
                                                        Practice</label>
                                                        <select id="state" class="selectSearch form-control" name="state">
                                                            @foreach($states as $row)
                                                            <option value="{{$row->id}}">{{$row->name}}</option>
                                                            @endforeach
                                                          </select>
                                                    {{-- <input type="text" name="state" required="required" spellcheck="true"
                                                           class="form-control"> --}}
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6 col-sm-12">
                                                <div class="pro-inner-row">
                                                    <label><i class="fa fa-building" aria-hidden="true"></i>
                                                        City</label>
                                                    <select id="city" class="selectSearch form-control" name="city"></select>
                                                </div>
                                            </div>
                                            <!--pro inner row end-->
                                            <div class="col-lg-6 col-sm-12">
                                                <div class="pro-inner-row">
                                                    <label><i class="fa fa-map-marker"></i> Zip Code</label>
                                                    <input type="text" name="zipcode" required="required" spellcheck="true"
                                                           class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6 col-sm-12">
                                                <div class="pro-inner-row">
                                                    <label><i class="fa fa-globe"></i> Country</label>
                                                    <input type="text" name="country" required="required" spellcheck="true"
                                                           class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-sm-12">
                                                <div class="pro-inner-row">
                                                    <label>&nbsp;</label>
                                                    <a href="javascript:;" data-repeater-delete href="#">Remove Office</a>
                                                </div>
                                                
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <button type="button" data-repeater-create class="add-new-btn">
                                    <i class="fa fa-plus-circle" aria-hidden="true"></i> Add New Office
                                </button>
                            </div>
                        </fieldset>
                        <!-- Step 3 -->
                        <h6>Public Profile</h6>
                        <fieldset>
                            <div class="row">
                                <div class="col-lg-6 col-sm-12">
                                    <div class="pro-inner-row">
                                        <label><i class="fa fa-user-circle" aria-hidden="true"></i> Title
                                            (optional)</label>
                                        <input type="text" name="therapist_title" required="required" spellcheck="true" class="form-control">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <div class="pro-inner-row">
                                        <label><i class="fa fa-user-md" aria-hidden="true"></i> Therapist Type</label>
                                        <input type="text" name="therapist_type" required="required" spellcheck="true" class="form-control">
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-sm-12">
                                    <div class="pro-inner-row">
                                        <label><i class="fa fa-id-card" aria-hidden="true"></i> Type of License</label>
                                        <input type="text" name="license_type" required="required" spellcheck="true" class="form-control">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <div class="pro-inner-row">
                                        <label><i class="fa fa-id-card" aria-hidden="true"></i> Years In
                                            Practice</label>
                                        <input type="text" name="years_in_practice" required="required" spellcheck="true" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-sm-12">
                                    <div class="pro-inner-row">
                                        <label><i class="fa fa-id-card" aria-hidden="true"></i> Licensed Number</label>
                                        <input type="text" name="license_in" required="required" spellcheck="true" class="form-control">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <div class="pro-inner-row">
                                        <label><i class="fa fa-university" aria-hidden="true"></i> Undergraduate
                                            Institution Name</label>
                                        <input type="text" name="undergraduate_institute" required="required" spellcheck="true" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-sm-12">
                                    <div class="pro-inner-row">
                                        <label><i class="fa fa-university" aria-hidden="true"></i> Postgraduate
                                            Institiution Name</label>
                                        <input type="text" name="postgraduate_institute" required="required" spellcheck="true" class="form-control">
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-sm-12">
                                    <label class="blue-label">Skills & Expertise</label>
                                    <select name="specialities_id[]" id="specialities" data-type="specialities" class="select-modal select2 form-control" multiple="multiple">
                                        @foreach($speciality as $spec)
                                            <option value="{{$spec->id}}">{{ $spec->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <label class="blue-label">Treatment orientation</label>
                                    <select id="treatment" name="treatments_id[]"  data-type="treatment" class="select-modal select2 form-control" multiple="multiple">
                                        @foreach($treatment as $treat)
                                            <option value="{{$treat->id}}">{{ $treat->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <label class="blue-label">Modality</label>
                                    <select id="modality" name="modalities_id[]" data-type="modality" class="select-modal select2 form-control" multiple="multiple">
                                        @foreach($modality as $modal)
                                            <option value="{{$modal->id}}">{{ $modal->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <label class="blue-label">Focus for clients age group</label>
                                    <select id="agegroup" name="age_groups_id[]" data-type="agegroup" class="select-modal select2 form-control" multiple="multiple">
                                        @foreach($agegroup as $age)
                                            <option value="{{$age->id}}">{{ $age->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <label class="blue-label">Focus for clients ethnicity</label>
                                    <select name="ethnicities_id[]" id="ethnicity" data-type="ethnicity" class="select-modal select2 form-control"  multiple="multiple">
                                        @foreach($ethnicity as $eth)
                                            <option value="{{$eth->id}}">{{ $eth->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <label class="blue-label">Focus for clients spoken language</label>
                                    <select name="languages_id[]" id="language" data-type="language" class="select-modal select2 form-control" multiple="multiple">
                                        @foreach($language as $lang)
                                            <option value="{{$lang->id}}">{{ $lang->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </fieldset>
                        <!-- Step 4 -->
                        <h6>Personal Details</h6>
                        <fieldset>
                            <div class="row">
                                <div class="col-12">
                                    <div class="pro-inner-row">
                                        <label><i class="fa fa-file-text" aria-hidden="true"></i> Personal
                                            Statement</label>
                                        <textarea name="statement" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="pro-img-main">
                                        <img src="{{ asset('/images/student-pro-girl.png') }}" id="user_image" alt="">
                                        <button type="button" name="file" class="change-cover" onclick="document.getElementById('upload').click()"><i class="fa fa-camera"></i></button>
                                        <input type="file"  accept="image/*" onchange="readURL(this, 'user_image')" id="upload" name="user_image">
                                        <input type="hidden" id="personal_img_base64" name="image">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="pro-inner-row">
                                        <label><i class="fa fa-clock-o" aria-hidden="true"></i> Your Availability Timing
                                            for Your Clients</label>
                                        <textarea name="time_availability" class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <h6>Professional Reference</h6>
                        <fieldset>
                            <div class="row mt-3 mb-5">
                                <div class="col-lg-6 col-sm-12">
                                    <div class="pro-inner-row">
                                        <label><i class="fa fa-user-circle"></i> Full Name of Reference</label>
                                        <input type="text" name="reference_name" required="required" spellcheck="true" class="form-control">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <div class="pro-inner-row">
                                        <label><i class="fa fa-user-circle"></i> Professional Title of Reference</label>
                                        <input type="text" name="reference_title" required="required" spellcheck="true" class="form-control">
                                    </div>
                                </div>
                                <!--pro inner row end-->
                                <!--pro inner row end-->
                                <div class="col-lg-12 col-sm-12">
                                    <div class="pro-inner-row">
                                        <label><i class="fa fa-envelope"></i> Email of Reference</label>
                                        <input type="email" name="reference_email" required="required" spellcheck="true" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <h6>Terms & Agreement</h6>
                        <fieldset>
                            <div class="row">
                                <div class="col-12">
                                    <h5>Introduction</h5>
                                    <p>Welcome to My Therapy Talk. If you continue to use this website, you are agreeing to comply with and be bound by the following terms and conditions of use, which together with our privacy policy govern My Therapy Talk relationship with you in relation to this website. If you disagree with any part of these terms and conditions, please do not use our website.
                                        The term ‘My Therapy Talk’ or ‘us’ or ‘we’ refer to the owner of the website.<br> The term ‘you’ ‘customer’ or ‘user’ refers to the user of our website.</p>
                                    <h5>Third Party</h5>
                                    <p>The use of this website is subject to the following terms of use: <br>
                                        The content of the pages of this website is for your general information and use only. It is subject to change without notice.<br>
                                        Neither we nor any third parties provide any warranty or guarantee as to the accuracy, timeliness, performance, completeness or suitability of the information and materials found or offered on this website for any particular purpose. You acknowledge that such information and materials may contain inaccuracies or errors, and we expressly exclude liability for any such inaccuracies or errors to the fullest extent permitted by law.</p>
                                    <h5>Using My Therapy Talk</h5>
                                    <p>We are not responsible for any financial losses or any other damages caused whatsoever. When using My Therapy Talk you agree to and understand that you will not:</p>
                                    <ul>
                                        <li>Break or violate any laws</li>
                                        <li>Post anything that breaks our rules</li>
                                        <li>Infringe any third party right</li>
                                        <li>Distribute viruses or harm anything of our interest</li>
                                        <li>Respect all users who use the website</li>
                                    </ul>
                                    <p>By using our site you agree that you are above the age of 18 or have the legal consent to therapy or have the consent of a parent or guradian. <br>
                                        My Therapy Talk grant you limited access and use to the website and users must not download, modify or copy any part of our website including information and content. The user agrees not to collect or store any product information or resell, copy, distribute or sell any part of our website or software. <br>
                                        To use our site and services you are required to create an account with My Therapy Talk and provide certain personal information. You must only create one account and only create an account for yourself. If you are creating an account for someone else, you must obtain permission from them beforehand.<br>
                                        By creating an acoount with us you agree that you will provide accurate and complete information. If My Therapy Talk have reason to believe that your account information is incorrect, false inaccurate or incomplete we may transition you to other service providers.</p>
                                    <h5>Content</h5>
                                    <p>You are completely responsible for the material and content you submit to our website. We are not to be held liable for any content posted nor are we to be held responsible. <br>
                                        We may monitor and record traffic to our site and collect the following information including but not limited to the IP address of your computer and the website which referred you to our website. We do this, so we can make ongoing improvements on our site based on what our customers actions are.<br>
                                        All content including but not limited to pictures, videos and text are copyrighted by My Therapy Talk unless stated. The pictures on our website for visual representation only.<br>
                                        You may not copy, reproduce, distribute or store any material on our website unless given express written permission by us.<br>
                                        Do not use this service for medical or emergency needs. If you experience a medical emergency please get in touch with a professional or your local health services.<br>
                                        You should not constitute any of the advice as professional medical advice and should always consult a licensed professional.</p>
                                    <h5>Personal Information</h5>
                                    <p>We may collect and store your personal information for reasons such as improving our site and making the user experience better. We may use them for advertisement purposes. We will never sell, rent or give your personal data to any third party company without your consent.<br>
                                        If we do, we will only collect personal identification information from users only if they themselves submit such information to us. Users can refuse to supply such information but it may stop them from using certain sites or accessing specific parts of our website.<br>
                                        We are not to be held responsible for any breaches to our security caused by third party. Keeping your personal information is of the utmost importance to us however we are not to be held liable for any data breaches.<br>
                                        This website contains material which is owned by or licensed to us. This material includes, but is not limited to, the design, layout, look, appearance and graphics. Reproduction is prohibited other than in accordance with the copyright notice, which forms part of these terms and conditions.<br>
                                        All trademarks reproduced in this website, which are not the property of, or licensed to the operator, are acknowledged on the website.<br>
                                        If you opt to sign up to our newsletter we may use your email address to send you information about our products or services. You do have the option to opt out of our mailing list and you can ask for personal data to stop being recorded at any time.<br>
                                        We reserve the right to disclose user information and personal information to law enforcement or authorities should we feel the need to. <br>
                                        The user agrees to be bound by the My Therapy Talk privacy policy which are available here.<br>
                                        My Therapy Talk are not liable for any failure or delay in performance or its obligations which is due to the result of any act, event, terrorism act, acts of God, governmental regulations, war, riots, fire, flood or anything else which is beyond our control. <br>
                                        We may suspend your access to our service at any time for any or no reason at all. </p>
                                    <h5>General</h5>
                                    <p>Unauthorized use of this website may give rise to a claim for damages and/or be a criminal offense.<br>
                                        This website may also include links to other websites. These links are provided for your convenience to provide further information. They do not signify that we endorse the website(s). We have no responsibility for the content of the linked website(s).<br>
                                        We do not accept any liability or responsibility for any delays caused by third parties. Your use of this website and any dispute arising out of such use of the website is subject to the laws of your specific country.<br>
                                        This website is owned and operated by My Therapy Talk <br>
                                        PO Box 928876 <br>
                                        San Diego, CA 92192   </p>
                                </div>
                                <div class="col-12">
                                    <label class="check">I accept the terms stated above
                                        <input spellcheck="true" type="checkbox" name="accept" value="1" >
                                        <span class="checkmark"></span>
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('accept') }}</strong>
                                        </span>
                                    </label>
                                </div>

                            </div>
                        </fieldset>
                    </form>
                    <div class="new-user">
                        <a href="{{ url('/login/therapist') }}" class="back" ><i class="fa fa-long-arrow-left"></i> Back to Login</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal" id="cropImagePop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!--<div class="modal-body" style="min-height:400px">-->
            <div class="modal-body" style="">
                <div id="upload-demo" style="width:350px"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="cropImageBtn" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</div>
<div class="login-fail-main user change-profile t-registration cat-popup">
    <div class="featured inner">
        <div class="modal fade bd-example-modal-lg" id="dataModal" tabindex="-1" role="dialog"
             aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <div id="dataModalRes"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')

    <script src="https://foliotek.github.io/Croppie/croppie.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <script src="{{ asset('js/therapist-auth.js') }}"></script>

    <script>
    $(document).ready(function() {
    $('.selectSearch').select2();

    $('#state').on('change', function() {
       
            var stateID = $(this).val();
            var url = $('#url').val() + '/get-cities/';
            if(stateID) {
                $.ajax({
                    url: url+stateID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {
                        $('#city').empty();
                        $.each(data, function(key, value) {
                            // console.log(key);
                            // console.log(value); return;
                            $('#city').append('<option value="'+ value.id +'">'+ value.city +'</option>');
                        });


                    }
                });
            }else{
                $('#city]').empty();
            }
    });
});
    </script>
@endsection
