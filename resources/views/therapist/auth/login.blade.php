@extends('layouts.therapist')
@section('title', ' Login')
@section('body-class',"vertical-layout vertical-menu 2-columns menu-expanded")
@section('body-col',"2-column")
@section('content')
@section('css')
<style>
 .loginBtn {
        box-sizing: border-box;
        position: relative;
        /* width: 13em;  - apply for fixed size */
        margin: 0.2em;
        padding: 0 15px 0 46px;
        border: none;
        text-align: left;
        line-height: 34px;
        white-space: nowrap;
        border-radius: 0.2em;
        font-size: 16px;
        color: #FFF;
        }
        .loginBtn:before {
        content: "";
        box-sizing: border-box;
        position: absolute;
        top: 0;
        left: 0;
        width: 34px;
        height: 100%;
        }
        .loginBtn:focus {
        outline: none;
        }
        .loginBtn:active {
        box-shadow: inset 0 0 0 32px rgba(0,0,0,0.1);
        }


        /* Facebook */
        .loginBtn--facebook {
        background-color: #4C69BA;
        background-image: linear-gradient(#4C69BA, #3B55A0);
        /*font-family: "Helvetica neue", Helvetica Neue, Helvetica, Arial, sans-serif;*/
        text-shadow: 0 -1px 0 #354C8C;
        }
        .loginBtn--facebook:before {
        border-right: #364e92 1px solid;
        background: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/14082/icon_facebook.png') 6px 6px no-repeat;
        }
        .loginBtn--facebook:hover,
        .loginBtn--facebook:focus {
        background-color: #5B7BD5;
        background-image: linear-gradient(#5B7BD5, #4864B1);
        }


        /* Google */
        .loginBtn--google {
        /*font-family: "Roboto", Roboto, arial, sans-serif;*/
        background: #DD4B39;
        }
        .loginBtn--google:before {
        border-right: #BB3F30 1px solid;
        background: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/14082/icon_google.png') 6px 6px no-repeat;
        }
        .loginBtn--google:hover,
        .loginBtn--google:focus {
        background: #E74B37;
        }
</style>
@endsection
<section class="register loginn">
    <div class="container">
        <div class="login-main-inner">
            <div class="login-main">
                <div class="row m-0">
                    <div class="col-md-6 col-sm-12 p-0">
                        <div class="login-img">
                            <img src="{{ asset('images/login-left-img.png') }}">
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 p-0 login-right">
                        <div class="register-main">
                            <h1>Login</h1>
                            <h2>Login to your account</h2>
                            @if (session('error'))
                                    <div class="alert alert-danger">{{ session('error') }}</div>
                                @endif
                            <div class="form-main">
                                <!--fields start here-->
                                <form method="POST" action='{{ route("therapist.login") }}' aria-label="{{ __('Login') }}">
                                    @csrf
                                    @if ($errors->has('email'))
                                        <div class="alert alert-danger" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </div>
                                    @endif
                                    @if ($errors->has('password'))
                                        <div class="alert alert-danger" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </div>
                                    @endif
                                    <div class="fields">
                                        <div class="row ">
                                            <div class="col-md-12 form-group">
                                                <i class="fa fa-user-circle"></i>
                                                <input name="email" spellcheck="true" type="text" class="form-control" placeholder="Email">
                                            </div>
                                            <div class="col-md-12 form-group">
                                                <i class="fa fa-key"></i>
                                                <input name="password" spellcheck="true" type="password" class="form-control" placeholder="Password">
                                            </div>
                                        </div>
                                        <div class="row align-items-end">
                                            <div class="col-md-6 col-6">
                                                <label class="check">Remember me
                                                    <input spellcheck="true" type="checkbox">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="col-md-6 col-6"><a href="" data-toggle="modal" data-target="#resetPassword">Forgot Password?</a></div>
                                        </div>
                                        <button type="submit" class="form-control">Login</button>
                                        <div class="new-user">
                                            <p>Not a Member? Register Now</p>
                                            <a href="{{ route('therapist.register') }}" class="login form-control">Register</a>
                                            <a href="{{ url('/') }}" class="login back" ><i class="fa fa-long-arrow-left"></i>back to home</a><br/>
                                        </div>
                                        <!--fields end here-->
                                        
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <a href="{{ url('/therapist/login/facebook') }}" class="loginBtn loginBtn--facebook"> Facebook</a>
                                            
                                        </div>
                                        <div class="col-md-6">
                                        <a href="{{ url('/therapist/login/google') }}" class="loginBtn loginBtn--google"> Google</a>
                                   
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="login-fail-main user  profile-view ">
        <div class="featured inner ">
            <div class="modal fade" id="resetPassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="forget-pass">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                            <div class="modal-body">
                                <div class="payment-main-popup">
                                    <form action="{{ route('therapist.send-verification-code') }}" method="post" id="ursdvfcd">
                                        @csrf
                                        <div class="payment-modal-main">
                                            <div class="payment-modal-inner">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <h1>Password Recovery</h1>
                                                    </div>
                                                    <div class="col-12 form-group">
                                                        <i class="fa fa-envelope" aria-hidden="true"></i>
                                                        <input type="text" name="email" class="form-control" spellcheck="true" placeholder="Enter Email Address">
                                                    </div>
                                                    <div class="col-12 form-group text-center blockui">
                                                        <button type="submit" class="can">Continue</button>
                                                    </div>
                                                    <div class="col-12 form-group register-main">
                                                        <a href="javascript:;" data-dismiss="modal" class="login back"><i class="fa fa-long-arrow-left"></i>back to login</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br/>
                                   
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="verifyCode" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="forget-pass">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                                <div class="modal-body">
                                    <div class="payment-main-popup payment-view-popup">
                                        <div class="payment-modal-main">
                                            <div class="payment-modal-inner">
                                                <form action="{{ route('therapist.verify-verification-code') }}" method="post" id="urvrvfcd">
                                                    @csrf
                                                <div class="row">
                                                    <div class="col-12">
                                                        <h1>Password Recovery</h1>
                                                    </div>
                                                    <div class="col-12 form-group">
                                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                                        <input type="number" name="code" class="form-control" spellcheck="true" placeholder="Enter Verification Code">
                                                    </div>
                                                    <div class="col-12 form-group text-center">
                                                        <button type="submit" class="can">Continue</button>
                                                    </div>
                                                    <div class="col-12 form-group register-main">
                                                        <a href="javascript:;" data-dismiss="modal" class="login back"><i class="fa fa-long-arrow-left"></i>back to login</a>
                                                    </div>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="updatePassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="forget-pass">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                                <div class="modal-body">
                                    <div class="payment-main-popup payment-view-popup">
                                        <div class="payment-modal-main">
                                            <div class="payment-modal-inner">
                                                <form action="{{ route('therapist.update-password') }}" method="post" id="uruppw">
                                                    @csrf
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <h1>Password Recovery</h1>
                                                        </div>
                                                        <div class="col-12 form-group">
                                                            <i class="fa fa-unlock-alt" aria-hidden="true"></i>
                                                            <input type="password" name="password" class="form-control" spellcheck="true" placeholder="Enter Password">
                                                        </div>
                                                        <div class="col-12 form-group">
                                                            <i class="fa fa-unlock-alt" aria-hidden="true"></i>
                                                            <input type="password" name="confirm_password" class="form-control" spellcheck="true" placeholder="Confirm Password">
                                                        </div>
                                                        <div class="col-12 form-group text-center">
                                                            <button type="submit" class="can" >Submit</button>
                                                        </div>
                                                        <div class="col-12 form-group register-main">
                                                            <a href="javascript:;" class="login back"><i class="fa fa-long-arrow-left"></i>back to login</a>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</section>



@endsection
@section('js')
    <script src="{{ asset('assets/js/auth.js') }}"></script>
@endsection