<!-- - var menuBorder = true-->
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">

    <meta name="author" content="PIXINVENT">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{config('app.name')}} - @yield('title')</title>
    <link rel="shortcut icon" href="{{URL::asset('assets/admin/images/favicon.ico')}}">
    <link href="https://fonts.googleapis.com/css?family=Barlow:300,300i,400,400i,500,500i,600,600i,700&display=swap" rel="stylesheet">
    <!-- Header -->
    @include('includes.admin.header')
    @yield('css')
</head>
<body class="@yield('body-class')" data-open="click" data-menu="vertical-menu" data-col="@yield('body-col')">

	@guest
		@yield('content')
	@else
		@include('includes.admin.top-bar')
		@include('includes.admin.nav')
		@yield('content')
	@endguest

	<!-- Footer -->
    <script>
      var base_url = '{{url("/")}}';
    </script>
    @include('includes.admin.footer')


    @yield('js')

	</body>
</html>
