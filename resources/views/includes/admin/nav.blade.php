<div class="main-menu menu-fixed menu-light menu-accordion"  data-scroll-to-active="true">
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class="nav-item {{ (Request::segment(1) == 'admin' && Request::segment(2) == '') ? 'active' : '' }}">
            <a href="{{ url('/admin') }}"><i class="fa fa-window-restore" aria-hidden="true"></i>
                    <span class="menu-title" data-i18n="">Dashboard</span>
                </a>
            </li>
            <li class="nav-item {{ (Request::segment(2) == 'assesment-forms') ? 'active' : '' }}">
                <a href="{{ route('assesments') }}"><i class="fa fa-file-text" aria-hidden="true"></i>
                    <span class="menu-title" data-i18n="">Assessment Forms</span>
                </a>
            </li>
            <li class="nav-item {{ (Request::segment(2) == 'users') ? 'active' : '' }}">
                <a href="{{ route('active-users') }}"><i class="fa fa-user-circle-o" aria-hidden="true"></i>
                    <span class="menu-title" data-i18n="">Users</span>
                </a>
            </li>
            <li class="nav-item {{ (Request::segment(2) == 'active-therapists') ? 'active' : '' }}">
                <a href="{{ route('active-therapists') }}"><i class="fa fa-user-md" aria-hidden="true"></i>
                    <span class="menu-title" data-i18n="">Therapist</span>
                </a>
            </li>
            <li class="nav-item {{ (Request::segment(2) == 'session-logs') ? 'active' : '' }}">
                <a href="{{ route('session-logs')}}"><i class="fa fa-calendar" aria-hidden="true"></i>
                    <span class="menu-title" data-i18n="">Session Log </span>
                </a>
            </li>
            {{-- <li class="nav-item {{ (Request::segment(2) == 'payment-logs') ? 'active' : '' }}">
                <a href="{{ route('payment-logs')}}"><i class="fa  fa-money" aria-hidden="true"></i>
                    <span class="menu-title" data-i18n="">Payment Log</span>
                </a>
            </li> --}}
<<<<<<< HEAD
            <li class="nav-item {{ (Request::segment(2) == 'payment-logs') ? 'active' : '' }} has-sub"><a href="#"><i class="fa  fa-money" aria-hidden="true"></i><span class="menu-title" data-i18n="">Payment Log</span></a>
=======
            <li class="nav-item {{ ((Request::segment(2) =='payment-logs')||Request::segment(2) =='payables') ? 'active' : '' }} "><a href="#"><i class="fa  fa-money" aria-hidden="true"></i><span class="menu-title" data-i18n="">Payment Log</span></a>
>>>>>>> f61012370c99f428af18f72e4dea9cc413d0d724
                <ul class="menu-content" style="">
                    <li class="is-shown"><a class="menu-item" href="{{ route('payment-logs')}}">Amount Received</a></li>
                    <li class="is-shown"><a class="menu-item" href="{{route('payment-logs.payables')}}">Amount Payable</a></li>
                </ul>
            </li>
            
            <li class="nav-item {{ (Request::segment(2) == 'package-management') ? 'active' : '' }}">
                <a href="{{ route('package-management')}}"><i class="fa fa-outdent" aria-hidden="true"></i>
                    <span class="menu-title" data-i18n="">Package Management</span>
                </a>
            </li>
            <li class="nav-item {{ (Request::segment(2) == 'feedbacks') ? 'active' : '' }}">
                <a href="{{ route('feedbacks')}}"><i class="fa fa-comments" aria-hidden="true"></i>
                    <span class="menu-title" data-i18n="">Feedback</span>
                </a>
            </li>
            <li class="nav-item {{ (Request::segment(2) == 'reports') ? 'active' : '' }}">
                <a href="{{ route('reports')}}"><i class="fa fa-comments" aria-hidden="true"></i>
                    <span class="menu-title" data-i18n="">Reports</span>
                </a>
            </li>
        </ul>
    </div>
</div>
