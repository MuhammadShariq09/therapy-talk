<script src="{{ asset('/app-assets/vendors/js/vendors.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/app-assets/vendors/js/tables/datatable/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/app-assets/js/scripts/tables/datatables/datatable-basic.js') }}" type="text/javascript"></script>
<script src="{{ asset('/app-assets/vendors/js/forms/select/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/app-assets/vendors/js/extensions/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/app-assets/vendors/js/extensions/fullcalendar.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/app-assets/js/scripts/extensions/fullcalendar.js') }}" type="text/javascript"></script>
<script src="{{ asset('/app-assets/js/core/app-menu.js') }}" type="text/javascript"></script>
<script src="{{ asset('/app-assets/js/core/app.js') }}" type="text/javascript"></script>
<script src="{{ asset('/app-assets/js/scripts/customizer.js') }}" type="text/javascript"></script>
<script src="{{ asset('/app-assets/js/scripts/modal/components-modal.js') }}" type="text/javascript"></script>
<script src="{{ asset('/app-assets/js/scripts/forms/select/form-select2.js') }}" type="text/javascript"></script>
<script src="{{ asset('app-assets/vendors/js/forms/repeater/jquery.repeater.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('app-assets/js/scripts/forms/form-repeater.js') }}" type="text/javascript"></script>

<!-- Toaster js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" type="text/javascript"></script>
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    
</script>
