<div class="main-menu menu-fixed menu-light menu-accordion"  data-scroll-to-active="true">
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class="nav-item {{ (Request::segment(2) == 'dashboard') ? 'active' : '' }}">
                <a href="{{ route('therapist.dashboard') }}"><i class="fa fa-window-restore" aria-hidden="true"></i>
                    <span class="menu-title" data-i18n="">Dashboard</span>
                </a>
            </li>
            <li class="nav-item {{ (Request::segment(2) == 'profile') ? 'active' : '' }}">
                <a href="{{ route('therapist.profile') }}"><i class="fa fa-file-text" aria-hidden="true"></i>
                    <span class="menu-title" data-i18n="">My Profile</span>
                </a>
            </li>
            @if(auth('therapist')->user()->is_verified == "1")
            <li class="nav-item {{ (Request::segment(2) == 'session') ? 'active' : '' }}">
                <a href="{{ route('therapist.session.logs') }}"><i class="fa fa-calendar" aria-hidden="true"></i>
                    <span class="menu-title" data-i18n="">Session Log </span>
                </a>
            </li>
            <li class="nav-item {{ (Request::segment(2) == 'payment') ? 'active' : '' }}">
                <a href="{{ route('therapist.payment') }}"><i class="fa  fa-money" aria-hidden="true"></i>
                    <span class="menu-title" data-i18n="">Payment Log</span>
                </a>
            </li>
            {{-- <li class="nav-item ">
                <a href="#"><i class="fa fa-comments" aria-hidden="true"></i>
                    <span class="menu-title" data-i18n="">Feedback</span>
                </a>
            </li> --}}
            <li class="nav-item {{ (Request::segment(2) == 'appointments') ? 'active' : '' }}">
                <a href="{{ route('therapist.appointments') }}"><i class="fa fa-comments" aria-hidden="true"></i>
                    <span class="menu-title" data-i18n="">My Appointments</span>
                </a>
            </li>
            @endif
        </ul>
    </div>
</div>
