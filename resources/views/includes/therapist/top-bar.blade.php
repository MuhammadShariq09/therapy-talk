<!-- fixed-top-->
<nav class="header-navbar navbar-expand-md navbar navbar-with-menu fixed-top navbar-light navbar-border">
        <div class="navbar-wrapper">
          <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
              <li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
              <li class="nav-item">
              <a class="navbar-brand" href="{{ route('therapist.dashboard')}}">
                    <img class="brand-logo" alt="{{env('APP_NAME')}}" src="{{ asset('assets/admin/images/logo.png') }}">
                </a>
              </li>
              <li class="nav-item d-md-none"> <a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i class="fa fa-ellipsis-v"></i></a> </li>
            </ul>
          </div>
          <div class="navbar-container content">
            <div class="collapse navbar-collapse" id="navbar-mobile">
              <ul class="nav navbar-nav mr-auto float-left">

              </ul>
              <ul class="nav navbar-nav float-right">
                <li>
                                <div class="dropdown notification-dropdown" style="margin-top: 16px;">
                                    <a class="dropdown-toggle" href="#" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <img src="{{ asset('images/bell-icon.png') }}"><span class="badge">{{ isset(auth()->user()->unreadNotifications) ? auth()->user()->unreadNotifications->count() : 0 }}</span>
                                    </a>
                                    @if(isset(auth()->user()->unreadNotifications) && auth()->user()->unreadNotifications->count() > 0)
                                    <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                                        <li class="dropdown-menu-header">
                                            <h6 class="dropdown-header m-0"> <span class="grey darken-2">Notifications</span> <span class="notification-tag badge-default badge-danger float-right m-0">{{ auth()->user()->unreadNotifications->count() }} New</span> </h6>
                                        </li>
                                        <li class="scrollable-container media-list ps-container ps-theme-dark ps-active-y" data-ps-id="cbae8718-1b84-97ac-6bfa-47d792d8ad89">
                                            @foreach (auth()->user()->unreadNotifications as $notification)
                                            <a onclick="markRead('{{$notification->id}}')" role="button">
                                                <div class="media">
                                                    <div class="media-left align-self-center"><i class="fa fa-bell" aria-hidden="true"></i>
                                                    </div>
                                                    <div class="media-body">
                                                        <p class="notification-text font-small-3 text-muted">{{ $notification->data['message'] }}</p>
                                                    </div>
                                                </div>
                                            </a>
                                            @endforeach
                                        </li>
                                        <!-- <li class="dropdown-menu-footer"><a onclick="markRead()" role="button" class="dropdown-item text-muted text-center" {{ \Carbon\Carbon::createFromTimeStamp(strtotime($notification->created_at))->diffForHumans() }}>Read all notifications</a></li> -->
                                    </ul>
                                    @endif
                                </div>
                            </li>
                  <li class="dropdown dropdown-user nav-item">
                    <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                        <span class="avatar avatar-online">
                        @if(!auth('therapist')->user()->image)
                            <span class="icon-bg-circle" >{{ucfirst(substr(auth('therapist')->user()->full_name,0,1))}}</span>
                        @else
                            <img src="{{ asset(auth('therapist')->user()->image) }}" alt="{{ucfirst(auth('therapist')->user()->full_name)}}" onerror="this.style.display='none'">
                        @endif
                        </span>
                        <span class="user-name">{{ auth()->guard('therapist')->user()->full_name}}</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                      <a class="dropdown-item" href="{{route('therapist.profile')}}"><i class="ft-user"></i>Profile</a>
                      <a href="{{ url('/therapist/logout') }}" class="dropdown-item" ><i class="ft-power"></i> Logout</a>
                    </div>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                 </li>
                 <li class="nav-item d-none d-md-block"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </nav>
