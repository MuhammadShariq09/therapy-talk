<div class="right">
    <h2>Reply: <span>Appointment Rejected</span></h2>
    <h3>You Booked Appointment For</h3>
    <div action="" class="view-t">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                <label for=""><i class="fa fa-calendar"
                                 aria-hidden="true"></i>Date</label>
                <input type="text" name=""
                       placeholder="{{ $session->requested_date }}"
                       id="" class="form-control" disabled="">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                <label for=""><i class="fa fa-clock-o"
                                 aria-hidden="true"></i>Time</label>
                <input type="text" name=""
                       placeholder="{{ $session->requested_time }}"
                       id="" class="form-control" disabled="">
            </div>
        </div>
    </div>
    @foreach($session->rejections as $reg)
        <div class="reason">
            <h3>Reason:</h3>
            <p>{{ $reg->reason }}</p>
        </div>
        <h3>Suggested Time and Date</h3>
        <div action="" class="view-t">
            <div class="row">
                <div
                    class="col-lg-6 col-md-6 col-sm-12 form-group">
                    <label for=""><i class="fa fa-calendar"
                                     aria-hidden="true"></i>Date</label>
                    <input type="text" name=""
                           placeholder="{{ $reg->suggested_date->format('Y-m-d') }}"
                           id="" class="form-control"
                           disabled="">
                </div>
                <div
                    class="col-lg-6 col-md-6 col-sm-12 form-group">
                    <label for=""><i class="fa fa-clock-o"
                                     aria-hidden="true"></i>Time</label>
                    <input type="text" name=""
                           placeholder="{{ $reg->suggested_time }}"
                           id="" class="form-control"
                           disabled="">
                </div>
            </div>
        </div>
    @endforeach
    <a href="#" class="btn-a"
       id="bookMe"
       data-reason="{{ $session->reason }}"
       data-type="{{ $session->type }}"
       data-duration="{{ $session->duration }}"
       data-date="{{$session->rejections->first()->suggested_date->format('m/d/Y')}}"
       data-time="{{$session->rejections->first()->suggested_time}}"
       data-toggle="modal" data-target="#bookAppointmentModal">Book
        Again</a>
</div>
