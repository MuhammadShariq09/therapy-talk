
<ul>
    @if(Request::segment(1) == 'login')
    <li class="{{ (Request::segment(1) == '') ? 'active' : '' }}"><a href="{{ url('/') }}">Home</a></li>
    @else
    <li class="{{ (Request::segment(1) == '') ? 'active' : '' }}"><a href="{{ url('/') }}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
    @endif
    <li class="{{ (Request::segment(1) == 'about-us') ? 'active' : '' }}"><a href="{{ url('about-us') }}">About Us </a></li>
    <li class="{{ (Request::segment(1) == 'pricing') ? 'active' : '' }}"><a href="{{ url('pricing') }}">Pricing </a></li>
    <li class="{{ (Request::segment(1) == 'how-it-works') ? 'active' : '' }}"><a href="{{ url('how-it-works') }}">How It Works</a></li>
    {{-- @if(auth()->user() && auth()->user()->is_active == 1) --}}
    <li class="{{ (Request::segment(1) == 'find-a-therapist') ? 'active' : '' }}"><a href="{{ route('user.find-a-therapist') }}">Find a Therapist</a></li>
    {{-- @endif --}}
    <li class="{{ (Request::segment(1) == 'faq') ? 'active' : '' }}"><a href="{{ url('faq') }}">FAQs</a></li>
    <li class="{{ (Request::segment(1) == 'blogs') ? 'active' : '' }}"><a href="{{ url('blogs') }}">Blogs</a></li>
    <li class="{{ (Request::segment(1) == 'contact-us') ? 'active' : '' }}"><a href="{{ url('contact-us') }}">Contact</a></li>
</ul>
