@if($session->reschedule)
    <div class="right">
        @foreach($session->reschedule as $index => $reschedule)
            <h2>Pending Reschedule</h2>
            <div class="row">
                <div class="col-lg-12 col-sm-12">
                    <h3>Rescheduled By {{ ($reschedule->action_by) ? "User" : "Therapist" }}:</h3>
                    <p>Suggested Date &amp; Time:</p>
                </div>
                <div class="col-lg-6 col-sm-12">
                    <div class="pro-inner-row">
                        <label><i class="fa fa-calendar" aria-hidden="true"></i> Date</label>
                        <input type="text" required="required" spellcheck="true"
                               disabled="" class="form-control" value="{{ $reschedule->suggested_date }} ">
                    </div>
                </div>
                <div class="col-lg-6 col-sm-12">
                    <div class="pro-inner-row">
                        <label><i class="fa fa-clock-o" aria-hidden="true"></i> Time</label>
                        <input type="text" required="required" spellcheck="true" disabled=""
                               class="form-control" value="{{ $reschedule->suggested_time }} ">
                    </div>
                </div>
                <div class="col-lg-12 col-sm-12 mt-2">
                    @if($reschedule->status == 0)
                        <p>Status: Waiting for reply</p>
                        @if($reschedule->action_by == 0)
                            <a href="#" class="btn-a" data-toggle="modal" data-target="#accept-reschedule-modal">Accept</a>
                            <a href="#" class="btn-a" data-toggle="modal" data-target="#ask-for-refund-modal">Ask for refund</a>
                            <div class="login-fail-main user admin-profile profile-view">
                                <div class="featured inner">
                                    <div class="modal fade" id="accept-reschedule-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="forget-pass register-form">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                                                    <div class="modal-body">
                                                        <div class="payment-main-popup">
                                                            <form action="{{ route('user.accept-reschedule', ['id' => $reschedule->id]) }}" method="post">
                                                                @csrf
                                                                <div class="payment-modal-main">
                                                                    <div class="payment-modal-inner">
                                                                        <div class="row">
                                                                            <div class="col-12">
                                                                                <h1 class="text-center">Are you sure you want to accept reschedule request?</h1>
                                                                            </div>
                                                                            <div class="col-12 text-center">
                                                                                <button type="button" class="can" data-dismiss="modal">Close</button>
                                                                                <button type="submit" class="can con" >Accept</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal fade" id="ask-for-refund-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="forget-pass register-form">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                                                    <div class="modal-body">
                                                        <div class="payment-main-popup">
                                                            <form action="{{ route('user.ask-for-refund', ['id' => $reschedule->id]) }}" method="post">
                                                                @csrf
                                                                <div class="payment-modal-main">
                                                                    <div class="payment-modal-inner">
                                                                        <div class="row">
                                                                            <div class="col-12">
                                                                                <h1 class="text-center">Are you sure you want to ask for Refund?</h1>
                                                                            </div>
                                                                            <div class="col-12 text-center">
                                                                                <button type="button" class="can" data-dismiss="modal">Close</button>
                                                                                <button type="submit" class="can con" >Accept</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @elseif($reschedule->status == 1)
                        @if($reschedule->t_suggested_date)
                            <h2>Therapist Suggested Date &amp; Time:</h2>
                            <div class="view-t">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                                        <label for=""><i class="fa fa-calendar" aria-hidden="true"></i>Date</label>
                                        <input type="text" name="" placeholder="{{ $reschedule->t_suggested_date }}" id="" class="form-control" disabled="">
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                                        <label for=""><i class="fa fa-clock-o" aria-hidden="true"></i>Time</label>
                                        <input type="text" name="" placeholder="{{ $reschedule->t_suggested_time }}" id="" class="form-control" disabled="">
                                    </div>
                                </div>
                            </div>
                        @endif
                            <p>Status: Approved</p>

                    @else
                        <p>Status: Rejected</p>
                        @if($session->is_refund)
                            <h4><span>Appointment Cancelled</span></h4>
                            <p>Our representative will soon contact you regarding your refund</p>
                        @endif
                        @if($session->is_refund !== 1 && $reschedule->t_suggested_date)
                            <h2>Therapist Suggested Date &amp; Time:</h2>
                            <div class="view-t">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                                        <label for=""><i class="fa fa-calendar" aria-hidden="true"></i>Date</label>
                                        <input type="text" name="" placeholder="{{ $reschedule->t_suggested_date }}" id="" class="form-control" disabled="">
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                                        <label for=""><i class="fa fa-clock-o" aria-hidden="true"></i>Time</label>
                                        <input type="text" name="" placeholder="{{ $reschedule->t_suggested_time }}" id="" class="form-control" disabled="">
                                    </div>
                                </div>
                                <a href="#" class="btn-a" data-toggle="modal" data-target="#approve_rejected_therapist_suggested_datetime_modal">Accept Appointment</a>
                                <div class="login-fail-main user admin-profile profile-view">
                                    <div class="featured inner">
                                        <div class="modal fade" id="approve_rejected_therapist_suggested_datetime_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="forget-pass register-form">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                                                        <div class="modal-body">
                                                            <div class="payment-main-popup">
                                                                <div action="">
                                                                    <div class="payment-modal-main">
                                                                        <div class="payment-modal-inner">
                                                                            <div class="row">
                                                                                <div class="col-12">
                                                                                    <h1 class="text-center">Are you sure you want to accept reschedule request?</h1>

                                                                                    <p>Appointment rescheduled on: {{ $reschedule->t_suggested_date }}</p>
                                                                                    <p>Appointment rescheduled at: {{ $reschedule->t_suggested_time }}</p>
                                                                                </div>
                                                                                <div class="col-12 text-center">
                                                                                    <button type="button" class="can" data-dismiss="modal">Close</button>
                                                                                    <a
                                                                                        href="{{ route('user.approve_rejected_therapist_suggested_datetime_modal', ['id' => $reschedule->id ]) }}" class="can con">Accept</a>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        @endif
                    @endif
                </div>
            </div>
        @endforeach
    </div>
@endif
@if($session->status == 1 && $session->payment && $diff_in_hours ?? '' ?? '' ?? '' < 48)
    @if(!count($session->reschedule))
    <div class="col-12">
        <label class="red-label">
            You can reschedule this appointment within 48 Hours
        </label>
        <div class="row">
            <div class="col-12">
                <button type="button" class="big-center-cta" data-toggle="modal"
                        data-target="#reschedule_appointment">Reschedule Appointment</button>
            </div>
        </div>
    </div>
    <div class="login-fail-main user admin-profile profile-view">
        <div class="featured inner">
            <div class="modal fade bd-example-modal-lg" id="reschedule_appointment" tabindex="-1" role="dialog"
                 aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                        <div class="payment-modal-main">
                            <div class="payment-modal-inner"> 
                                <h1>Reschedule Appointment</h1>
                                <form method="post" action="{{ route('user.reschedule-approved-appointment', ['id' => $session->id ]) }}">
                                    @csrf
                                    <div class="row">
                                        <div class="col-12 form-group">
                                            <div class="pro-inner-row">
                                                <input id="select-date"
                                                       type="text" name="suggested_date" required="required" spellcheck="true"
                                                       class="form-control" placeholder="Expiry Date">
                                            </div>
                                        </div>
                                        <div class="col-12 form-group">
                                            <div class="pro-inner-row">
                                                <input type="text" name="suggested_time" id="timepicker" class="form-control" placeholder="Suggested Time">
                                            </div>
                                        </div>
                                        <div class="col-12 text-center">
                                            <button type="submit" class="can">Send to client</button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
@endif
