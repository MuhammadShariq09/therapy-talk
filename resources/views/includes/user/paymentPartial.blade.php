@if($session->is_paid==0)
<h4>
    Please pay for the session with in 24 hour to prevent auto-cancellation of session
</h4>
<input type="hidden" name="id" value="{{$session->id}}" id="session_id">
<a href="#" class="btn-a" data-toggle="modal" data-target="#payment-option-modal">Pay Now</a>

@else 
<h2><span class="text-success">Appointment Booked</span></h2>
@endif
{{-- <a href="javascript:;" class="btn-a" data-toggle="modal"
   data-target="#pay_now_modal">Pay Now</a> --}}

@php
    if(isset($subscription)){
        $date = new DateTime(date('Y-m-d', strtotime($subscription->expiry_date)));
       $now = new DateTime('now');
       $interval = $date->diff($now);
    }
   
 //  dd($interval->days>= 0)
@endphp
   
<div class="login-fail-main user admin-profile profile-view">
    <div class="featured inner">
      <div class="modal fade" id="payment-option-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="forget-pass register-form">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
              <div class="modal-body">
                <div class="payment-main-popup">
                  <form action="">
                    <div class="payment-modal-main">
                      <div class="payment-modal-inner">
                        <div class="row">
                          <div class="col-md-6 col-12">
                            <div class="option-box">
                              <p>Pay Directly<br>for session</p>
                              <button type="button" class="can" data-dismiss="modal" id="cont-pay">proceed</button>
                            </div>
                          </div>
                          <div class="col-md-6 col-12">
                            <div class="option-box">
                              <p>Use Package<br>for session</p>
                             {{--  @if(isset($interval) && $interval->days>=0)
                              <a class="can" data-toggle="modal" data-target="#expired">proceed</a> --}}
                             {{--  @elseif($pkg_usage!==null) --}}
                              @if($pkg_usage!==null)
                              <button type="button" class="can" data-dismiss="modal" id="cont2">proceed</button>
                              @else 
                              <a class="can" data-toggle="modal" data-target="#subscribe-pkg-modal">proceed</a>
                              @endif
                            </div>
                          </div>
                        </div>

                      </div>
                    </div>
                  </form>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal fade" id="direct-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="forget-pass register-form">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
              <div class="modal-body">
                <div class="payment-main-popup pay_for_booking">
                  <form method="post" action="{{ route('user.appointment.payment', ['id' => $session->id ]) }}" id="stripe-pay">
                    @csrf
                  <input type="hidden" id="stripKey" value="{{config('services.stripe.PUBLISH_KEY')}}">
                  <input type="hidden" value="" id="stripe_token" class="stripe_token">
                  <input type="hidden" name="id" value="{{$session->id}}" id="session-id" class="stripe_token">
                    <div class="payment-modal-main">
                      <div class="payment-modal-inner">
                        <div class="row">
                          <div class="col-12 form-group">
                            <label for="">Credit Card Number</label>
                            <input type="number" id="session-card-number" class="form-control" spellcheck="true" placeholder="Credit Card Number">
                          </div>
                          <div class="col-12 form-group">
                            <label for="">Card Holder Name</label>
                            <input type="text" id="session-card-holder" class="form-control" spellcheck="true" placeholder="Card Holder Name">
                          </div>
                          <div class="col-lg-6 col-12 form-group">
                            <label for="">CVV</label>
                            <input type="number" id="session-card-cvc" class="form-control" spellcheck="true" placeholder="CVV">
                          </div>
                          <div class="col-lg-6 col-12 form-group">
                            <label for="">Expiry Date</label>
                            <input type="text" id="session-card-expiry" id="select-date" onchange="getStripeToken()" placeholder="Expiry Date">
                          </div>
                          <div class="col-12 text-center blockui">
                            <button type="button" class="can" data-dismiss="modal">go back</button>
                            <button type="button" onclick="pay()" class="can con" >Pay </button>
                          </div>

                        </div>

                      </div>
                    </div>
                  </form>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal fade" id="package-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="forget-pass  register-form">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
              <div class="modal-body">
                <div class="payment-main-popup payment-view-popup">
                  <div class="payment-modal-main">
                    <div class="payment-modal-inner">
                    <div class="row">
                      <div class="col-12">
                          <h1>Package Details</h1>
                      </div>
                    </div> 
                    @if($pkg_usage!==null)
                      <div class="row">
                        <div class="col-12">
                              <div class="table-responsive">
                                <table class="table">
                                  <tbody>
                                    <tr>
                                      <td>Live Chats</td>
                                      <td>Total {{$pkg_usage->live_chat}}</td>
                                      <td>Used {{$pkg_usage->live_chat_availed}}</td>
                                      <td>Left {{$pkg_usage->live_chat-$pkg_usage->live_chat_availed}}</td>
                                    </tr>
                                    <tr>
                                      <td>Voice Call</td>
                                      <td>Total {{$pkg_usage->voice_call}}</td>
                                      <td>Used {{$pkg_usage->voice_call_availed}}</td>
                                      <td>Left {{$pkg_usage->voice_call-$pkg_usage->voice_call_availed}}</td>
                                    </tr>
                                    <tr>
                                      <td>Video Call</td>
                                      <td>Total {{$pkg_usage->video_call}}</td>
                                      <td>Used {{$pkg_usage->video_call_availed}}</td>
                                      <td>Left {{$pkg_usage->video_call-$pkg_usage->video_call_availed}}</td>
                                    </tr>
                                  </tbody>
                                </table>
                             
                           
                            <div class="text-center blockui">
                              <a class="can" href="{{url('/pricing')}}">Buy new package</a>
                              @if($session->session_type==1 && ($pkg_usage->live_chat==$pkg_usage->live_chat_availed))
                                <button type="button" class="can" id="cont3">Confirm</button>
                              @elseif($session->session_type==2 && ($pkg_usage->voice_call==$pkg_usage->voice_call_availed))
                                <button type="button" class="can" id="cont3">Confirm</button>
                              @elseif($session->session_type==3 && ($pkg_usage->video_call==$pkg_usage->video_call_availed))
                              <button type="button" class="can" id="cont3">Confirm</button>
                              @else
                                <button type="button" class="can" onclick="usePackage()">Confirm</button>
                              @endif
                            </div>
                          </div>
                        </div>
                      </div>
                    @endif
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal fade" id="package-modal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="forget-pass  register-form">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
              <div class="modal-body">
                <div class="payment-main-popup payment-view-popup">
                  <div class="payment-modal-main">
                    <div class="payment-modal-inner">
                      <div class="row">
                        <div class="col-12">
                          <h1 class="text-center">you dont have any {{ ($session->session_type == 1) ? 'Live Chat' : ($session->session_type == 2) ? "Voice Call" : "Video Call" }} available in this package</h1>
                            <div class="text-center">
                              <button class="can"  data-dismiss="modal">choose another package</button>
                              <button class="can" id="renew">Renew package</button>
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal fade" id="renew-package-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="forget-pass register-form">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
              <div class="modal-body">
                <div class="payment-main-popup pay_for_booking">
                  <input type="hidden" id="stripKey" value="{{config('services.stripe.PUBLISH_KEY')}}">
                  
                  <input type="hidden" value="" id="stripe_token" class="stripe_token">
                  <input type="hidden" name="id" value="{{$session->id}}" id="session-id">
                    <div class="payment-modal-main">
                      <div class="payment-modal-inner">
                        <div class="row">
                          <div class="col-12 form-group">
                            <label for="">Credit Card Number</label>
                            <input type="number" id="card-number" class="form-control" spellcheck="true" placeholder="Credit Card Number">
                          </div>
                          <div class="col-12 form-group">
                            <label for="">Card Holder Name</label>
                            <input type="text" id="card-holder" class="form-control" spellcheck="true" placeholder="Card Holder Name">
                          </div>
                          <div class="col-lg-6 col-12 form-group">
                            <label for="">CVV</label>
                            <input type="number" id="card-cvc" class="form-control" spellcheck="true" placeholder="CVV">
                          </div>
                          <div class="col-lg-6 col-12 form-group">
                            <label for="">Expiry Date</label>
                            <input type="text" id="card-expiry" id="select-date" onchange="getStripeToken()" placeholder="Expiry Date">
                          </div>
                          <div class="col-12 text-center blockui">
                            <button type="button" class="can" data-dismiss="modal">go back</button>
                            <button type="button" onclick="renewPackage()" class="can con" >Pay </button>
                          </div>

                        </div>

                      </div>
                    </div>
                  
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal fade" id="subscribe-pkg-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="forget-pass">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
              <div class="modal-body">
                <div class="payment-main-popup">
                  <form action="">
                    <div class="payment-modal-main">
                      <div class="payment-modal-inner">
                        <div class="row">
                          <div class="col-12">
                            <h1 class="text-center">Please subscribe to a package to continue.</h1>
                          </div>
  
                          <div class="col-12 text-center">
                          <a href="{{url('/pricing')}}" class="btn-a bkappnmt can" >Subscribe</a>
                          </div>
  
                        </div>
  
                      </div>
                    </div>
                  </form>
                </div>
  
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal fade" id="expired" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="forget-pass">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
              <div class="modal-body">
                <div class="payment-main-popup">
                  <form action="">
                    <div class="payment-modal-main">
                      <div class="payment-modal-inner">
                        <div class="row">
                          <div class="col-12">
                            <h1 class="text-center">Your package has been expired. Please renew your package to proceed.</h1>
                          </div>
  
                          <div class="col-12 text-center">
                          <a href="{{url('/subscription-logs')}}" class="btn-a bkappnmt can" >Renew Package</a>
                          </div>
  
                        </div>
  
                      </div>
                    </div>
                  </form>
                </div>
  
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>



  </div>

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script>
    // $('#cont').on('click', function(){
    //   $('#payment-option-modal').modal('hide');
    //   $('#direct-modal').modal('show');
    // });
    $('#cont-pay').on('click', function(){
      $('#payment-option-modal').modal('hide');
      $('#direct-modal').modal('show');
    });
    $('#cont2').on('click', function(){
      $('#payment-option-modal').modal('hide');
      $('#package-modal').modal('show');
    });
    $('#cont3').on('click', function(){
      $('#package-modal').modal('hide');
      $('#package-modal2').modal('show');
    });

    $('#renew').on('click', function(){
      $('#package-modal2').modal('hide');
      $('#renew-package-modal').modal('show');
    });

   // $('#session-card-number').mask("0000-0000-0000-0000", {placeholder: "____-____-____-____"});
    $('#session-card-cvc').mask("000", {placeholder: "___"});
    $('#session-card-expiry').mask("00/0000", {placeholder: "__/____"});

    $('#card-cvc').mask("000", {placeholder: "___"});
    $('#card-expiry').mask("00/0000", {placeholder: "__/____"});

    function stripeResponseHandler(status, response) {
    var token = response['id'];
    document.getElementById('stripe_token').value = token;
    console.log('input token',  $('#stripe_token').val());
    }

    function getStripeToken(){
    var date = $('#session-card-expiry').val();
    var arr = date.split('/');
    Stripe.setPublishableKey($('#stripKey').val());

    Stripe.createToken({
          number: $('#session-card-number').val(),
          cvc: $('#session-card-cvc').val(),
          exp_month: arr[0],
          exp_year: arr[1]
        }, stripeResponseHandler);
    }

    function pay(){
    
    $('#direct-modal').block({ message: 'Processing' });
    $.ajax({
        url: "{{route('user.appointment.payment')}}",
        data: {
          id : $('#session-id').val(),
          stripe_token : $('#stripe_token').val(),
          card : $('#session-card-number').val(),
          cvv : $('#session-card-cvc').val(),
          expiry : $('#session-card-expiry').val(),
          card_holder : $('#session-card-holder').val()
        },
        type: 'post',
        success: function (response) {
            $('#direct-modal').unblock({ message: 'Processing' });
            if(response.errors){
              toastr.error("Transaction failed, Please once again add your card details.", "Error!");
            }else{
              toastr.success(response.message, 'Success');
              setTimeout(function () {
                  window.location.href = $('#url').val() + '/my-session-logs';
              }, 1000);
            }
            
        },
        error: function (error) {
            $('#direct-modal').unblock({ message: 'Processing' });
            let errors = error.responseJSON.errors;
            Object.keys(errors).forEach(key=>{
                toastr.error(errors[key], "Error!");
            });
        }
    })
}

function usePackage(){
    var id = $('#session_id').val();
    $('#package-modal').block({ message: 'Processing' });
    $.ajax({
        url: "{{route('user.use-package', $session->id)}}",
       
        type: 'get',
        success: function (response) {
            $('#package-modal').unblock({ message: 'Processing' });
            toastr.success(response.message, 'Success');
            setTimeout(function () {
                window.location.href = $('#url').val() + '/my-session-logs';
            }, 1000);
        },
        error: function (error) {
            $('#package-modal').unblock({ message: 'Processing' });
            let errors = error.responseJSON.errors;
            Object.keys(errors).forEach(key=>{
                toastr.error(errors[key], "Error!");
            });
        }
    })

}

function renewPackage(){
  $('#renew-package-modal').block({ message: 'Processing' });
    $.ajax({
        url: "{{route('user.renew-package')}}",
        data: {
          id : $('#session-id').val(),
          stripe_token : $('#stripe_token').val(),
          card : $('#card-number').val(),
          cvv : $('#card-cvc').val(),
          expiry : $('#card-expiry').val(),
          card_holder : $('#card-holder').val()
        },
        type: 'post',
        success: function (response) {
            $('#renew-package-modal').unblock({ message: 'Processing' });
            toastr.success(response.message, 'Success');
            setTimeout(function () {
                window.location.href = $('#url').val() + '/my-session-logs';
            }, 1000);
        },
        error: function (error) {
            $('#renew-package-modal').unblock({ message: 'Processing' });
            let errors = error.responseJSON.errors;
            Object.keys(errors).forEach(key=>{
                toastr.error(errors[key], "Error!");
            });
        }
    })
}

  </script>
  <script type="text/javascript">
    $(document).ready(function(){
      $('.tab-a').click(function(){
        $(".tab").removeClass('tab-active');
        $(".tab[data-id='"+$(this).attr('data-id')+"']").addClass("tab-active");
        $(".tab-a").removeClass('active-a');
        $(this).parent().find(".tab-a").addClass('active-a');
      });
    });

 

  </script>
 
  
@endsection



