<div class="right">
<h2>Reply: <span>{{ ($session->status == 3) ? "Session Started" : "Appointment Accepted"}}</span></h2>
    <h3>The appointment details are</h3>
    <div class="view-t">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                <label for=""><i class="fa fa-calendar"
                                 aria-hidden="true"></i>Date</label>
                <input type="text" name=""
                       placeholder="{{ isset($session->reschedule[0]) ? $session->reschedule[0]->suggested_date : $session->requested_date }}"
                       id="" class="form-control" disabled="">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                <label for=""><i class="fa fa-clock-o"
                                 aria-hidden="true"></i>Time</label>
                <input type="text" name=""
                       placeholder="{{isset($session->reschedule[0]) ? $session->reschedule[0]->suggested_time : $session->requested_time }}"
                       id="" class="form-control" disabled="">
            </div>
           @php /*
            <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                <label for=""><i class="fa fa-money"
                                 aria-hidden="true"></i>Fee</label>
                <input type="text" name=""
                       placeholder="{{ '$'.config('app.fee')[$session->duration ] }}"
                       id="" class="form-control" disabled="">
            </div> */
            @endphp
        </div>
    </div>
</div>
