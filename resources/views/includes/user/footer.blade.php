<footer>
    <div class="container">
        <div class="footer-top">
            <div class="row">
                <div class="col-md-3 col-sm-6 col-12">
                    <div class="footer footer-1"> <a><img src="images/footer-logo.png" class="img-fluid" alt=""></a>
                        <p>Disclaimer: If you are in a life-threatening situation, do not use this site. Call the National Suicide Prevention Lifeline, a free, 24-hour hotline, at 1-800-273-8255. Your call will be routed to the crisis center near you. If your issue is an emergency, call 911 or go to your nearest emergency room.</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-12">
                    <div class="footer footer-2">
                        <h6>QUICK LINKS</h6>
                        @include('includes.user.nav')
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-12">
                    <div class="footer footer-3">
                        <h6>Company</h6>
                        <ul>
                            <li>My Therapy Talk<br>PO Box 928876<br>
                                San Diego, CA 92192<a href="https://www.google.com/maps/search/10190+14Street+A%2F58+California,+USA/@37.1931153,-123.7964139,6z/data=!3m1!4b1" target="_blank"><br>
                                    10190 14 Street A/58 California, USA</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-12">
                    <div class="footer footer-4">
                        <h6>Contact Us</h6>
                        <ul>
                            <li>Support Chat</li>
                            <li><a href="tel:+">1800 310 254 2525</a></li>
                            <ul class="social-icon">
                                <li><a href="#"><i class="fa fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-vimeo" aria-hidden="true"></i></a></li>
                            </ul>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <p> {{ config('app.name') }} , {{ date('Y') }}, All Rights Reserved</p>
    </div>
</footer>
<script
    src="https://code.jquery.com/jquery-3.4.1.min.js"
    integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
    crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<!-- Toaster js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" type="text/javascript"></script>
<script src="{{ asset('js/jquery.slicknav.min.js') }}"></script>
<script src="{{ asset('js/wow.min.js') }}"></script>
<script src="{{ asset('js/datatables.min.js') }}"></script>
<script src="{{ asset('js/datatable-basic.js') }}"></script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAHPUufTlBkF5NfBT3uhS9K4BbW2N-mkb4&libraries=places&callback=initAutocomplete"
        async defer></script>
<script>
    $(document).ready(function() {
        $('#menu').slicknav();
    });
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
