<div class="right">
    <h2>Status: <span>Session Completed</span></h2>
    <h3>Appointment in 3 days </h3>
    <form action="" class="view-t">
      <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 form-group">
          <label for=""><i class="fa fa-calendar" aria-hidden="true"></i>Date</label>
          <input type="text" name="" placeholder="19 dec 2018"  class="form-control" disabled="">
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 form-group">
          <label for=""><i class="fa fa-clock-o" aria-hidden="true"></i>Time</label>
          <input type="text" name="" placeholder="9:00 AM - 10:00 AM"  class="form-control" disabled="">
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 form-group">
          <label for=""><i class="fa fa-money" aria-hidden="true"></i>Fee</label>
          <input type="text" name="" placeholder="$ 300"  class="form-control" disabled="">
        </div>
      </div>
    </form>
    <div class="session-rating">
      <label>Session Rating:</label>
      <div class="rating" data-rate-value=5></div>
      {{-- <ul>
        <li><a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
        <li><a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
        <li><a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
        <li><a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
        <li><a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
      </ul> --}}
    </div>

    <h3>Give Feedback: </h3>
    <form action="" class="view-t">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 form-group">
          <textarea type="text" id="feedback" placeholder="Enter Feedback Here"  class="form-control"></textarea>
        </div>
      </div>
    </form>

   <button type="submit" onclick="giveFeedback({{ $session->therapist->id }})" class="btn-a">Submit</button> 
  </div>