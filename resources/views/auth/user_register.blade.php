@extends('layouts.app')
@section('title', ' Register')
@section('body-class',"vertical-layout vertical-menu 2-columns menu-expanded  modal-open")
@section('body-col',"2-column")
@section('content')
<section class="register loginn">
<div class="container"></div>
<div class="login-fail-main user admin-profile profile-view loginn reg-form">
<div class="featured inner ">
<div class="modal" style="display: block" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered" role="document">
<div class="modal-content">
<div class="forget-pass register-form">
<div class="modal-body">
<div class="payment-main-popup">
    <div action="">
        <div class="payment-modal-main">
            <div class="payment-modal-inner">
                <div class="row">
                    @if (session('success'))
                        <div class="alert alert-success">{{ session('success') }}</div>
                    @endif
                    @if (session('error'))
                        <div class="alert alert-danger">{{ session('error') }}</div>
                    @endif
                    <form action="{{ route('register') }}" method="post" enctype="multipart/form-data" id="regForm">
                        @csrf
                        <input type="hidden" name="assessment_token" value="{{$assessment_token ?? ''}}">
                        <div class="col-12">
                            <h1>Register</h1>
                            <label>As A Member
                            </label>
                            <div class="row u-profile">
                                <div class="col-12">
                                    <div class="attached">
                                        <img src="{{ asset('images/student-pro-girl.png') }}" class="img-full " id="preview" alt="">
                                        <button type="button" class="camera-btn" onclick="document.getElementById('upload').click()"><i class="fa fa-camera"></i></button>
                                        <input type="file" onchange="readURL(this, 'preview')" id="upload" name="file">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="fields">
                            <div class="row">
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-sm-12 form-group">
                                            <i class="fa fa-user-circle" aria-hidden="true"></i>
                                            <input spellcheck="true" value="{{old('full_name')}}" name="full_name" type="text" spellcheck="true" class="form-control" placeholder="First Name">
                                            @if ($errors->has('full_name'))
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('full_name') }}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                        {{-- <div class="col-sm-6 form-group">
                                            <i class="fa fa-user-circle" aria-hidden="true"></i>
                                            <input spellcheck="true" value="{{old('last_name')}}" name="last_name" type="text" spellcheck="true" class="form-control" placeholder="Last Name">
                                            @if ($errors->has('last_name'))
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('last_name') }}</strong>
                                                    </span>
                                            @endif

                                        </div> --}}
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-sm-6 form-group">
                                            <i class="fa fa-envelope"></i>
                                            <input {{ isset($email) ? 'readonly': ''}}  spellcheck="true" type="email" name="email" spellcheck="true" class="form-control"
                                                   value="{{$email ?? ''}}" placeholder="Enter Email">

                                        </div>
                                        <div class="col-sm-6 form-group">
                                            <i class="fa fa-phone" aria-hidden="true"></i>
                                            <input spellcheck="true" value="{{old('phone')}}" name="phone" type="number" spellcheck="true"
                                                   class="form-control" placeholder="Phone">
                                            @if ($errors->has('phone'))
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('phone') }}</strong>
                                                    </span>
                                            @endif

                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-sm-6 form-group">
                                            <i class="fa fa-calendar" aria-hidden="true"></i>
                                            <input type="date" value="{{old('date_of_birth')}}" name="date_of_birth"
                                                   class="form-control" placeholder="Date of Birth">
                                            @if ($errors->has('date_of_birth'))
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('date_of_birth') }}</strong>
                                                    </span>
                                            @endif

                                        </div>
                                        <div class="col-6 form-group d-flex align-items-center gender-field">
                                            <label for=""><i class="fa fa-transgender" aria-hidden="true"></i> Gender</label>
                                            <label class="radio-container">Male
                                                <input type="radio" name="gender" checked="checked" value="0" name="radio">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="radio-container">Female
                                                <input type="radio" name="gender" value="1" name="radio">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="radio-container">Non Binary
                                                <input type="radio" name="gender" value="2" name="radio">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-sm-12 form-group">
                                            <i class="fa fa-globe" aria-hidden="true"></i>
                                            <input spellcheck="true" name="address" id="autocomplete" onFocus="geolocate()"
                                                   type="text" value="{{old('address')}}"
                                                   spellcheck="true" class="form-control" placeholder="Address">
                                            @if ($errors->has('address'))
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('address') }}</strong>
                                                    </span>
                                            @endif

                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-sm-6 form-group">
                                            <i class="fa fa-globe" aria-hidden="true"></i>
                                            <input spellcheck="true" value="{{old('country')}}"
                                                   id="country" name="country" type="text" spellcheck="true" class="form-control"
                                                   placeholder="Country">
                                            @if ($errors->has('country'))
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('country') }}</strong>
                                                    </span>
                                            @endif

                                        </div>
                                        <div class="col-sm-6 form-group">
                                            <i class="fa fa-map" aria-hidden="true"></i>
                                            <input spellcheck="true" value="{{old('state')}}" id="administrative_area_level_1" name="state"
                                                   type="text" spellcheck="true" class="form-control" placeholder="state">
                                            @if ($errors->has('state'))
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('state') }}</strong>
                                                    </span>
                                            @endif

                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-sm-6 form-group">
                                            <i class="fa fa-map-marker"></i>
                                            <input spellcheck="true" value="{{old('city')}}" id="locality" name="city" type="text"
                                                   spellcheck="true" class="form-control" placeholder="Enter City">
                                            @if ($errors->has('city'))
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('city') }}</strong>
                                                    </span>
                                            @endif

                                        </div>
                                        <div class="col-sm-6 form-group">
                                            <i class="fa fa-map-pin" aria-hidden="true"></i>
                                            <input spellcheck="true" value="{{old('zipcode')}}" id="postal_code" name="zipcode"
                                                   type="text" spellcheck="true" class="form-control" placeholder="Zip Code">
                                            @if ($errors->has('zipcode'))
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('zipcode') }}</strong>
                                                    </span>
                                            @endif

                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-sm-6 form-group">
                                            <i class="fa fa-unlock-alt" aria-hidden="true"></i>
                                            <input spellcheck="true" name="password" type="password" spellcheck="true"
                                                   class="form-control" placeholder="Enter Password">
                                            @if ($errors->has('password'))
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                        <div class="col-sm-6 form-group">
                                            <i class="fa fa-unlock-alt" aria-hidden="true"></i>
                                            <input spellcheck="true" name="password_confirmation" type="password" spellcheck="true" class="form-control" placeholder="Re-Type Password">
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 form-group">
                                    <h1>Terms & Conditions</h1>
                                    <a style="float: left;" href="{{ route('terms-and-conditions')}}">Please read our terms and conditions</a>
                                </div>
                                <div class="col-12 form-group">
                                    <label class="check">I Accept
                                        <input spellcheck="true" type="checkbox" name="accept" value="1" >
                                        <span class="checkmark"></span>
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('accept') }}</strong>
                                        </span>
                                    </label>
                                </div>
                                <div class="col-12 form-group blockui">
                                    <button type="submit" class="can form-control" data-dismiss="modal" id="cont3">Register</button>
                                </div>
                                <div class="col-12 form-group register-main">
                                    <a href="{{ url('/login') }}" class="login back" ><i class="fa fa-long-arrow-left"></i>back to login</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>


@endsection

@section('js')
    <script src="{{ asset('js/address_autocomplete.js') }}"></script>
    <script src="{{ asset('assets/js/auth.js') }}"></script>
@endsection
