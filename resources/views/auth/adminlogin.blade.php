@extends('layouts.master')
@section('title', ' Administrator Login')
@section('body-class',"vertical-layout vertical-menu 2-columns menu-expanded")
@section('body-col',"2-column")
@section('content')
<section class="register loginn">
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-xl-3"></div>
            <div class="col-md-8 col-xl-6 col-12">
                <div class="register-main">
                    <img src="{{ asset('assets/admin/images/login-logo.png') }}" class="img-full" alt="logo">
                    <div class="form-main">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="img-div">
                                    <div class="img-div-inner"> </div>
                                </div>
                            </div>
                        </div>
                        <!--fields start here-->
                        <form method="POST" action='{{ url("login/admin") }}' aria-label="{{ __('Login') }}">
                            @csrf
                            <div class="fields">
                                <div class="row">
                                    <div class="col-md-12">
                                        <i class="fa fa-user-circle" aria-hidden="true"></i>
                                        <input type="email" id="email" name="email" value="{{ old('email') }}" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" spellcheck="true" placeholder="User Email">
                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="col-md-12">
                                        <i class="fa fa-lock" aria-hidden="true"></i>
                                        <input type="password" id="password" name="password" spellcheck="true" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Password">
                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-6">
                                        <label class="check">Remember me
                                            <input type="checkbox" checked="checked">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                    <div class="col-md-6 col-6">
                                        <a href="{{ route('password.request') }}">Forgot Password?</a>
                                    </div>
                                </div>
                               <div class="row">
                                <div class="col-12">
                                    <button type="submit" class="form-control">Login</button>
                                </div>
                                   
                                    <div class="col-md-12 col-12 text-center">
                                        <a href="{{url('/')}}" class="login-back">Back To Website</a>
                                    </div>
                               </div> 
                                

                                <!--fields end here-->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-xl-3"></div>
        </div>
    </div>
</section>


@endsection
