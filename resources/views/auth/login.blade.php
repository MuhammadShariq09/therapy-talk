@extends('layouts.app')
@section('title', ' Login')
@section('body-class',"")
@section('body-col',"2-column")
@section('content')
@section('css')
    <style>
        .assestment_section{
            display: none;
        }
        .assestment_section.active {
            display: block;
        }

        .loginBtn {
        box-sizing: border-box;
        position: relative;
        /* width: 13em;  - apply for fixed size */
        margin: 0.2em;
        padding: 0 15px 0 46px;
        border: none;
        text-align: left;
        line-height: 34px;
        white-space: nowrap;
        border-radius: 0.2em;
        font-size: 16px;
        color: #FFF;
        }
        .loginBtn:before {
        content: "";
        box-sizing: border-box;
        position: absolute;
        top: 0;
        left: 0;
        width: 34px;
        height: 100%;
        }
        .loginBtn:focus {
        outline: none;
        }
        .loginBtn:active {
        box-shadow: inset 0 0 0 32px rgba(0,0,0,0.1);
        }


        /* Facebook */
        .loginBtn--facebook {
        background-color: #4C69BA;
        background-image: linear-gradient(#4C69BA, #3B55A0);
        /*font-family: "Helvetica neue", Helvetica Neue, Helvetica, Arial, sans-serif;*/
        text-shadow: 0 -1px 0 #354C8C;
        }
        .loginBtn--facebook:before {
        border-right: #364e92 1px solid;
        background: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/14082/icon_facebook.png') 6px 6px no-repeat;
        }
        .loginBtn--facebook:hover,
        .loginBtn--facebook:focus {
        background-color: #5B7BD5;
        background-image: linear-gradient(#5B7BD5, #4864B1);
        }


        /* Google */
        .loginBtn--google {
        /*font-family: "Roboto", Roboto, arial, sans-serif;*/
        background: #DD4B39;
        }
        .loginBtn--google:before {
        border-right: #BB3F30 1px solid;
        background: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/14082/icon_google.png') 6px 6px no-repeat;
        }
        .loginBtn--google:hover,
        .loginBtn--google:focus {
        background: #E74B37;
        }
    </style>
@endsection
<section class="register loginn">
    <div class="container">
        <div class="login-main-inner">
            <div class="login-main">
                <div class="row m-0">
                    <div class="col-md-6 col-sm-12 p-0">
                        <div class="login-img">
                            <img src="{{ asset('images/login-left-img.png') }}">
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 p-0 login-right">
                        <div class="register-main">
                            <h1>Login</h1>
                            <h2>Login to your account</h2>
                            <div class="form-main">
                                @if (session('success'))
                                    <div class="alert alert-success">{{ session('success') }}</div>
                                @endif
                                @if (session('error'))
                                    <div class="alert alert-danger">{{ session('error') }}</div>
                                @endif
                                <!--fields start here-->
                                <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                                    @csrf
                                    <div class="fields">
                                        <div class="row ">
                                            <div class="col-md-12 form-group">
                                                <i class="fa fa-envelope" aria-hidden="true"></i>
                                                <input spellcheck="true" type="text" name="email" class="form-control" spellcheck="true" placeholder="Email Address">
                                                @if ($errors->has('email'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="col-md-12 form-group">
                                                <i class="fa fa-key"></i>
                                                <input spellcheck="true" name="password" type="password" spellcheck="true" class="form-control" placeholder="Password">
                                                @if ($errors->has('password'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="row align-items-end">
                                            <div class="col-md-6 col-6">
                                            </div>
                                            <div class="col-md-6 col-6">
                                                <a href="javascript:;"
                                                   data-toggle="modal"
                                                   data-target="#resetPassword">Forgot Password?</a></div>
                                        </div>
                                        <button type="submit" class="form-control">Login</button>
                                        <div class="new-user">
                                            <p>Not a Member? Register Now</p>
                                            <a data-toggle="modal" href="javascript:;" data-target="#registerModal" class="registerFrm login form-control">Register</a>
                                            
                                            <a href="{{ url('/') }}" class="login back" ><i class="fa fa-long-arrow-left"></i>back to home</a>
                                        </div>
                                        <!--fields end here-->
                                    </div>
                                    <br/>
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <a href="{{ url('/user/login/facebook') }}" class="loginBtn loginBtn--facebook"> Facebook</a>
                                            
                                        </div>
                                        <div class="col-md-6">
                                        <a href="{{ url('/user/login/google') }}" class="loginBtn loginBtn--google"> Google</a>
                                   
                                        </div>
                                    </div>
                                        
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="login-fail-main user admin-profile profile-view loginn reg-form payment-modal-inner">
            <div class="featured inner ">
              <div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                    <div class="forget-pass register-form">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                      <div class="modal-body">
                        <div class="payment-main-popup">
                           <div class="payment-modal-inner">
                        <form action="{{ route('register') }}" method="post" enctype="multipart/form-data" id="regForm">
                            @csrf
                            
                            <div class="col-12">
                                <h1>Register</h1>
                                <label>As A Member
                                </label>
                                <div class="row u-profile">
                                    <div class="col-12">
                                        <div class="attached">
                                            <img src="{{ asset('images/student-pro-girl.png') }}" class="img-full " id="preview" alt="">
                                            <button type="button" class="camera-btn" onclick="document.getElementById('upload').click()"><i class="fa fa-camera"></i></button>
                                            <input type="file" onchange="readURL(this, 'preview')" id="upload" accept=".gif,.jpg,.jpeg,.png" name="file">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="fields">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="row">
                                            <div class="col-sm-12 form-group">
                                                <i class="fa fa-user-circle" aria-hidden="true"></i>
                                                <input spellcheck="true" value="{{old('full_name')}}" name="full_name" type="text" spellcheck="true" class="form-control" placeholder="Full Name">
                                                @if ($errors->has('full_name'))
                                                    <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('full_name') }}</strong>
                                                        </span>
                                                @endif
                                            </div>
                                            {{-- <div class="col-sm-6 form-group">
                                                <i class="fa fa-user-circle" aria-hidden="true"></i>
                                                <input spellcheck="true" value="{{old('last_name')}}" name="last_name" type="text" spellcheck="true" class="form-control" placeholder="Last Name">
                                                @if ($errors->has('last_name'))
                                                    <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('last_name') }}</strong>
                                                        </span>
                                                @endif
    
                                            </div> --}}
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="row">
                                            <div class="col-sm-6 form-group">
                                                <i class="fa fa-envelope"></i>
                                                <input {{ isset($email) ? 'readonly': ''}}  spellcheck="true" type="email" name="email" spellcheck="true" class="form-control"
                                                       value="{{$email ?? ''}}" placeholder="Enter Email">
    
                                            </div>
                                            <div class="col-sm-6 form-group">
                                                <i class="fa fa-phone" aria-hidden="true"></i>
                                                <input spellcheck="true" value="{{old('phone')}}" name="phone" type="number" spellcheck="true"
                                                       class="form-control" placeholder="Phone">
                                                @if ($errors->has('phone'))
                                                    <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('phone') }}</strong>
                                                        </span>
                                                @endif
    
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="row">
                                            <div class="col-sm-6 form-group">
                                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                                <input type="text" value="{{old('date_of_birth')}}" name="date_of_birth"
                                                       class="form-control" placeholder="Date of Birth">
                                                @if ($errors->has('date_of_birth'))
                                                    <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('date_of_birth') }}</strong>
                                                        </span>
                                                @endif
    
                                            </div>
                                            <div class="col-6 form-group d-flex align-items-center gender-field">
                                                <label for=""><i class="fa fa-transgender" aria-hidden="true"></i> Gender</label>
                                                <label class="radio-container">Male
                                                    <input type="radio" name="gender" checked="checked" value="0" name="radio">
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="radio-container">Female
                                                    <input type="radio" name="gender" value="1" name="radio">
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="radio-container">Non Binary
                                                    <input type="radio" name="gender" value="2" name="radio">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="row">
                                            <div class="col-sm-12 form-group">
                                                <i class="fa fa-globe" aria-hidden="true"></i>
                                                <input spellcheck="true" name="address" id="autocomplete" onFocus="geolocate()"
                                                       type="text" value="{{old('address')}}"
                                                       spellcheck="true" class="form-control" placeholder="Address">
                                                @if ($errors->has('address'))
                                                    <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('address') }}</strong>
                                                        </span>
                                                @endif
    
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="row">
                                            <div class="col-sm-6 form-group">
                                                <i class="fa fa-globe" aria-hidden="true"></i>
                                                <input spellcheck="true" value="{{old('country')}}"
                                                       id="country" name="country" id="country" type="text" spellcheck="true" class="form-control"
                                                       placeholder="Country">
                                                @if ($errors->has('country'))
                                                    <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('country') }}</strong>
                                                        </span>
                                                @endif
    
                                            </div>
                                            <div class="col-sm-6 form-group">
                                                {{-- <i class="fa fa-map" aria-hidden="true"></i> --}}
                                                <select id="state" class="form-control" name="state">
                                                    <option>Select State...</option>
                                                    @foreach($states as $row)
                                                    <option value="{{$row->id}}">{{$row->name}}</option>
                                                    @endforeach
                                                  
                                                </select>
                                                <input  id="administrative_area_level_1" type="hidden" />
                                                @if ($errors->has('state'))
                                                    <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('state') }}</strong>
                                                        </span>
                                                @endif
    
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="row">
                                            <div class="col-sm-6 form-group">
                                                {{-- <i class="fa fa-map-marker"></i> --}}
                                                <select id="city" class="form-control" name="city" placeholder="Enter City">
                                                    <option>Select City...</option>
                                                </select>
                                                {{-- <input spellcheck="true" value="{{old('city')}}" id="locality" name="city" type="text"
                                                       spellcheck="true" class="form-control" placeholder="Enter City"> --}}
                                                @if ($errors->has('city'))
                                                    <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('city') }}</strong>
                                                        </span>
                                                @endif
    
                                            </div>
                                            <div class="col-sm-6 form-group">
                                                <i class="fa fa-map-pin" aria-hidden="true"></i>
                                                <input spellcheck="true" value="{{old('zipcode')}}" id="zipcode" name="zipcode"
                                                       type="text" spellcheck="true" class="form-control" placeholder="Zip Code">
                                                @if ($errors->has('zipcode'))
                                                    <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('zipcode') }}</strong>
                                                        </span>
                                                @endif
    
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="row">
                                            <div class="col-sm-6 form-group">
                                                <i class="fa fa-unlock-alt" aria-hidden="true"></i>
                                                <input spellcheck="true" name="password" type="password" spellcheck="true"
                                                       class="form-control" placeholder="Enter Password">
                                                @if ($errors->has('password'))
                                                    <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('password') }}</strong>
                                                        </span>
                                                @endif
                                            </div>
                                            <div class="col-sm-6 form-group">
                                                <i class="fa fa-unlock-alt" aria-hidden="true"></i>
                                                <input spellcheck="true" name="password_confirmation" type="password" spellcheck="true" class="form-control" placeholder="Re-Type Password">
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 form-group">
                                        <h1>Terms & Conditions</h1>
                                        <a style="float: left;" href="{{ route('terms-and-conditions')}}">Please read our terms and conditions</a>
                                    </div>
                                    <div class="col-12 form-group">
                                        <label class="check">I Accept
                                            <input spellcheck="true" type="checkbox" name="accept" value="1" >
                                            <span class="checkmark"></span>
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('accept') }}</strong>
                                            </span>
                                        </label>
                                    </div>
                                    <div class="col-12 form-group blockui">
                                        <button type="submit" class="can form-control">Register</button>
                                    </div>
                                    <div class="col-12 form-group register-main">
                                        <a href="{{ url('/login') }}" class="login back" ><i class="fa fa-long-arrow-left"></i>back to login</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="login-fail-main user admin-profile profile-view loginn">
                <div class="featured inner ">
              <div class="modal fade" id="sendCode" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="forget-pass register-form code-popup">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                          <div class="modal-body">
                            <div class="payment-main-popup payment-view-popup">
                              <div class="payment-modal-main">
                                <div class="payment-modal-inner">
                                  <div class="row justify-content-center">
                                    <div class="col-12 form-group">
                                      <h1 class="text-center">Please enter verification code sent to your email</h1>
                                    </div>
                                    <form action="{{ route('verify-account') }}" method="post" id="verifyAccount">
                                        <div class="col-12 form-group">
                                        <div class="code-input blockui">
                                            <input type="number" name="code[]">
                                            <input type="number" name="code[]">
                                            <input type="number" name="code[]">
                                            <input type="number" name="code[]">
                                        </div>
                                        <a role="button" onclick="resendCode()">resend code</a>
                                        </div>
                                        <div class="col-12 form-group text-center blockui">
                                        <button type="submit" class="can" >Done</button>
                                        </div>
                                    </form>
          
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

        <div class="login-fail-main user admin-profile profile-view ">
            <div class="featured inner ">
                <div class="modal fade" id="resetPassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="forget-pass">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                                <div class="modal-body">
                                    <div class="payment-main-popup">
                                        <form action="{{ route('user.send-verification-code') }}" method="post" id="ursdvfcd">
                                            @csrf
                                            <div class="payment-modal-main">
                                                <div class="payment-modal-inner">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <h1>Password Recovery</h1>
                                                        </div>
                                                        <div class="col-12 form-group">
                                                            <i class="fa fa-envelope" aria-hidden="true"></i>
                                                            <input type="text" name="email" class="form-control" spellcheck="true" placeholder="Enter Email Address">
                                                        </div>
                                                        <div class="col-12 form-group text-center blockui">
                                                            <button type="submit" class="can">Continue</button>
                                                        </div>
                                                        <div class="col-12 form-group register-main">
                                                            <a href="javascript:;" data-dismiss="modal" class="login back"><i class="fa fa-long-arrow-left"></i>back to login</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="verifyCode" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="forget-pass">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                                <div class="modal-body">
                                    <div class="payment-main-popup payment-view-popup">
                                        <div class="payment-modal-main">
                                            <div class="payment-modal-inner">
                                                <form action="{{ route('user.verify-verification-code') }}" method="post" id="urvrvfcd">
                                                    @csrf
                                                <div class="row">
                                                    <div class="col-12">
                                                        <h1>Password Recovery</h1>
                                                    </div>
                                                    <div class="col-12 form-group">
                                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                                        <input type="number" name="code" class="form-control" spellcheck="true" placeholder="Enter Verification Code">
                                                    </div>
                                                    <div class="col-12 form-group text-center">
                                                        <button type="submit" class="can">Continue</button>
                                                    </div>
                                                    <div class="col-12 form-group register-main">
                                                        <a href="javascript:;" data-dismiss="modal" class="login back"><i class="fa fa-long-arrow-left"></i>back to login</a>
                                                    </div>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="updatePassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="forget-pass">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                                <div class="modal-body">
                                    <div class="payment-main-popup payment-view-popup">
                                        <div class="payment-modal-main">
                                            <div class="payment-modal-inner">
                                                <form action="{{ route('user.update-password') }}" method="post" id="uruppw">
                                                    @csrf
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <h1>Password Recovery</h1>
                                                        </div>
                                                        <div class="col-12 form-group">
                                                            <i class="fa fa-unlock-alt" aria-hidden="true"></i>
                                                            <input type="password" name="password" class="form-control" spellcheck="true" placeholder="Enter Password">
                                                        </div>
                                                        <div class="col-12 form-group">
                                                            <i class="fa fa-unlock-alt" aria-hidden="true"></i>
                                                            <input type="password" name="confirm_password" class="form-control" spellcheck="true" placeholder="Confirm Password">
                                                        </div>
                                                        <div class="col-12 form-group text-center">
                                                            <button type="submit" class="can" >Submit</button>
                                                        </div>
                                                        <div class="col-12 form-group register-main">
                                                            <a href="javascript:;" class="login back"><i class="fa fa-long-arrow-left"></i>back to login</a>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="login-fail-main user admin-profile profile-view loginn reg-form">
            <div class="featured inner ">
                <div class="modal fade" id="assesmentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="forget-pass register-form assesment-test">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                                <div class="modal-body">
                                    <div id="assestmentData"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="login-fail-main user admin-profile profile-view loginn">
            <div class="featured inner ">
                <div class="modal fade" id="successModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="forget-pass register-form thanku-popup">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                                <div class="modal-body">
                                    <div class="payment-main-popup payment-view-popup">
                                        <div class="payment-modal-main">
                                            <div class="payment-modal-inner">
                                                <div class="row">
                                                    <div class="col-12 form-group">
                                                        <h1 class="text-center">Thank you for taking the time to share openly and honestly</h1>

                                                    </div>
                                                    <div class="col-12 form-group">
                                                        <label>We will get back to you with your results on your email.</label>
                                                    </div>
                                                    <div class="col-12 form-group text-center">
                                                        <a href="{{ url('/') }}" class="can">Go Back To Home</a>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('js')
    <script src="{{ asset('assets/js/auth.js') }}"></script>
    <script>
        $(document).ready(function() {
       
        $('#state').on('change', function() {
        
                var stateID = $(this).val();
                var url = $('#url').val() + '/get-cities/';
                if(stateID) {
                    $.ajax({
                        url: url+stateID,
                        type: "GET",
                        dataType: "json",
                        success:function(data) {
                            $('#city').empty();
                            $.each(data, function(key, value) {
                                // console.log(key);
                                // console.log(value); return;
                                $('#city').append('<option value="'+ value.id +'">'+ value.city +'</option>');
                            });
    
    
                        }
                    });
                }else{
                    $('#city]').empty();
                }
        });
    });
        </script>

        <script src="{{ asset('js/address_autocomplete.js') }}"></script>

@endsection
