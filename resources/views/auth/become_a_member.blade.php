<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <title>My Therapy Talk - User Assessment</title>
  <link rel="shortcut icon" href="{{URL::asset('assets/admin/images/favicon.ico')}}">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Barlow:300,300i,400,400i,500,500i,600,600i,700&display=swap" rel="stylesheet">

  <link rel="stylesheet" type="text/css" href="{{ asset('css/wizard.css')}}">
  <!-- BEGIN Custom CSS-->
  <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css')}}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/responsive.css')}}">
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/u_style.css') }}">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css" />
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" />
  <!-- BEGIN VENDOR CSS-->
  <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
  

  <style>
    .blockUI.blockMsg.blockElement {
        width: 100px !important;
        height: 100px !important;
        border-radius: 50%;
        color: transparent !important;
        background-color: transparent !important;
        transform: translateX(-50%) translateY(-50%);
        top: 50% !important;
        left: 50% !important;
        border-color: white !important;
        border-top-color: #00b7d5 !important;
        animation: spinner 1.2s ease-in-out infinite;
    }

    @keyframes spinner {
        0% {
            transform: translateX(-50%) translateY(-50%) rotate(0deg);
        }
        100% {
            transform: translateX(-50%) translateY(-50%) rotate(360deg);
        }
    }
</style>

  <!-- END Custom CSS-->
</head>
<body>
  {{-- @include('includes.user.top-bar') --}}
  <section class="register profile-view assesment-form blockui">

    <div class="container">
      <div class="card pro-main">
        <div class="row">
          <div class="col-md-12 col-sm-12 text-center">
            <h1>Assesment Test</h1>
          </div>
        </div>
        @if(auth()->user()->assessment->last() && auth()->user()->assessment->last()->status ==1)
        <div class="row ">
          <div class="col-12 ">
            <h2 style="text-align: center!important;">You have already Submitted your assessment.</h2>
          </div>
        </div>
        @else

        <div class="row ">
          <div class="col-12">
            <form action="{{ url('assessment-form') }}" method="post" class="number-tab-steps wizard-circle " id="assesmentForm">

                @csrf
              <!-- Step 1 -->
              <h6></h6>
              <fieldset>
                <div class="row">
                  <div class="col-12 form-group">
                    <p>What is your gender?</p>
                  </div>
                  <div class="col-12 form-group d-flex align-items-center gender-field">
                    <label class="radio-container">Male
                      <input type="radio" value="Male"checked="checked" name="item[1]">
                      <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">Female
                      <input type="radio" value="Female"checked="checked" name="item[1]">
                      <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">Non Binary
                      <input type="radio" value="Non-Binary"checked="checked" name="item[1]">
                      <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-12 form-group">
                    <p>How old are you?</p>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                    <input class="form-control" name="item[2]">
                  </div>
                  <div class="col-12 form-group">
                    <p>Do you consider yourself to be spiritual or religious? </p>
                  </div>
                  <div class="col-12 form-group d-flex align-items-center gender-field">
                    <label class="radio-container">No
                      <input type="radio" value="No"checked="checked" name="item[3]">
                      <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">Yes
                      <input type="radio" value="Yes"checked="checked" name="item[3]">
                      <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-12 form-group">
                    <p>Have you ever been in counseling or therapy before?</p>
                  </div>
                  <div class="col-12 form-group d-flex align-items-center gender-field">
                    <label class="radio-container">No
                      <input type="radio" value="No"checked="checked" name="item[4]">
                      <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">Yes
                      <input type="radio"  value="Yes"checked="checked" name="item[4]">
                      <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-12 form-group">
                    <p>How would you rate your current physical health?</p>
                  </div>
                  <div class="col-12 form-group d-flex align-items-center gender-field">
                    <label class="radio-container">Good
                      <input type="radio" value="Good"checked="checked" name="item[5]">
                      <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">Fair
                      <input type="radio"  value="Fair"checked="checked" name="item[5]">
                      <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">Poor
                      <input type="radio"  value="Poor"checked="checked" name="item[5]">
                      <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-12 form-group">
                    <p>How would you rate your current eating habits?</p>
                  </div>
                  <div class="col-12 form-group d-flex align-items-center gender-field">
                    <label class="radio-container">Good
                      <input type="radio" value="Good"checked="checked" name="item[6]">
                      <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">Fair
                      <input type="radio" value="Fair"checked="checked" name="item[6]">
                      <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">Poor
                      <input type="radio" value="Poor"checked="checked" name="item[6]">
                      <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-12 form-group">
                    <p>How would you rate your current financial status?</p>
                  </div>
                  <div class="col-12 form-group d-flex align-items-center gender-field">
                    <label class="radio-container">Good
                      <input type="radio" value="Good" checked="checked" name="item[7]">
                      <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">Fair
                      <input type="radio" value="Fair"checked="checked" name="item[7]">
                      <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">Poor
                      <input type="radio" value="Poor"checked="checked" name="item[7]">
                      <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-12 form-group">
                    <p>Are you currently experiencing anxiety, panic attacks or have any phobias? </p>
                  </div>
                  <div class="col-12 form-group d-flex align-items-center gender-field">
                    <label class="radio-container">No
                      <input type="radio" value="No" checked="checked" name="item[8]">
                      <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">Yes
                      <input type="radio" value="Yes"checked="checked" name="item[8]">
                      <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-12 form-group">
                    <p>Are you currently experiencing overwhelming sadness, grief, or depression? </p>
                  </div>
                  <div class="col-12 form-group d-flex align-items-center gender-field">
                    <label class="radio-container">No
                      <input type="radio" value="No"checked="checked" name="item[9]">
                      <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">Yes
                      <input type="radio" value="Yes"checked="checked" name="item[9]">
                      <span class="checkmark"></span>
                    </label>
                  </div>
                </div>
              </fieldset>
              <!-- Step 2 -->
              <h6></h6>
              <fieldset>
                <div class="row">
                  <div class="col-12">
                    <label class="title-label">Over the past 2 weeks, how often have you been bothered by any of the following problems:</label>
                    <input class="form-control" name="item[10]">
                  </div>
                  <div class="col-12 form-group">
                    <p>Little interest or pleasure in doing things. </p>
                  </div>
                  <div class="col-12 form-group d-flex align-items-center gender-field">
                    <label class="radio-container">Not at all
                        <input type="radio" value="Not at all" checked="checked" name="item[11]">
                        <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">Several Days
                        <input type="radio" value="Several Days" checked="checked" name="item[11]">
                        <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">More than Half the days
                        <input type="radio" value="More than Half the days" checked="checked" name="item[11]">
                        <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">Nearly Every day
                        <input type="radio" value="Nearly Every day" checked="checked" name="item[11]">
                        <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-12 form-group">
                    <p>Moving or speaking so slowly that other people could have noticed? Or the opposite - being
                    so fidgety or restless that you have been moving around a lot more than usual. </p>
                  </div>
                  <div class="col-12 form-group d-flex align-items-center gender-field">
                    <label class="radio-container">Not at all
                        <input type="radio" value="Not at all" checked="checked" name="item[12]">
                        <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">Several Days
                        <input type="radio" value="Several Days" checked="checked" name="item[12]">
                        <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">More than Half the days
                        <input type="radio" value="More than Half the days" checked="checked" name="item[12]">
                        <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">Nearly Every day
                        <input type="radio" value="Nearly Every day" checked="checked" name="item[12]">
                        <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-12 form-group">
                    <p>Little interest or pleasure in doing things.</p>
                  </div>
                  <div class="col-12 form-group d-flex align-items-center gender-field">
                    <label class="radio-container">Not at all
                        <input type="radio" value="Not at all" checked="checked" name="item[13]">
                        <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">Several Days
                        <input type="radio" value="Several Days" checked="checked" name="item[13]">
                        <span class="checkmark"></span>
                    </label>
                   <label class="radio-container">More than Half the days
                        <input type="radio" value="More than Half the days" checked="checked" name="item[13]">
                        <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">Nearly Every day
                        <input type="radio" value="Nearly Every day" checked="checked" name="item[13]">
                        <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-12 form-group">
                    <p>Feeling down, depressed or hopeless.</p>
                  </div>
                  <div class="col-12 form-group d-flex align-items-center gender-field">
                    <label class="radio-container">Not at all
                        <input type="radio" value="Not at all" checked="checked" name="item[14]">
                        <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">Several Days
                        <input type="radio" value="Several Days" checked="checked" name="item[14]">
                        <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">More than Half the days
                        <input type="radio" value="More than Half the days" checked="checked"  name="item[14]">
                        <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">Nearly Every day
                        <input type="radio" value="Nearly Every day" checked="checked" name="item[14]">
                        <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-12 form-group">
                    <p>Trouble falling asleep, staying asleep, or sleeping too much.</p>
                  </div>
                  <div class="col-12 form-group d-flex align-items-center gender-field">
                    <label class="radio-container">Not at all
                        <input type="radio" value="Not at all" checked="checked" name="item[15]">
                        <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">Several Days
                        <input type="radio" value="Several Days" checked="checked" name="item[15]">
                        <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">More than Half the days
                        <input type="radio" value="More than Half the days" checked="checked" name="item[15]">
                        <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">Nearly Every day
                        <input type="radio" value="Nearly Every day" checked="checked" name="item[15]">
                        <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-12 form-group">
                    <p>Feeling tired or having little energy.</p>
                  </div>
                  <div class="col-12 form-group d-flex align-items-center gender-field">
                    <label class="radio-container">Not at all
                        <input type="radio" value="Not at all" checked="checked" name="item[16]">
                        <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">Several Days
                        <input type="radio" value="Several Days" checked="checked" name="item[16]">
                        <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">More than Half the days
                        <input type="radio" value="More than Half the days" checked="checked" name="item[16]">
                        <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">Nearly Every day
                        <input type="radio" value="Nearly Every day" checked="checked" name="item[16]">
                        <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-12 form-group">
                    <p>Poor appetite or overeating. </p>
                  </div>
                  <div class="col-12 form-group d-flex align-items-center gender-field">
                    <label class="radio-container">Not at all
                        <input type="radio" value="Not at all" checked="checked" name="item[17]">
                        <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">Several Days
                        <input type="radio" value="Several Days" checked="checked" name="item[17]">
                        <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">More than Half the days
                        <input type="radio" value="More than Half the days" checked="checked" name="item[17]">
                        <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">Nearly Every day
                        <input type="radio" value="Nearly Every day" checked="checked" name="item[17]">
                        <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-12 form-group">
                    <p>Trouble concentrating on things, such as reading the newspaper or watching television. </p>
                  </div>
                  <div class="col-12 form-group d-flex align-items-center gender-field">
                    <label class="radio-container">Not at all
                        <input type="radio" value="Not at all" checked="checked" name="item[18]">
                        <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">Several Days
                        <input type="radio" value="Several Days" checked="checked" name="item[18]">
                        <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">More than Half the days
                        <input type="radio" value="More than Half the days" checked="checked" name="item[18]">
                        <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">Nearly Every day
                        <input type="radio" value="Nearly Every day" checked="checked" name="item[18]">
                        <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-12 form-group">
                    <p>Feeling bad about yourself - or that you are a failure or have let yourself or your family down. </p>
                  </div>
                  <div class="col-12 form-group d-flex align-items-center gender-field">
                    <label class="radio-container">Not at all
                        <input type="radio" value="Not at all" checked="checked" name="item[19]">
                        <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">Several Days
                        <input type="radio" value="Several Days" checked="checked" name="item[19]">
                        <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">More than Half the days
                        <input type="radio" value="More than Half the days" checked="checked" name="item[19]">
                        <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">Nearly Every day
                        <input type="radio" value="Nearly Every day" checked="checked" name="item[19]">
                        <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-12 form-group">
                    <p>Thoughts that you would be better off dead or of hurting yourself in some way. </p>
                  </div>
                  <div class="col-12 form-group d-flex align-items-center gender-field">
                    <label class="radio-container">Not at all
                        <input type="radio" value="Not at all" checked="checked" name="item[20]">
                        <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">Several Days
                        <input type="radio" value="Several Days" checked="checked" name="item[20]">
                        <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">More than Half the days
                        <input type="radio" value="More than Half the days" checked="checked" name="item[20]">
                        <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">Nearly Every day
                        <input type="radio" value="Nearly Every day" checked="checked" name="item[20]">
                        <span class="checkmark"></span>
                    </label>
                  </div>
                </div>
              </fieldset>
              <!-- Step 3 -->
              <h6></h6>
              <fieldset>
                <div class="row">
                    <div class="col-12 form-group">
                        <p>22) Over The Past 2 Weeks, How Often Have You Been Bothered By Any Of The Following Problems</p>
                        <input class="form-control" name="item[22]">
                    </div>
                  <div class="col-12 form-group">
                    <p>How difficult have these problems made it for you to do your work, take care of things
                    at home, or get along with other people?</p>
                  </div>
                  <div class="col-12 form-group d-flex align-items-center gender-field">
                    <label class="radio-container">Not Difficult all
                        <input type="radio" value="Not Difficult all" checked="checked" name="item[23]">
                        <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">Somewhat Difficult
                        <input type="radio" value="Somewhat Difficult" checked="checked" name="item[23]">
                        <span class="checkmark"></span>
                    </label>
                   <label class="radio-container">Very DIfficult
                        <input type="radio" value="Very DIfficult" checked="checked" name="item[23]">
                        <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">Extremly Difficult
                        <input type="radio" value="Extremly Difficult" checked="checked" name="item[23]">
                        <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-12 form-group">
                    <p>Are you currently experiencing any chronic pain? </p>
                  </div>
                  <div class="col-12 form-group d-flex align-items-center gender-field">
                    <label class="radio-container">No
                        <input type="radio" value="No" checked="checked" name="item[24]">
                        <span class="checkmark"></span>
                    </label>
                   <label class="radio-container">Yes
                        <input type="radio" value="Yes" checked="checked" name="item[24]">
                        <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-12 form-group">
                    <p>Are you currently employed? </p>
                  </div>
                  <div class="col-12 form-group d-flex align-items-center gender-field">
                    <label class="radio-container">No
                        <input type="radio" value="No" checked="checked" name="item[25]">
                        <span class="checkmark"></span>
                    </label>
                                                                        <label class="radio-container">Yes
                        <input type="radio" value="Yes" checked="checked" name="item[25]">
                        <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-12 form-group">
                    <p>Do you have any problems or worries about intimacy? </p>
                  </div>
                  <div class="col-12 form-group d-flex align-items-center gender-field">
                    <label class="radio-container">No
                        <input type="radio" value="No" checked="checked" name="item[26]">
                        <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">Yes
                        <input type="radio" value="Yes" checked="checked" name="item[26]">
                        <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-12 form-group">
                    <p>Are you currently taking any medication? </p>
                  </div>
                  <div class="col-12 form-group d-flex align-items-center gender-field">
                    <label class="radio-container">No
                        <input type="radio" value="No" checked="checked" name="item[27]">
                        <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">Yes
                        <input type="radio" value="Yes" checked="checked" name="item[27]">
                        <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-12 form-group">
                    <p>When is the last time you had a plan for suicide? </p>
                  </div>
                  <div class="col-12 form-group d-flex align-items-center gender-field">
                    <label class="radio-container">Never
                        <input type="radio" value="Never" checked="checked" name="item[28]">
                        <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">Over a week ago
                        <input type="radio" value="Over a week ago" checked="checked" name="item[28]">
                        <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">this week
                        <input type="radio" value="this week" checked="checked" name="item[28]">
                        <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">Over 3 month ago
                        <input type="radio" value="Over 3 month ago" checked="checked" name="item[28]">
                        <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">Over a year ago
                        <input type="radio" value="Over a year ago" checked="checked" name="item[28]">
                        <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-12 form-group">
                    <p>How often do you drink alcohol? </p>
                  </div>
                  <div class="col-12 form-group d-flex align-items-center gender-field">
                    <label class="radio-container">Never
                        <input type="radio" value="Never" checked="checked" name="item[29]">
                        <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">Infrequently
                        <input type="radio" value="Infrequently" checked="checked" name="item[29]">
                        <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">Monthly
                        <input type="radio" value="Monthly" checked="checked" name="item[29]">
                        <span class="checkmark"></span>
                    </label>
                   <label class="radio-container">Weekly
                        <input type="radio" value="Weekly" checked="checked" name="item[29]">
                        <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">Daily
                        <input type="radio" value="Daily" checked="checked" name="item[29]">
                        <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-12 form-group">
                    <p>Please enter your email</p>
                  </div>
                  <div class="col-12 form-group email-field">
                    <div class="row">
                      <div class="col-lg-6 col-md-6 col-sm-12">
                      <i class="fa fa-envelope" aria-hidden="true"></i><input type="text" class="form-control" value="{{ auth()->user()->email ?? "" }}" readonly  name="email" spellcheck="true" placeholder="Enter Email Address">

                      </div>
                    </div>
                  </div>
                </div>
              </fieldset>


            </form>
            <div class="col-12 form-group register-main">
            <a href="{{ url('/')}}" data-dismiss="modal" class="login back"><i class="fa fa-long-arrow-left"></i>back to Home</a>
            </div>
          </div>
        </div>
        @endif
      </div>
    </div>
    <div class="login-fail-main user admin-profile profile-view loginn">
        <div class="featured inner ">
            <div class="modal fade" id="successModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="forget-pass register-form thanku-popup">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                            <div class="modal-body">
                                <div class="payment-main-popup payment-view-popup">
                                    <div class="payment-modal-main">
                                        <div class="payment-modal-inner">
                                            <div class="row">
                                                <div class="col-12 form-group">
                                                    <h1 class="text-center">Thank you for taking the time to share openly and honestly</h1>

                                                </div>
                                                <div class="col-12 form-group">
                                                    <label>We will get back to you with your results on your email.</label>
                                                </div>
                                                <div class="col-12 form-group text-center">
                                                    <a href="{{ url('/') }}" class="can">Go Back To Home</a>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </section>

  @include('includes.user.footer')
  <script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
  <script src="http://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js"></script>
  <!--block modal end here-->
  <!-- ////////////////////////////////////////////////////////////////////////////-->
  <!-- BEGIN VENDOR JS-->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.6.2/modernizr.min.js"></script>
  <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <script src="{{ asset('js/jquery.steps.min.js')}}" type="text/javascript"></script>
  <script src="{{ asset('js/wizard-steps.js')}}" type="text/javascript"></script>
  <!-- END STACK JS-->
  <!-- BEGIN PAGE LEVEL JS-->
  <!-- END PAGE LEVEL JS-->
  </body>
  </html>