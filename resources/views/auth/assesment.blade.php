
<div class="payment-main-popup payment-view-popup">
    <div class="payment-modal-main">
        <div class="payment-modal-inner">
            <div class="row">
                <div class="col-12">
                    <h1>Assesment Test</h1>
                </div>
                <div class="col-12">
                    <form action="{{ route('submit-assesment-form') }}" method="post" id="assesmentForm">
                        @csrf
                        @php $i = 0; $j=1; @endphp
                        @foreach($data->chunk(8) as $index => $chunk)
                            <div class="assestment_section fields" id="assestment_section{{$index}}">
                                <div class="row">
                                @foreach($chunk as $key => $item)
                                        <div class="col-12 form-group">
                                            @if($item['type'] == 'radio')
                                                <div class="col-12 form-group">
                                                    <p>{{ $j }}) {{ $item['question'] }}</p>
                                                </div>
                                                <div class="col-12 form-group d-flex align-items-center gender-field">
                                                    @foreach($item['values'] as $value)
                                                        <label class="radio-container">{{$value['key']}}
                                                            <input type="radio" value="{{$value['key']}}" checked="checked" name="item[{{$j}}]">
                                                            <span class="checkmark"></span>
                                                        </label>
                                                    @endforeach
                                                      </div>
                                            @else
                                                <div class="col-12 form-group">
                                                    <p>{{ $j }}) {{ $item['question'] }}</p>
                                                    <input class="form-control" name="item[{{$j}}]"></input>
                                                </div>
                                            @endif
                                        </div>
                                        @php $j++ @endphp
                                    @endforeach
                                    @if($loop->last)
                                        <div class="col-12 form-group">
                                            <p>Please enter your email</p>
                                        </div>
                                        <div class="col-12 form-group">
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-12">
                                                    <i class="fa fa-envelope" aria-hidden="true"></i>
                                                    <input type="text" name="email" class="form-control" spellcheck="true" placeholder="Enter Email Address">
                                                </div>
                                            </div>

                                        </div>
                                    @endif

                                    @if($i != 0)
                                    <div class="col-lg-6 col-md-6 col-sm-12 form-group text-center">
                                        <button type="button" onclick="back({{ $i }})" class="can" id="contback1">Back</button>
                                    </div>
                                    @endif
                                    <div class="col-lg-6 col-md-6 col-sm-12 form-group text-center">
                                        @if($loop->last)
                                        <button type="button"
                                                onclick="submitAssesmentForm()"
                                                class="can con" id="cont4">Submit</button>
                                        @else
                                            <button type="button"
                                                    onclick="next(this, {{ $i }})"
                                                    class="can con" id="cont4">Continue</button>

                                        @endif
                                    </div>
                                </div>
                            </ul>
                            @php $i++; $j++ @endphp
                            </div>
                        @endforeach
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
