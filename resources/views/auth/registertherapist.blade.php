@extends('layouts.master')
@section('title', ' Tberapist Register')
@section('body-class',"vertical-layout vertical-menu 2-columns menu-expanded")
@section('body-col',"2-column")
@section('content')
    <section class="app-content u-c-p edit-profile t-registration">
        <div class="container">
            <div class="card pro-main">
                <div class="row">
                    <div class="col-md-12 col-sm-12 text-center">
                        <h1>Therapist Registration</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">

                        <form action="#" class="number-tab-steps wizard-circle">
                            <!-- Step 1 -->
                            <h6>Account</h6>
                            <fieldset>
                                <div class="row">
                                    <div class="col-lg-6 col-sm-12">
                                        <div class="pro-inner-row">
                                            <label><i class="fa fa-user-circle"></i> First Name</label>
                                            <input type="text" required="required" spellcheck="true" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-sm-12">
                                        <div class="pro-inner-row">
                                            <label><i class="fa fa-user-circle"></i> Last Name</label>
                                            <input type="text" required="required" spellcheck="true" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 col-sm-12">
                                        <div class="pro-inner-row">
                                            <label><i class="fa fa-envelope"></i> Work Email</label>
                                            <input type="email" required="required" spellcheck="true" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-sm-12">
                                        <div class="pro-inner-row">
                                            <label><i class="fa fa-unlock-alt" aria-hidden="true"></i> Password</label>
                                            <input type="text" required="required" spellcheck="true" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-sm-12">
                                        <div class="pro-inner-row">
                                            <label><i class="fa fa-unlock-alt" aria-hidden="true"></i> Confirm Password</label>
                                            <input type="text" required="required" spellcheck="true" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <!-- Step 2 -->
                            <h6>Office Location</h6>
                            <fieldset>
                                <div class="form-group mb-2 contact-repeater">
                                    <div data-repeater-list="repeater-group">
                                        <div class="mb-1" data-repeater-item>
                                            <div class="row">
                                                <div class="col-lg-6 col-sm-12">
                                                    <div class="pro-inner-row">
                                                        <label><i class="fa fa-id-card" aria-hidden="true"></i> Name of Practice (optional)</label>
                                                        <input type="text" required="required" spellcheck="true" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-sm-12">
                                                    <div class="pro-inner-row">
                                                        <label><i class="fa fa-globe" aria-hidden="true"></i> Practice Website</label>
                                                        <input type="text" required="required" spellcheck="true" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6 col-sm-12">
                                                    <div class="pro-inner-row">
                                                        <label><i class="fa fa-phone"></i> Work Phone #</label>
                                                        <input type="number" required="required" spellcheck="true" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-sm-12">
                                                    <div class="pro-inner-row">
                                                        <label><i class="fa fa-map-marker" aria-hidden="true"></i> Address (line 1)</label>
                                                        <input type="text" required="required" spellcheck="true" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6 col-sm-12">
                                                    <div class="pro-inner-row">
                                                        <label><i class="fa fa-map-marker" aria-hidden="true"></i> Address (line 2)</label>
                                                        <input type="text" required="required" spellcheck="true" class="form-control">
                                                    </div>
                                                </div>
                                                <!--pro inner row end-->
                                                <div class="col-lg-6 col-sm-12">
                                                    <div class="pro-inner-row">
                                                        <label><i class="fa fa-building" aria-hidden="true"></i> City</label>
                                                        <input type="text" required="required" spellcheck="true" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6 col-sm-12">
                                                    <div class="pro-inner-row">
                                                        <label><i class="fa fa-map" aria-hidden="true"></i> State of Practice</label>
                                                        <input type="text" required="required" spellcheck="true" class="form-control">
                                                    </div>
                                                </div>
                                                <!--pro inner row end-->
                                                <div class="col-lg-6 col-sm-12">
                                                    <div class="pro-inner-row">
                                                        <label><i class="fa fa-map-marker"></i> Zip Code</label>
                                                        <input type="text" required="required" spellcheck="true" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6 col-sm-12">
                                                    <div class="pro-inner-row">
                                                        <label><i class="fa fa-globe"></i> Country</label>
                                                        <input type="text" required="required" spellcheck="true" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="button" data-repeater-create class="add-new-btn">
                                        <i class="fa fa-plus-circle" aria-hidden="true"></i> Add Second Office
                                    </button>
                                </div>
                            </fieldset>
                            <!-- Step 3 -->
                            <h6>Public Profile</h6>
                            <fieldset>
                                <div class="row">
                                    <div class="col-lg-6 col-sm-12">
                                        <div class="pro-inner-row">
                                            <label><i class="fa fa-user-circle" aria-hidden="true"></i> Title (optional)</label>
                                            <input type="text" required="required" spellcheck="true" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-sm-12">
                                        <div class="pro-inner-row">
                                            <label><i class="fa fa-user-md" aria-hidden="true"></i> Therapist Type</label>
                                            <select>
                                                <option>Therapist Type</option>
                                                <option>Therapist Type</option>
                                                <option>Therapist Type</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-sm-12">
                                        <div class="pro-inner-row">
                                            <label><i class="fa fa-id-card" aria-hidden="true"></i> Type of License</label>
                                            <input type="text" required="required" spellcheck="true" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-sm-12">
                                        <div class="pro-inner-row">
                                            <label><i class="fa fa-id-card" aria-hidden="true"></i> Years In Practice</label>
                                            <input type="text" required="required" spellcheck="true" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-sm-12">
                                        <div class="pro-inner-row">
                                            <label><i class="fa fa-id-card" aria-hidden="true"></i> Licensed</label>
                                            <input type="text" required="required" spellcheck="true" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-sm-12">
                                        <div class="pro-inner-row">
                                            <label><i class="fa fa-university" aria-hidden="true"></i> Undergraduate Institution Name</label>
                                            <input type="text" required="required" spellcheck="true" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-sm-12">
                                        <div class="pro-inner-row">
                                            <label><i class="fa fa-university" aria-hidden="true"></i> Postgraduate Institiution Name</label>
                                            <input type="text" required="required" spellcheck="true" class="form-control">
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-sm-12">
                                        <label class="blue-label">Skills & Expertise</label>
                                        <select id="skillsandexpertise" class="select2 form-control" multiple="multiple">
                                            <optgroup >
                                                <option>Depression</option>
                                                <option>Anxiety</option>
                                                <option>Depression</option>
                                                <option>Anxiety</option>
                                                <option>Depression</option>
                                                <option>Anxiety</option>
                                                <option>Depression</option>
                                                <option>Anxiety</option>
                                            </optgroup>
                                        </select>
                                    </div>
                                    <div class="col-lg-6 col-sm-12">
                                        <label class="blue-label">Treatment orientation</label>
                                        <select id="skillsandexpertise1" class="select2 form-control" multiple="multiple">
                                            <optgroup >
                                                <option>Depression</option>
                                                <option>Anxiety</option>
                                                <option>Depression</option>
                                                <option>Anxiety</option>
                                                <option>Depression</option>
                                                <option>Anxiety</option>
                                                <option>Depression</option>
                                                <option>Anxiety</option>
                                            </optgroup>
                                        </select>
                                    </div>
                                    <div class="col-lg-6 col-sm-12">
                                        <label class="blue-label">Modality</label>
                                        <select id="skillsandexpertise2" class="select2 form-control" multiple="multiple">
                                            <optgroup >
                                                <option>Children</option>
                                                <option>Couple</option>
                                            </optgroup>
                                        </select>
                                    </div>
                                    <div class="col-lg-6 col-sm-12">
                                        <label class="blue-label">Focus for clients age group</label>
                                        <select id="skillsandexpertise3" class="select2 form-control" multiple="multiple">
                                            <optgroup >
                                                <option>Adults</option>
                                                <option>Children (6-9 years old)</option>
                                            </optgroup>
                                        </select>
                                    </div>
                                    <div class="col-lg-6 col-sm-12">
                                        <label class="blue-label">Focus for clients ethnicity</label>
                                        <select id="skillsandexpertise4" class="select2 form-control" multiple="multiple">
                                            <optgroup >
                                                <option>Asian</option>
                                            </optgroup>
                                        </select>
                                    </div>
                                    <div class="col-lg-6 col-sm-12">
                                        <label class="blue-label">Focus for clients spoken language</label>
                                        <select id="skillsandexpertise5" class="select2 form-control" multiple="multiple">
                                            <optgroup >
                                                <option>English</option>
                                            </optgroup>
                                        </select>
                                    </div>
                                </div>
                            </fieldset>
                            <!-- Step 4 -->
                            <h6>Personal Details</h6>
                            <fieldset>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="pro-inner-row">
                                            <label><i class="fa fa-file-text" aria-hidden="true"></i> Personal Statement</label>
                                            <textarea class="form-control"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="pro-img-main">
                                            <img src="images/student-pro-girl.png" alt="">
                                            <button name="file" class="change-cover" onclick="document.getElementById('upload').click()"><i class="fa fa-camera"></i></button>
                                            <input type="file" id="upload" name="file">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="pro-inner-row">
                                            <label><i class="fa fa-clock-o" aria-hidden="true"></i> Your Availability Timing for Your Clients</label>
                                            <textarea class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <h6>Professional Reference</h6>
                            <fieldset>
                                <div class="row mt-3 mb-5">
                                    <div class="col-lg-6 col-sm-12">
                                        <div class="pro-inner-row">
                                            <label><i class="fa fa-user-circle"></i> Full Name of Reference</label>
                                            <input type="text" required="required" spellcheck="true" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-sm-12">
                                        <div class="pro-inner-row">
                                            <label><i class="fa fa-user-circle"></i> Professional Title of Reference</label>
                                            <input type="text" required="required" spellcheck="true" class="form-control">
                                        </div>
                                    </div>
                                    <!--pro inner row end-->
                                    <!--pro inner row end-->
                                    <div class="col-lg-12 col-sm-12">
                                        <div class="pro-inner-row">
                                            <label><i class="fa fa-envelope"></i> Email of Reference</label>
                                            <input type="email" required="required" spellcheck="true" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <h6>Terms & Agreement</h6>
                            <fieldset>
                                <div class="row">
                                    <div class="col-12">
                                        {{-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labo re et dolore magna aliqua  Enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.  Excepteur sint occaecat cupidatat non proident. Lorem ipsum dolor sit amet, cons ectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore. et dolore magna aliqua. Enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.  Excepteur sint occaecat cupidatat non proident.sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p> --}}
                                    </div>
                                    <div class="col-12">
                                        <label class="check">I accept the terms stated above
                                            <input spellcheck="true" type="checkbox">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>

                                </div>
                            </fieldset>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="login-fail-main user change-profile t-registration cat-popup">
        <div class="featured inner">
            <div class="modal fade bd-example-modal-lg" id="datamodal1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                        <div class="payment-modal-main">
                            <div class="payment-modal-inner">
                                <h2>Specialities</h2>

                                <div class="row">
                                    <div class="col-12">
                                        <div class="search-area">
                                            <input type="search" name="search" placeholder="Search Specialities">
                                            <button type="submit">Search</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <ul class="cat-sorting">
                                            <li class="active"><a href="#">(0-9)</a></li>
                                            <li><a href="#">A</a></li>
                                            <li><a href="#">B</a></li>
                                            <li><a href="#">C</a></li>
                                            <li><a href="#">D</a></li>
                                            <li><a href="#">E</a></li>
                                            <li><a href="#">F</a></li>
                                            <li><a href="#">G</a></li>
                                            <li><a href="#">H</a></li>
                                            <li><a href="#">I</a></li>
                                            <li><a href="#">J</a></li>
                                            <li><a href="#">K</a></li>
                                            <li><a href="#">L</a></li>
                                            <li><a href="#">M</a></li>
                                            <li><a href="#">N</a></li>
                                            <li><a href="#">O</a></li>
                                            <li><a href="#">P</a></li>
                                            <li><a href="#">Q</a></li>
                                            <li><a href="#">R</a></li>
                                            <li><a href="#">S</a></li>
                                            <li><a href="#">T</a></li>
                                            <li><a href="#">U</a></li>
                                            <li><a href="#">V</a></li>
                                            <li><a href="#">W</a></li>
                                            <li><a href="#">X</a></li>
                                            <li><a href="#">Y</a></li>
                                            <li><a href="#">Z</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <ul class="cat-list">
                                            <li>
                                                <label class="check">Addiciton
                                                    <input spellcheck="true" type="checkbox">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="check">Adjusting to Change / Life Transitions
                                                    <input spellcheck="true" type="checkbox">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="check">Adjustment to College
                                                    <input spellcheck="true" type="checkbox">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="check">Adoption / Foster Care Issues
                                                    <input spellcheck="true" type="checkbox">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="check">Adult
                                                    <input spellcheck="true" type="checkbox">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="check">Aggression and Violence
                                                    <input spellcheck="true" type="checkbox">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </li>


                                        </ul>

                                    </div>
                                    <div class="col-sm-4">

                                        <ul class="cat-list">
                                            <li>
                                                <label class="check">Aging and Geriatric Issues
                                                    <input spellcheck="true" type="checkbox">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="check">Agoraphobia
                                                    <input spellcheck="true" type="checkbox">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="check">Alcohol Abuse
                                                    <input spellcheck="true" type="checkbox">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="check">Alzheimer's
                                                    <input spellcheck="true" type="checkbox">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="check">Anger Management
                                                    <input spellcheck="true" type="checkbox">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="check">Antisocial Personality
                                                    <input spellcheck="true" type="checkbox">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </li>


                                        </ul>
                                    </div>
                                    <div class="col-sm-4">
                                        <ul class="cat-list">
                                            <li>
                                                <label class="check">Anxiety
                                                    <input spellcheck="true" type="checkbox">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="check">Art
                                                    <input spellcheck="true" type="checkbox">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="check">Asperger's Syndrome
                                                    <input spellcheck="true" type="checkbox">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="check">Athletes
                                                    <input spellcheck="true" type="checkbox">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="check">Attachment Issues
                                                    <input spellcheck="true" type="checkbox">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="check">Attention Deficit (ADHD)
                                                    <input spellcheck="true" type="checkbox">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </li>


                                        </ul>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 text-center">

                                        <button type="submit" class="big-center-cta" data-dismiss="modal">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
