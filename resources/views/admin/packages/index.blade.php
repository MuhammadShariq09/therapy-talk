@extends('layouts.master')
@section('title',' Package Management')
@section('body-class',"vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar")
@section('body-col',"2-columns")
@section('content')


<div class="app-content content">

    <div class="content-wrapper">
  
      <div class="content-body"> 
  
        <!-- Basic form layout section start -->
  
        <section id="configuration" class="search view-cause">
  
          <div class="row">
  
            <div class="col-12">
              @if(Session::has('message'))
                  <div class="alert alert-success">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      <strong>{{ Session::get('message')  }}</strong>
                  </div>
              @endif
              @if(Session::has('error'))
                  <div class="alert alert-danger">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      <strong>{{ Session::get('error')  }}</strong>
                  </div>
              @endif
  
              <div class="card rounded pad-20">
  
                <div class="card-content collapse show">
  
                  <div class="card-body table-responsive card-dashboard">
                  <form method="GET">
                    <div class="row">
                        <div class="col-12">
                            <h1 class="pull-left">PACKAGE MANAGEMENT</h1>
                        </div>
                    </div>
                        <div class="row date-input">

                          <div class="col-lg-12 form-group d-flex align-items-center">
                            <label class="w-100" for="">To Date:</label>
                            <input  type="date" id="select-date" name="from" class="form-control">
                          </div>
                          <div class="col-lg-12 form-group d-flex align-items-center">
                            <label class="w-100" for="">From Date:</label>
                            <input  type="date" id="select-date2" name="to" class="form-control">
                          </div>
                          <div class="col-lg-12 d-flex align-items-center form-group">
                              <div class="add-user">
                                <button type="submit" class="btn btn-primary" name="search"> Search</button>
                              </div>
                          </div>
                        </div>
                       
                    
                        
                    </div>
                  </form>
                    <div class="clearfix"></div>
                    <div class="maain-tabble mt-3">
                      <table class="table table-striped table-bordered zero-configuration">
                        <thead>
                          <tr>
                            <th>S.No #</th>
                            <th>Package Name</th>
                            <th>Amount</th>
                            <th>Actions</th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach($packages as $key => $package)
                                <tr>
                                    <td>{{ ++$key }}</td>
                                    <td>{{ $package->title }}</td>
                                    <td>${{ $package->amount }}</td>
                                    
                                    <td>
                                        <div class="btn-group mr-1 mb-1">
                                            <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i></button>
                                            <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(4px, 23px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                <a class="dropdown-item"
                                                    href="{{ route('package-management.view', ['id' => $package->id]) }}"><i class="fa fa-eye"></i>View </a>
                                                <a class="dropdown-item"
                                                    href="{{ route('package-management.edit', ['id' => $package->id]) }}"><i class="fa fa-pencil"></i>Edit </a>
                                            </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
  
                      </table>
  
                    </div>
  
                  </div>
  
                </div>
              </div>
            </div>
  
          </div>
  
        </section>
      </div>
  
    </div>
  
  </div>
  

@endsection

@section('js')
{{-- <script>
    $('#select-date').datepicker({
      uiLibrary: 'bootstrap4',
      format: 'mm/dd/yyyy'
    });
    $('#select-date2').datepicker({
                uiLibrary: 'bootstrap4',
                format: 'mm/dd/yyyy'
              });
  </script> --}}
@endsection