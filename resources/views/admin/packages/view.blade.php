@extends('layouts.master')
@section('title',' Package Management View')
@section('body-class',"vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar")
@section('body-col',"2-columns")
@section('content')


<div class="app-content content">

    <div class="content-wrapper">
  
      <div class="content-body"> 
        <section id="configuration" class="u-c-p u-profile a-edit">
            <div class="row">
              <div class="col-12">
                <div class="card pro-main">
                  <div class="row">
                    <div class="col-12">
                      <h1>View Package</h1>
                    </div>
                  </div>
                  <div class="row mt-3">
                    <div class="col-12">
                        <label class="blue-label">View Package</label>
                    </div>
                    <div class="col-lg-12 col-12 mt-2">
                      <div class="profile-frm">
                        <div class="row">

                          <div class="col-lg-6 col-sm-12">
                            <div class="pro-inner-row">
                              <label><i class="fa fa-th-large" aria-hidden="true"></i> Package Name</label>
                              <input type="text" required="required" spellcheck="true" class="form-control" value="{{$package->title}}" disabled placeholder="Package A">
                            </div>
                          </div>
                          <div class="col-lg-6 col-sm-12">
                            <div class="pro-inner-row">
                              <label><i class="fa fa-money"></i>Package Amount</label>
                              <input type="text" required="required" spellcheck="true" class="form-control" value="{{'$'.$package->amount}}" disabled placeholder="$100">
                            </div>
                          </div>
                          <div class="col-lg-6 col-sm-12">
                            <div class="pro-inner-row">
                              <label><i class="fa fa-video-camera" aria-hidden="true"></i> Video Calls</label>
                              <input type="number" required="required" spellcheck="true" class="form-control" value="{{$package->video_call}}" disabled placeholder="03">
                            </div>
                          </div>
                          <div class="col-lg-6 col-sm-12">
                            <div class="pro-inner-row">
                              <label><i class="fa fa-phone"></i> Voice Calls</label>
                              <input type="number" required="required" spellcheck="true" class="form-control" value="{{$package->voice_call}}" disabled placeholder="04">
                            </div>
                          </div>
                          <div class="col-lg-6 col-sm-12">
                            <div class="pro-inner-row">
                              <label><i class="fa fa-comment" aria-hidden="true"></i> Live Chats</label>
                              <input type="number" required="required" spellcheck="true" class="form-control" value="{{$package->live_chat}}" disabled placeholder="04">
                            </div>
                          </div>
                          <div class="col-lg-6 col-sm-12">
                            <div class="pro-inner-row">
                              <label><i class="fa fa-comment" aria-hidden="true"></i> Month</label>
                              <input type="number" required="required" spellcheck="true" class="form-control" value="{{$package->month}}" disabled placeholder="04">
                            </div>
                          </div>
                          <!-- <div class="col-lg-6 col-sm-12">
                            <div class="pro-inner-row">
                              <label><i class="fa fa-check-circle-o" aria-hidden="true"></i> Status</label>
                              <div class="toggle btn btn-primary" data-toggle="toggle" style="width: 60.5156px; height: 35px;"><input type="checkbox" checked="" data-toggle="toggle" data-on="Active" data-off="active"><div class="toggle-group"><label class="btn btn-primary toggle-on">Active</label><label class="btn btn-default active toggle-off">active</label><span class="toggle-handle btn btn-default"></span></div></div>
                            </div>
                          </div> -->

                          <div class="col-12 text-center w-100">
                            {{-- <button type="submit" class="save">Cancel</button>
                              <button type="submit" class="save">Update</button> --}}
                          </div>
                        </div>
                      </div>
                    </div>
                    <!--profile form end-->

                  </div>

                </div>
              </div>
            </div>
          </section>
      </div>
  
    </div>
  
  </div>
  

@endsection

@section('js')

@endsection