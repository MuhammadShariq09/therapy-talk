@extends('layouts.master')
@section('title',' Package Management Edit')
@section('body-class',"vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar")
@section('body-col',"2-columns")
@section('css')
<style>
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>
@endsection
@section('content')


<div class="app-content content">

    <div class="content-wrapper">
  
      <div class="content-body"> 
        <section id="configuration" class="u-c-p u-profile a-edit">
            <div class="row">
              <div class="col-12">
                <div class="card pro-main">
                  <div class="row">
                    <div class="col-12">
                      <h1>Edit Package</h1>
                    </div>
                  </div>
                <form method="POST" action="{{route('package-management.update')}}">
                    @csrf
                  <div class="row mt-3">
                    <div class="col-12">
                        <label class="blue-label">Edit Package</label>
                    </div>
                    <div class="col-lg-12 col-12 mt-2">
                      <div class="profile-frm">
                        <div class="row">
                        <input type="hidden" name="id" value="{{$package->id}}" />

                          <div class="col-lg-6 col-sm-12">
                            <div class="pro-inner-row">
                              <label><i class="fa fa-th-large" aria-hidden="true"></i> Package Name</label>
                            <input type="text" required="required" spellcheck="true" class="form-control" name="title" value="{{$package->title}}" placeholder="Package A">
                            </div>
                          </div>
                          <div class="col-lg-6 col-sm-12">
                            <div class="pro-inner-row">
                              <label><i class="fa fa-money"></i>Package Amount</label>
                              <input type="text" required="required" spellcheck="true" class="form-control" name="amount" value="{{$package->amount}}" placeholder="$100">
                            </div>
                          </div>
                          <div class="col-lg-6 col-sm-12">
                            <div class="pro-inner-row">
                              <label><i class="fa fa-video-camera" aria-hidden="true"></i> Video Calls</label>
                              <input type="number" required="required" spellcheck="true" class="form-control" name="video_call" value="{{$package->video_call}}" placeholder="03">
                            </div>
                          </div>
                          <div class="col-lg-6 col-sm-12">
                            <div class="pro-inner-row">
                              <label><i class="fa fa-phone"></i> Voice Calls</label>
                              <input type="number" required="required" spellcheck="true" class="form-control" name="voice_call" value="{{$package->voice_call}}" placeholder="04">
                            </div>
                          </div>
                          <div class="col-lg-6 col-sm-12">
                            <div class="pro-inner-row">
                              <label><i class="fa fa-comment" aria-hidden="true"></i> Live Chats</label>
                              <input type="number" required="required" spellcheck="true" class="form-control" name="live_chat" value="{{$package->live_chat}}" placeholder="04">
                            </div>
                          </div>
                          <div class="col-lg-6 col-sm-12">
                            <div class="pro-inner-row">
                              <label><i class="fa fa-comment" aria-hidden="true"></i> Month</label>
                              <input type="number" required="required" spellcheck="true" class="form-control" value="{{$package->month}}" name="month" placeholder="04">
                            </div>
                          </div>
                          <div class="col-lg-6 col-sm-12">
                            <div class="pro-inner-row">
                            <label><i class="fa fa-check-circle-o" aria-hidden="true"></i> Status</label>
                              <label class="switch">
                             
                                <input type="checkbox" name="status" value="1" {{$package->status==1 ? 'checked' : ""}}>
                                <span class="slider round"></span>
                              </label>
                            </div>
                            </div>
                          <!-- <div class="col-lg-6 col-sm-12">
                            <div class="pro-inner-row">
                              <label><i class="fa fa-check-circle-o" aria-hidden="true"></i> Status</label>
                              <div class="toggle btn btn-primary" data-toggle="toggle" style="width: 60.5156px; height: 35px;"><input type="checkbox" checked="" data-toggle="toggle" data-on="Active" data-off="active"><div class="toggle-group"><label class="btn btn-primary toggle-on">Active</label><label class="btn btn-default active toggle-off">active</label><span class="toggle-handle btn btn-default"></span></div></div>
                            </div>
                          </div> -->
                          

                          <div class="col-12 text-center w-100">
                            {{-- <button type="submit" class="save">Cancel</button> --}}
                              <button type="submit" class="save">Update</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!--profile form end-->

                  </div>
                  </form>
                </div>
              </div>
            </div>
          </section>
      </div>
  
    </div>
  
  </div>
  

@endsection

@section('js')

@endsection