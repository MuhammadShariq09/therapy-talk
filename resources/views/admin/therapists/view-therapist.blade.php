@extends('layouts.master')
@section('title',' View Therapist')
@section('body-class',"vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar")
@section('body-col',"2-columns")

@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="">
                <div class="content-body">
                    <section id="configuration" class="u-c-p u-profile a-edit">
                        <div class="row">
                            <div class="col-12">
                                <div class="card pro-main">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-8 col-sm-12">
                                            <h1 class="pb-2">Therapist Profile</h1>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12 col-12">
                                            <div class="profile-frm">
                                                <div class="row">

                                                    <div class="col-12">
                                                        <div class="pro-img-main">
                                                            <img src="{{ asset($therapist->image) }}" alt="">
                                                        </div>
                                                    </div>

                                                    <!--col end-->
                                                    <div class="col-lg-12 col-sm-12">
                                                        <h1>Account</h1>
                                                        <div class="pro-inner-row">
                                                            <label><i class="fa fa-therapist-circle"></i>Full Name</label>
                                                            <input type="text"  value="{{$therapist->full_name}}" required="required" spellcheck="true" class="form-control" disabled placeholder="Name">
                                                        </div>
                                                    </div>
                                                    {{-- <div class="col-lg-6 col-sm-12">
                                                        <div class="pro-inner-row">
                                                            <label><i class="fa fa-therapist-circle"></i> Last Name</label>
                                                            <input type="text" value="{{$therapist->last_name}}" required="required" spellcheck="true" class="form-control" disabled placeholder="Name">
                                                        </div>
                                                    </div> --}}
                                                    <!--pro inner row end-->
                                                    <!--pro inner row end-->
                                                    <div class="col-lg-6 col-sm-12">
                                                        <div class="pro-inner-row">
                                                            <label><i class="fa fa-envelope"></i> Email</label>
                                                            <input type="email" value="{{$therapist->email}}" required="required" spellcheck="true" class="form-control" disabled placeholder="Work Email">
                                                        </div>
                                                    </div>
                                                </div>
                                                @foreach($therapist->offices as $key => $office)
                                               
                                                <h1>Office # {{++$key}}</h1>
                                                <div class="row mt-3">
                                                    <div class="col-lg-6 col-sm-12">
                                                        <div class="pro-inner-row">
                                                            <label><i class="fa fa-id-card" aria-hidden="true"></i> Name of Practice (optional)</label>
                                                            <input type="text" value="{{ $office->practice_name }}" required="required" spellcheck="true" class="form-control" disabled placeholder="Name of Practice (optional)">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-sm-12">
                                                        <div class="pro-inner-row">
                                                            <label><i class="fa fa-globe" aria-hidden="true"></i> Practice Website</label>
                                                            <input type="text"  value="{{ $office->practice_website }}" required="required" spellcheck="true" class="form-control" disabled placeholder="Practice Website">
                                                        </div>
                                                    </div>
                                                    <!--pro inner row end-->
                                                    <div class="col-lg-6 col-sm-12">
                                                        <div class="pro-inner-row">
                                                            <label><i class="fa fa-phone"></i> Work Phone #</label>
                                                            <input type="number" value="{{ $office->phone }}" required="required" spellcheck="true" class="form-control" disabled placeholder="Phone Number">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-sm-12">
                                                        <div class="pro-inner-row">
                                                            <label><i class="fa fa-map-marker" aria-hidden="true"></i> Address (line 1)</label>
                                                            <input type="text" value="{{ $office->address }}" required="required" spellcheck="true" class="form-control" disabled placeholder="Address (line 1)">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-sm-12">
                                                        <div class="pro-inner-row">
                                                            <label><i class="fa fa-map-marker" aria-hidden="true"></i> Address (line 2)</label>
                                                            <input type="text" value="{{ $office->address_2 }}" required="required" spellcheck="true" class="form-control" disabled placeholder="Address (line 2)">
                                                        </div>
                                                    </div>
                                                    <!--pro inner row end-->
                                                    <div class="col-lg-6 col-sm-12">
                                                        <div class="pro-inner-row">
                                                            <label><i class="fa fa-map" aria-hidden="true"></i> State of Practice</label>
                                                            <input type="text" value="{{ $office->states->name ?? '' }}" required="required" spellcheck="true" class="form-control" disabled placeholder="State">
                                                        </div>
                                                    </div>
                                                  
                                                    <div class="col-lg-6 col-sm-12">
                                                        <div class="pro-inner-row">
                                                            <label><i class="fa fa-building" aria-hidden="true"></i> City</label>
                                                            <input type="text" value="{{ $office->cities->city ?? '' }}" required="required" spellcheck="true" class="form-control" disabled placeholder="City">
                                                        </div>
                                                    </div>
                                                   
                                                    <!--pro inner row end-->
                                                    <div class="col-lg-6 col-sm-12">
                                                        <div class="pro-inner-row">
                                                            <label><i class="fa fa-map-marker"></i> Zip Code</label>
                                                            <input type="text" value="{{$office->zipcode}}" required="required" spellcheck="true" class="form-control" disabled placeholder="Zip Code">
                                                        </div>
                                                    </div>
                                                    <!--pro inner row end-->
                                                    <div class="col-lg-6 col-sm-12">
                                                        <div class="pro-inner-row">
                                                            <label><i class="fa fa-globe"></i> Country</label>
                                                            <input type="text" value="{{ $office->country }}" required="required" spellcheck="true" class="form-control" disabled placeholder="Country">
                                                        </div>
                                                    </div>
                                                    <!--pro inner row end-->
                                                </div>
                                                @endforeach
                                                <h1>Public Profile</h1>
                                                <div class="row mt-3">
                                                    <div class="col-lg-6 col-sm-12">
                                                        <div class="pro-inner-row">
                                                            <label><i class="fa fa-therapist-circle" aria-hidden="true"></i> Title (optional)</label>
                                                            <input type="text" value="{{$therapist->therapist_title }}" required="required" spellcheck="true" class="form-control" disabled placeholder="Title (optional)">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-sm-12">
                                                        <div class="pro-inner-row">
                                                            <label><i class="fa fa-therapist-md" aria-hidden="true"></i> Therapist Type</label>
                                                            <input type="text" value="{{$therapist->therapist_type }}" required="required" spellcheck="true" class="form-control" disabled placeholder="Therapist Type">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-sm-12">
                                                        <div class="pro-inner-row">
                                                            <label><i class="fa fa-id-card" aria-hidden="true"></i> Type of License</label>
                                                            <input type="text" value="{{$therapist->license_type }}" required="required" spellcheck="true" class="form-control" disabled placeholder="Type of License">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-sm-12">
                                                        <div class="pro-inner-row">
                                                            <label><i class="fa fa-id-card" aria-hidden="true"></i> Years in Practice</label>
                                                            <input type="text" value="{{$therapist->years_in_practice }}" required="required" spellcheck="true" class="form-control" disabled placeholder="Type of License">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-sm-12">
                                                        <div class="pro-inner-row">
                                                            <label><i class="fa fa-id-card" aria-hidden="true"></i> License in (optional)</label>
                                                            <input type="text" value="{{$therapist->license_in }}" required="required" spellcheck="true" class="form-control" disabled placeholder="Licensed in (optional)">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-6 col-lg-12 col-sm-12">
                                                        <div class="pro-inner-row">
                                                            <label><i class="fa fa-university" aria-hidden="true"></i> Undergraduate Institution Name (optional)</label>
                                                            <input type="text" value="{{$therapist->undergraduate_institute }}"  required="required" spellcheck="true" class="form-control" disabled placeholder="Undergraduate Institution Name (optional)">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-6 col-lg-12 col-sm-12">
                                                        <div class="pro-inner-row">
                                                            <label><i class="fa fa-university" aria-hidden="true"></i> Postgraduate Institiution Name (optional)</label>
                                                            <input type="text" value="{{$therapist->postgraduate_institute }}"  required="required" spellcheck="true" class="form-control" disabled placeholder="Postgraduate Institiution Name (optional)">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-6 col-sm-12">
                                                        <label class="blue-label">Skills & Expertise</label>
                                                        <select id="specialities" data-type="specialities" disabled class="select-modal select2 form-control" multiple="multiple">
                                                            @foreach($specialities as $spec)
                                                                <option
                                                                {{  (in_array($spec->id, explode(',', $therapist->specialities_id))) ? "selected" : "" }}
                                                                value="{{$spec->id}}">{{ ucfirst($spec->name)}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-6 col-sm-12">
                                                        <label class="blue-label">Treatment orientation</label>
                                                        <select id="treatment"  data-type="treatment" disabled class="select-modal select2 form-control" multiple="multiple">
                                                            @foreach($treatment_orientations as $treat)
                                                                <option
                                                                    {{  (in_array($treat->id, explode(',', $therapist->treatments_id))) ? "selected" : "" }}
                                                                    value="{{$treat->id}}">{{ ucfirst($treat->name)}}</option>

                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-6 col-sm-12">
                                                        <label class="blue-label">Modality</label>
                                                        <select id="modality" data-type="modality" disabled class="select-modal select2 form-control" multiple="multiple">
                                                            @foreach($modalities as $modal)
                                                                <option
                                                                    {{  (in_array($modal->id, explode(',', $therapist->modalities_id))) ? "selected" : "" }}
                                                                    value="{{$modal->id}}">{{ ucfirst($modal->name)}}</option>

                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-6 col-sm-12">
                                                        <label class="blue-label">Focus for clients age group</label>
                                                        <select id="agegroup" data-type="agegroup" disabled  class="select-modal select2 form-control" multiple="multiple">
                                                            @foreach($age_groups as $age)
                                                                <option
                                                                    {{  (in_array($age->id, explode(',', $therapist->age_groups_id))) ? "selected" : "" }}
                                                                    value="{{$age->id}}">{{ ucfirst($age->name)}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-6 col-sm-12">
                                                        <label class="blue-label">Focus for clients ethnicity</label>
                                                        <select id="ethnicity" data-type="ethnicity" disabled class="select-modal select2 form-control"  multiple="multiple">
                                                            @foreach($ethnicities as $eth)
                                                                <option
                                                                    {{  (in_array($eth->id, explode(',', $therapist->ethnicities_id))) ? "selected" : "" }}
                                                                    value="{{$eth->id}}">{{ ucfirst($eth->name)}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-6 col-sm-12">
                                                        <label class="blue-label">Focus for clients spoken language</label>
                                                        <select id="language" data-type="language" disabled class="select-modal select2 form-control" multiple="multiple">
                                                            @foreach($languages as $lang)
                                                                <option
                                                                    {{  (in_array($lang->id, explode(',', $therapist->languages_id))) ? "selected" : "" }}
                                                                    value="{{$lang->id}}">{{ ucfirst($lang->name)}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <h1>Personal Details</h1>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="pro-inner-row">
                                                            <label><i class="fa fa-file-text" aria-hidden="true"></i> Personal Statement</label>
                                                            <textarea class="form-control" disabled>{{$therapist->statement}}</textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="pro-inner-row">
                                                            <label><i class="fa fa-clock-o" aria-hidden="true"></i> Your Availability Timing for Your Clients</label>
                                                            <textarea class="form-control" disabled>{{$therapist->time_availability}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <h1>Professional Reference</h1>
                                                <div class="row">
                                                    <div class="col-lg-6 col-sm-12">
                                                        <div class="pro-inner-row">
                                                            <label><i class="fa fa-therapist-circle"></i> Full Name of Reference</label>
                                                            <input type="text" value="{{$therapist->reference_name}}" required="required" spellcheck="true" class="form-control" disabled placeholder="Name">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-sm-12">
                                                        <div class="pro-inner-row">
                                                            <label><i class="fa fa-therapist-circle"></i> Professional Title of Reference</label>
                                                            <input type="text" value="{{$therapist->reference_title}}" required="required" spellcheck="true" class="form-control" disabled placeholder="Name">
                                                        </div>
                                                    </div>
                                                    <!--pro inner row end-->
                                                    <!--pro inner row end-->
                                                    <div class="col-lg-6 col-sm-12">
                                                        <div class="pro-inner-row">
                                                            <label><i class="fa fa-envelope"></i> Email of Reference</label>
                                                            <input type="email"  value="{{$therapist->reference_email}}" required="required" spellcheck="true" class="form-control" disabled placeholder="Email Address">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--row end-->
                                        </div>
                                        <!--profile form end-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
    <div class="login-fail-main therapist change-pass">
        <div class="featured inner">
            <div class="modal fade bd-example-modal-lg" id="vpcpm" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                        <div class="payment-modal-main">
                            <div class="payment-modal-inner"> <img src="{{ asset('images/change-password.png') }}" class="img-fluid" alt="">
                                <h3>Change Password</h3>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="pro-inner-row">
                                            <i class="fa fa-lock"></i>
                                            <input type="password" id="vpop" required="required" spellcheck="true" class="form-control" placeholder="Current Password">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="pro-inner-row">
                                            <i class="fa fa-lock"></i>
                                            <input type="password" id="vpnp" required="required" spellcheck="true" class="form-control" placeholder="New Password">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="pro-inner-row">
                                            <i class="fa fa-lock"></i>
                                            <input type="password" id="vpcnp" required="required" spellcheck="true" class="form-control" placeholder="Re-Type Password">
                                        </div>
                                    </div>
                                    <div class="col-12 text-center">
                                        <button type="button" class="vpcpb can">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="login-fail-main therapist rejection-modal">
        <div class="featured inner">
            <div class="modal fade bd-example-modal-lg" id="rejection-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                        <div class="row mt-2 pt-2">
                            <div class="col-md-12 col-sm-12 text-center">
                                <h1>Rejection Reason</h1>
                            </div>
                        </div>
                        <div class="row p-4">
                            <div class="col-12  text-center" >
                                <h4>Your profile was rejected for the following reasons</h4>
                                <p class="light-p" id="reason"></p>
                                <p>Please edit your profile details so we can review it again.</p>
                            </div>
                            <div class="col-12 text-center">
                                <a href="{{ route('therapist.profile.edit') }}" class="big-center-cta">Edit Profile</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection

@section('js')
<script src="{{ asset('js/profile.js') }}"></script>

@endsection
