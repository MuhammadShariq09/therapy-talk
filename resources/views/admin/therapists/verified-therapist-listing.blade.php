@extends('layouts.master')

@section('title',' Therapist Listings')
@section('body-class',"vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar")
@section('body-col',"2-columns")

@section('content')

    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-body">
                <!-- Basic form layout section start -->
                <section id="configuration" class="search view-cause">
                    <div class="row">
                        <div class="col-12">
                            <div class="card rounded pad-20">
                                <div class="card-content collapse show">
                                    <div class="card-body table-responsive card-dashboard">
                                        <div class="row">
                                            <div class="col-12">
                                                @if(Session::has('message'))
                                                    <div class="alert alert-success">
                                                        <strong>{{ Session::get('message')  }}</strong>
                                                    </div>
                                                @endif
                                                @if(Session::has('error'))
                                                    <div class="alert alert-danger">
                                                        <strong>{{ Session::get('error')  }}</strong>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="col-12">
                                                <h1 class="pull-left">Un-Verified Therapists</h1>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>
                                        <div class="maain-tabble mt-3">
                                            <table class="table table-striped table-bordered zero-configuration">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Therapist Name</th>
                                                        <th>Email</th>
                                                        <th>Status</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php $i = 0; @endphp
                                                    @foreach($therapists as $user)
                                                    @php $i++; @endphp
                                                        <tr id="row{{$user->id}}">
                                                            <td>{{$i}}</td>
                                                            <td class="user-td">
                                                                @if($user->image == "")
                                                                    <span data-toggle="popover" data-content="johny" class="circle" style="background: #3C5088;">{{ucfirst(substr($user->full_name,0,1))}}</span>
                                                                @else
                                                                    <span class="avatar avatar-online">
                                                                        <img class="tab-img" src="{{ asset($user->image) }}" alt="{{ucfirst($user->full_name)}}" onerror="this.style.display='none'">
                                                                    </span>
                                                                @endif
                                                                <a href="{{ route('view-therapist', ['id' => $user->id]) }}">{{ucfirst($user->full_name)}}</a>
                                                            </td>
                                                            <td>{{$user->email}}</td>
                                                            @if($user->is_rejected)
    <td><span class="badge badge-danger">Rejected</span></td>
                                                                <td>
   <a onclick="$('#reason').val('{{$user->rejected_reason}}')" data-toggle="modal" class="white btn btn-default showReason" data-target="#showReason" href="#">Yes (View Reason)</a>
    <a class="btn btn-primary white" href="{{ route('admin.therapist.approve', ['id' => $user->id ]) }}">Approve/Verify</a>

                                                                </td>
                                                            @elseif($user->is_verified == "0")
    <td><span class="badge badge-primary">Pending</span></td>
                                                                <td>
    <div class="btn-group mr-1 mb-1 my-action">
        <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i></button>
           <div class="dropdown-menu blockui" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 21px, 0px); top: 0px; left: 0px; will-change: transform;">
    <a data-toggle="modal" data-id="{{$user->id}}" class="dropdown-item rejecttp" data-target="#rejectTherapist" href="#"><i class="fa fa-check"></i>Reject</a>
    <a class="dropdown-item" href="{{ route('admin.therapist.approve', ['id' => $user->id ]) }}"><i class="fa fa-check"></i>Approve/Verify</a>
</div>
</div>
                                                                </td>
                                                            @else
                                                                <td><span class="badge badge-primary">Verified</span></td>
                                                            @endif
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!--main card end-->
                            </div>
                        </div>
                    </div>
                </section>
                <!-- // Basic form layout section end -->
            </div>
        </div>
    </div>

    <!-- Delete Modal -->
    <div class="login-fail-main user">
        <div class="featured inner">
            <div class="modal fade bd-example-modal-lg" id="rejectTherapist" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lgg">
                    <div class="modal-content">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                        <form method="post" id="reject-user">
                            @csrf
                            <input type="hidden" class="user_id" id="tpid" value="" />
                            <div class="payment-modal-main">
                                <div class="payment-modal-inner"> <img src="{{asset('images/block-modal.png') }}" class="img-fluid" alt="">
                                    <h3>Are you sure you want to reject this therapist?</h3>
                                    <div class="row">

                                        <div class="col-12 text-center">
                                            <textarea name="rejection_reason"  class="form-control" id="rejection_reason" cols="30" rows="10" placeholder="Rejection reason"></textarea>
                                        </div>
                                        <div class="col-12 text-center">
                                            <button type="button" data-dismiss="modal" class="can">Cancel</button>
                                            <button type="button" id="reject" class="rejectThp">Reject</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- Delete Modal -->
    <div class="login-fail-main user">
        <div class="featured inner">
            <div class="modal fade bd-example-modal-lg" id="showReason" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lgg">
                    <div class="modal-content">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                        <form method="post" id="reject-user">
                            <div class="payment-modal-main">
                                <div class="payment-modal-inner"> <img src="{{asset('images/block-modal.png') }}" class="img-fluid" alt="">
                                    <h3>Rejection Reason</h3>
                                    <div class="row">
                                        <div class="col-12 text-center">
                                            <textarea name="reason" disabled class="form-control" id="reason" cols="30" rows="10" placeholder="Rejection reason"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="login-fail-main user">
        <div class="featured inner">
            <div class="modal fade bd-example-modal-lg" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lgg">
                    <div class="modal-content">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                        <form action="{{ url('') }}"  method="post" id="">
                            @csrf
                            @method('delete')
                            <input type="hidden" class="user_id" name="user_id" id="user_id" value="" />
                            <div class="payment-modal-main">
                                <div class="payment-modal-inner"> <img src="{{asset('images/block-modal.png') }}" class="img-fluid" alt="">
                                    <h3>Are you sure you want to delete this therapist?</h3>
                                    <div class="row">
                                        <div class="col-12 text-center">
                                            <button type="button" data-dismiss="modal" class="can">Cancel</button>
                                            <button type="submit" id="deleteUser" class="con">Delete</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection('content')

@section('js')
<script type="text/javascript">

    $('#deleteModal').on('show.bs.modal', function (e) {

        let action = $(e.relatedTarget).attr('href');
        $('#delete-user').attr('action', action);
        // $(this).find('.user_id').val(userID);
    });

    $('.rejecttp').on('click', function(){
        let id = $(this).data('id');
        $('#tpid').val(id);
    })

    $('.rejectThp').on('click', function() {
        if($('#rejection_reason').val() == ''){
            toastr.error('Please enter rejection reason', 'Error');
        }else{
            $.ajax({
                url: '{{route("admin.therapist.reject")}}',
                data: {reason: $('#rejection_reason').val(), id:$('#tpid').val() },
                type: 'post',
                success: function(response){
                    toastr.success(response.message, 'Success');
                    window.location.reload();
                }
            });

        }
    });

    $('.statusButton').on('change', function(){
        var value = $(this).val();
        var target = $(this).data("id");
        $.ajax({
            url: '{{ route("update-therapist-verified")}}',
            data: {value: value, target:target },
            type: 'get',
            success: function(response){
                if(response.status == 200){
                     toastr.success(response.msg, 'Success');
                     $('#row'+target).remove();
                }
                else{
                     toastr.error('Error');
                     $('.statusButton').val(!value);
                }
            }
        });
    });


    $("#status").change(function(){
        var status = this.value;
        toastr.success(status);
        if(status=="0") {
            toastr.success(status);
        } else {
            toastr.success(status);
        }
    });
</script>
@endsection('js')
