@extends('layouts.master')

@section('title',' Add Therapist')
@section('body-class',"vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar")
@section('body-col',"2-columns")
@section('css')
    <link rel='stylesheet' href='https://foliotek.github.io/Croppie/croppie.css'>
@endsection
@section('content')

<div class="app-content content">
    <div class="content-wrapper">
        <div class="">
            <div class="content-body">
                <!-- Basic form layout section start -->
                <section id="configuration" class="u-c-p u-profile a-edit">
                    <div class="row">
                        <div class="col-12">
                            <div class="card pro-main">
                                <div class="row">
                                    <div class="col-md-6 col-sm-12">
                                        <h1>Add therapist</h1>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="{{ url('admin')}}">HOME</a></li>
                                            <li class="breadcrumb-item"><a href="{{route('active-therapists')}}">Therapist</a></li>
                                            <li class="breadcrumb-item active">Add therapist</li>
                                        </ol>
                                    </div>

                                </div>
                                @if ($errors->any())
                                    @foreach ($errors->all() as $error)
                                        <div class="alert alert-danger">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                            {{$error}}
                                        </div>
                                    @endforeach
                                @endif
                                <form method="post" action="{{ route('submit-therapist') }}" enctype="multipart/form-data" novalidate class="form">
                                @csrf
                                    <div class="row">
                                    <div class="col-lg-12 col-12">
                                        <div class="profile-frm">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="pro-img-main">
                                                        <img src="{{ asset('images/Profile_03.png') }}" id="user_image" alt="">
                                                        <button type="button" name="file" class="change-cover" onclick="document.getElementById('upload').click()"><i class="fa fa-camera"></i></button>
                                                        <input type="file"  accept="image/*" onchange="readURL(this, 'user_image')" id="upload" name="user_image">
                                                        <input type="hidden" id="personal_img_base64" name="image">
                                                    </div>
                                                </div>
                                                <!--col end-->

                                                <div class="col-lg-12 col-sm-12 col-md-12 pb-2">
                                                    <h1>Account</h1>
                                                </div>
                                                <div class="col-lg-6 col-sm-12">
                                                    <div class="pro-inner-row">
                                                        <label><i class="fa fa-user-circle"></i> Full Name</label>
                                                        <input type="text" name="full_name" id="full_name" maxlength="50" value="{{ old('full_name') }}" class="form-control" placeholder="Full Name">
                                                        @if ($errors->has('full_name'))
                                                            <span class="invalid-feedback" role="alert" style="display:block;">
                                                                <strong>{{ $errors->first('full_name') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                {{-- <div class="col-lg-6 col-sm-12">
                                                    <div class="pro-inner-row">
                                                        <label><i class="fa fa-user-circle"></i> Last Name</label>
                                                        <input type="text" name="last_name" id="last_name" maxlength="50" value="{{ old('first_name') }}" class="form-control" placeholder="First Name">
                                                        @if ($errors->has('last_name'))
                                                            <span class="invalid-feedback" role="alert" style="display:block;">
                                                                <strong>{{ $errors->first('last_name') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div> --}}

                                                <!--pro inner row end-->

                                                <!--pro inner row end-->
                                                <div class="col-lg-6 col-sm-12">
                                                    <div class="pro-inner-row">
                                                        <label><i class="fa fa-envelope"></i> Email Address</label>
                                                        <input type="email" name="email" id="email" maxlength="100" value="{{ old('email') }}" class="form-control" placeholder="Email Address">
                                                        @if ($errors->has('email'))
                                                            <span class="invalid-feedback" role="alert" style="display:block;">
                                                                <strong>{{ $errors->first('email') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12 col-sm-12 col-md-12 pb-2">
                                                    <h1>Office Location</h1>
                                                </div>
                                            </div>
                                            <div class="form-group mb-2 contact-repeater">
                                                
                                                <div data-repeater-list="repeater-group">
                                                    <div class="row" data-repeater-item>
                                                        <div class="col-lg-6 col-sm-12">

                                                            <div class="pro-inner-row">
                                                                <label><i class="fa fa-id-card" aria-hidden="true"></i> Name of Practice (optional)</label>
                                                            <input type="text" name="practice_name" id="practice_name" value="{{old('practice_name')}}" class="form-control" placeholder="Name of Practice (optional)">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-sm-12">
                                                            <div class="pro-inner-row">
                                                                <label><i class="fa fa-globe" aria-hidden="true"></i> Practice Website</label>
                                                                <input type="text" name="practice_website" id="practice_website" value="{{old('practice_website')}}" class="form-control" placeholder="Practice Website">
                                                                @if ($errors->has('practice_website'))
                                                                    <span class="invalid-feedback" role="alert" style="display:block;">
                                                                        <strong>{{ $errors->first('practice_website') }}</strong>
                                                                    </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-sm-12">
                                                            <div class="pro-inner-row">
                                                                <label><i class="fa fa-phone"></i> Work Phone #</label>
                                                                <input type="text" name="phone" id="phone" onkeydown="javascript: return event.keyCode == 69 ? false : true"  value="{{ old('phone') }}" class="form-control phone" maxlength="14"placeholder="Work Phone #">
                                                                @if ($errors->has('phone'))
                                                                    <span class="invalid-feedback" role="alert" style="display:block;">
                                                                        <strong>{{ $errors->first('phone') }}</strong>
                                                                    </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-sm-12">
                                                            <div class="pro-inner-row">
                                                                <label><i class="fa fa-map-marker" aria-hidden="true"></i> Address (line 1)</label>
                                                                <input type="text" name="address" id="autocomplete" onfocus="geolocate()" class="form-control" value="{{ old('address') }}" placeholder="Address (line 1)">
                                                                <input type="hidden" id="latitude" class="form-control" name="latitude"   value="{{ old('latitude') }}">
                                                                <input type="hidden" id="longitude" class="form-control" name="longitude"  value="{{ old('longitude') }}">
                                                                @if ($errors->has('address'))
                                                                    <span class="invalid-feedback" role="alert" style="display:block;">
                                                                        <strong>{{ $errors->first('address') }}</strong>
                                                                    </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-sm-12">
                                                            <div class="pro-inner-row">
                                                                <label><i class="fa fa-map-marker" aria-hidden="true"></i> Address (line 2) (Optional)</label>
                                                                <input type="text" class="form-control" name="address_2" id="address_2" value="{{ old('address_2') }}"
                                                                       placeholder="Address (line 2)">
                                                            </div>
                                                        </div>
                                                       
                                                        <div class="col-lg-6 col-sm-12">
                                                            <div class="pro-inner-row">
                                                                <label><i class="fa fa-map" aria-hidden="true"></i> State of Practice</label>
                                                                <select id="state" class="selectSearch form-control" name="state">
                                                                    @foreach($states as $row)
                                                                    <option value="{{$row->id}}">{{$row->name}}</option>
                                                                    @endforeach
                                                                  </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-sm-12">
                                                            <div class="pro-inner-row">
                                                                <label><i class="fa fa-building" aria-hidden="true"></i> City</label>
                                                                <select id="city" class="selectSearch form-control" name="city" placeholder="City"></select>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-sm-12">
                                                            <div class="pro-inner-row">
                                                                <label><i class="fa fa-map-marker"></i> Zip Code</label>
                                                                <input type="text" name="zipcode" id="postal_code" value="{{ old('zipcode') }}" class="form-control"
                                                                       placeholder="Zip Code">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-sm-12">
                                                            <div class="pro-inner-row">
                                                                <label><i class="fa fa-globe"></i> Country</label>
                                                                <input type="text" name="country" id="country" value="{{ old('country') }}" class="form-control" placeholder="country">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-sm-12">
                                                            <button type="button" data-repeater-delete class=" btn btn-default">
                                                                <i class="fa fa-trash-o" aria-hidden="true"></i> Remove
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <button type="button" data-repeater-create class=" btn btn-primary">
                                                    <i class="fa fa-plus-circle" aria-hidden="true"></i> Add New Office
                                                </button>

                                            </div>

                                            <div class="row">
                                                <div class="col-lg-12 col-sm-12 col-md-12 pb-2">
                                                    <h1>Public Profile</h1>
                                                </div>
                                                <div class="col-lg-6 col-sm-12">
                                                    <div class="pro-inner-row">
                                                        <label><i class="fa fa-user-circle" aria-hidden="true"></i> Title (optional)</label>
                                                        <input type="text" class="form-control" name="therapist_title" id="therapist_title" value="{{ old('therapist_title') }}" placeholder="Title (optional)">
                                                    </div>
                                                </div>

                                                <div class="col-lg-6 col-sm-12">
                                                    <div class="pro-inner-row">
                                                        <label><i class="fa fa-user-md" aria-hidden="true"></i> Therapist Type</label>
                                                        <input type="text" class="form-control" name="therapist_type" id="therapist_type" value="{{ old('therapist_type') }}" placeholder="Therapist Type">
                                                        @if ($errors->has('therapist_type'))
                                                            <span class="invalid-feedback" role="alert" style="display:block;">
                                                                <strong>{{ $errors->first('therapist_type') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="col-lg-6 col-sm-12">
                                                    <div class="pro-inner-row">
                                                        <label><i class="fa fa-id-card" aria-hidden="true"></i> Type of License</label>
                                                        <input type="text" class="form-control" name="license_type" id="license_type" value="{{ old('license_type') }}" placeholder="Type of License">
                                                        @if ($errors->has('license_type'))
                                                            <span class="invalid-feedback" role="alert" style="display:block;">
                                                                <strong>{{ $errors->first('license_type') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>


                                                <div class="col-lg-6 col-sm-12">
                                                    <div class="pro-inner-row">
                                                        <label><i class="fa fa-id-card" aria-hidden="true"></i> License in (optional)</label>
                                                        <input type="text" class="form-control" name="license_in" id="license_in" value="{{ old('license_in') }}" placeholder="Licensed in (optional)">
                                                    </div>
                                                </div>

                                                <div class="col-lg-6 col-sm-12">
                                                    <div class="pro-inner-row">
                                                        <label><i class="fa fa-university" aria-hidden="true"></i> Undergraduate Institution Name (optional)</label>
                                                        <input type="text" class="form-control" name="undergraduate_institute" id="undergraduate_institute" value="{{ old('undergraduate_institute') }}" placeholder="Undergraduate Institution Name (optional)">
                                                    </div>
                                                </div>

                                                <div class="col-lg-6 col-sm-12">
                                                    <div class="pro-inner-row">
                                                        <label><i class="fa fa-university" aria-hidden="true"></i> Postgraduate Institiution Name (optional)</label>
                                                        <input type="text" class="form-control" name="postgraduate_institute" id="postgraduate_institute" value="{{ old('postgraduate_institute') }}" placeholder="Postgraduate Institiution Name (optional)">
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6 col-sm-12">
                                                    <label class="blue-label">Specialties</label>
                                                    <select data-type="specialities" class="select-modal select2 form-control" name="specialities_id[]" id="specialities" multiple="multiple">
                                                        @foreach($specialities as $speciality)
                                                        <option
                                                        {{  (old('specialities_id')) && (in_array($speciality->id, old('specialities_id'))) ? "selected" : "" }}
                                                         value="{{$speciality->id}}"> {{ ucfirst($speciality->name)}}
                                                        </option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('specialities_id'))
                                                        <span class="invalid-feedback" role="alert" style="display:block;">
                                                            <strong>{{ $errors->first('specialities_id') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>

                                                <div class="col-lg-6 col-sm-12">
                                                    <label class="blue-label">Treatment orientation</label>
                                                    <select data-type="treatment" class="select-modal select2 form-control" name="treatments_id[]" id="treatment" multiple="multiple">
                                                        @foreach($treatment_orientations as $treatment)
                                                            <option {{  (old('treatments_id')) && (in_array($treatment->id, old('treatments_id'))) ? "selected" : "" }}
                                                            value="{{$treatment->id}}">{{ ucfirst($treatment->name)}}</option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('treatments_id'))
                                                        <span class="invalid-feedback" role="alert" style="display:block;">
                                                            <strong>{{ $errors->first('treatments_id') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>

                                                <div class="col-lg-6 col-sm-12">
                                                    <label class="blue-label">Modality</label>
                                                    <select data-type="modality" class="select-modal select2 form-control" name="modalities_id[]" id="modality" multiple="multiple">
                                                        @foreach($modalities as $modality)
                                                            <option {{  (old('modalities_id')) && (in_array($modality->id, old('modalities_id'))) ? "selected" : "" }}
                                                            value="{{$modality->id}}">{{ ucfirst($modality->name)}}</option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('modalities_id'))
                                                        <span class="invalid-feedback" role="alert" style="display:block;">
                                                            <strong>{{ $errors->first('modalities_id') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>

                                                <div class="col-lg-6 col-sm-12">
                                                    <label class="blue-label">Focus for clients age group</label>
                                                    <select data-type="agegroup" class="select-modal  select2 form-control" name="age_groups_id[]"
                                                            id="agegroup" multiple="multiple">
                                                        @foreach($age_groups as $age_group)
                                                            <option {{  (old('age_groups_id')) && (in_array($age_group->id, old('age_groups_id'))) ? "selected" : "" }}
                                                             value="{{$age_group->id}}">{{ ucfirst($age_group->name)}}</option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('age_groups_id'))
                                                        <span class="invalid-feedback" role="alert" style="display:block;">
                                                            <strong>{{ $errors->first('age_groups_id') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>

                                                <div class="col-lg-6 col-sm-12">
                                                    <label class="blue-label">Focus for clients ethnicity</label>
                                                    <select data-type="ethnicity" class="select-modal select2 form-control" name="ethnicities_id[]"
                                                            id="ethnicity" multiple="multiple">
                                                        @foreach($ethnicities as $ethnicity)
                                                            <option {{  (old('ethnicities_id')) && (in_array($ethnicity->id, old('ethnicities_id'))) ? "selected" : "" }}
                                                             value="{{$ethnicity->id}}">{{ ucfirst($ethnicity->name)}}</option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('ethnicities_id'))
                                                        <span class="invalid-feedback" role="alert" style="display:block;">
                                                            <strong>{{ $errors->first('ethnicities_id') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>

                                                <div class="col-lg-6 col-sm-12">
                                                    <label class="blue-label">Focus for clients spoken language</label>
                                                    <select data-type="language" class="select-modal select2 form-control" id="language" name="languages_id[]" multiple="multiple">
                                                        @foreach($languages as $language)
                                                            <option {{  (old('languages_id')) && (in_array($language->id, old('languages_id'))) ? "selected" : "" }}
                                                             value="{{$language->id}}">{{ ucfirst($language->name)}}</option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('languages_id'))
                                                        <span class="invalid-feedback" role="alert" style="display:block;">
                                                            <strong>{{ $errors->first('languages_id') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>

                                            </div>

                                            <div class="row">
                                                <div class="col-lg-12 col-sm-12 col-md-12 pb-2">
                                                    <h1>Personal Details</h1>
                                                </div>
                                                <div class="col-12">
                                                    <div class="pro-inner-row">
                                                        <label><i class="fa fa-file-text" aria-hidden="true"></i> Personal Statement</label>
                                                        <textarea class="form-control" name="statement" id="statement" placeholder="Personal Statement">{{ old('statement') }}</textarea>
                                                        @if ($errors->has('statement'))
                                                            <span class="invalid-feedback" role="alert" style="display:block;">
                                                                <strong>{{ $errors->first('statement') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="col-12">
                                                    <div class="pro-inner-row">
                                                        <label><i class="fa fa-clock-o" aria-hidden="true"></i> Your Availability Timing for Your Clients</label>
                                                        <textarea class="form-control" name="time_availability" id="time_availability"  placeholder="Your Availability Timing for Your Clients">{{ old('time_availability') }}</textarea>
                                                        @if ($errors->has('time_availability'))
                                                            <span class="invalid-feedback" role="alert" style="display:block;">
                                                                <strong>{{ $errors->first('time_availability') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-12 col-sm-12 col-md-12 pb-2">
                                                    <h1>Personal Reference</h1>
                                                </div>

                                                <div class="col-lg-6 col-sm-12">
                                                    <div class="pro-inner-row">
                                                        <label><i class="fa fa-user-circle"></i> Reference Name</label>
                                                        <input type="text"  name="reference_name" id="reference_name" value="{{ old('reference_name') }}" class="form-control" placeholder="Reference Name">
                                                        @if ($errors->has('reference_name'))
                                                            <span class="invalid-feedback" role="alert" style="display:block;">
                                                                <strong>{{ $errors->first('reference_name') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-sm-12">

                                                    <div class="pro-inner-row">
                                                        <label><i class="fa fa-user-circle"></i> Professional Title of Reference</label>
                                                        <input type="text" class="form-control" name="reference_title" id="reference_title" value="{{ old('reference_title') }}" placeholder="Reference Title">
                                                        @if ($errors->has('reference_title'))
                                                            <span class="invalid-feedback" role="alert" style="display:block;">
                                                                <strong>{{ $errors->first('reference_title') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <!--pro inner row end-->

                                                <!--pro inner row end-->
                                                <div class="col-lg-6 col-sm-12">
                                                    <div class="pro-inner-row">
                                                        <label><i class="fa fa-envelope"></i> Reference Email Address</label>
                                                        <input type="email" name="reference_email" id="reference_email" value="{{ old('reference_email') }}" class="form-control" placeholder="Reference Email Address">
                                                        @if ($errors->has('reference_email'))
                                                            <span class="invalid-feedback" role="alert" style="display:block;">
                                                                <strong>{{ $errors->first('reference_email') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="row">

                                             <!--pro inner row end-->
                                             <div class="col-lg-6 col-sm-12">
                                                <div class="pro-inner-row">
                                                    <label><i class="fa fa-lock"></i> Password</label>
                                                    <input type="password" name="password" id="password" class="form-control"placeholder="Password">
                                                    @if ($errors->has('password'))
                                                        <span class="invalid-feedback" role="alert" style="display:block;">
                                                            <strong>{{ $errors->first('password') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <!--pro inner row end-->
                                            <div class="col-lg-6 col-sm-12">
                                                <div class="pro-inner-row">
                                                    <label><i class="fa fa-lock"></i> Confirm Password</label>
                                                    <input type="password" name="password_confirmation" id="confirm_password" class="form-control"placeholder="Confirm Password">
                                                    @if ($errors->has('password_confirmation'))
                                                        <span class="invalid-feedback" role="alert" style="display:block;">
                                                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>

                                                <div class="col-12 text-center w-100">

                                                    <button type="submit" class="big-center-cta">Add therapist</button>

                                                </div>
                                                <br>
                                            </div>
                                        </div>
                                        <!--row end-->

                                    </div>
                                    <!--profile form end-->

                                </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>

    <div class="modal" id="cropImagePop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <!--<div class="modal-body" style="min-height:400px">-->
                <div class="modal-body" style="">
                    <div id="upload-demo" style="width:350px"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="cropImageBtn" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
    <div class="login-fail-main user change-profile t-registration cat-popup">
        <div class="featured inner">
            <div class="modal fade bd-example-modal-lg" id="dataModal" tabindex="-1" role="dialog"
                 aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <div id="dataModalRes"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection('content')

@section('js')
<script>
    $(document).ready(function() {
    $('.selectSearch').select2();

    $('#state').on('change', function() {
      
            var stateID = $(this).val();
            var url = $('#url').val() + '/get-cities/';
            if(stateID) {
                $.ajax({
                    url: url+stateID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {
                        $('#city').empty();
                        $.each(data, function(key, value) {
                            // console.log(key);
                            // console.log(value); return;
                            $('#city').append('<option value="'+ value.id +'">'+ value.city +'</option>');
                        });


                    }
                });
            }else{
                $('#city]').empty();
            }
    });
});
    </script>
<!-- Bootstrap Date-Picker Plugin -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
<script src="https://foliotek.github.io/Croppie/croppie.js"></script>
<script>

    $('.select-modal').on('select2:opening', function (e) {
        let this_ = $(this);
        let type = $(this).data('type');
        $('#dataModal').modal('toggle');
        console.log('i am here');
        fetchDataModal(type)
        setTimeout(function(){
            this_.select2("close"); }, 100);
    });

    function fetchDataModal(type){
        let url = base_url + "/therapist-data-modal?type="+type;
        let values = $('#'+type).val();
        console.log(values)
        $.ajax({
            url: url,
            type: 'post',
            data: {values: values},
            success: function (response) {
                $('#dataModalRes').html(response);
            }
        });
    }

    $(document).on('click', '.submitDataModal', function(){
        let type = $(this).data('type');
        let values = [];
        $.each($("input[name='datamodal[]']:checked"), function(){
            values.push($(this).val());
        });
        $('#'+type).val(values);
        $('#'+type).trigger('change');

    });

    var $uploadCrop,
    tempFilename,
    rawImg,
    imageId;

    $uploadCrop = $('#upload-demo').croppie({
        enableExif: true,
        viewport: {
            width: 200,
            height: 200,
            type: 'circle'
        },
        boundary: {
            width: 250,
            height: 250
        }
    });
    var nogo;
    $('#upload').on('change', function(e){
        var f = e.target.files[0];
        if(f.size > 5242880){
            toastr.error('You cant not select file greater than 5 MB');
            $(this).val('');
            nogo=1;
        }else{
                console.log('here');
            nogo=0;
            $('#cropImagePop').modal('toggle');
            var reader = new FileReader();
            reader.onload = function(e) {
                $uploadCrop.croppie('bind', {
                    url: e.target.result
                }).then(function() {
                    console.log('jQuery bind complete');
                });
            }
            reader.readAsDataURL(this.files[0]);
        }
    });

    $('#cropImageBtn').on('click', function(ev) {
        $uploadCrop.croppie('result', {
            type: 'base64',
            format: 'jpeg',
            size: { width: 300, height: 300 }
        }).then(function(resp) {
            if(!nogo){
                $('#user_image').attr('src', resp);
            }else{
                $('#user_image').attr('src', base_url + 'images/Profile_03.png');
            }
            $('#personal_img_base64').val(resp);
            $('#cropImagePop').modal('hide');
        });
    });

    function readURL(input, target) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#'+target).attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    function phoneFormatter() {
        $('.phone').on('input', function() {
            var number = $(this).val().replace(/[^\d]/g, '')
            if (number.length == 7) {
                number = number.replace(/(\d{3})(\d{4})/, "$1-$2");
            } else if (number.length == 10) {
                number = number.replace(/(\d{3})(\d{3})(\d{4})/, "($1) $2-$3");
            }
            $(this).val(number)
        });
    };

    $(phoneFormatter);

    $('#dob input').datepicker({
        //autoclose: true,
        endDate: new Date()
    });
</script>


<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAHPUufTlBkF5NfBT3uhS9K4BbW2N-mkb4&libraries=places&callback=initAutocomplete" async defer></script>
<script type="text/javascript">
  // This example displays an address form, using the autocomplete feature
  // of the Google Places API to help users fill in the information.

  // This example requires the Places library. Include the libraries=places
  // parameter when you first load the API. For example:
  // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

  var placeSearch, autocomplete;
  var componentForm = {
      /*locality: 'long_name',
      administrative_area_level_1: 'short_name',
      country: 'long_name',
      postal_code: 'short_name'*/

      /*street_number: 'short_name',
      route: 'long_name',*/
      locality: 'long_name',
      administrative_area_level_1: 'short_name',
      country: 'long_name',
      postal_code: 'short_name'
  };

  function initAutocomplete() {
      // Create the autocomplete object, restricting the search to geographical
      // location types.
      autocomplete = new google.maps.places.Autocomplete(
          /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
          {types: ['geocode']});

      // When the user selects an address from the dropdown, populate the address
      // fields in the form.
      autocomplete.setComponentRestrictions(
          {'country': ['us']});
      autocomplete.addListener('place_changed', fillInAddress);
      /*
      google.maps.event.addListener(autocomplete, 'place_changed', function() {
      });*/
  }

  function fillInAddress() {
      // Get the place details from the autocomplete object.
      var place = autocomplete.getPlace();
      console.log(place.geometry.location.lat());
      console.log(place.geometry.location.lng());
      document.getElementById("latitude").value = place.geometry.location.lat();
      document.getElementById("longitude").value = place.geometry.location.lng();
      for (var component in componentForm) {
          try{
              document.getElementById(component).value = '';
              document.getElementById(component).disabled = false;
          }catch(e){

          }
      }

      // Get each component of the address from the place details
      // and fill the corresponding field on the form.
      for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          console.log(addressType)
          if (componentForm[addressType]) {
              var val = place.address_components[i][componentForm[addressType]];
              document.getElementById(addressType).value = val;
          }
      }
  }

  // Bias the autocomplete object to the user's geographical location,
  // as supplied by the browser's 'navigator.geolocation' object.
  function geolocate() {
      if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
              var geolocation = {
                  lat: position.coords.latitude,
                  lng: position.coords.longitude
              };
              var circle = new google.maps.Circle({
                  center: geolocation,
                  radius: position.coords.accuracy
              });
              autocomplete.setBounds(circle.getBounds());
          });
      }
  }
</script>


    

   
@endsection

