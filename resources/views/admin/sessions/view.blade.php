@extends('layouts.master')
@section('title',' Session Log Detail')
@section('body-class',"vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar")
@section('body-col',"2-columns")
@section('css')

<style> 
.profile-frm ul {
    list-style-type: none;
    margin: 0 0 25px;
    padding: 0;
    overflow: hidden;
    display: flex;
    justify-content: center;
}
.rating li {
  float: left;
}

.rating li a {
  display: block;
  color: white;
  text-align: center;
  padding: 16px;
  text-decoration: none;
}

.rating li a:hover {
  background-color: #111111;
}
</style> 
@endsection
@section('content')

<div class="app-content content">
    <div class="content-wrapper">
      <div class="">
        <div class="content-body"> 
          <!-- Basic form layout section start -->
          <section id="configuration" class="u-c-p u-profile a-edit">
            <div class="row">
              <div class="col-12">
                <div class="card pro-main">
                  <div class="row">
                    <div class="col-md-6 col-sm-12">
                      <h1>session log</h1>
                    </div>
                      <div class="col-md-6 col-sm-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('admin')}}">HOME</a></li>
                                <li class="breadcrumb-item"><a href="a-session-log.html">Session Log</a></li>
                                <li class="breadcrumb-item active">view details</li>
                            </ol>
                        </div>
                      <div class="col-12">
                          <div class="add-user text-center"> <a href="{{ route('view-user', ['id' => $session->user->id]) }}" class="user"><i class="fa fa-eye" aria-hidden="true"></i> View Profile</a> </div>
                      </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-12 col-12">
                      <div class="profile-frm">
                        <div class="row">
                           <div class="col-12 text-center">
                            <div class="pro-img-main">
                                <img src="{{ isset($session->user->image) ? asset($session->user->image) : asset('images/Profile_03.png') }}"alt="{{ucfirst($session->user->full_name)}}" onerror="this.style.display='none'">
                            </div>
                            <h4>{{ $session->user->name}}</h4>
                            <ul><div class="rating" ></div></ul>
                            </div>
                          <!--col end-->
                          
                          <div class="col-12">
                            <!-- <div class="pro-inner-row">
                              <label><i class="fa fa-info-circle" aria-hidden="true"></i> About:</label>
                              <textarea class="form-control" disabled>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labo re et dolore magna aliqua. Enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
                                </textarea>
                            </div> -->
                            </div>
                             <div class="col-lg-6 col-sm-12">
                                <div class="card rounded">
                                     <div class="card-content">
                                         <div class="card-body">
                                            <div class="row">
                                                <div class="col-lg-6 col-sm-12">
                                                <div class="pro-inner-row">
                                                <label><i class="fa fa-users" aria-hidden="true"></i> Session type</label>
                                                <input type="text" value="{{ $session->type }}" spellcheck="true" disabled class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-sm-12">
                                                <div class="pro-inner-row">
                                                <label><i class="fa fa-clock-o" aria-hidden="true"></i> Session Duration</label>
                                                <input type="text" value="{{ $session->duration }} Hour" spellcheck="true" disabled class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-12">
                                            <div class="pro-inner-row">
                                                <label><i class="fa fa-file-text" aria-hidden="true"></i> Reason for seeking therapy:</label>
                                                <textarea class="form-control" disabled="">{{ $session->reason }}</textarea>
                                            </div>
                                            </div>
                                            </div>
                                         </div>
                                        
                                    </div>
                                 </div>
                            </div>
                            
                            <div class="col-lg-6 col-sm-12">
                            
                                <div class="card rounded">
                                     <div class="card-content">
                                        
                                         <div class="card-body">
                                             <label class="blue-label">
                                             Status: 
                                             @if($session->status == 0)
                                             Appointment request received
                                             @elseif($session->status == 1)
                                                @if($session->is_refund)
                                                Appointment Cancelled/Refunded
                                                @elseif($session->payment)
                                                Appointment scheduled
                                                @else
                                                Appointment accepted waiting for response
                                                @endif
                                            @else
                                                Appointment request Rejected
                                            @endif
                                             </label>
                                             
                                             {{-- <p>I am available for for Tele-therapy on Mondays and Wednesdays from</p> --}}
                                            <h6>Appointment in {{ $diff_in_days }}  </h6>
                                            <div class="row">
                                                <div class="col-lg-6 col-sm-12">
                                                    <div class="pro-inner-row">
                                                    <label><i class="fa fa-calendar" aria-hidden="true"></i> Date</label>
                                                    <input type="date" value="{{$session->requested_date}}" spellcheck="true" disabled class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-sm-12">
                                                    <div class="pro-inner-row">
                                                    <label><i class="fa fa-clock-o" aria-hidden="true"></i> Time</label>
                                                    <input type="time" value="{{$session->requested_time}}" spellcheck="true" disabled class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-sm-12">
                                                    <div class="pro-inner-row">
                                                    <label><i class="fa fa-money" aria-hidden="true"></i> Fee($)</label>
                                                    <input type="text" value="{{$session->amount}}" spellcheck="true" disabled class="form-control">
                                                    </div>
                                                </div>
                                             </div>
                                         </div>
                                        
                                    </div>
                                 </div>
                            </div>
                            
                           
                            
                            
                            </div>
                          
                          
                      </div>
                      <!--row end--> 
                      
                    </div>
                    <!--profile form end--> 
                    
                  </div>
                  
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  </div>
      
    


@endsection

@section('js')
<script>
        $(function () {
            var ratingValue = {{ $therapist_rating }}, rounded = (ratingValue | 0);
        
            for (var j = 0; j < 5; j++) {
            str = '<li class="active"><i class="fa ';
            if (j < rounded) {
                str += "fa-star";
            } else if ((ratingValue - j) > 0 && (ratingValue - j) < 1) {
                str += "fa-star-half-o";
            } else {
                str += "fa-star-o";
            }
            str += '" aria-hidden="true"></i><li>';
            $(".rating").append(str);
            }
        
        
        })
 </script>

@endsection