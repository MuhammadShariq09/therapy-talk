@extends('layouts.master')
@section('title',' Session Logs')
@section('body-class',"vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar")
@section('body-col',"2-columns")
@section('content')


<div class="app-content content">

    <div class="content-wrapper">
  
      <div class="content-body"> 
  
        <!-- Basic form layout section start -->
  
        <section id="configuration" class="search view-cause">
  
          <div class="row">
  
            <div class="col-12">
  
              <div class="card rounded pad-20">
  
                <div class="card-content collapse show">
  
                  <div class="card-body table-responsive card-dashboard">
                  <form method="GET">
                    <div class="row">
                        <div class="col-12"><h1 class="pull-left">session log</h1></div>
                    </div>
                    <div class="row date-input">
                        <div class="col-lg-12 form-group d-flex align-items-center">
                            <label class="w-100" for="">To Date:</label>
                            <input  type="date" id="select-date" name="from" class="form-control">
                          </div>
                          <div class="col-lg-12 form-group d-flex align-items-center">
                            <label class="w-100" for="">From Date:</label>
                            <input  type="date" id="select-date2" name="to" class="form-control">
                          </div>
                            <div class="col-lg-12 form-group d-flex align-items-center"> 
                                <label>Filter By Status Type:</label>
                                <select name="status">
                                        <option value="">Select...</option>
                                        <option value="0">Pending Reply</option>
                                        <option value="1">Appointment Cancelled/Refunded</option>
                                        <option value="1">Appointment reschedule Pending</option>
                                        <option value="1">Appointment accepted</option>
                                        <option value="2">Appointment rejected</option>
                                        <option value="3">Session Started</option>
                                        <option value="3">Session Expired</option>
                                        <option value="4">Session Completed</option>
                                    </select>
                            </div>
                            <div class="col-lg-12 form-group d-flex align-items-center"> 
                                <div class="add-user">
                                <button type="submit" class="btn-primary" >Search</button>
                                </div>
                            </div>
                    </div>
                        
                    
                  </form>
                    <div class="clearfix"></div>
                    <div class="maain-tabble mt-3">
                      <table class="table table-striped table-bordered zero-configuration">
                        <thead>
                          <tr>
                            <th>#</th>
                            
                            <th>User Name</th>
                            <th>Therapist Name</th>
                            <th>session date & time</th>
                            <th>session fee</th>
                            <th>status</th>
                            <th>Actions</th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach($sessions as $key => $session)
                                <tr>
                                    <td>{{ ++$key }}</td>
                                    <td class="user-td">
                                            <span class="avatar avatar-online">
                                                    <img style="height: 38px;" class="tab-img" src="{{ isset($session->user->image) ? asset($session->user->image) : ''  }}" alt="" onerror="this.style.display='none'">
                                                </span>
                                       {{ isset($session->user) ? ucfirst($session->user->full_name) : ''}}
                                    </td>
                                    <td class="user-td">
                                            <span class="avatar avatar-online">
                                                    <img style="height: 38px;" class="tab-img" src="{{isset($session->therapist->image) ? asset($session->therapist->image) : '' }}" alt="{{ucfirst($session->therapist->full_name)}}" onerror="this.style.display='none'">
                                                </span>
                                        {{ isset($session->therapist) ? ucfirst($session->therapist->full_name) : ''}}
                                    </td>
                                    <td>{{ $session->requested_date }} {{$session->requested_time}}</td>
                                    @if($session->payment_type==1)
                                      <td>${{ $session->session_type==3 ? ($session->session_type==2 ? config('app.voice_call') : config('app.video_call')) : config('app.live_chat')   }}</td>
                                    @else
                                    <td>${{$session->amount}}</td>
                                    @endif
                                    @if($session->status == 0)
                                                <td><label class="white badge badge-primary">Pending Reply</label></td>
                                            @elseif($session->status == 1)
                                                @if($session->is_refund)
                                                    <td><label class="white badge badge-danger">Appointment Cancelled/Refunded</label></td>
                                                @elseif(count($session->reschedule) && $session->reschedule->first()->status == 0)
                                                    <td><label class="white badge badge-success">Appointment reschedule Pending</label></td>
                                                @else
                                                    <td><label class="white badge badge-success">Appointment accepted</label></td>
                                                @endif
                                            @elseif($session->status == 2)
                                                <td><label class="white badge badge-danger">Appointment rejected</label></td>
                                            @elseif($session->status == 3)
                                                @if($interval->days >0)
                                                <td><label class="white badge badge-secondary">Session Expired</label></td>
                                                @else
                                                <td><label class="white badge badge-info">Session Started</label></td>
                                                @endif
                                            @elseif($session->status == 4)
                                                <td><label class="white badge badge-success">Session Completed</label></td>
                                            @endif
                                    <td>
                                        <div class="btn-group mr-1 mb-1">
                                            <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i></button>
                                            <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(4px, 23px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                <a class="dropdown-item"
                                                    href="{{ route('session-logs.view', ['id' => $session->id]) }}"><i class="fa fa-eye"></i>View </a>
                                            </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
  
                      </table>
  
                    </div>
  
                  </div>
  
                </div>
              </div>
            </div>
  
          </div>
  
        </section>
      </div>
  
    </div>
  
  </div>
  

@endsection

@section('js')
{{-- <script>
    $('#select-date').datepicker({
      uiLibrary: 'bootstrap4',
      format: 'mm/dd/yyyy'
    });
    $('#select-date2').datepicker({
                uiLibrary: 'bootstrap4',
                format: 'mm/dd/yyyy'
              });
  </script> --}}
@endsection