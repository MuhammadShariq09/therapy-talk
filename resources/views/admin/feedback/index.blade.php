@extends('layouts.master')
@section('title',' Feedbacks')
@section('body-class',"vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar")
@section('body-col',"2-columns")
@section('content')

<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-body"> 
        <section id="configuration" class="search view-cause">
          <div class="row">
            <div class="col-12">
              <div class="card rounded pad-20">
                <div class="card-content collapse show">
                  <div class="card-body table-responsive card-dashboard">
                  <form method="GET">
                    <div class="row">
                        <div class="col-12">
                            <h1 class="pull-left">Feedback</h1>
                        </div>
                         
                    </div>
                    <div class="row date-input">

                          <div class="col-lg-12 form-group d-flex align-items-center">
                            <label class="w-100" for="">To Date:</label>
                            <input  type="date" id="select-date" name="from" class="form-control">
                          </div>
                          <div class="col-lg-12 form-group d-flex align-items-center">
                            <label class="w-100" for="">From Date:</label>
                            <input  type="date" id="select-date2" name="to" class="form-control">
                          </div>
                          <div class="col-lg-12 d-flex align-items-center form-group">
                              <div class="add-user">
                                <button type="submit" class="btn btn-primary" name="search"> Search</button>
                              </div>
                          </div>
                        </div>
                  </form>
                    <div class="clearfix"></div>
                    <div class="maain-tabble mt-1" style="width:100%">
                      <table class="table table-striped table-bordered zero-configuration" >
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>date</th>
                            <th>User type</th>
                            <th>Name</th>
                            <!-- <th>email</th>
                            <th>Subject</th> -->
                            <th>Actions</th>
                          </tr>
                        </thead>
                        <tbody>
                            @if(isset($feedback))
                            @foreach($feedbacks as $key => $feedback)
                      
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>{{ $feedback->created_at }}</td>
                                <td>{{ ($feedback->type == 1) ? "User" : "Therapist" }}</td>
                                @if($feedback->user_id != "")
                                <td class="user-td">
                                    @if($feedback->user->image == "")
                                        <span data-toggle="popover" data-content="johny" class="circle"
                                                style="background: #3C5088;">{{ucfirst(substr($feedback->user->full_name,0,1))}}</span>
                                    @else
                                        <span class="avatar avatar-online">
                                                <img class="tab-img" src="{{ asset($feedback->user->image) }}" alt="{{ucfirst($feedback->user->full_name)}}" onerror="this.style.display='none'">
                                            </span>
                                    @endif
                                    {{ucfirst($feedback->user->full_name)}}
                                </td>
                                @else 
                                <td class="user-td">
                                    @if($feedback->therapist->image == "")
                                        <span data-toggle="popover" data-content="johny" class="circle"
                                                style="background: #3C5088;">{{ucfirst(substr($feedback->therapist->full_name,0,1))}}</span>
                                    @else
                                        <span class="avatar avatar-online">
                                                <img class="tab-img" src="{{ asset($feedback->therapist->image) }}" alt="{{ucfirst($feedback->therapist->full_name)}}" onerror="this.style.display='none'">
                                            </span>
                                    @endif
                                    {{ucfirst($feedback->therapist->full_name)}}
                                </td>
                                @endif
                                <!-- <td>{{ $feedback->email }}</td>
                                <td>{{ $feedback->subject }}</td> -->
                                
                                <td>
                                    <div class="btn-group mr-1 mb-1">
                                        <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i></button>
                                        <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(4px, 23px, 0px); top: 0px; left: 0px; will-change: transform;">
                                            <a class="dropdown-item"
                                                href="{{ route('feedbacks.view', ['id' => $feedback->id]) }}"><i class="fa fa-eye"></i>View </a>
                                        </div>
                                </td>
                            </tr>
                        @endforeach
                        @else 
                        <tr><td>No record found</td></tr>
                        @endif
                        </tbody>
  
                      </table>
  
                    </div>
  
                  </div>
  
                </div>
  
                <!--main card end--> 
  
                
  
              </div>
  
            </div>
  
          </div>
  
        </section>
  
        <!-- // Basic form layout section end --> 
  
      </div>
  
    </div>
  
  </div>




@endsection
@section('js')
{{-- <script>
    $('#select-date').datepicker({
      uiLibrary: 'bootstrap4',
      format: 'mm/dd/yyyy'
    });
    $('#select-date2').datepicker({
                uiLibrary: 'bootstrap4',
                format: 'mm/dd/yyyy'
              });
  </script> --}}
@endsection