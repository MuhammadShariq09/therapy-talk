@extends('layouts.master')
@section('title',' Feedback Details')
@section('body-class',"vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar")
@section('body-col',"2-columns")
@section('content')

<div class="app-content content">
    <div class="content-wrapper">
      <div class="">
        <div class="content-body"> 
          <!-- Basic form layout section start -->
          <section id="configuration" class="u-c-p u-profile a-edit">
            <div class="row">
              <div class="col-12">
                <div class="card pro-main">
                  <div class="row">
                    <div class="col-md-6 col-sm-12">
                      <h1>Feedback Detail</h1>
                    </div>
                      <div class="col-md-6 col-sm-12">
                            <ol class="breadcrumb">
                              <li class="breadcrumb-item"><a href="{{url('/admin')}}">HOME</a></li>
                                <li class="breadcrumb-item"><a href="{{route('feedbacks')}}">Feedback</a></li>
                                <li class="breadcrumb-item active">Feedback Detail</li>
                            </ol>
                        </div>
                    </div>
                  <div class="row">
                    <div class="col-lg-12 col-12">
                      <div class="profile-frm">
                        <div class="row">
                          <div class="col-lg-4 col-sm-12">
                            <div class="pro-inner-row">
                              <label><i class="fa fa-user-circle"></i>User Name</label>
                            <input type="text" value="{{ ($feedback->user_id != "") ? ucfirst($feedback->user->full_name) : ""}}" spellcheck="true" class="form-control" disabled placeholder="Name">
                            </div>
                            </div>
                             <div class="col-lg-4 col-sm-12">
                                <div class="pro-inner-row">
                                    <label><i class="fa fa-envelope"></i> Rating</label>
                                <input type="email" value="{{ $feedback->rating }}" spellcheck="true" class="form-control" disabled placeholder="">
                                </div>
                            </div>
                            <div class="col-lg-4 col-sm-12">
                                <div class="pro-inner-row">
                                <label><i class="fa fa-user-circle"></i> Therapist Name</label>
                                <input type="text" value="{{ ucfirst($feedback->therapist->full_name) }}" spellcheck="true" class="form-control" disabled placeholder="Member">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-sm-12">
                                <div class="pro-inner-row">
                                <label><i class="fa fa-file-text" aria-hidden="true"></i> Session ID#</label>
                                <input type="text" value="{{ $feedback->session_id }}" spellcheck="true" class="form-control" disabled placeholder="Session ID">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-sm-12">
                                <div class="pro-inner-row">
                                    <label><i class="fa fa-info-circle" aria-hidden="true"></i> Feedback</label>
                                    <input type="text" value="{{ $feedback->feedback }}" spellcheck="true" class="form-control" disabled placeholder="">
                                </div>
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  </div>




@endsection