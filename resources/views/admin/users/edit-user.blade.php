@extends('layouts.master')

@section('title',' Edit User')
@section('body-class',"vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar")
@section('body-col',"2-columns")
@section('css')
    <link rel='stylesheet' href='https://foliotek.github.io/Croppie/croppie.css'>
@endsection
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="">
                <div class="content-body">
                    <!-- Basic form layout section start -->
                    <section id="configuration" class="u-c-p u-profile a-edit">
                        <div class="row">
                            <div class="col-12">
                                <div class="card pro-main">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">
                                            <h1>Edit New user</h1>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <ol class="breadcrumb">
                                                <li class="breadcrumb-item"><a href="{{ url('/admin')}}">HOME</a></li>
                                            <li class="breadcrumb-item"><a href="{{ route('active-users')}}">USERs </a></li>
                                                <li class="breadcrumb-item active">Edit users</li>
                                            </ol>
                                        </div>

                                    </div>
                                    <form method="post" action="{{ route('update-user') }}" enctype="multipart/form-data" novalidate class="form">
                                        @csrf
                                        <input type="hidden" name="user_id" value="{{$user->id}}">
                                        <div class="row">
                                            <div class="col-lg-12 col-12">
                                                <div class="profile-frm">
                                                    <div class="row">

                                                        <div class="col-12">
                                                            <div class="pro-img-main">
                                                                @if($user->image)
                                                                <img src="{{ asset($user->image) }}" id="user_image" alt="">
                                                                @else
                                                                <img src="{{ asset('images/Profile_03.png') }}" id="user_image" alt="">
                                                                @endif
                                                                <button type="button" name="file" class="change-cover" onclick="document.getElementById('upload').click()"><i class="fa fa-camera"></i></button>
                                                                <input type="file"  accept="image/*" id="upload" name="user_image">
                                                                <input type="hidden" id="personal_img_base64" name="image">
                                                            </div>
                                                        </div>
                                                        <!--col end-->

                                                        <div class="col-lg-12 col-sm-12">

                                                            <div class="pro-inner-row">
                                                                <label><i class="fa fa-user-circle"></i> Full Name</label>
                                                                <input type="text" name="full_name" id="full_name" maxlength="50" value="{{ old('full_name') ?? $user->full_name }}" class="form-control" placeholder="Full Name">
                                                                @if ($errors->has('full_name'))
                                                                    <span class="invalid-feedback" role="alert" style="display:block;">
                                                                        <strong>{{ $errors->first('full_name') }}</strong>
                                                                    </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        {{-- <div class="col-lg-6 col-sm-12">

                                                            <div class="pro-inner-row">
                                                                <label><i class="fa fa-user-circle"></i> Last Name</label>
                                                                <input type="text" name="last_name" id="last_name" maxlength="50" value="{{ old('last_name')  ?? $user->last_name }}" class="form-control" placeholder="Last Name">
                                                                @if ($errors->has('last_name'))
                                                                    <span class="invalid-feedback" role="alert" style="display:block;">
                                                                        <strong>{{ $errors->first('last_name') }}</strong>
                                                                    </span>
                                                                @endif
                                                            </div>
                                                        </div> --}}
                                                        <!--pro inner row end-->

                                                        <!--pro inner row end-->
                                                        <div class="col-lg-6 col-sm-12">
                                                            <div class="pro-inner-row">
                                                                <label><i class="fa fa-envelope"></i> Email Address</label>
                                                                <input type="email" name="email" id="email" maxlength="100" value="{{ old('email') ?? $user->email }}" class="form-control" readonly placeholder="Email Address">
                                                                @if ($errors->has('email'))
                                                                    <span class="invalid-feedback" role="alert" style="display:block;">
                                                                        <strong>{{ $errors->first('email') }}</strong>
                                                                    </span>
                                                                @endif
                                                            </div>
                                                        </div>

                                                        <!--pro inner row end-->
                                                        <div class="col-lg-6 col-sm-12">
                                                            <div class="pro-inner-row">
                                                                <label><i class="fa fa-phone"></i> Phone Number</label>
                                                                <input type="text" name="phone" id="phone" value="{{ old('phone') ?? $user->phone }}" class="form-control phone" maxlength="14">
                                                                @if ($errors->has('phone'))
                                                                    <span class="invalid-feedback" role="alert" style="display:block;">
                                                                        <strong>{{ $errors->first('phone') }}</strong>
                                                                    </span>
                                                                @endif
                                                            </div>
                                                        </div>

                                                        {{-- <div class="col-lg-6 col-sm-12">
                                                            <div class="pro-inner-row">
                                                                <label><i class="fa fa-calendar" aria-hidden="true"></i> Date of Birth</label>
                                                                <input type="text" name="dob" id="dob" value="{{ old('dob') ?? $user->dob }}" class="form-control" >
                                                                @if ($errors->has('dob'))
                                                                    <span class="invalid-feedback" role="alert" style="display:block;">
                                                                        <strong>{{ $errors->first('dob') }}</strong>
                                                                    </span>
                                                                @endif
                                                            </div>
                                                        </div> --}}

                                                        <div class="col-lg-6 col-sm-12">
                                                            <div class="pro-inner-row" >
                                                                <label><i class="fa fa-calendar" aria-hidden="true"></i> Date of Brith</label>
                                                                <input type="text"  name="date_of_birth"  value="{{ old('date_of_birth') ?? $user->date_of_birth }}" id="date_of_birth" class="form-control">
                                                                @if ($errors->has('date_of_birth'))
                                                                    <span class="invalid-feedback" role="alert" style="display:block;">
                                                                        <strong>{{ $errors->first('date_of_birth') }}</strong>
                                                                    </span>
                                                                @endif
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-6 col-sm-12">
                                                            <div class="pro-inner-row">
                                                                <label><i class="fa fa-transgender" aria-hidden="true"></i> Gender</label>
                                                                <div class="row">
                                                                    <div class="col-md-6 col-sm-12">
                                                                        <div class="blue-radio">
                                                                            Male
                                                                            <input type="radio" {{ ($user->gender == 0) ? "checked" : "" }} value="0" name="gender">
                                                                            <span class="checkmark"></span>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-6 col-sm-12">
                                                                        <div class="blue-radio">
                                                                            Female
                                                                            <input type="radio" {{ ($user->gender == 1) ? "checked" : "" }} value="1" name="gender">
                                                                            <span class="checkmark"></span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6 col-sm-12">
                                                                        <div class="blue-radio">
                                                                            Non Binary
                                                                            <input type="radio" {{ ($user->gender == 2) ? "checked" : "" }} value="2" name="gender">
                                                                            <span class="checkmark"></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!--pro inner row end-->
                                                        <div class="col-12">
                                                            <div class="pro-inner-row">
                                                                <label><i class="fa fa-map" aria-hidden="true"></i> Address</label>
                                                                <input type="text" name="address" id="autocomplete" onfocus="geolocate()" class="form-control" value="{{ old('address') ?? $user->address }}">
                                                                <input type="hidden" id="latitude" class="form-control" name="latitude"   value="{{ old('latitude') }}">
                                                                <input type="hidden" id="longitude" class="form-control" name="longitude"  value="{{ old('longitude') }}">
                                                                @if ($errors->has('address'))
                                                                    <span class="invalid-feedback" role="alert" style="display:block;">
                                                                        <strong>{{ $errors->first('address') }}</strong>
                                                                    </span>
                                                                @endif
                                                            </div>
                                                        </div>


                                                        <!--pro inner row end-->
                                                        <div class="col-lg-6 col-sm-12">
                                                            <div class="pro-inner-row">
                                                                <label><i class="fa fa-building" aria-hidden="true"></i> City</label>
                                                                <input readonly type="text" name="city" id="locality" value="{{ old('city') ?? $user->city }}" class="form-control">
                                                            </div>
                                                        </div>

                                                        <!--pro inner row end-->
                                                        <div class="col-lg-6 col-sm-12">
                                                            <div class="pro-inner-row">
                                                                <label><i class="fa fa-globe"></i> Country</label>
                                                                <input readonly type="text" name="country" id="country" value="{{ old('country') ?? $user->country }}" class="form-control">
                                                            </div>
                                                        </div>

                                                        <!--pro inner row end-->
                                                        <div class="col-lg-6 col-sm-12">
                                                            <div class="pro-inner-row">
                                                                <label><i class="fa fa-map" aria-hidden="true"></i> State</label>
                                                                <input readonly type="text" name="state" id="administrative_area_level_1" value="{{ old('state') ?? $user->state }}" class="form-control">
                                                            </div>
                                                        </div>
                                                        <!--pro inner row end-->

                                                        <div class="col-lg-6 col-sm-12">
                                                            <div class="pro-inner-row">
                                                                <label><i class="fa fa-map-marker"></i> Zip Code</label>
                                                                <input readonly type="text" name="zipcode" id="postal_code" value="{{ old('zipcode') ?? $user->zipcode }}" class="form-control">
                                                            </div>
                                                        </div>

                                                        <!--pro inner row end-->
                                                        <div class="col-lg-6 col-sm-12">
                                                            <div class="pro-inner-row">
                                                                <label><i class="fa fa-lock"></i>Update Password or Leave Blank</label>
                                                                <input type="password" name="password" id="password" class="form-control">
                                                                @if ($errors->has('password'))
                                                                    <span class="invalid-feedback" role="alert" style="display:block;">
                                                                        <strong>{{ $errors->first('password') }}</strong>
                                                                    </span>
                                                                @endif
                                                            </div>
                                                        </div>

                                                        <br>
                                                    </div>
                                                </div>
                                                <!--row end-->

                                            </div>
                                            <!--profile form end-->

                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button type="submit" class="save">Update user <i class="fa fa-user"></i></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="cropImagePop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <!--<div class="modal-body" style="min-height:400px">-->
                <div class="modal-body" style="">
                    <div id="upload-demo" style="width:350px"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="cropImageBtn" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
@endsection('content')

@section('js')
<!-- Bootstrap Date-Picker Plugin -->
{{-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/> --}}

<script src="https://foliotek.github.io/Croppie/croppie.js"></script>
<script>
    var $uploadCrop,
    tempFilename,
    rawImg,
    imageId;

    $uploadCrop = $('#upload-demo').croppie({
        enableExif: true,
        viewport: {
            width: 200,
            height: 200,
            type: 'circle'
        },
        boundary: {
            width: 250,
            height: 250
        }
    });
    var nogo;
    $('#upload').on('change', function(e){
        var f = e.target.files[0];
        if(f.size > 5242880){
            toastr.error('You cant not select file greater than 5 MB');
            $(this).val('');
            nogo=1;
        }else{
                console.log('here');
            nogo=0;
            $('#cropImagePop').modal('toggle');
            var reader = new FileReader();
            reader.onload = function(e) {
                $uploadCrop.croppie('bind', {
                    url: e.target.result
                }).then(function() {
                    console.log('jQuery bind complete');
                });
            }
            reader.readAsDataURL(this.files[0]);
        }
    });

    $('#cropImageBtn').on('click', function(ev) {
        $uploadCrop.croppie('result', {
            type: 'base64',
            format: 'jpeg',
            size: { width: 300, height: 300 }
        }).then(function(resp) {
            if(!nogo){
                $('#user_image').attr('src', resp);
            }else{
                $('#user_image').attr('src', base_url + 'images/Profile_03.png');
            }
            $('#personal_img_base64').val(resp);
            $('#cropImagePop').modal('hide');
        });
    });

    function readURL(input, target) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#'+target).attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    function phoneFormatter() {
        $('.phone').on('input', function() {
            var number = $(this).val().replace(/[^\d]/g, '')
            if (number.length == 7) {
                number = number.replace(/(\d{3})(\d{4})/, "$1-$2");
            } else if (number.length == 10) {
                number = number.replace(/(\d{3})(\d{3})(\d{4})/, "($1) $2-$3");
            }
            $(this).val(number)
        });
    };

    $(phoneFormatter);

    // $('#dob input').datepicker({
    //     //autoclose: true,
    //     endDate: new Date()
    // });
</script>


<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAHPUufTlBkF5NfBT3uhS9K4BbW2N-mkb4&libraries=places&callback=initAutocomplete" async defer></script>
<script type="text/javascript">
  // This example displays an address form, using the autocomplete feature
  // of the Google Places API to help users fill in the information.

  // This example requires the Places library. Include the libraries=places
  // parameter when you first load the API. For example:
  // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

  var placeSearch, autocomplete;
  var componentForm = {
      /*locality: 'long_name',
      administrative_area_level_1: 'short_name',
      country: 'long_name',
      postal_code: 'short_name'*/

      /*street_number: 'short_name',
      route: 'long_name',*/
      locality: 'long_name',
      administrative_area_level_1: 'short_name',
      country: 'long_name',
      postal_code: 'short_name'
  };

  function initAutocomplete() {
      // Create the autocomplete object, restricting the search to geographical
      // location types.
      autocomplete = new google.maps.places.Autocomplete(
          /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
          {types: ['geocode']});

      // When the user selects an address from the dropdown, populate the address
      // fields in the form.
      autocomplete.setComponentRestrictions(
          {'country': ['us']});
      autocomplete.addListener('place_changed', fillInAddress);
      /*
      google.maps.event.addListener(autocomplete, 'place_changed', function() {
      });*/
  }

  function fillInAddress() {
      // Get the place details from the autocomplete object.
      var place = autocomplete.getPlace();
      console.log(place.geometry.location.lat());
      console.log(place.geometry.location.lng());
      document.getElementById("latitude").value = place.geometry.location.lat();
      document.getElementById("longitude").value = place.geometry.location.lng();
      for (var component in componentForm) {
          try{
              document.getElementById(component).value = '';
              document.getElementById(component).disabled = false;
          }catch(e){

          }
      }

      // Get each component of the address from the place details
      // and fill the corresponding field on the form.
      for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          console.log(addressType)
          if (componentForm[addressType]) {
              var val = place.address_components[i][componentForm[addressType]];
              document.getElementById(addressType).value = val;
          }
      }
  }

  // Bias the autocomplete object to the user's geographical location,
  // as supplied by the browser's 'navigator.geolocation' object.
  function geolocate() {
      if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
              var geolocation = {
                  lat: position.coords.latitude,
                  lng: position.coords.longitude
              };
              var circle = new google.maps.Circle({
                  center: geolocation,
                  radius: position.coords.accuracy
              });
              autocomplete.setBounds(circle.getBounds());
          });
      }
  }
</script>
@endsection('js')
