@extends('layouts.master')

@section('title',' User Listings')
@section('body-class',"vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar")
@section('body-col',"2-columns")

@section('content')

    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-body">
                <!-- Basic form layout section start -->
                <section id="configuration" class="search view-cause">
                    <div class="row">
                        <div class="col-12">
                            <div class="card rounded pad-20">
                                <div class="card-content collapse show">
                                    <div class="card-body table-responsive card-dashboard">
                                        <div class="row">
                                            <div class="col-12">
                                                @if(Session::has('message'))
                                                    <div class="alert alert-success">
                                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                        <strong>{{ Session::get('message')  }}</strong>
                                                    </div>
                                                @endif
                                                @if(Session::has('error'))
                                                    <div class="alert alert-danger">
                                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                        <strong>{{ Session::get('error')  }}</strong>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="col-12">
                                                <h1 class="pull-left">Users</h1>
                                            </div>
                                            <div class="col-12">
                                            <div class="add-user text-center"> <a href="{{ route('add-user') }}" class="user"><i class="fa fa-plus-circle"></i> add user</a> </div>
                                            <div class="add-user text-center"> <a href="{{ route('blocked-users')}}" class="user"><i class="fa fa-ban" aria-hidden="true"></i> Blocked Users</a> </div>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>
                                        <div class="maain-tabble mt-3">
                                            <table class="table table-striped table-bordered zero-configuration">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Username</th>
                                                        <th>Email</th>
                                                        <th>Active Status</th>
                                                        <th>Actions</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php $i = 0; @endphp
                                                    @foreach($users as $user)
                                                    @php $i++; @endphp
                                                        <tr id="row{{$user->id}}">
                                                            <td>{{$i}}</td>
                                                            <td class="user-td">
                                                                @if($user->image == "")
                                                                    <span data-toggle="popover" data-content="johny" class="circle" style="background: #3C5088;">{{ucfirst(substr($user->full_name,0,1))}}</span>
                                                                @else
                                                                    <span class="avatar avatar-online">
                                                                        <img class="tab-img" src="{{ asset($user->image) }}" alt="{{ucfirst($user->full_name)}}" onerror="this.style.display='none'">
                                                                    </span>
                                                                @endif
                                                                <a href="{{ route('view-user', ['id' => $user->id]) }}">{{ucfirst($user->full_name)}}</a>
                                                            </td>
                                                            <td>{{$user->email}}</td>
                                                            <td>
                                                                <select name="status" class="form-control statusButton"  data-id="{{$user->id}}" >
                                                                    <option value="0" {{ $user->is_active == 0 ? 'selected' : '' }}>In-active</option>
                                                                    <option value="1" {{ $user->is_active == 1 ? 'selected' : '' }}>Active</option>
                                                                </select>
                                                            </td>
                                                            <td>
                                                                <div class="btn-group mr-1 mb-1 my-action">
                                                                    <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i></button>
                                                                    <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 21px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                                        <a class="dropdown-item" href="{{ route('view-user', ['id' => $user->id]) }}">
                                                                            <i class="fa fa-eye"></i>View
                                                                        </a>
                                                                        <a class="dropdown-item" href="{{ route('edit-user', ['id' => $user->id]) }}">
                                                                            <i class="fa fa-pencil"></i>Edit
                                                                        </a>

                                                                        <a class="dropdown-item" href="{{ route('delete-user', [$user->id]) }}" data-toggle="modal" data-id="{{$user->id}}" data-target="#deleteModal">
                                                                            <i class="fa fa-ban"></i>Block </a>
                                                                        </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!--main card end-->
                            </div>
                        </div>
                    </div>
                </section>
                <!-- // Basic form layout section end -->
            </div>
        </div>
    </div>

    <!-- Delete Modal -->
    <div class="login-fail-main user">
        <div class="featured inner">
            <div class="modal fade bd-example-modal-lg" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lgg">
                    <div class="modal-content">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                        <form  method="post" id="delete-user">
                            @csrf
                            @method('delete')
                            <input type="hidden" class="user_id" name="user_id" id="user_id" value="" />
                            <div class="payment-modal-main">
                                <div class="payment-modal-inner"> <img src="images/block-modal.png" class="img-fluid" alt="">
                                    <h3>Are you sure you want to block this user?</h3>
                                    <div class="row">
                                        <div class="col-12 text-center">
                                            <button type="button" data-dismiss="modal" class="can">Cancel</button>
                                            <button type="submit" id="deleteUser" class="con">Block</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection('content')

@section('js')
<script type="text/javascript">

    // var userID;
    // $('.my-action div a').click(function() {
    //     userID= $(this).attr('data-id');
    // });

    $('#deleteModal').on('show.bs.modal', function (e) {

        let action = $(e.relatedTarget).attr('href');
        $('#delete-user').attr('action', action);
        // $(this).find('.user_id').val(userID);
    });

    $('.statusButton').on('change', function(){
        var value = $(this).val();
        var target = $(this).data("id");
        $.ajax({
            url: '{{ route("update-user-status")}}',
            data: {value: value, target:target },
            type: 'get',
            success: function(response){
                if(response.status == 200){
                     toastr.success(response.msg, 'Success');
                     $('#row'+target).remove();
                }
                else{
                     toastr.error('Error');
                     $('.statusButton').val(!value);
                }
            }
        });
    });


    $("#status").change(function(){
        var status = this.value;
        toastr.success(status);
        if(status=="0") {
            toastr.success(status);
        } else {
            toastr.success(status);
        }
    });
</script>
@endsection('js')
