
@extends('layouts.master')

@section('title',' Admin Dashboard')
@section('body-class',"vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar")
@section('body-col',"2-columns")

@section('content')

<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="combination-charts">
                <div class="row">
                    <div class="col-12">
                        <div class="card rounded admin-overview-main">
                            <div class="row">
                                <div class="col-12">
                                    <h1>dashboard</h1>

                                </div>

                                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 d-flex">
                                    <div class="card rounded w-100">
                                        <div class="card-content">
                                            <div class="card-body top-card cleartfix">

                                                <div class="media align-items-stretch">
                                                    <div class="align-self-center">
                                                        <i class="mr-2"><img src="{{ asset('images/globe-icon.png') }}"></i>
                                                    </div>
                                                    <div class="media-body align-self-center">
                                                        <span>Total<br>Members</span>
                                                    </div>

                                                    <div class="media-body align-self-center">
                                                    <h4>{{ $users }}</h4>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 d-flex">
                                    <div class="card rounded w-100">
                                        <div class="card-content">
                                            <div class="card-body top-card cleartfix">

                                                <div class="media align-items-stretch">
                                                    <div class="align-self-center">
                                                        <i class="mr-2"><img src="{{ asset('images/doctor-icon.png') }}"></i>
                                                    </div>
                                                    <div class="media-body align-self-center">
                                                        <span>Total<br>Therapist</span>
                                                    </div>

                                                    <div class="media-body align-self-center">
                                                        <h4>{{ $therapists }}</h4>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 d-flex">
                                    <div class="card rounded w-100">
                                        <div class="card-content">
                                            <div class="card-body top-card cleartfix">

                                                <div class="media align-items-stretch">
                                                    <div class="align-self-center">
                                                        <i class="mr-2"><img src="{{ asset('images/calender-icon.png') }}"></i>
                                                    </div>
                                                    <div class="media-body align-self-center">
                                                        <span>Total Sessions<br>Created</span>
                                                    </div>

                                                    <div class="media-body align-self-center">
                                                        <h4>{{ $sessions }}</h4>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--col chart end-->

                                <div class="col-xl-6 col-lg-6 col-md-12 mt-3 d-flex">
                                    <div class="card w-100">
                                        <div class="card-content">
                                            <div class="card-body">
                                                <h5>Total appointments per month</h5>
                                                <canvas id="canvas" height="280" width="600"></canvas>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-12 mt-3 d-flex">
                                    <div class="card w-100">
                                        <div class="card-content">
                                            <div class="card-body">
                                                <h5>Total members registered per month</h5>
                                                <canvas id="canvas-user" height="280" width="600"></canvas>
                                                {{-- <img src="{{ asset('images/map-2.png') }}"> --}}
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-12 mt-3 d-flex">
                                    <div class="card w-100">
                                        <div class="card-content">
                                            <div class="card-body">
                                                <h5>Total therapist registered per month</h5>
                                                <canvas id="canvas-therapist" height="280" width="600"></canvas>
                                                {{-- <img src="{{ asset('images/map-1.png') }}"> --}}
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="col-xl-6 col-lg-6 col-md-12 mt-3 d-flex">
                                    <div class="card w-100">
                                        <div class="card-content">
                                            <div class="card-body">
                                                <h5>Activity Log</h5>
                                            </div>
                                            <ul class="list-group list-group-flush">
                                                <li class="list-group-item">
                                                    <a href="#" class="media border-0">
                                                        <div class="media-left pr-1">
                                                            <span class="avatar avatar-md"><img class="media-object rounded-circle" src="{{ asset('images/client-profile-img.jpg') }}" alt="Generic placeholder image"><i></i></span>
                                                        </div>
                                                        <div class="media-body w-100">
                                                            <h6 class="list-group-item-heading">John User subscribed for gold package </h6>
                                                            <p class="list-group-item-text mb-0">02 Hour Ago</p>
                                                        </div>
                                                    </a>
                                                </li>

                                                <li class="list-group-item">
                                                    <a href="#" class="media border-0">
                                                        <div class="media-left pr-1">
                                                            <span class="avatar avatar-md avatar-online"><img class="media-object rounded-circle" src="{{ asset('images/client-profile-img.jpg') }}" alt="Generic placeholder image"><i></i></span>
                                                        </div>
                                                        <div class="media-body w-100">
                                                            <h6 class="list-group-item-heading">Kristopher Candy </h6>
                                                            <p class="list-group-item-text mb-0">15 May 2019</p>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li class="list-group-item">
                                                    <a href="#" class="media border-0">
                                                        <div class="media-left pr-1">
                                                            <span class="avatar avatar-md avatar-online"><img class="media-object rounded-circle" src="{{ asset('images/client-profile-img.jpg') }}" alt="Generic placeholder image"><i></i></span>
                                                        </div>
                                                        <div class="media-body w-100">
                                                            <h6 class="list-group-item-heading">Kristopher Candy </h6>
                                                            <p class="list-group-item-text mb-0">15 May 2019</p>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li class="list-group-item">
                                                    <a href="#" class="media border-0">
                                                        <div class="media-left pr-1">
                                                            <span class="avatar avatar-md avatar-online"><img class="media-object rounded-circle" src="{{ asset('images/client-profile-img.jpg') }}" alt="Generic placeholder image"><i></i></span>
                                                        </div>
                                                        <div class="media-body w-100">
                                                            <h6 class="list-group-item-heading">Kristopher Candy </h6>
                                                            <p class="list-group-item-text mb-0">15 May 2019</p>
                                                        </div>
                                                    </a>
                                                </li>
                                            </ul>
                                            <div class="card-body text-center">
                                                <a href="#" class="card-link">View More</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
@endsection('content')


@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.bundle.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.js"></script>
<script>
    const d = @json($totalAppointments);
    console.log(d.map( m => m.key ));
    const e = @json($totalTherapists);
    
    const f = @json($totalUsers);

    window.onload = function() {
        var ctx = document.getElementById("canvas").getContext("2d");
        window.myBar = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: d.map( m => m.key ),
                datasets: [{
                    label: '',
                    data: d.map( m => m.value ),
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                elements: {
                    rectangle: {
                        borderWidth: 2,
                        borderColor: 'rgb(0, 255, 0)',
                        borderSkipped: 'bottom'
                    }
                },
                responsive: true,
                title: {
                    display: true,
                    text: 'TOTAL APPOINTMENTS PER MONTH'
                }
            }
        });
        
        var ctx = document.getElementById("canvas-therapist").getContext("2d");
        window.myBar = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: e.map( m => m.key ),
                datasets: [{
                    label: '',
                    data: e.map( m => m.value ),
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                elements: {
                    rectangle: {
                        borderWidth: 2,
                        borderColor: 'rgb(0, 255, 0)',
                        borderSkipped: 'bottom'
                    }
                },
                responsive: true,
                title: {
                    display: true,
                    text: 'TOTAL THERAPIST REGISTERED PER MONTH'
                }
            }
        });

        var ctx = document.getElementById("canvas-user").getContext("2d");
        window.myBar = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: f.map( m => m.key ),
                datasets: [{
                    label: '',
                    data: f.map( m => m.value ),
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                elements: {
                    rectangle: {
                        borderWidth: 2,
                        borderColor: 'rgb(0, 255, 0)',
                        borderSkipped: 'bottom'
                    }
                },
                responsive: true,
                title: {
                    display: true,
                    text: 'TOTAL MEMBERS REGISTERED PER MONTH'
                }
            }
        });


    };
</script>


<script type="text/javascript">
 /*require.config({
        paths: {
            echarts: 'app-assets/vendors/js/charts/echarts'
        }
    });


require(
        [
            'echarts',
            'echarts/chart/bar',
            'echarts/chart/line'
        ],


        // Charts setup
        function (ec) {

            // Initialize chart
            // ------------------------------
            var myChart = ec.init(document.getElementById('basic-column'));

            // Chart Options
            // ------------------------------
            chartOptions = {

                // Setup grid
                grid: {
                    x: 40,
                    x2: 40,
                    y: 35,
                    y2: 25
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis'
                },

                // Add legend
                legend: {
                    data: ['Featured', 'Sold','Trending']
                },

                // Add custom colors
                color: ['#FF7588','#00B5B8', '#556AF3'],

                // Enable drag recalculate
                calculable: true,

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    data: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value'
                }],


                // Add series
                series: [
                    {
                        name: 'Featured',
                        type: 'bar',
                        data: [],
                        itemStyle: {
                            normal: {
                                label: {
                                    show: true,
                                    textStyle: {
                                        fontWeight: 500
                                    }
                                }
                            }
                        },
                        markLine: {
                            data: [{type: 'average', name: 'Average'}]
                        }
                    },
                    {
                        name: 'Sold',
                        type: 'bar',
                        data: [],
                        itemStyle: {
                            normal: {
                                label: {
                                    show: true,
                                    textStyle: {
                                        fontWeight: 500
                                    }
                                }
                            }
                        },
                        markLine: {
                            data: [{type: 'average', name: 'Average'}]
                        }
                    },
                    {
                        name: 'Trending',
                        type: 'bar',
                        data: [],
                        itemStyle: {
                            normal: {
                                label: {
                                    show: true,
                                    textStyle: {
                                        fontWeight: 500
                                    }
                                }
                            }
                        },
                        markLine: {
                            data: [{type: 'average', name: 'Average'}]
                        }
                    }
                ]
            };

            // Apply options
            // ------------------------------

            myChart.setOption(chartOptions);


            // Resize chart
            // ------------------------------

            $(function () {

                // Resize chart on menu width change and window resize
                $(window).on('resize', resize);
                $(".menu-toggle").on('click', resize);

                // Resize function
                function resize() {
                    setTimeout(function() {

                        // Resize chart
                        myChart.resize();
                    }, 200);
                }
            });
        }
    );*/
    
/*=========================================================================================
    File Name: column.js
    Description: Chartjs column chart
    ----------------------------------------------------------------------------------------
    Item Name: Stack - Responsive Admin Theme
    Version: 3.0
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

// Column chart
// ------------------------------
/*$(window).on("load", function(){

    //Get the context of the Chart canvas element we want to select
    var ctx = $("#column-chart");

    // Chart Options
    var chartOptions = {
        // Elements options apply to all of the options unless overridden in a dataset
        // In this case, we are setting the border of each bar to be 2px wide and green
        elements: {
            rectangle: {
                borderWidth: 2,
                borderColor: 'rgb(0, 255, 0)',
                borderSkipped: 'bottom'
            }
        },
        responsive: true,
        maintainAspectRatio: false,
        responsiveAnimationDuration:500,
        legend: {
            position: 'top',
        },
        scales: {
            xAxes: [{
                display: true,
                gridLines: {
                    color: "#f3f3f3",
                    drawTicks: false,
                },
                scaleLabel: {
                    display: true,
                }
            }],
            yAxes: [{
                display: true,
                gridLines: {
                    color: "#f3f3f3",
                    drawTicks: false,
                },
                scaleLabel: {
                    display: true,
                }
            }]
        }
    };

    // Chart Data
    var chartData = {
        labels: ["January", "February", "March", "April", "May", "June", "July","August","September","October","November","December"],
        datasets: [{
            label: "Featured Properties",
            data: [],
            backgroundColor: "#873D6B",
            hoverBackgroundColor: "rgba(135,61,107,.9)",
            borderColor: "transparent"
        }, {
            label: "Sold Properties",
            data: [],
            backgroundColor: "#13184C",
            hoverBackgroundColor: "rgba(19,24,76,.9)",
            borderColor: "transparent"
        },
        {
            label: "Trending Properties",
            data: [],
            backgroundColor: "#D8A439",
            hoverBackgroundColor: "rgba(216,164,57,.9)",
            borderColor: "transparent"
        }]
    };

    var config = {
        type: 'bar',

        // Chart Options
        options : chartOptions,

        data : chartData
    };

    // Create the chart
    var lineChart = new Chart(ctx, config);
});*/
	</script>
  <!-- END STACK JS-->
  <!-- BEGIN PAGE LEVEL JS-->
  <!-- END PAGE LEVEL JS-->

@endsection('js')