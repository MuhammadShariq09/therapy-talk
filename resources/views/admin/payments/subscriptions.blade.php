@extends('layouts.master')
@section('title',' Subscription Logs')
@section('body-class',"vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar")
@section('body-col',"2-columns")
@section('content')

<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-body"> 
        <section id="configuration" class="search view-cause">
          <div class="row">
            <div class="col-12">
              <div class="card rounded pad-20">
                <div class="card-content collapse show">
                  <div class="card-body table-responsive card-dashboard">
                    <div class="row">
                        <div class="col-12">
                            <h1 class="pull-left">Subscription payment log</h1>
                        </div>
                        <div class="col-xl-6 col-lg-7 col-md-6 col-sm-12">
                            <div class="row">
                                <div class="col-12"><label>Sort By:</label></div>
                                <div class="col-lg-6 col-md-12 col-sm-12">
                                    <div class="blue-radio">Subscribed on<input type="radio" checked="checked" name="radio"><span class="checkmark"></span></div>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12">
                                    <div class="blue-radio">Expires on<input type="radio" name="radio"><span class="checkmark"></span></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-5 col-md-6 col-sm-12">
                          <div class="add-user text-center date-input "> 
                              <input type="date" placeholder="To: Date"> 
                            </div>
                            <div class="add-user text-center date-input"> <input type="date" placeholder="From: Date"> </div>
                            <div class="add-user text-center date-input"> 
                                <select>
                                    <option>1</option>
                                    <option>1</option>
                                    <option>1</option>
                                 </select> 
                            </div>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="maain-tabble mt-3">
                      <table class="table table-striped table-bordered zero-configuration">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Therapist Name</th>
                            <th>Subscription type</th>
                            <th>Package amount</th>
                            <th>Subscribed on</th> 
                            <th>my earning</th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach($subscriptions as $key => $subscription)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td class="user-td">
                                    @if($subscription->user->image == "")
                                        <span data-toggle="popover" data-content="johny" class="circle"
                                                style="background: #3C5088;">{{ucfirst(substr($subscription->user->first_name,0,1))}}</span>
                                    @else
                                        <span class="avatar avatar-online">
                                                <img class="tab-img" src="{{ asset($subscription->user->image) }}" alt="{{ucfirst($subscription->user->first_name).' '.ucfirst($subscription->user->last_name)}}">
                                            </span>
                                    @endif
                                    {{ucfirst($subscription->user->first_name).' '.ucfirst($subscription->user->last_name)}}
                                </td>
                                <td>{{ $subscription->package->title }}</td>
                                <td>{{ $subscription->package->amount }}</td>
                                <td>{{ $subscription->package->created_at }}</td>
                                <td>{{ $subscription->package->amount }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>
  
      



@endsection