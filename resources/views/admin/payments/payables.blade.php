@extends('layouts.master')
@section('title',' Payables')
@section('body-class',"vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar")
@section('body-col',"2-columns")
@section('content')

<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-body"> 
        <section id="configuration" class="search view-cause">
            <div class="row">
              <div class="col-12">
                <div class="card rounded pad-20">
                  <div class="card-content collapse show">
                    <div class="card-body table-responsive card-dashboard">
                      <div class="row">
                        <div class="col-12">
                          <h1 class="pull-left">Payment Log</h1>
                        </div>
                        <div class="col-12">
                          <label class="blue-label">Amount Payable</label>
                        </div>
                      </div>


                        <div class="row">
                          <div class="col-12">
                            <ul class="nav nav-tabs nav-underline no-hover-bg">
                          <li class="nav-item">
                            <a class="nav-link active" id="base-tab31" data-toggle="tab" aria-controls="tab31" href="#tab31" aria-expanded="true">Therapists</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" id="base-tab32" data-toggle="tab" aria-controls="tab32" href="#tab32" aria-expanded="false">Users</a>
                          </li>
                        </ul>
                        <div class="tab-content">
                          <div role="tabpanel" class="tab-pane active" id="tab31" aria-expanded="true" aria-labelledby="base-tab31">
                            <div class="row justify-content-end">

                          <div class="col-lg-6 col-md-8 col-sm-12">
                            <div class="row date-input">
                              <div class="col-lg-12 form-group d-flex align-items-center justify-content-end">
                                <select>
                                <option>Select Month</option>
                                <option>January</option>
                                <option>Feburary</option>
                                
                              </select>
                              </div>
                              <div class="col-lg-12 form-group d-flex align-items-center">
                                <label class="w-100" for="">To Date:</label>
                                <input  type="date" id="select-date"  class="form-control">
                              </div>
                              <div class="col-lg-12 form-group d-flex align-items-center">
                                <label class="w-100" for="">From Date:</label>
                                <input  type="date" id="select-date2" class="form-control">
                              </div>
                            </div>
                          </div>
                        </div>
                            <div class="row">
                              <div class="col-md-12 col-sm-12">
                                  <div class="row maain-tabble mt-1">
                                        <table class="table table-striped table-bordered zero-configuration">
                                          <thead>
                                            <tr>
                                              <th>S.No #</th>
                                              <th>Therapist Name</th>
                                              {{-- <th>Amount</th> --}}
                                              <th>Payment Month</th>
                                              <th>Status</th>
                                              <th>Action</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                            @foreach($therapists as $key => $row)
                                           
                                            <tr>
                                            <td>{{++$key}}</td>
                                            <td class="user-td">
                                                <span class="avatar avatar-online">
                                                    <img class="tab-img" src="{{ asset($row->image) }}" alt="{{ucfirst($row->full_name)}}" onerror="this.style.display='none'">
                                                </span>
                                                {{ isset($row->full_name) ? ucfirst($row->full_name) : ''}}
                                            </td>
                                              {{-- <td>${{$row->amount}}</td> --}}
                                              <td>{{date('M', strtotime($row->requested_date))}}</td>
                                              <td>{{$row->is_therapist_paid===1 ? "Paid" : "Un Paid"}}</td>
                                              <td>
                                                <div class="btn-group mr-1 mb-1">
                                                <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i></button>
                                                <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 21px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                  <a class="dropdown-item" href="{{route('payment-logs.payables.view', $row->trainer_id)}}"><i class="fa fa-eye" aria-hidden="true"></i>View </a>

                                                </div>
                                              </div>
                                              </td>
                                            </tr>
                                            @endforeach
                                          </tbody>
                                        </table>
                                      </div>
                                    </div>
                                  </div>
                              </div><!--tab 1 end-->
                              <div class="tab-pane" id="tab32" aria-labelledby="base-tab32">
                                <div class="row justify-content-end">
                                <div class="col-lg-6 col-md-8 col-sm-12">

                            <div class="row date-input">
                              <div class="col-lg-12 form-group d-flex align-items-center justify-content-end">
                                <select>
                                <option>Select Month</option>
                                <option>January</option>
                                <option>Feburary</option>
                                
                              </select>
                              </div>
                              <div class="col-lg-12 form-group d-flex align-items-center">
                                <label class="w-100" for="">To Date:</label>
                                <input  type="date" id="select-date"  class="form-control">
                              </div>
                              <div class="col-lg-12 form-group d-flex align-items-center">
                                <label class="w-100" for="">From Date:</label>
                                <input  type="date" id="select-date2" class="form-control">
                              </div>
                            </div>
                          </div>
                        </div>
                            <div class="row">
                              <div class="col-md-12 col-sm-12">
                                  <div class="row maain-tabble mt-1">
                                        <table class="table table-striped table-bordered zero-configuration">
                                          <thead>
                                            <tr>
                                              <th>S.No #</th>
                                              <th>Session id</th>
                                              <th>User Name</th>
                                              <th>Amount</th>
                                              <th>Status</th>
                                              <th>Action</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                          @foreach($users as $key=>$row)
                                            <tr>
                                               <td>{{++$key}}</td>
                                              <td>{{$row->id}}</td>
                                              <td class="user-td">
                                                <span class="avatar avatar-online">
                                                    <img class="tab-img" src="{{ isset($row->user->image) ? asset($row->user->image) : ''  }}" alt="" onerror="this.style.display='none'">
                                                </span>
                                                  {{ isset($row->user) ? ucfirst($row->user->full_name) : ''}}
                                              </td>
                                              <td>${{$row->amount}}</td>
                                              <td>{{$row->refund_paid===0 ? "Un Paid" : "Paid"}}</td>
                                              <td>
                                                <div class="btn-group mr-1 mb-1">
                                                <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i></button>
                                                <div class="dropdown-menu blockui" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 21px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                    <a class="dropdown-item paid" data-id="{{ $row->id }}">
                                                        <i class="fa fa-check"></i>Change Status
                                                    </a>
                                                    
                                                </div>
                                              </div>
                                              </td>
                                            </tr>
                                            @endforeach
                                          </tbody>
                                        </table>
                                      </div>
                                    </div>
                                  </div>
                              </div>
                          </div>
                        </div>

                      </div>




                    </div>
                  </div>
                  <!--main card end-->

                </div>
              </div>
            </div>
          </section>
      </div>
    </div>
  </div>
  
 
@endsection

@section('js')
<script>
$('.paid').on('click', function () {
    let id = $(this).data('id');
    $.confirm({
        class: 'ass_rej',
        title: 'Are you sure you want to mark paid?',
        buttons: {
            formSubmit: {
                text: 'Submit',
                btnClass: 'btn-info',
                action: function () {
                    let post = new Object();
                    post.id = id;
                   // $('#assesment').block({ message: 'Processing' });
                    $.ajax({
                        url: base_url + '/admin/refund-paid/'+id,
                        data: post,
                        type: 'post',
                        success: function (response) {
                            toastr.success('Refund marked as paid.', 'Success');
                           // $('#assesment').unblock();
                            setTimeout(function(){ window.location.reload(); }, 700);

                        },
                        error: function (response) {

                        }
                    })

                }
            },
            cancel: function () {
                //close
            },
        }
    });
});
</script>
@endsection