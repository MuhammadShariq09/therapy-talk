@extends('layouts.master')
@section('title',' Payable Details')
@section('body-class',"vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar")
@section('body-col',"2-columns")
@section('content')

<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-body"> 
        <section id="configuration" class="search view-cause payable-details">
            <div class="row">
              <div class="col-12">
                <div class="card rounded pad-20">
                  <div class="card-content collapse show">
                    <div class="card-body table-responsive card-dashboard">
                      <div class="row">
                        <div class="col-12">
                          <h1 class="pull-left">Amount payable details</h1>
                        </div>
                      
                      </div>
                      <div class="row">
                        <div class="col-md-6 col-12">
                          <label class="blue-label black-label">Therapist Information</label>
                          <div class="row">
                            <div class="col-12  form-group">
                            <label class="m-0">Therapis Name:</label>
                            <input type="text" value="{{$therapist->full_name}}" spellcheck="true" class="form-control" disabled placeholder="Name">
                          </div>
                          <div class="col-12 form-group">
                            <label class="m-0">Therpaist Email:</label>
                            <input type="text" value="{{$therapist->email}}" spellcheck="true" class="form-control" disabled placeholder="Email">
                          </div>
                            
                          </div>
                        </div>
                        <div class="col-md-6 col-12">
                          <label class="blue-label black-label">Payment Information</label>
                          <div class="row">
                            <div class="col-6  form-group">
                            <label class="m-0">Payment Month</label>
                            <input type="text" spellcheck="true" class="form-control" value="{{$current_month.'/'.$current_year}}" disabled placeholder="jan/2020">
                          </div>
                          <div class="col-6 form-group">
                            <label class="m-0">Total Sessions</label>
                          <input type="text" spellcheck="true" class="form-control" value="{{count($current_month_sessions)}}" disabled placeholder="30">
                          </div>
                            <div class="col-6 form-group">
                            <label class="m-0">Amount Payable</label>
                            <input type="hidden" id="net_total" value="{{$net_total}}" />
                            <input type="hidden" id="total_sessions" value="{{$total_sessions}}" />
                            <input type="text" value="{{'$'.$net_total}}" spellcheck="true" class="form-control" disabled placeholder="$30">
                          </div>
                          </div>
                        </div>
                        <div class="col-md-6 col-12">
                          <label class="blue-label black-label">Session Information</label>
                          <div class="row">
                            <div class="col-6  form-group">
                            <label class="m-0">Total Session</label>
                            <input type="text" spellcheck="true" value="{{count($current_month_sessions)}}" class="form-control" disabled placeholder="jan/2020">
                          </div>
                          <div class="col-6 form-group">
                            <label class="m-0">Live Chat</label>
                            <input type="hidden" id="live_chat" value="{{session_type_count($therapist->id, $current_month, $current_year, 1)}}" />
                            <input type="text" required="required" value="{{ '$'.$total_live_chat}}" spellcheck="true" class="form-control" disabled placeholder="30">
                          </div>
                            <div class="col-6 form-group">
                            <label class="m-0">Voice Calls</label>
                            <input type="hidden" id="voice_call" value="{{session_type_count($therapist->id, $current_month, $current_year, 2)}}" />
                            <input type="text" value="{{'$'.$total_voice_call}}" spellcheck="true" class="form-control" disabled placeholder="$30">
                          </div>
                          <div class="col-6 form-group">
                            <label class="m-0">Video Calls</label>
                            <input type="hidden" id="video_call" value="{{session_type_count($therapist->id, $current_month, $current_year, 3)}}" />
                            <input type="text" value="{{'$'.$total_video_call}}" spellcheck="true" class="form-control" disabled placeholder="$30">
                          </div>
                          </div>
                        </div>
                       
                        <div class="col-md-6 col-12">
                            <div class="row date-input">
                              @if(count($current_month_sessions)>0)
                             
                              <div class="col-lg-12 form-group d-flex align-items-center justify-content-end">
                                <select id="payment">
                                <option>Status</option>
                                <option value="1" {{(isset($paid) && $paid==true) ? "selected" : "" }}>Paid</option>
                                <option value="0" {{(isset($paid) && $paid==false) ? "selected" : "" }}>Unpaid</option>
                              </select>
                              </div>
                              @endif
                              <div class="col-lg-12 form-group d-flex align-items-center">
                                <label class="w-100" for="">To Date:</label>
                                <input  type="date" id="select-date"  class="form-control">
                              </div>
                              <div class="col-lg-12 form-group d-flex align-items-center">
                                <label class="w-100" for="">From Date:</label>
                                <input  type="date" id="select-date2" class="form-control">
                              </div>
                            </div>
                          </div>
                      </div>
          <div class="row justify-content-end">

                          
                        </div>

                        <div class="row">
                          <div class="col-12">
                            <div class="maain-tabble">
                        <table class="table table-striped table-bordered zero-configuration">
                          <thead>
                            <tr>
                              <th>S.No #</th>
                              <th>Session id</th>
                              <th>user name</th>
                              <th>user email</th>
                              <th>session status</th>
                              <th>session type</th>
                              <th>date</th>
                            </tr>
                          </thead>
                          <tbody>
                          @foreach($current_month_sessions as $key=>$row)
                            <tr>
                              <td>{{++$key}}</td>
                              <td>{{$row->id}}</td>
                              <td>
                                <span class="avatar avatar-online">
                                    <img class="tab-img" src="{{ isset($row->user->image) ? asset($row->user->image) : ''  }}" alt="" onerror="this.style.display='none'">
                                </span>
                                  {{ isset($row->user) ? ucfirst($row->user->full_name) : ''}}
                              </td>
                              <td>{{$row->user->email}}</td>
                             
                              <td>Completed</td>
                              
                              <td>{{ $row->session_type == 1 ? 'Live chat' : ($row->session_type== 2 ? "Voice call" : "Video call") }}</td>
                              <td>{{$row->requested_date}}</td>
                            </tr>
                            @endforeach
                          </tbody>
                        </table>
                      </div>
                          </div>
                        </div>

                      </div>




                    </div>
                  </div>
                  <!--main card end-->

                </div>
              </div>
            </div>
          </section>
      </div>
    </div>
  </div>
  
  
@php
/*
 function session_type_count($trainer_id, $month, $year, $type)
    {
        if($type===3){ //video call
            $count = \DB::table('sessions')->where('trainer_id', $trainer_id)->whereStatus(4)->where('month', $month)->where('year', $year)->where('session_type',3)->where('is_therapist_paid',0)->count();
        }
        else if($type===2){ //voice call
            $count = \DB::table('sessions')->where('trainer_id', $trainer_id)->whereStatus(4)->where('month', $month)->where('year', $year)->where('session_type',2)->where('is_therapist_paid',0)->count();
        }
        else{ //live chat
            $count = \DB::table('sessions')->where('trainer_id', $trainer_id)->whereStatus(4)->where('month', $month)->where('year', $year)->where('session_type',1)->where('is_therapist_paid',0)->count();
        }
        
        return $count;
    }
    */
@endphp
@endsection

@section('js')
<script>
$(document).ready(function() {
    $('#payment').change(function(){
      //alert('here');
      // console.log('status', $(this).val());
      // return;
      $.ajax({
            url: base_url + '/admin/pay-therapist/',
            data: {
              therapist_id: {{$therapist->id}},
              month:'0'+{{$current_month}},
              year:{{$current_year}},
              status:$(this).val(),
              live_chat:$('#live_chat').val(),
              voice_call:$('#voice_call').val(),
              video_call:$('#video_call').val(),
              amount:$('#net_total').val(),
              sessions:$('#total_sessions').val()
            },
            type: 'post',
            success: function (response) {
                toastr.success(response.message, 'Success');
               
               // setTimeout(function(){ window.location.reload(); }, 700);

            },
            error: function (response) {

            }
        })
    });
});
</script>
@endsection