@extends('layouts.master')
@section('title',' Payment Logs')
@section('body-class',"vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar")
@section('body-col',"2-columns")
@section('content')

<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-body"> 
        <section id="configuration" class="search view-cause">
          <div class="row">
            <div class="col-12">
              <div class="card rounded pad-20">
                <div class="card-content collapse show">
                  <div class="card-body table-responsive card-dashboard">
                    <div class="row">
                      <div class="col-12">
                        <h1 class="pull-left">Payment Log</h1>
                      </div>
                      <div class="col-12">
                        <label class="blue-label">Amount Received</label>
                      </div>
                    </div>
                    <div class="row justify-content-end">
                      
                      <div class="col-lg-6 col-md-8 col-sm-12">
                        <form method="GET">
                        <div class="row date-input">
                          <div class="col-lg-12 form-group d-flex align-items-center justify-content-end">
                            <select>
                            <option>Select Month</option>
                            <option>January</option>
                            <option>Feburary</option>
                            
                          </select>
                          </div>
                          <div class="col-lg-12 form-group d-flex align-items-center">
                            <label class="w-100" for="">To Date:</label>
                            <input  type="date" id="select-date" name="from" class="form-control">
                          </div>
                          <div class="col-lg-12 form-group d-flex align-items-center">
                            <label class="w-100" for="">From Date:</label>
                            <input  type="date" id="select-date2" name="to" class="form-control">
                          </div>
<div class="col-lg-12 form-group">
                          <div class="add-user date-input d-flex align-items-center">
                            <button type="submit" class="btn btn-primary" name="search"> Search</button>
                          </div>
                          </div>
                        </div>
                      </form>
                      </div>
                      
                    </div>
                   
                    <div class="clearfix"></div>
                    <div class="maain-tabble mt-3">
                      <table class="table table-striped table-bordered zero-configuration">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Payment Type</th>
                            <th>Amount</th>
                            <th>Date</th>
                            {{-- <th>Action</th> --}}
                          </tr>
                        </thead>
                        <tbody>
                            @foreach($sessions as $key => $session)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>{{ $session->payment_type==1 ? 'Package' : 'Single Session' }}</td>
                                @if($session->payment_type==1)
                                <td>${{ $session->session_type==3 ? ($session->session_type==2 ? config('app.voice_call') : config('app.video_call')) : config('app.live_chat')   }}</td>
                               @else
                               <td>${{$session->amount}}</td>
                               @endif
                                <td>{{ $session->requested_date  }}</td>
                                <td>
                                  {{-- <div class="btn-group mr-1 mb-1">
                                      <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i></button>
                                      <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(4px, 23px, 0px); top: 0px; left: 0px; will-change: transform;">
                                          <a class="dropdown-item"
                                              href="{{ route('payment-logs.view', ['id' => $session->id]) }}"><i class="fa fa-eye"></i>View </a>
                                         
                                      </div>
                              </td> --}}
                            </tr>
                        @endforeach
                        </tbody>
                      </table>
                    </div>

                    <div class="row">
                        <div class="col-md-12 text-center center-btn">
                            {{-- <a class="save" href="{{ route('subscriptions') }}">Subscription payment log</a>
                            <a class="save" data-toggle="modal" data-target=".bd-example-modal-lg">Package management</a>    --}}
                        </div>
                    </div>

                  </div>
                </div>
                <!--main card end-->
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>
  
  <div class="login-fail-main user pkg-modal">
    <div class="featured inner">
      <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lgg">
          <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <div class="payment-modal-main">
              <div class="payment-modal-inner"> <img src="{{ asset('/images/moda-manage-package.png')}}" class="img-fluid" alt="">
                 <h3>Package Management</h3>
                  <div class="row"> 
                    <div class="col-12 text-center">
                      <div class="table-responsive">
                        <table class="table table-borderless">
                            <thead>
                                <tr>
                                    <th>Type</th>
                                    <th>Fee</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($packages as $row)
                                <form
                                  action="{{ route('package.edit', ['id' => $row->id ]) }}"
                                  method="post">
                                  @csrf
                                <tr>
                                    <td>{{$row->title}}</td>
                                    <td><input type="text" value="{{$row->amount }}" name="amount" spellcheck="true" class="form-control"  placeholder="$30"></td>
                                    <td><div class="btn-group">
                                        <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i></button>
                                        <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 21px, 0px); top: 0px; left: 0px; will-change: transform;">
                                        <button style="font-size: 16px !important;" class="dropdown-item" type="submit"><i class="fa fa-edit"></i>Edit </button> </div>
                                        </div>
                                    </td>
                                </tr>
                                </form>
                                @endforeach
                            </tbody>
                        </table>    
                    </div>
                </div>
                </div> 
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>





@endsection

@section('js')
<script>
function payTherapist(id, status){
  console.log(status.value);
  $.ajax
  ({
    type: "POST",
    url: "{{ route('pay.therapist')}}",
    data: { status:status.value, id: id  },
    success: function(response)
    {
      toastr.success(response, 'Success');
    } 
  });
}
</script>
{{-- <script>
    $('#select-date').datepicker({
      uiLibrary: 'bootstrap4',
      format: 'mm/dd/yyyy'
    });
    $('#select-date2').datepicker({
                uiLibrary: 'bootstrap4',
                format: 'mm/dd/yyyy'
              });
  </script> --}}

@endsection