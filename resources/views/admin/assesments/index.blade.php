@extends('layouts.master')
@section('title',' Assesments Form')
@section('body-class',"vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar")
@section('body-col',"2-columns")
@section('content')
    <div class="app-content content" id="assesment">
        <div class="content-wrapper">
            <div class="content-body">
                <!-- Basic form layout section start -->
                <section id="configuration" class="search view-cause">
                    <div class="row">
                        <div class="col-12">
                            <div class="card rounded pad-20">
                                <div class="card-content collapse show">
                                    <div class="card-body table-responsive card-dashboard">
                                        <div class="row">
                                            <div class="col-12">
                                            </div>
                                            <div class="col-12">
                                                <h1 class="pull-left">Assesment Forms</h1>
                                            </div>
                                            <div class="col-12">
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>
                                        <div class="maain-tabble mt-3">
                                            <table class="table table-striped table-bordered zero-configuration">
                                                <thead>
                                                <tr>
                                                    <th>S.no</th>
                                                    <th>Email</th>
                                                    <th>Date</th>
                                                    <th>Assesment Form</th>
                                                    <th>Status</th>
                                                    <th>Actions</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @php $i = 0; @endphp
                                                @foreach($assements as $index => $assement)
                                                    @php $i++; @endphp
                                                    <tr>
                                                        <td>{{++$index}}</td>
                                                        <td>{{ $assement->email }}</td>
                                                        <td>{{ $assement->created_at->format(config('app.dateformat')) }}</td>
                                                        <td><a href="{{ route('assesments.pdf', ['id' => $assement->id]) }}" class="pdf-icon">File.pdf <i class="fa fa-download"></i></a></td>
                                                        @if($assement->status == 0)
                                                            <td><span class="badge badge-primary">Pending</span></td>
                                                            <td>
                                                                <div class="btn-group mr-1 mb-1 my-action">
                                                                    <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i></button>
                                                                    <div class="dropdown-menu blockui" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 21px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                                        <a class="dropdown-item approve" data-id="{{ $assement->id }}">
                                                                            <i class="fa fa-check"></i>Approve
                                                                        </a>
                                                                        <a class="dropdown-item reject" data-id="{{ $assement->id }}" href="javascript:;" >
                                                                            <i class="fa fa-ban"></i>Rejected </a>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        @elseif($assement->status == 1)
                                                            <td><span class="badge badge-success">Verified</span></td>
                                                            <td></td>
                                                        @else
                                                            <td><span class="badge badge-danger">Rejected</span></td>
                                                            <td></td>
                                                        @endif
                                                        
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!--main card end-->
                            </div>
                        </div>
                    </div>
                </section>
                <!-- // Basic form layout section end -->
            </div>
        </div>
    </div>

@endsection('content')

@section('js')
    <script src="{{ asset('js/assesment.js') }}"></script>
@endsection
