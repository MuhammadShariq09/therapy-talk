<div class="payment-main-popup payment-view-popup">
    <div class="payment-modal-main">
        <div class="payment-modal-inner">
            <div class="row">
                <div class="col-12">
                    <h1>
                        Assesment Test
                        <span style="font-size:15px; padding-top:10px; float:right">Email: {{$email}} </span>
                    </h1>
                </div>
                <div class="col-12">
                    @php $i = 0; $j=1; $k=0; @endphp
                        @foreach($data->chunk(8) as $index => $chunk)
                            <div class="assestment_section fields" id="assestment_section{{$index}}">
                                <div class="row">
                                    @foreach($chunk as $key => $item)
                                        <div class="col-12 form-group">

                                            @if($item['type'] == 'radio')
                                                <div class="col-12 form-group">
                                                    <p>{{ $j }}) {{ $item['question'] }}</p>
                                                </div>
                                                <div class="col-12 form-group d-flex align-items-center gender-field">
                                                    @foreach($item['values'] as $value)
                                                        @if($value['key'] == $answers[$k])
                                                            {{ $value['key'] }}
                                                        @endif
                                                    @endforeach
                                                </div>
                                            @else
                                                <div class="col-12 form-group">
                                                    <p>{{ $j }}) {{ $item['question'] }}</p>
                                                    <p>{{$answers[$k]}}</p>
                                                </div>
                                            @endif
                                        </div>
                                        @php $j++; $k++; @endphp
                                    @endforeach
                                    @if($loop->last)
                                        <div class="col-12 form-group">
                                            <p>Please enter your email</p>
                                        </div>
                                        <div class="col-12 form-group">
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-12">
                                                    <i class="fa fa-envelope" aria-hidden="true"></i>
                                                    <input type="text"
                                                           name="email" class="form-control"
                                                           spellcheck="true"
                                                           value="{{$email}}"
                                                           placeholder="Enter Email Address">
                                                </div>
                                            </div>

                                        </div>
                                    @endif

                                </div>
                                </ul>
                                @php $i++; $j++ @endphp
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
</div>
