<!-- - var menuBorder = true-->
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="shortcut icon" href="{{URL::asset('assets/admin/images/favicon.ico')}}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <link rel="canonical" href="https://www.mytherapytalk.com/">
    <meta name="author" content="PIXINVENT">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <input id="url" type="hidden" value="{{ url('/')}}" >
    <title>{{config('app.name')}} - @yield('title')</title>
    
    <link href="https://fonts.googleapis.com/css?family=Barlow:300,300i,400,400i,500,500i,600,600i,700&display=swap" rel="stylesheet">
    <!-- Header -->
    @include('includes.therapist.header')
    @yield('css')
    <style>
      .blockUI.blockMsg.blockElement {
          width: 100px !important;
          height: 100px !important;
          border-radius: 50%;
          color: transparent !important;
          background-color: transparent !important;
          transform: translateX(-50%) translateY(-50%);
          top: 50% !important;
          left: 50% !important;
          border-color: white !important;
          border-top-color: #00b7d5 !important;
          animation: spinner 1.2s ease-in-out infinite;
      }

      @keyframes spinner {
          0% {
              transform: translateX(-50%) translateY(-50%) rotate(0deg);
          }
          100% {
              transform: translateX(-50%) translateY(-50%) rotate(360deg);
          }
      }
  </style>
</head>
<body class="@yield('body-class')" data-open="click" data-menu="vertical-menu" data-col="@yield('body-col')">

@if(auth()->guard('therapist')->user())
    @include('includes.therapist.top-bar')
    @include('includes.therapist.nav')
    @yield('content')
@else
    @yield('content')
@endif

<!-- Footer -->
<script>
    var base_url = '{{url("/")}}';
</script>
@include('includes.therapist.footer')
<script>
    
function markRead(id=""){
  var url = $('#url').val() + '/markasread/'+id;
  
  $.ajax({
      url: url,
      type: 'get',
      success: function(response){
        toastr.success(response.message, 'Success');
        setTimeout(function () {
            window.location.href = response.url;
        }, 1000);
      }
  })
}


</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js"></script>

@yield('js')

</body>
</html>
