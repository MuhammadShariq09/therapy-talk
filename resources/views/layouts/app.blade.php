<!-- - var menuBorder = true-->
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <link rel="shortcut icon" href="{{URL::asset('assets/admin/images/favicon.ico')}}?v=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="author" content="mytherapytalk">
    <link rel="canonical" href="https://www.mytherapytalk.com/">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <input id="url" type="hidden" value="{{ url('/')}}" >
    <title>{{config('app.name')}} - @yield('title')</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Barlow:300,400,500,600,700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet">
    <!-- Header -->
    @include('includes.user.header')
    @yield('css')
    <style>
        .blockUI.blockMsg.blockElement {
            width: 100px !important;
            height: 100px !important;
            border-radius: 50%;
            color: transparent !important;
            background-color: transparent !important;
            transform: translateX(-50%) translateY(-50%);
            top: 50% !important;
            left: 50% !important;
            border-color: white !important;
            border-top-color: #00b7d5 !important;
            animation: spinner 1.2s ease-in-out infinite;
        }

        @keyframes spinner {
            0% {
                transform: translateX(-50%) translateY(-50%) rotate(0deg);
            }
            100% {
                transform: translateX(-50%) translateY(-50%) rotate(360deg);
            }
        }
    </style>
</head>
<body class="@yield('body-class')" data-open="click" data-menu="vertical-menu" data-col="@yield('body-col')">

@guest
    @yield('content')
@else
    @include('includes.user.top-bar')
    @yield('content')
    <section class="notified">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-12">
                    <h3>Be Notified</h3>
                    <p>Stay updated to know more about us. We hope to be of help to you in any way possible.</p>
                </div>
                <div class="col-md-5 col-sm-12 align-self-center">
                    <form action="#">
                        <div class="form-group">
                            <input type="email" class="form-control" id="email" placeholder="Enter email to receive our newsletter" name="email" required="" autocomplete="off" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR4nGP6zwAAAgcBApocMXEAAAAASUVORK5CYII=&quot;);">
                        </div>
                    </form>
                </div>
                <div class="col-md-3 col-sm-12 align-self-center">
                    <button type="button" id="subscribe" class="btn">Subscribe</button>
                </div>
            </div>
            <!--row-->
        </div>
    </section>
@endguest

<!-- Footer -->
<script>
    var base_url = '{{url("/")}}';
</script>
@include('includes.user.footer')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js"></script>
<script>
    $(document).ready(function() {
        $('#menu').slicknav();
    });
</script>

<script>
    
function markRead(id=""){
  var url = $('#url').val() + '/markasread/'+id;
  $.ajax({
      url: url,
      type: 'get',
      success: function(response){
        console.log(response.url);
        toastr.success(response.message, 'Success');
        console.log(response.url);
        setTimeout(function () {
            window.location.href = response.url;
        }, 1000);
      }
  })
}
</script>
<script>
    var baseUrl = "{{url('/')}}";
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
    function redirect(path){
        window.location = baseUrl + '/' + path;
    }
    $('#subscribe').on('click', function(){
          $.ajax({
              url: baseUrl + "/subscribe",
              data: {email: $('#email').val() },
              type: 'post',
              success: function(response){
                  toastr.success(response.message,'Success');
                  $('#email').val('');
              },
              error: function(error){
                  toastr.error(error.responseJSON.error,'Error');
              }
          })
    });
    // $(document).ready(function() { $('#menu').slicknav(); });
    // new WOW().init();
    // jQuery( '#home' ).owlCarousel( {
    //   loop: true,
    //   margin: 0,
    //   autoplay: true,
    //   autoplayTimeout: 3000,
    //   autoplayHoverPause: false,
    //   responsiveClass: true,
    //   responsive: {
    //     0: {
    //       items: 1,
    //       nav: false
    //     },
    //     600: {
    //       items: 1,
    //       nav: false
    //     },
    //     1000: {
    //       items: 1,
    //       nav: false,
    //     }
    //   }
    // } );
  </script>

@yield('js')
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/5ebd9a66967ae56c5219deff/default';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
</script>
<!--End of Tawk.to Script-->
</body>
</html>
